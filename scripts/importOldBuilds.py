#!/bin/env /usr/bin/python3
import sys, os, glob, json, datetime

sys.path.append(os.path.join(os.path.dirname(__file__), "..", "src"))

from occam.builds.manager import BuildManager
from occam.builds.write_manager import BuildWriteManager
from occam.objects.manager import ObjectManager
from occam.keys.write_manager import KeyWriteManager
from occam.accounts.manager import AccountManager

objects = ObjectManager()
buildManager = BuildManager()
buildWriteManager = BuildWriteManager()
keyWriteManager = KeyWriteManager()
accountManager = AccountManager()

person = accountManager.currentPerson()

# Go through all of the notes directories for the json

root_path = os.getenv("OCCAM_ROOT") or os.path.join(os.getenv("HOME"), ".occam")

notes_path = os.path.join(root_path, "notes")

for filename in glob.iglob(os.path.join(notes_path, "*", "*", "*", "*", "*", "builds", "*.json"), recursive=True):
  with open(filename, "r") as f:
    buildsInfo = json.loads(f.read())
    builds = [d.get('value') for d in buildsInfo.get('ids')]
    for build in builds:
      buildInfo = buildsInfo[build['id']][0]['value']
      id = build['id']
      revision = build['revision']

      # Look up task object
      task = objects.retrieve(id = id, revision = revision)

      uid = task.uid
      elapsed = buildInfo.get('time')
      host = buildInfo.get('origin', {}).get('host')
      port = buildInfo.get('origin', {}).get('port')
      published = buildInfo.get('date')
      published = datetime.datetime.strptime(published, "%Y-%m-%dT%H:%M:%S.%f")
      backend = buildInfo.get('backend')

      taskInfo = objects.infoFor(task)
      object = taskInfo.get('builds')
      object = objects.retrieve(id = object.get('id'), revision = object.get('revision'))

      # Determine build hash
      buildHash = buildManager.retrieveHash(object.uid, object.revision, id)
      print(object.id)

      signature, signed, verifyKeyId = keyWriteManager.signBuild(object, person.identity, task, buildHash, published)

      buildWriteManager.datastore.write.createBuild(id            = object.id,
                                                    uid           = object.uid,
                                                    revision      = object.revision,
                                                    buildId       = task.id,
                                                    buildUid      = task.uid,
                                                    buildRevision = task.revision,
                                                    backend       = backend,
                                                    elapsed       = elapsed,
                                                    host          = host,
                                                    port          = port,
                                                    identity      = task.identity,
                                                    verifyKeyId   = verifyKeyId,
                                                    signed        = signed,
                                                    signature     = signature,
                                                    published     = published)
