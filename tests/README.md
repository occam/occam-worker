# Testing

Run `tox` to execute every test.

```
tox
```

You can specify, if you wish, the exact test file to run:

```
tox -- tests/unit/workflows/manager_test.py
```

## Coverage

We use `coverage` to generate an html report for the coverage. When you run
`tox`, it will automatically generate an html report in the `htmlcov` directory.
You can navigate to this directly and run a web server to see the results (and
keep it running in the background to reload as you work)

```
cd htmlcov
python3 -m http.server
```

or using ruby

```
cd htmlcov
ruby -run -e httpd . -p 8000
```

And then open a browser to http://localhost:8000/

## Object Mocking

Since using the native database and storage layers is naïve, we have a mocking
layer to generate objects in memory.

First, import the ObjectManager mock.

```
from tests.unit.objects.mock import ObjectManagerMock
```

Then, within your test or setup block, you can instantiate the object manager
with the appropriate objects. Here, we use a common method to generate and
keep track of a multihash identifier for the object. (It may, then, also
generate automatically a 'uid')

```
from tests.helper import multihash

self.workflowId = multihash()

self.workflows.objects = ObjectManagerMock([
  (
    {
      "id": self.workflowId,
      "type": "workflow",
      "name": "Test Workflow",
      "file": "data.json",
    },
    {
      "data.json": """{
        "connections": [
          {
          }
        ]
      }"""
    },
  ),

  {
    "id": multihash(),
    "name": "my object",
    "type": "plot"
  }
])
```

In this case, we are adding a workflow object. The Mock takes an array as a
first argument which consists of the object info. Optionally, you can give it
a tuple containing the object metadata and then a dict for each file. The file
list is a dict with the path of the file and the file data as the value.

We have it as "workflows.objects" so that the WorkflowManager uses the mock
instead of the real thing (or failing because it itself is a mock). This is
likely a common convention for testing the Managers.

It's also a good plan to, contrary to the above, move the json content that is
used to initialize objects into a file on disk. For the workflows, there will be
a directory "data" which contains such test data. In this case, the
initialization looks like:

```
{
  "connections": [
    {
      "id": "%s",
      "revision": "%s",
      "type": "data",
      "name": "My Data"
    }
  ]
}
```

```
import json
from tests.helper import multihash

self.workflowId = multihash()

self.dataId = multihash()
self.dataRevision = multihash("sha1")

with open('data/workflow_with_single_head.json') as f:
  workflow_data = json.load(f)

self.workflows.objects = ObjectManagerMock([
  (
    {
      "id": self.workflowId,
      "type": "workflow",
      "name": "Test Workflow",
      "file": "data.json",
    },
    {
      "data.json": workflow_data % (
        self.dataId,
        self.dataRevision
      )
    },
  ),

  {
    "id": self.dataId,
    "revision": self.dataRevision,
    "name": "MyData",
    "type": "data"
  }
])
```

In this case, you will notice that the saved info on disk contains several `%s`
throughout. These will be replaced via a format string operator as shown above.
Here, they are replaced with the id and revision which are randomly generated
so as to create some dynamic input for the tests.

We then also have to store a record of the object we are using within the
workflow data using the same id and revision so it can be looked up on its own.
This is a fairly advanced case. Most cases should be much simpler: just the
storing/retrieval of some objects.
