# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from db     import Base

from sqlalchemy import Column, Integer, String, ForeignKey, Float

class EnvironmentTarget(Base):
  __tablename__ = 'environment_targets'

  id                = Column(Integer, primary_key=True)

  # We can find a path targetting one of two situations:

  # An environment/architecture pair
  environment     = Column(String(128))
  architecture    = Column(String(128))

  # A native backend
  backend         = Column(String(128))
