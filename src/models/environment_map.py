# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from db     import Base

from sqlalchemy import Column, Integer, String, ForeignKey, Float

from models.provider import Provider
from models.environment_target import EnvironmentTarget

class EnvironmentMap(Base):
  __tablename__ = 'environment_maps'

  id                = Column(Integer, primary_key=True)
  provider_id       = Column(Integer, ForeignKey('providers.id'))
  environment_target_id = Column(Integer, ForeignKey('environment_targets.id'))
