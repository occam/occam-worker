# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from log    import Log

import os
import codecs
import subprocess

from manager import manager, uses

@manager("capabilities")
class CapabilityManager:
  """
  This OCCAM manager handles querying for system properties.
  """

  def __init__(self):
    """
    Initialize the node manager.
    """

    self.apparmor = None
    self.x11      = None

  def nvidia(self):
    """
    Returns True when NVIDIA drivers are present on the system
    """

    path = self.occam.system.locateLibrary('libnvidia-opencl')

    return not path is None

  def cuda(self):
    """
    Returns True when CUDA is present on the system.
    """

    path = self.occam.system.locateLibrary('libnvcuvid')

    return not path is None

  def X11(self):
    """
    Returns True when X11 is present on the system.
    """

    if self.x11 is None:
      x11_path = "/tmp/.X11-unix"
      self.x11 = os.path.exists(x11_path)

    return self.x11

  def pulseAudio(self):
    """
    Returns True when Pulseaudio (Unix audio driver interface) is present on the system.
    """

    return True

  def alsa(self):
    """
    Returns True when ALSA (Unix audio driver interface) is present on the system.
    """

    return True

  def appArmor(self):
    """
    Returns True when AppArmor is present on the system.
    """
    if self.apparmor is None:
      ret = False

      apparmor_path = "/sys/module/apparmor/parameters/enabled"
      if os.path.exists(apparmor_path):
        f = open(apparmor_path, 'r')
        if f.read().strip() == "Y":
          ret = True

      self.apparmor = ret

    return self.apparmor

  def userId(self):
    """
    Returns the current user ID for the running occam process.
    """

    # TODO: platform independence (for Windows??):

    p = subprocess.Popen(['id', '-u', os.getenv('USER')], stdout=subprocess.PIPE)
    user_id = p.stdout.read().decode('utf-8').strip()
    p.wait()

    return user_id

  def groupId(self):
    """
    Returns the current user's primary group ID for the running occam process.
    """

    # TODO: platform independence (for Windows??):

    p = subprocess.Popen(['id', '-g', os.getenv('USER')], stdout=subprocess.PIPE)
    group_id = p.stdout.read().decode('utf-8').strip()
    p.wait()

    return group_id
