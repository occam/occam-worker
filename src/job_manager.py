# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from log import loggable

@loggable
class JobManager:
  """
  This manages deployment of jobs and any job backends.
  """

  def __init__(self):
    """
    Instantiates the Job Manager.
    """

  def pull(self, updateStatus=None):
    from db import DB
    session = self.databaseSession()
    job = session.query(DB.Job).filter_by(status           = "queued",
                                          codependant      = 0,
                                          has_dependencies = 0).limit(1).first()

    if not job is None and not updateStatus is None:
      update_query = session.query(DB.Job).filter_by(id = job.id, status = "queued")
      update_query = update_query.update({"status": updateStatus})
      session.flush()
      session.commit()

    return job

  def pullCodependants(self, job):
    from db import DB
    return DB.session().query(DB.Job).filter(DB.Job.id.in_(DB.session().query(DB.JobCodependency.depends_on_job_id).filter_by(job_id = job.id)))

  def complete(self, job):
    from db import DB
    # Update dependent jobs (if any)
    total_dependencies = (DB.session().query(DB.Job).filter_by(id = DB.session().query(DB.JobDependency.depends_on_job_id).filter_by(job_id = DB.session().query(DB.JobDependency.job_id).filter_by(depends_on_job_id = job.id))).count())

    if total_dependencies == 1:
      master_job = (DB.session().query(DB.Job).filter_by(id = DB.session().query(DB.JobDependency.job_id).filter_by(depends_on_job_id = job.id))).first()
      master_job.has_dependencies = 0
      print("Clearing block to run job #" + str(master_job.id))

    # Update job status to finished
    job.status = "finished"

    DB.session().commit()
