# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from log import Log
from object import Object

from managers.command import command, option, argument

@command('tag')
@argument("object", type="object", nargs="?")
@argument("version")
class Tag:
  def do(self):
    Log.header("Applying tag")

    if self.options.object is None:
      # pull from path
      obj = Object(path=".", occam=self.occam)
    else:
      obj = self.occam.objects.retrieve(self.options.object.id, revision=self.options.object.revision)

    self.occam.objects.appendInvocation(obj.uuid, "versions", self.options.version, obj.revision, revision=None)

    Log.write("Version %s applied to revision %s" % (self.options.version, obj.revision))
