# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Adds an empty workflow to the experiment in the current directory
# occam attach --add "New Workflow"

# Creates a new workflow and attaches the given object to it.
# occam attach 2223413a-92c6-11e4-b64f-001fd05bb228 --add "Memory Simulation"

# Adds the given object to the first workflow
# occam attach 2223413a-92c6-11e4-b64f-001fd05bb228

# Adds the given object to the second workflow
# occam attach 2223413a-92c6-11e4-b64f-001fd05bb228 --workflow 1

# Adds the given object to the second workflow as input to the 3rd node
# occam attach 2223413a-92c6-11e4-b64f-001fd05bb228 --workflow 1 --connect 2

# Adds the given object to the first workflow as input to the 3rd node to the given experiment
# occam attach 2223413a-92c6-11e4-b64f-001fd05bb228 --workflow 0 --connect 2 --to 6c59ae66-9bb5-11e6-934a-08002707ce53

from log import Log

from managers.command import command, option, argument

@command('attach',
  category      = 'Workflow Management',
  documentation = "Adds the given object to the current workflow.")
@argument("object", type = "object", nargs="?")
@option("-c", "--create",   action  = "store",
                            dest    = "creates_object_type",
                            help    = "the object type to create and attach")
@option("-g", "--group",    action  = "store",
                            dest    = "creates_object_group",
                            help    = "the object group to create and attach")
@option("-C", "--connect",  action  = "store",
                            dest    = "node_id",
                            default = -1,
                            type    = int,
                            help    = "the workflow node to attach this node to")
@option("-i", "--workflow", action  = "store",
                            dest    = "workflow_index",
                            help    = "the index of the workflow to modify.",
                            type    = int,
                            default = 0)
@option("-a", "--add",      action  = "store",
                            dest    = "new_workflow_name",
                            help    = "the name of the new workflow to add to the experiment.")
@option("-t", "--to",       action  = "store",
                            dest    = "to_object",
                            type    = "object",
                            help    = "the experiment id to attach the workflow object to")
@option("-w", "--within",   action  = "store",
                            dest    = "within",
                            type    = "object",
                            help    = "the workset that contains the experiment")
class Attach:
  def do(self):
    Log.header("Attaching Object")

    if self.options.to_object:
      obj = self.occam.objects.retrieve(self.options.to_object.id, revision=self.options.to_object.revision, version=self.options.to_object.version)
      workset = obj.workset()
    else:
      obj, workset = self.occam.objects.retrieveLocal(path=".")

    if obj is None:
      Log.error("Cannot find the object to attach this to.")
      return -1

    creates_object_type = None
    if self.options.creates_object_type:
      creates_object_type = self.options.creates_object_type
      type = creates_object_type
      name = "generated"

    if not obj.objectInfo().get('type') == 'experiment':
      Log.error("not within an experiment.")
      return -1

    workflow = None
    if self.options.new_workflow_name:
      # Add the workflow
      #obj.addWorkflow()
      pass
    else:
      # Get the workflow
      workflow = obj.workflow(self.options.workflow_index)

    if workflow is None:
      Log.error("Workflow not found.")
      return -1

    return 0

    info = obj.objectInfo()

    if creates_object_type:
      connection = {
        "object": {
          "type"    : creates_object_type,
          "name"    : name,
          "created" : True
        },
        "to": node_id
      }

      if self.options.creates_object_group:
        connection['object']['group'] = self.options.creates_object_group

      experiment_info["workflow"]["connections"].append(connection)

      commit_message = 'attached created %s object\n\nOCCAM automated commit' % (creates_object_type)

      experiment.purgeGenerates()
      experiment.updateObject(experiment_info, commit_message)

      # TODO: Add ourselves to the ignore file for the containing object ??

      if tempPath:
        Log.noisy("removing %s" % path)
        shutil.rmtree(path)

      Log.done("Attached creation of %s object" % (type))
      return 0
    elif self.options.object_id:
      object = self.occam.objects.search(uuid=self.options.object_id).first()
    else:
      object = self.occam.objects.search(object_type=type, name=name).first()

    if object is None:
      Log.error("cannot find %s object %s" % (type, name))
      return -1

    object_id = object.uid

    # Determine what revisions of this object we have and find the revision queried
    object_path = self.occam.objects.pathFor(object_id)
    Log.write("Object in %s" % (object_path))
    object = Object(object_path, uuid=object_id, occam=self.occam)

    # Look up revision
    local = False
    useGlobal = self.options.base
    base = True
    if not object_revision is None:
      global_revision = object.fullRevision(object_revision)
    else:
      # Find the latest revision
      global_revision = object.head()

    # Look up any objects in our local workset and use those first
    workset = self.occam.objectAt(path, searchFor='workset')
    if workset is None:
      parent_obj = self.occam.objectAt(os.path.join(path, '..'), recurse=True)
      objects = [parent_obj]
    else:
      objects = workset.objects()

    if not useGlobal:
      for local_object in objects:
        local_object_info = local_object.objectInfo()
        if local_object_info["name"] == name and local_object_info["type"] == type:
          # Look for the specified revision
          local = True
          object = local_object
          if not object_revision is None:
            # Use the given revision
            object_revision = object.fullRevision(object_revision)
          else:
            # Find the latest revision
            object_revision = local_object.head()

        if local:
          Log.write("Using local copy. To use global base specify: --base or use revision: %s" % (global_revision))
          break

    # Add object to the workflow
    if object_revision is None or (useGlobal and not local):
      object_revision = global_revision

    if node_id == -1:
      # Main workflow node, if none exists
      # Otherwise, an error
      if "workflow" in experiment_info and "connections" in experiment_info["workflow"] and len(experiment_info["workflow"]["connections"]) > 0:
        Log.error("must specify what object to connect this to.")
        return -1

      experiment_info["workflow"] = {
        "connections": []
      }
    else:
      # Attach to existing node
      # Error if that node does not exist
      pass

    # Determine if the given revision is a match to an object revision

    # Create default configuration
    # TODO: put this code somewhere else. it overlaps with commands.Configure
    objectPath = object.path
    objectInfo = object.objectInfo()

    object_type = objectInfo['type']
    object_name = objectInfo['name']
    object_uuid = objectInfo['id']

    connection = {
      "object": {
        "type"    : object_type,
        "name"    : object_name,
        "revision": object_revision,
        "local"   : local,
        "id"      : object_uuid
      },
      "to": node_id
    }

    if 'group' in objectInfo:
      connection['object']['group'] = objectInfo['group']

    experiment_info["workflow"]["connections"].append(connection)

    connection_id = str(len(experiment_info["workflow"]["connections"]) - 1)

    # Pull out configuration schema for this object
    def set_default(schema, current):
      for k in (schema.get("__ordering") or schema.keys()):
        v = schema[k]
        if "type" in v and not isinstance(v["type"], dict):
          # Item
          if "default" in v:
            if "type" in v:
              # Ensure the correct type, just in case
              if v["type"] == "int":
                v["default"] = int(v["default"])
              elif v["type"] == "float":
                v["default"] = float(v["default"])
            current[k] = v["default"]
          else:
            Log.warning("no default for key %s" % (k))
        elif isinstance(v, str):
          # Group Metadata
          pass
        else:
          # Group
          if not k in current:
            current[k] = {}
          set_default(v, current[k])

    configurations_path = os.path.join(experiment.path, "config")
    if not os.path.exists(configurations_path):
      Log.write("creating configurations directory")
      os.mkdir(configurations_path)

    configurations_path = os.path.realpath(os.path.join(configurations_path, connection_id))
    if not os.path.exists(configurations_path):
      Log.write("creating object configuration directory")
      os.mkdir(configurations_path)

    # For every configuration available for this object, create a config
    # file containing the defaults when the file doesn't already exist.
    for configuration in (objectInfo.get('configurations') or []):
      ret = {}
      configuration_filename = configuration['file']
      configuration_name = configuration['name']
      schema = configuration['schema']

      if isinstance(schema, str):
        schema = object.git.retrieveJSON(schema)

      # TODO: handle when schema_file does not exist
      #       this is when it wants configuration placed inside the
      #       object.json of the runner

      configuration_file_path = os.path.join(configurations_path, configuration_filename)

      if not os.path.exists(configuration_file_path):
        Log.write("adding default configuration %s for %s object %s" % (configuration_name, type, name))

        set_default(schema, ret)

        f = open(configuration_file_path, 'w+')
        f.write(json.dumps(ret))
        f.close()

        Log.write("writing %s" % (configuration_file_path))

        # Update object
        # TODO: remove this git nonsense
        experiment.purgeGenerates()

        experiment.git.add(configurations_path)
        experiment.commit('Adds configuration for %s %s' % (type, name))
      else:
        Log.write("configuration %s already exists. doing nothing." % (configuration_name))

    commit_message = 'attached %s %s (%s)\n\nOCCAM automated commit' % (object_type, object_name, object_revision)
    revisions = experiment.updateObject(experiment_info, commit_message)
    Log.write("New revision: ", end="")
    Log.output(revisions[0], padding="")
    if (len(revisions) > 1):
      Log.write("New workset revision: ", end="")
      Log.output(revisions[-1], padding="")

    # TODO: Add ourselves to the ignore file for the containing object ??

    if tempPath:
      Log.noisy("removing %s" % path)
      shutil.rmtree(path)

    Log.done("Attached %s object %s" % (type, name))
    return 0
