# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import subprocess
import os
import codecs
import json

import unittest
import doctest

from log import Log

from commands.manager import command, option

@command('test',
  category      = 'System Commands',
  documentation = "Runs the tests.")
@option("-d", "--dirty", action = "store_true",
                         dest   = "dirty",
                         help   = "will display without colors and pretty text")
@option("-c", "--color", action = "store_true",
                         dest   = "color",
                         help   = "will display with colors")
@option("-j", "--json", action = "store_true",
                        dest   = "json",
                        help   = "will output as json")
class Test:
  """
  This class will handle the command that will run the tests for occam-worker.
  """

  def moduleStringToObject(self, string):
    """
    Will turn something like foo_bar into FooBar.
    """
    parts = string.split('_')

    ret = "".join([piece.title() for piece in parts])

    return ret

  def correctFunctionName(self, string):
    """
    Will turn something like DetermineObject into determineObject.
    """

    if len(string) > 0:
      string = string[0].lower() + string[1:]

    return string

  def underscoreStringToDescription(self, string):
    parts = string.split('_')

    ret = parts[0]
    for piece in parts[1:]:
      ret = ret + " " + piece

    return ret

  def testStringToObject(self, item):
    if item.startswith('Test'):
      item = self.correctFunctionName(item[4:])
    elif item.startswith('test_'):
      item = item[5:]
      item = self.underscoreStringToDescription(item)
    else:
      item = None

    return item

  def outputLineAsJSON(self, stack, parts):
    result = parts[0]
    if result == " ":
      return stack

    parts = parts[1:]
    for i, part in enumerate(parts):
      if len(stack) <= i or stack[i] != part:
        preceding = ""
        succeeding = ": {"
        symbol = ""
        color = "0"

        if i == len(parts) - 1:
          pass

        # Output closing braces for all parts of the stack we are now leaving
        for j in reversed(range(i,len(stack)-1)):
          end = "}"
          if j == len(stack) - 2:
            end = "]"
          Log.output("\n" + ("  " * (j+1)) + end, end="", padding="")

        if len(stack)-1 >= i and i > 0:
          preceding = ","

        if i == len(parts)-2:
          succeeding = ": ["
        if i == len(parts)-1:
          part = {
            "message": part,
            "result":  result
          }
          succeeding = ""

        Log.output(preceding + "\n" + ("  " * (i+1)) + json.dumps(part) + succeeding, end="", padding="")
        stack = stack[0:i]
        stack.append(part)
    return stack

  def outputSymbol(self, result):
    if result == ".":
      color = "1;32"
      symbol = "✓"
    elif result == "F":
      color = "1;31"
      symbol = "×"
    elif result == "E":
      color = "1;31"
      symbol = "E"

    preceding = "\x1b[100D\x1b[4C\x1b[" + color + "m"
    succeeding = "\x1b[0m"
    Log.output(preceding + symbol + succeeding, padding="")

  def do(self):
    """
    Run the command.
    """

    Log.header("Running tests")
    Log.output("", padding="")

    # Run py.test and send its report to stderr and throw away stdout
    our_path = os.path.join(os.path.dirname(__file__), '..', '..')
    testSuite = unittest.defaultTestLoader.discover(os.path.join(our_path, "tests"), pattern="*_test.py", top_level_dir=our_path)
    result = unittest.TestResult()

    def run(suite, result, last_object=None, last_method=None):
      if last_object is None:
        last_object = [None]
        last_method = [None]

      if isinstance(suite, unittest.TestSuite):
        for testCase in suite:
          run(testCase, result, last_object=last_object, last_method=last_method)
      else:
        items = suite.id().split('.')
        items = items[1:]
        
        # Everything up to the token starting with "Test" is a directory path
        # The "Test*" string is the method within the object
        # The "test_" is a single test

        test_path = []
        test_object = None
        test_method = None
        test_task   = None

        for item in items:
          if test_object is None:
            if item.endswith("_test"):
              test_object = self.moduleStringToObject(item[:-5])
            else:
              test_path.append(item)
          elif test_method is None:
            test_method = self.testStringToObject(item)
          else:
            test_task = self.testStringToObject(item)

        new_object = False

        if last_object[0] is None or last_object[0] != test_object:
          Log.output(".".join(test_path + [""]) + test_object, padding="")
          last_object[0] = test_object
          new_object = True

        if new_object or last_method[0] is None or last_method[0] != test_method:
          Log.output("  " + test_method, padding="")
          last_method[0] = test_method

        Log.output("    - " + test_task, end=" ", padding="")

        errors_count  = len(result.errors)
        failure_count = len(result.failures)
        skipped_count = len(result.skipped)

        suite.run(result)

        # Print Result
        if len(result.failures) > failure_count:
          # Test Failed
          result = "F"
        elif len(result.errors) > errors_count:
          # Test Errored Out
          result = "E"
        elif len(result.skipped) > skipped_count:
          # Test skipped
          result = "S"
        else:
          result = "."

        self.outputSymbol(result)

    run(testSuite, result)

    #doctest.testmod()
