# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from log import Log

from managers.command import command, option, argument

from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from pyvirtualdisplay import Display

@command('capture',
  category      = 'Extensions',
  documentation = "Takes a screenshot of a widget.")
class Capture:
  def do(self):
    display = Display(visible=0, size=(800, 600))
    display.start()

    capabilities = DesiredCapabilities.FIREFOX
    capabilities['marionette'] = True
    capabilities['acceptSslCerts'] = True

    profile = webdriver.FirefoxProfile()
    profile.accept_untrusted_certs = True
    profile.assume_untrusted_cert_issuer = False

    browser = webdriver.Firefox(firefox_profile=profile, capabilities=capabilities)
    browser.get("https://occam.cs.pitt.edu/objects/e307a13a-9224-11e5-82e9-001fd05bb228/a80e6aacb6825e51e5b12673643ea687c95d0d70/raw/widget.html")
    browser.save_screenshot("test.png")

    browser.close()

    return 0
