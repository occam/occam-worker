# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os           # path functions
import codecs
import sys
import json         # For parsing recipes

from occam.log            import Log

from datetime import datetime

# TODO: respond to error codes and return False on errors

class GitRepository:
  """ This support class will handle retrieving OCCAM metadata from objects given
  a revision and a object path.
  """

  @staticmethod
  def popen(command, stdout=None, stdin=None, stderr=None, cwd=None, env=None):
    import subprocess
    if stdout is None:
      stdout = subprocess.DEVNULL

    if stderr is None:
      stderr = subprocess.DEVNULL

    return subprocess.Popen(command, stdout=stdout, stdin=stdin, stderr=stderr, cwd=cwd, env=env)

  @staticmethod
  def create(path, alternates=None):
    """ Creates a git repository for a new artifact.
    """

    Log.noisy("creating git repository in %s" % (path))

    import subprocess
    p = GitRepository.popen(['git', 'init'], stdout=subprocess.PIPE, cwd=path)
    p.wait()

    # Remove silly 'master' branch. Masters are a terrible, oppressive idea.
    # They do not exist, and should not exist, within a distributed system.
    # Give it a random identifier >:]
    from uuid import uuid4
    import subprocess
    p = GitRepository.popen(['git', 'checkout', '-b', str(uuid4())], stdout=subprocess.PIPE, cwd=path)
    p.wait()

    # Delete the master branch
    p = GitRepository.popen(['git', 'branch', '-d', 'master'], stdout=subprocess.PIPE, cwd=path)
    p.wait()

    # Turn off garbage collection
    p = GitRepository.popen(['git', 'config', '--local', 'gc.auto', '0'], stdout=subprocess.PIPE, cwd=path)
    p.wait()

    # Add alternates (if requested)
    if alternates:
      with open(os.path.join(path, ".git", "objects", "info", "alternates"), "w+") as f:
        f.write(alternates)
        f.write("/.git/objects")

    return GitRepository(path)

  @staticmethod
  def exists(path):
    return os.path.exists(os.path.join(path, '.git'))

  @staticmethod
  def version():
    """ Returns the git version as an integer.
    
    Returns:
      (int): It will return MMMmmmppp (where M is the major version, m is minor (with
             leading zeroes), and p is the patch level (with leading zeroes)).
    """

    # Cache result
    if not hasattr(GitRepository, '_version'):
      # Run git --version to retrieve the version string.
      import subprocess
      p = GitRepository.popen(['git', '--version'], stdout=subprocess.PIPE)

      # Parse out the version sequence
      # Pulls out the first line of stdout, strips the whitespace
      # And then splits on the '.' to form an array of three parts
      version = p.stdout.read().split(b'\n')[0].strip().decode('utf-8')
      parts = version.split(' ')[-1].split('.')

      # Pad parts array out until it has 4 items
      while len(parts) < 4:
        parts.append(0)

      # Encode the version as a number
      ret = 0
      for part in parts:
        ret = ret * 1000 + int(part)

      GitRepository._version = ret

    return GitRepository._version

  def __init__(self, path, revision=None, uuid=None, cert=None):
    if not revision:
      revision = "HEAD"

    revision = GitRepository.hexFromMultihash(revision)

    self.uuid = uuid
    self.revision = revision

    env = None
    if not cert is None:
      env = dict(os.environ, GIT_SSL_CAINFO=cert)

    import re
    if re.match(r'^[a-z]+://', path):
      # Open a remote git repository (by cloning locally)

      import tempfile     # For temporary directories
      tmpdir = os.path.realpath(tempfile.mkdtemp(prefix="occam-"))
      import subprocess
      p = GitRepository.popen(['git', 'clone', path, tmpdir], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL, cwd=tmpdir, env=env).wait()
      self.tmp  = True
      self.path = tmpdir
    else:
      # Open a local git repository (by referencing it directly)
      self.tmp  = False
      self.path = path

    # TODO: check for revision errors, throw exception

  def __del__(self):
    if self.tmp:
      import shutil
      shutil.rmtree(self.path)

  @staticmethod
  def hasRevision(path, revision="HEAD"):
    revision = GitRepository.hexFromMultihash(revision)

    import subprocess
    p = GitRepository.popen(['git', 'log', revision], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL, cwd=path)

    errorCode = p.wait()
    return errorCode == 0

  @staticmethod
  def fullRevision(path, revision="HEAD"):
    revision = GitRepository.hexFromMultihash(revision)

    import subprocess
    p = GitRepository.popen(['git', 'rev-parse', revision], stdout=subprocess.PIPE, cwd=path)

    ret = p.stdout.read().decode('utf-8').strip()

    errorCode = p.wait()
    if errorCode != 0:
      return False

    # Encode as a multihash
    return GitRepository.hexToMultihash(ret)

  @staticmethod
  def uuid():
    """ Creates a uuid for this git repository.
    """

    from uuid import uuid1
    return str(uuid1())

  @staticmethod
  def hexToMultihash(revision):
    """ Returns the multihash for a normal SHA-1 revision.
    """

    # Create a URI by hashing the public key with a multihash
    from occam.storage.plugins.ipfs_vendor.multihash import multihash
    revisionBytes = bytearray.fromhex(revision)
    hashedBytes = bytearray([multihash.SHA1, len(revisionBytes)])
    hashedBytes.extend(revisionBytes)

    from occam.storage.plugins.ipfs_vendor.base58 import b58encode
    ret = b58encode(bytes(hashedBytes))

    return ret

  @staticmethod
  def hexFromMultihash(multihash):
    """ Returns the normal expected hex for a multihash.
    """

    # Detect a multihash, otherwise do nothing
    if len(multihash) != 30:
      return multihash

    from occam.storage.plugins.ipfs_vendor.base58 import b58decode
    hashedBytes = b58decode(multihash)

    from occam.storage.plugins.ipfs_vendor.multihash import multihash as _multihash
    hashedBytes = _multihash.decode(hashedBytes)

    import binascii

    ret = binascii.hexlify(bytearray(hashedBytes)).decode('utf-8')

    return ret

  def retrieveJSON(self, filepath):
    """ Returns the object metadata for a particular revision of an object that is
    local on the disk or remote. If it is remote, it has to fetch the history.
    """

    from collections import OrderedDict

    import subprocess
    p = GitRepository.popen(['git', 'show', "%s:./%s" % (self.revision, filepath)], stdout=subprocess.PIPE, cwd=self.path)
    reader = codecs.getreader('utf-8')

    try:
      object_info = json.load(reader(p.stdout), object_pairs_hook=OrderedDict)
    except:
      object_info = {}
    code = p.wait()
    if code != 0:
      raise IOError("File Not Found")

    return object_info

  def retrieveFile(self, filepath, start = 0, length = None):
    """
    Returns the data of a file for a particular revision of an object that is
    local on the disk or remote. If it is remote, it has to fetch the history.
    """

    import subprocess
    p = GitRepository.popen(['git', 'show', self.revision + ":./" + filepath], stdout=subprocess.PIPE, cwd=self.path)

    ret = None
    p.stdout.read(start)
    ret = p.stdout.read(length)
    p.kill()
    code = p.wait()
    if code != 0 and code != -9:
      raise IOError("File Not Found")

    return ret

  def retrieveFileStat(self, filepath):
    """ Returns the file status of the given file within the git repository.
    """

    if filepath.startswith("/") and filepath != "/":
      filepath = filepath[1:]

    import subprocess
    p = GitRepository.popen(['git', 'ls-tree', self.revision, "-l", "--", filepath], stdout=subprocess.PIPE, cwd=self.path)

    info = None
    for line in p.stdout:
      info = {}
      line = line.decode('utf-8').rstrip("\r\n")
      prime_components = line.split('\t')
      components = [x.strip() for x in prime_components[0].split()]
      info['stat'] = components[0]
      info['type'] = components[1]
      info['hash'] = components[2]
      if components[3] == "-":
        info['size'] = None
      else:
        try:
          info['size'] = int(components[3])
        except:
          info['size'] = 0

      info['name'] = prime_components[1]

    code = p.wait()

    if code != 0:
      raise IOError("File Not Found")

    return info

  def retrieveDirectory(self, filepath):
    """ Returns the directory listing of the given filepath.

    Returns a dictionary containing the information within the directory::

      {
        "items": [{
          "name": "foo.txt",   # name of the file
          "size": 1384,        # file size in bytes
        }, ... ],
      }
    """

    if filepath.startswith("/") and filepath != "/":
      filepath = filepath[1:]

    ret = {}
    ret['items'] = []

    import subprocess
    p = GitRepository.popen(['git', 'ls-tree', self.revision + ":./" + filepath, "-l"], stdout=subprocess.PIPE, cwd=self.path)

    for line in p.stdout:
      info = {}
      line = line.decode('utf-8').rstrip("\r\n")
      prime_components = line.split('\t')
      components = [x.strip() for x in prime_components[0].split()]
      info['stat'] = components[0]
      info['type'] = components[1]
      info['hash'] = components[2]
      if components[3] == "-":
        info['size'] = None
      else:
        try:
          info['size'] = int(components[3])
        except:
          info['size'] = 0

      info['name'] = prime_components[1]
      ret['items'].append(info)

    code = p.wait()

    if code != 0:
      raise IOError("File Not Found")

    return ret

  def history(self, path = None, limit=10):
    """ Returns a list of log entries from the given revision.
    """

    ret = []

    dateType = "iso8601-strict"

    if GitRepository.version() < 2000000000:
      # Git 1.x does not have the good iso8601...
      dateType = "iso8601"

    import subprocess
    p = GitRepository.popen(['git', 'log', '--date=%s' % (dateType), '-%s' % (str(limit)), self.revision], stdout=subprocess.PIPE, cwd=self.path)

    commit = None
    readingCommit = False

    for line in p.stdout:
      info = {}
      line = line.decode('utf-8').rstrip("\r\n")

      commit = commit or {"message":  "",
                          "author":   "",
                          "date":     "",
                          "revision": ""}

      if readingCommit:
        if line == "":
          readingCommit = False
          ret.append(commit)
          commit = None
        else:
          commit["message"] = commit.get("message", "") + line + "\n"
      else:
        if line.startswith("commit "):
          commit["revision"] = GitRepository.hexToMultihash(line[7:].strip())
        elif line.startswith("Author: "):
          commit["author"] = line[8:-1].strip()
        elif line.startswith("Date: "):
          commit["date"] = line[6:].strip()

          if dateType == "iso8601":
            # We need to make this date conform
            import re
            commit["date"] = commit["date"].replace(" ", "T", 1).replace(" ", "")
            commit["date"] = re.sub(r"([+-]\d\d)(\d\d)$", "\\1:\\2", commit["date"])

        if line == "":
          readingCommit = True

    if commit:
      commit["message"] = commit.get('message', '    ')[4:]
      ret.append(commit)

    code = p.wait()

    if code != 0:
      raise IOError("File Not Found")

    return ret

  def name(self):
    return self.objectInfo().get("name") or ""

  def type(self):
    return self.objectInfo().get("type") or ""

  def authors(self):
    return self.objectInfo().get("authors") or []

  def description(self):
    return self.objectInfo().get("description") or ""

  def objectInfo(self):
    """
    Returns the object metadata for a particular revision of an object that is
    local on the disk or remote. If it is remote, it has to fetch the history.
    """

    return self.retrieveJSON('object.json')

  def configurations(self):
    objectInfo = self.objectInfo()
    if "configurations" in objectInfo:
      return objectInfo["configurations"]
    else:
      return []

  def outputs(self):
    objectInfo = self.objectInfo()
    if "outputs" in objectInfo:
      return objectInfo["outputs"]
    else:
      return []

  def inputs(self):
    objectInfo = self.objectInfo()
    if "inputs" in objectInfo:
      return objectInfo["inputs"]
    else:
      return []

  # TODO: rename to installInfo
  def installPackages(self):
    objectInfo = self.objectInfo()
    if "install" in objectInfo:
      if isinstance(objectInfo["install"], list):
        return objectInfo["install"]
      else:
        return [objectInfo["install"]]
    else:
      return []

  def buildInfo(self):
    return self.objectInfo().get("build") or {}

  def runInfo(self):
    return self.objectInfo().get("run") or {}

  def input(self, name, type):
    return {}

  def schema(self):
    objectInfo = self.objectInfo()

  def commit(self, message, filepath='-a'):
    Log.noisy("commiting '%s' to %s" % (message, self.path))
    import subprocess
    p = GitRepository.popen(['git', 'commit', filepath, '--author="occam <occam@occam.cs.pitt.edu>"', '--file=-'], stdin=subprocess.PIPE, cwd=self.path, env={"GIT_COMMITTER_NAME": "occam", "GIT_COMMITTER_EMAIL": "occam@occam.cs.pitt.edu"})
    p.stdin.write(message.encode('utf-8'))
    p.stdin.close()
    p.wait()

    revision = self.head()

    return revision

  def delete(self, filename):
    p = GitRepository.popen(['git', 'rm', filename], cwd=self.path)
    p.wait()

    return True

  def move(self, old, new):
    # TODO: handle errors
    p = GitRepository.popen(['git', 'mv', old, new], cwd=self.path)
    p.wait()

    return True

  def add(self, filename='*'):
    # TODO: handle errors
    p = GitRepository.popen(['git', 'add', filename], cwd=self.path)
    p.wait()

    return True

  def initialCommit(self, revision="HEAD"):
    """ Returns the first commit from the given reference.
    """

    revision = GitRepository.hexFromMultihash(revision)

    import subprocess
    p = GitRepository.popen(['git', 'rev-list', "--max-parents=0", revision], stdout=subprocess.PIPE, cwd=self.path)

    ret = p.stdout.read().decode('utf-8').strip()

    errorCode = p.wait()
    if errorCode != 0:
      return False

    return ret

  def head(self):
    return GitRepository.fullRevision(self.path)

  def parent(self, revision="HEAD"):
    """ Returns the parent revision.
    """

    return GitRepository.fullRevision(self.path, "%s^" % (revision))

  def clone(self, to, shared=False):
    opts = []
    if shared:
      opts.append("-s")

    p = GitRepository.popen(['git', 'clone', self.path, to, *opts], cwd=self.path)
    p.wait()

    return GitRepository(to)

  def submoduleRevisionFor(self, path):
    """ Returns the revision tag for the submodule at the given path.
    """

    import subprocess
    p = GitRepository.popen(['git', 'submodule', 'status', path], stdout=subprocess.PIPE, cwd=self.path)
    ret = p.stdout.read().decode('utf-8').strip().split(' ')[0]
    if ret.startswith('+') or ret.startswith('-'):
      ret = ret[1:]

    p.wait()

    return ret

  def submoduleInit(self):
    """ Instantiates the submodule metadata for this repository.
    """

    p = GitRepository.popen(['git', 'submodule', 'init'], cwd=self.path)
    p.wait()

  def submoduleUpdate(self):
    """ Updates the submodules within this git repository. Assumes the submodules
    were initialized via submoduleInit().
    """

    p = GitRepository.popen(['git', 'submodule', 'update'], cwd=self.path)
    p.wait()

  def addRemote(self, name, path):
    # TODO: handle errors
    p = GitRepository.popen(['git', 'remote', 'add', name, path], cwd=self.path)
    p.wait()

    return True

  def rmRemote(self, name):
    # TODO: handle errors
    p = GitRepository.popen(['git', 'remote', 'rm', name], cwd=self.path)
    p.wait()

    return True

  def fetch(self, name):
    # TODO: handle errors
    p = GitRepository.popen(['git', 'fetch', name], cwd=self.path)
    p.wait()

    return True

  def deleteRef(self, ref):
    """ Deletes a reference.
    """

    revision = GitRepository.hexFromMultihash(revision)

    p = GitRepository.popen(['git', 'update-ref', '-d', '--', ref], cwd=self.path)
    p.wait()

  def updateRef(self, ref, revision):
    """ Updates the reference.
    """

    revision = GitRepository.hexFromMultihash(revision)

    p = GitRepository.popen(['git', 'update-ref', '--', ref, revision], cwd=self.path)
    p.wait()

  def hasBranch(self, name):
    """ Returns True if the given branch exists.
    """

    p = GitRepository.popen(['git', 'show-branch', '--', name], cwd=self.path)

    errorCode = p.wait()
    return errorCode == 0

  def fetchRemoteBranches(self, name):
    # TODO: handle errors
    p = GitRepository.popen(['git', 'branch', '--track', name], cwd=self.path)
    p.wait()
    p = GitRepository.popen(['git', 'fetch', name], cwd=self.path)
    p.wait()

    return True

  def fetchPath(self, path):
    # TODO: handle errors
    p = GitRepository.popen(['git', 'fetch', '--', path], cwd=self.path)
    p.wait()

    return True

  def branch(self):
    """ Returns the name of the current branch.
    """
    # TODO: handle errors
    import subprocess
    p = GitRepository.popen(['git', 'rev-parse', '--abbrev-ref', 'HEAD'], stdout=subprocess.PIPE, cwd=self.path)

    ret = p.stdout.read().decode('utf-8').strip()

    p.wait()

    return ret

  def addBranch(self, name):
    # TODO: handle errors
    p = GitRepository.popen(['git', 'branch', name], cwd=self.path)
    p.wait()

    return True

  def switchBranch(self, name):
    # TODO: handle errors
    p = GitRepository.popen(['git', 'checkout', name], cwd=self.path)
    p.wait()

    return True

  def checkoutBranch(self, branch_name, old_branch_name=None, remote_name=None):
    # TODO: handle errors
    if old_branch_name is None:
      old_branch_name = branch_name

    if remote_name is None:
      p = GitRepository.popen(['git', 'checkout', '-b', branch_name], cwd=self.path)
    else:
      p = GitRepository.popen(['git', 'checkout', '-b', branch_name, '%s/%s' % (remote_name, old_branch_name)], cwd=self.path)
    p.wait()

    return True

  def reset(self, revision, hard=True):
    revision = GitRepository.hexFromMultihash(revision)

    if hard:
      p = GitRepository.popen(['git', 'reset', "--hard", revision], cwd=self.path)
    else:
      p = GitRepository.popen(['git', 'reset', revision], cwd=self.path)
    p.wait()

    return True

  def stash(self):
    p = GitRepository.popen(['git', 'stash'], cwd=self.path)
    p.wait()

    return True

  def stashPop(self):
    p = GitRepository.popen(['git', 'stash', 'pop'], cwd=self.path)
    p.wait()

    return True

  def config(self):
    """ Returns a ConfigParser representing the git config if it exists. Will return
    an empty dictionary otherwise.
    """

    configPath = os.path.join(self.path, ".git", "config")
    config = {}
    if os.path.exists(configPath):
      import configparser # For parsing the git config
      config = configparser.ConfigParser()
      config.read(configPath)

    return config

  def configUpdate(self, config):
    """ Takes the given ConfigParser and writes it back out to overwrite the current
    git config.
    """

    configPath = os.path.join(self.path, ".git", "config")
    if os.path.exists(configPath):
      with open(configPath, 'w') as configFile:
        config.write(configFile)

  def gitmodules(self):
    """ Returns a ConfigParser representing the .gitmodules files, if it exists.
    Will return an empty dictionary otherwise.
    """

    # TODO: this could be in any path, I believe

    gitmodulesPath = os.path.join(self.path, ".gitmodules")
    gitmodules = {}
    if os.path.exists(gitmodulesPath):
      import configparser # For parsing the git config
      gitmodules = configparser.ConfigParser()
      gitmodules.read(gitmodulesPath)

    return gitmodules

  @staticmethod
  def isAt(url, cert=None):
    # Look for git repo
    result = -1

    # Determine the environment
    env = None
    if not cert is None:
      env = dict(os.environ, GIT_SSL_CAINFO=cert)

    try:
      result = GitRepository.popen(['git', 'ls-remote', url], env=env).wait()
    except:
      Log.error("cannot open resource: %s" % (sys.exc_info()[0]))

    return result == 0
