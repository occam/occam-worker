# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import re

class Semver:
  @staticmethod
  def parseVersionRange(version):
    WILDCARDS = ['*', 'x', 'X']

    rangeParts = version.split('...')
    rangeItem = []

    rangeStart = []
    rangeEnd   = []
    inclusive  = True

    for part in rangeParts:
      if len(rangeEnd) > 0:
        # Invalid version
        raise ValueError("ranged version cannot contain wildcards")

      currentRange = []
      part = part.strip()

      # Ignore initial 'v' or '=' or '=='
      if part[0] == "=":
        part = part[1:]
      if part[0] == "=":
        part = part[1:]
      if part[0] == "v":
        part = part[1:]

      # Get the condition
      if part[0] in "><!":
        if len(part) > 1 and part[1] == "=":
          rangeItem.append(part[0:2])
          part = part[2:]
        else:
          rangeItem.append(part[0:1])
          part = part[1:]

      # Split on '.'
      versionParts = part.split('.')

      lastTag = None
      for versionTag in versionParts:
        if versionTag in WILDCARDS:
          # The rest of the field is a wildcard
          rangeEnd = currentRange.copy()

          if lastTag is None:
            rangeItem.append('>=')
            rangeItem.append(currentRange)
            rangeEnd = []
            currentRange = []
            break

          try:
            lastTag = re.search(r'\d+', lastTag).group()
            if lastTag == "":
              lastTag = "0"
            rangeEnd[-1] = int(lastTag) + 1
            inclusive = False
          except:
            # Invalid version
            raise ValueError("cannot create a range; expected an incremental value")

          break

        try:
          currentRange.append(int(versionTag))
        except:
          currentRange.append(versionTag)
        lastTag = versionTag

      if len(currentRange) > 0:
        if len(rangeItem) > 0 and not isinstance(rangeItem[0], str):
          if inclusive:
            rangeItem.append('<=')
          else:
            rangeItem.append('<')

        rangeItem.append(currentRange)

      if len(rangeEnd) > 0:
        if inclusive:
          rangeItem.append('<=')
        else:
          rangeItem.append('<')
        rangeItem.append(rangeEnd)

    # Subdue invalid rangeItem
    if len(rangeItem) > 0 and len(rangeItem[0]) > 0 and isinstance(rangeItem[0], str):
      # Range starts with a condition. Can only be 2 items
      if len(rangeItem) >= 2:
        rangeItem = rangeItem[0:2]
    elif len(rangeItem) > 2:
      # Max for range is 3 items
      if len(rangeItem) > 3:
        rangeItem = rangeItem[0:3]

    return rangeItem

  @staticmethod
  def parseVersion(version):
    """ Parses the given version ranges.

    Returns:
      A list of range sets where any one should be satisfied: [ rangeset, rangeset, rangeset, ... ]

      A rangeset is a set of ranges that must all be satisfied: [ range, range, ... ]

      Where each range is a tuple of [ value ], [ condition, value ], or
      [ start, condition, end ]

      And each version item is a tuple of [ major, minor, patch, prerelease ]

    Examples:

      Each of these shows the input version string followed by a ``=>`` and the resulting array::

        * => [['>', [0, 0, 0, 0]]]

        1, 1.x => [[[1, 0, 0, 0], '<', [2, 0, 0, 0]]]

        1.2 => [[[1, 2, 0, 0], '<', [1, 3, 0, 0]]]

        1.2...1.3 => [[[1, 2, 0, 0], '<=', [1, 3, 0, 0]]]

        1.2 || 1.4...1.5 => [[[1,2,0,0]], [[1, 4, 0, 0], '<=', [1, 5, 0, 0]]]

        1.4.2.alpha.2 => [[[1, 4, 2, 'alpha.2']]]

        !=2.5.0, >=2.4 => [[['!=', [2, 5, 0, 0]], ['>=', [2, 4, 0, 0]]]]
    """

    # We will create a list of ranges
    rangesets = []

    # Split on each possible range
    versionList = version.split('||')

    # Pull apart the numeric values for each item
    for versionItems in versionList:
      versionItems = list(map(str.strip, versionItems.split(',')))
      ranges = []

      # Expand "~= a.b.c.d" versions to ">= a.b.c.d, = a.b.x.x"
      for i,versionItem in enumerate(versionItems):
        if versionItem.startswith("~="):
          versionItems[i] = ">" + versionItem[1:]
          versionItem = versionItems[i]
          items = versionItem[2:].split(".")
          items = items + ["0"] * (4 - len(items))
          items = items[:2] + ["x"] * (len(items) - 2)
          versionItems.append("=" + ".".join(items))

        rangeItem = Semver.parseVersionRange(versionItem)
        ranges.append(rangeItem)

      rangesets.append(ranges)

    return rangesets
 
  @staticmethod
  def versionMatches(a, condition, b):
    """
    """

    # The input a and b are arrays representing the version string
    # "4.3.1.p1" would be [4, 3, 1, "p1"]

    # Expand each version array into a canonical form
    a = Semver.padVersion(a)
    b = Semver.padVersion(b)

    if condition == "!=":
      return a != b
    if condition == "<":
      return a < b
    elif condition == "<=":
      return a <= b
    elif condition == ">":
      return a > b
    elif condition == ">=":
      return a >= b
    elif condition == "=":
      return a == b

  @staticmethod
  def versionInList(versionRangesets, version):
    """
    """
    parsedVersion = Semver.parseVersionRange(version)[0]

    for versionRangeset in versionRangesets:
      # The overall truth of the rangeset
      truth = True

      for versionRange in versionRangeset:
        # Whether or not the range is valid
        valid = False
        if len(versionRange) == 3:
          # start 'condition' end
          start = versionRange[0]
          condition = versionRange[1]
          end = versionRange[2]

          if Semver.versionMatches(parsedVersion, '>=', start):
            if Semver.versionMatches(parsedVersion, condition, end):
              valid = True
        elif len(versionRange) == 2:
          # 'condition' value
          condition = versionRange[0]
          value = versionRange[1]
          if Semver.versionMatches(parsedVersion, condition, value):
            valid = True
        elif len(versionRange) == 1:
          # Exact
          value = versionRange[0]
          if Semver.versionMatches(parsedVersion, "=", value):
            valid = True

        if not valid:
          truth = False
          break

      if truth:
        return True

    return False

  @staticmethod
  def padVersion(version):
    """ Returns the given version array with a canonical length.
    """

    if isinstance(version, str):
      version = Semver.parseVersionRange(version)[0]
    
    ret = []
    for item in version:
      ret.append(("." if isinstance(item, str) else "0") + str(item).zfill(20))
      
    ret.extend(["." + "0".zfill(19)] * (10-len(ret)))

    return ret

  @staticmethod
  def sortVersionList(versionList):
    """ Sorts the given list by version number.
    """

    # Ensures all items are strings of the same length, zero padded so they naturally sort with ascii inclusions
    # The version list is an array of such strings for major, minor, etc and all arrays are also padded to be the same length
    return sorted(versionList, key = Semver.padVersion)

  @staticmethod
  def resolveVersion(versionRequested, versionList):
    """ Returns the version number in the given list that matches the version requested or None.

    Returns:
      None when the available versions in versionList does not contain a satisfiable version.
    """

    # Sort the version list
    versionList = Semver.sortVersionList(versionList)

    rangesets = Semver.parseVersion(versionRequested)

    for version in reversed(versionList):
      if Semver.versionInList(rangesets, version):
        return version

    return None
