# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.log        import Log
from occam.group      import Group
from occam.object     import Object
from occam.experiment import Experiment

import os
import json

from uuid import uuid1

class Workset():
  """
  This class represents an OCCAM workset.
  """

  @staticmethod
  def create(path, name):
    """
    Creates the workset on disk.
    """

    Object.create(path, name, "workset", root=None, build=False, belongsTo=None, createPath=True)
    workset = Workset(path=path)
    return workset

  def __init__(self, path, uuid=None, revision=None):
    """
    Creates an instance of a workset wrapping an existing workset at the
    given path. It can take any path and will search for the workset in
    parent paths until finding a workset object.
    """

    self.object = Object(path=path, uuid=uuid, revision=revision, root=None)

  def __getattr__(self, method_name):
    """
    This will override method calls which don't exist on Experiment and
    pass them to the internal Object.
    """
    return getattr(self.object, method_name)
