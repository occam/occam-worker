# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import re, math

class Compiler:
  """ Implements a simple compiler (transpiler really) for configuration validations.

  This will generate and evaluate python strings built carefully from very simple
  token-based values.
  """

  class Tokenizer:
    """ Implements a tokenizer for the validations.
    """

    def __init__(self):
      """ Creates a new tokenizer.
      """
      self.tokens = []

    def push(self, token, data=None):
      """ Pushes the given token to the tokenizer stream.
      """

      self.tokens.append(token)
      if token == "identifier" or token == "number":
        self.tokens.append(data)

    def tokenize(self, str):
      """ Tokenizes the given string containing simple code.
      """

      last_character = ''
      state = 0
      token = ""

      for c in str:
        if state == 0:
          # Reading identifier
          if re.match(r'[*+-\/()%=<>!]', c):
            # Switch states
            state = 1

            # Push token
            if re.match(r'^\d+$', token):
              self.push("number", token)
            elif re.match(r'^\w+$', token):
              self.push("identifier", token)
            elif len(token):
              return False

            token = ""
            last_character = ""
        elif state == 1:
          # Reading symbol
          if c == '*' and last_character == '*' and len(token) == 1:
            # **
            pass
          elif (c == '=' and (last_character == '<' or
                              last_character == '>' or
                              last_character == '=' or
                              last_character == '!') and len(token) == 1):
            # ==, <=, >=, !=
            pass
          elif re.match(r'\d|\w', c):
            # Switch states
            state = 0

            # Push token
            self.push(token)
            token = ""
            last_character = ""
          elif (re.match(r'[*+-\/()%=<>!]', c)):
            # Push token
            self.push(token)
            token = ""
            last_character = ""

        if c == ' ':
          self.push(token)
          token = ""
          last_character = ""
        else:
          token = token + c
          last_character = c

      if re.match(r'^\d+$', token):
        self.push("number", token)
      elif re.match(r'^\w+$', token):
        self.push("identifier", token)
      else:
        self.push(token)

      return self.tokens

  def compile(code):
    """ Compiles the given code into an executable string.
    """

    tokens = Compiler.Tokenizer().tokenize(code)

    lastToken = ""
    leftParens = 0
    output = ""

    for token in tokens:
      if lastToken == "identifier":
        if token == "log2":
          output += "log2"
        elif token == "floor":
          output += "floor"
        elif token == "ceiling":
          output += "ceiling"
        elif token == "log":
          output += "log"
        elif token == "x":
          output += "x"
        else:
          output += "___var_%s___" % (token)
      elif lastToken == "number":
        output += "(" + token + ")"
      elif (token == "identifier" or token == "number"):
        pass
      elif token == "==":
        output += "=="
      else:
        if token == "(":
          leftParens += 1
        elif token == ")":
          if leftParens < 1:
            return False
          leftParens -= 1
        output = output + token

      lastToken = token

    def ceiling(x):
      return math.ceil(x)

    def floor(x):
      return math.floor(x)

    def log2(x):
      return math.log(x,2)

    def log(x,y):
      return math.log(x,y)

    eval_context = {
      'floor':   floor,
      'ceiling': ceiling,
      'log2':    log2,
      'log':     log,
    }

    return output, eval_context

  def execute(code, values):
    """ Executes the given code with the given value as 'x'.
    """

    output, eval_context = Compiler.compile(code)

    # TODO: is this safe???
    localVariables = {}

    for k,v in values.items():
      if k == "x":
        localVariables[k] = v
      else:
        localVariables["___var_%s___" % (k)] = v

    return eval(output, localVariables, eval_context)
