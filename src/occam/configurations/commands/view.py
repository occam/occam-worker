# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json

from occam.commands.manager import command, option, argument

from occam.object     import Object
from occam.log        import Log
from occam.key_parser import KeyParser

from occam.manager import uses

from occam.objects.manager        import ObjectManager
from occam.configurations.manager import ConfigurationManager

@command('configurations', 'view',
  category      = 'Configuration Management',
  documentation = "Shows the configuration options for the given object")
@argument("object",   type  = "object",
                      help  = "The configuration object to view.")
@argument("index",    nargs = "?",
                      help  = "The index of the input for the configuration to view. If none, views the object.")
@option("-k", "--key", action = "store",
                       dest   = "key",
                       help   = "the key whose value we'll pull from the object metadata or JSON file")
@uses(ObjectManager)
@uses(ConfigurationManager)
class ViewCommand:
  def printData(self, data):
    if self.options.key:
      parser = KeyParser()
      try:
        value = parser.get(data, self.options.key)
      except:
        Log.error("Key could not be found in document")
        return -1

      value = json.dumps(value)
      Log.output(value, padding="")
    else:
      Log.output(json.dumps(data))

  def do(self):
    # Get the object to update
    obj = self.objects.resolve(self.options.object, person = self.person)

    if obj is None:
      Log.error("cannot find object with id %s" % (self.options.object.id))
      return -1

    # Pull out the defaults
    if not self.options.index:
      data = self.configurations.dataFor(obj)
    else:
      data = self.configurations.defaultsFor(obj, int(self.options.index))

    self.printData(data)
