# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json

from occam.commands.manager import command, option, argument

from occam.object import Object
from occam.log    import Log

from occam.manager import uses

from occam.objects.write_manager  import ObjectWriteManager
from occam.configurations.manager import ConfigurationManager

@command('configurations', 'set',
  category      = 'Configuration Management',
  documentation = "Updates items within a configuration object.")
@argument("object",   type = "object",
                      help = "The configuration object to update.")
@option("-i", "--item", action  = "append",
                        dest    = "items",
                        nargs   = "+",
                        default = [],
                        help    = "the key/value pair to set. If no value is given, the key is deleted or written to its default.")
@option("-j", "--json", dest   = "to_json",
                        action = "store_true",
                        help   = "returns result as a json document")
@uses(ObjectWriteManager)
@uses(ConfigurationManager)
class SetCommand:
  def do(self):
    # Get the object to update
    obj = self.objects.resolve(self.options.object, person = self.person)

    if obj is None:
      Log.error("cannot find object with id %s" % (self.options.object.id))
      return -1

    # Create/use a mutable copy
    root, obj, path = self.objects.temporaryClone(obj, person = self.person)

    info = self.objects.infoFor(obj)
    objName = info.get('name', 'unknown')
    Log.write("Updating %s" % (objName))

    # Pull out the current data
    data = self.configurations.dataFor(obj)
    if data is None:
      # The data file doesn't exist in this object
      # We need to create it
      data = {}
      self.objects.write.addFileTo(obj, info["file"], json.dumps(data))

    # Pull out the defaults
    defaults = self.configurations.defaultsFor(obj)

    # Pull out the schema, which will be stored within the configuration object
    schema = self.configurations.schemaFor(obj, person = self.person)

    # For each key, set the configuration option
    for item in self.options.items:
      key, *value = item
      if len(value) == 1:
        value = value[0]

      new_value = None
      error     = None
      if len(value) == 0:
        self.configurations.delete(key, data, schema)
      else:
        new_value, error = self.configurations.set(key, value, data, schema)

      if error:
        Log.error("Could not set %s: %s" % (key, error))
        return -1

    self.objects.write.addFileTo(obj, info["file"], json.dumps(data))
    self.objects.write.commit(obj, self.person.identity, message="Updates configuration files")

    # Store the updated configuration object
    self.objects.write.store(obj, self.person.identity)

    ret = {}
    ret["updated"] = []

    for x in (obj.roots or []):
      ret["updated"].append({
        "id": x.id,
        "uid": x.uid,
        "revision": x.revision,
        "position": x.position,
      })

    ret["updated"].append({
      "id": obj.id,
      "uid": obj.uid,
      "revision": obj.revision,
      "position": obj.position,
    })

    if self.options.to_json:
      Log.output(json.dumps(ret))
    else:
      Log.write("new object id: ", end ="")
      Log.output("%s" % (obj.id), padding="")
      Log.write("new object revision: ", end ="")
      Log.output("%s" % (obj.revision), padding="")

    Log.done("Successfully created %s" % (self.objects.infoFor(obj).get('name')))
    return 0
