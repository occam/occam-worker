# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import copy

from occam.key_parser import KeyParser

class Permutor:
  def __init__(self, space, values):
    self.space  = space
    self.values = values

    # We will be parsing keys
    self.parser = KeyParser()

    # Keep track of the number of permutations we've returned
    self.returnedCount = 0

    # Keeping track of how many variations we've gone through thus far
    self.slotIndex = -1

  def nextSpace(self):
    self.slotIndex += 1

    self.keys = []
    self.indices = {}

    if self.slotIndex >= len(self.space):
      raise StopIteration()

    self.slot = self.space[self.slotIndex]
    self.keys = list(self.slot.keys())
    for k in self.keys:
      self.indices[k] = 0
    if len(self.keys) > 0:
      self.indices[self.keys[0]] = -1

  def __iter__(self):
    return self

  def __next__(self):
    """ Generate the next permutation of the captured configuration space.
    """

    ret = copy.deepcopy(self.values)

    if self.slotIndex == -1:
      try:
        self.nextSpace()
      except:
        pass

    # There is a more (computationally) efficient way of dealing with creating
    # the next permutation. You can keep a copy of the last generated options
    # and simply deepcopy that (copy it as you keep it so you aren't copying
    # a possibly mutated version that you are returning) and update the keys
    # as you iterate. (you change at most two keys, but usually just one)

    for i, key in enumerate(self.keys):
      self.indices[key] += 1
      if self.indices[key] >= len(self.slot[key]):
        self.indices[key] = 0
        if i == len(self.keys) - 1:
          self.nextSpace()
          return self.__next__()
        continue
      break

    for k,v in self.indices.items():
      self.parser.set(ret, k, self.slot[k][v][0], create = True)

    if len(self.space) <= 1 and len(self.indices) == 0 and self.returnedCount == 1:
      raise StopIteration()

    self.returnedCount += 1
    return ret
