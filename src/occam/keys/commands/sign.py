# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json
import os

from occam.commands.manager import command, option, argument

from occam.object import Object
from occam.log    import Log

from occam.manager import uses

from occam.objects.manager    import ObjectManager
from occam.keys.write_manager import KeyWriteManager

@command('keys', 'sign',
  category      = 'Key Management',
  documentation = "Signs the given object.")
@uses(KeyWriteManager)
@uses(ObjectManager)
@argument("object", type = "object", nargs = '?', help = "the object to sign")
class KeysSign:
  def do(self):
    if self.person is None:
      Log.error("Must be authenticated to sign an object.")
      return -1

    obj = self.objects.resolve(self.options.object, person = self.person)

    if obj is None:
      Log.error("Could not find the requested object.")
      return -1

    try:
      id, signature, published = self.keys.write.signObject(obj, self.person.identity)
    except:
      Log.error("Could not verify the signature based on the verification key.")
      return -1

    # If it verifies, then store the signature
    self.keys.write.store(obj, signature, self.person.identity, id, published)

    return 0
