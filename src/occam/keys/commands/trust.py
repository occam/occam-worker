# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json
import os

from occam.commands.manager import command, option, argument

from occam.object import Object
from occam.log    import Log

from occam.manager import uses

from occam.objects.manager    import ObjectManager
from occam.keys.write_manager import KeyWriteManager

@command('keys', 'trust',
  category      = 'Key Management',
  documentation = "Trusts the given identity.")
@uses(KeyWriteManager)
@uses(ObjectManager)
@argument("uri", type = str, help = "the identity URI to trust")
class KeysTrustCommand:
  def do(self):
    if self.person is None:
      Log.error("Must be authenticated to create a relationship.")
      return -1

    signingKey = self.keys.write.signingKeyFor(self.person.identity)
    signingKey = self.keys.importKey(signingKey)
    ourVerifyingKey = self.keys.importKey(self.keys.verifyingKeyFor(self.person.identity))

    verifyingKey = self.keys.verifyingKeyFor(self.options.uri)
    signature = self.keys.write.sign(verifyingKey.encode('utf-8'), signingKey)

    # Verify that the public can check the signature
    verified = self.keys.verify(verifyingKey.encode('utf-8'), signature, ourVerifyingKey)
    if not verified:
      Log.error("Could not verify the signature based on the verification key.")
      return -1

    # If it verifies, then store the signature
    self.keys.write.trust(self.person.identity, self.options.uri, signature)
    return 0
