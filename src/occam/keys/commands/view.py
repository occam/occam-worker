# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json
import os

from occam.commands.manager import command, option, argument

from occam.object import Object
from occam.log    import Log

from occam.manager import uses

from occam.keys.manager import KeyManager, KeyFormatError, KeyIdentityUnknownError

@command('keys', 'view',
  category      = 'Identity Management',
  documentation = "Lists the public key for the given identity URI")
@uses(KeyManager)
@argument("identity_uri", help    = "The identity URI to view.")
@option("-f", "--format", help    = "The key format to display. (OpenSSH, PEM)",
                          default = "PEM")
@option("-v", "--verify", help    = "Views the verification key for this identity.",
                          dest    = "verify",
                          action  = "store_true")
@option("-a", "--all",    help    = "display all metadata about this identity",
                          dest    = "all",
                          action  = "store_true")
class KeysView:
  def do(self):
    if self.options.all:
      import base64

      publicKey = self.keys.identityFor(self.options.identity_uri)

      ret = {}
      ret['publicKey'] = {
        'data': base64.b64encode(publicKey.encode('utf-8')).decode('utf-8'),
        'encoding': 'base64',
        'format': 'PEM'
      }

      verifyKeys = self.keys.verifyingKeysFor(self.options.identity_uri)

      # Pull out signature for this key

      ret['verifyingKeys'] = [{
        'key': {
          'id': record.id,
          'data': base64.b64encode(record.key.encode('utf-8')).decode('utf-8'),
          'published': record.published.isoformat() + "Z",
          'encoding': 'base64',
          'format': 'PEM'
        },
        'signature': {
          'data': base64.b64encode(record.signature).decode('utf-8'),
          'encoding': 'base64',
          'format': 'PKCS1_v1_5',
          'digest': 'SHA512'
        }
      } for record in verifyKeys]

      import json
      Log.output(json.dumps(ret))
      return 0

    if self.options.verify:
      identity = self.keys.verifyingKeysFor(self.options.identity_uri)[0].key
    else:
      identity = self.keys.identityFor(self.options.identity_uri)

    ret = identity

    if self.options.format != "PEM":
      key = self.keys.importKey(ret)
      ret = self.keys.exportKey(key, self.options.format)

    Log.pipe(ret)
    return 0
