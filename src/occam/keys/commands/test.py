# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json
import os

from occam.commands.manager import command, option, argument

from occam.object import Object
from occam.log    import Log

from occam.manager import uses

from occam.keys.write_manager import KeyWriteManager

@command('keys', 'test',
  category      = 'Object Inspection',
  documentation = "Displays the metadata for the object.")
@argument("uri")
@uses(KeyWriteManager)
class KeysTest:
  def do(self):
    publicKey = self.keys.identityFor(self.options.uri)
    privateKey = self.keys.write.identityKeyFor(self.options.uri)

    identity  = self.keys.importKey(privateKey)

    verifyKeys = self.keys.verifyingKeysFor(self.options.uri)
    for key in verifyKeys:
      # Recompute the signature of the key
      signature = self.keys.write.signKey(key.id, key.key, key.published, self.keys.importKey(privateKey))

      #self.keys.write.datastore.write.createVerifyKey(self.options.uri, "a", key.key, signature, key.published)

    return 0
