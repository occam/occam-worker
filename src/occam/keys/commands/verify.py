# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json
import os

from occam.commands.manager import command, option, argument

from occam.object import Object
from occam.log    import Log

from occam.manager import uses

from occam.objects.manager import ObjectManager
from occam.keys.manager    import KeyManager, KeySignatureNotFoundError

@command('keys', 'verify',
  category      = 'Key Management',
  documentation = "Verifies the given object.")
@uses(KeyManager)
@uses(ObjectManager)
@argument("object", type = "object", nargs = '?', help = "the object to verify")
class KeysVerify:
  def do(self):
    obj = self.objects.resolve(self.options.object, person = self.person)

    if obj is None:
      Log.error("Could not find the requested object.")
      return -1

    if obj.identity is None:
      Log.error("Need to specify an identity.")
      return -1

    # Discover the signature, if it exists
    verified = False
    try:
      signatures = self.keys.signaturesFor(obj, obj.identity)
    except KeySignatureNotFoundError:
      signatures = []

    for signature in signatures:
      # Verify that the public can check the signature
      verified = self.keys.verifyObject(obj, signature.signature, obj.identity, signature.verify_key_id, signature.published)
      if verified:
        break

    if not verified:
      Log.error("Could not verify the signature based on the verification key.")
      return -1

    return 0
