# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.log     import loggable
from occam.manager import manager, uses

import os

from occam.objects.manager   import ObjectManager
from occam.keys.manager      import KeyManager, KeyError
from occam.databases.manager import DataNotUniqueError

from Crypto           import Random
from Crypto.PublicKey import RSA
from Crypto.Cipher    import PKCS1_OAEP
from Crypto.Signature import PKCS1_v1_5
from Crypto.Hash      import SHA512, SHA384, SHA256, SHA, MD5

@loggable
@manager("keys.write", reader=KeyManager)
@uses(ObjectManager)
@uses(KeyManager)
class KeyWriteManager:
  """ This manages the retrieval, use, and creation of private keys.
  """

  def createIdentity(self):
    """ Creates a new identity.

    Returns:
      tuple: The IdentityRecord and IdentityKeyRecord respectively.
    """

    # Create an identity keypair
    publicKey, privateKey = self.keys.newKeyPair()

    # Create a signing key
    verifyKey, signingKey = self.keys.newKeyPair()

    import datetime
    published = datetime.datetime.now()

    # Sign the signing key
    identity  = self.keys.importKey(privateKey)
    verify_key_id = self.keys.idFor(verifyKey)

    uri = self.keys.idFor(publicKey)
    signature = self.signKey(uri, verifyKey, published, identity)

    # Store VerifyKey
    verifyKeyRecord = self.datastore.write.createVerifyKey(uri, verify_key_id, verifyKey, signature, published)

    # Store SigningKey
    signingKeyRecord = self.datastore.write.createSigningKey(signingKey, verifyKeyRecord)

    # Store Identity Key
    identityKeyRecord = self.datastore.write.createIdentityKey(uri, privateKey, verifyKeyRecord)

    # Store Identity
    identityRecord = self.datastore.write.createIdentity(uri, publicKey)

    return (identityRecord, identityKeyRecord,)

  def discover(self, uri, publicKey):
    """ Discovers the given identity.
    """

    # Verify public key matches identifier
    if uri != self.keys.idFor(publicKey):
      return None

    try:
      return self.datastore.write.createIdentity(uri, publicKey)
    except DataNotUniqueError as e:
      return True

  def discoverKey(self, uri, id, verifyKey, signature, published, revokes, publicKey):
    """ Discovers a new key for the given identity.
    """

    if self.keys.verifyKey(id, verifyKey, published, signature, self.keys.importKey(publicKey)):
      try:
        return self.datastore.write.createVerifyKey(uri, id, verifyKey, signature, published)
      except DataNotUniqueError as e:
        return True
    else:
      return None

  def identityKeyFor(self, uri):
    """ Retrieves the private identifying key for the given Person.

    This may be a difficult task if the identifying key is obscured or divided
    amongst many servers. This key is only necessary for the creation of new
    signing keys.

    Returns:
      privateKey: The private key for this Person.
    """

    ret = self.datastore.write.retrieveIdentityKey(uri)

    if ret is None:
      raise KeyIdentityUnknownError(uri)

    return ret.key

  def signingKeyFor(self, uri):
    """ Retrieves the private signing key for the given Person.

    This is a private key used to sign objects or other actions or messages.

    Returns:
      SigningKeyRecord: The private key for this Person for signing objects.
    """

    ret = self.datastore.write.retrieveSigningKeyFor(uri)

    if ret is None:
      raise KeyIdentityUnknownError(uri)

    return ret

  def revokeKey(self, person):
    """ Revokes the key for the given person.

    This will invalidate the signing key and create a new signing key for the
    given person. This key will, then, be signed by the identifying key.
    """

  def signKey(self, id, verifyingKey, published, privateKey):
    """ Signs the given key using the given identity.

    Returns:
      bytes: A digest representing the derived signature.
    """

    # Sign their public key
    token = self.keys.keyTokenFor(id, verifyingKey, published)
    return self.sign(token, privateKey)

  def decrypt(self, cipherText, privateKey):
    """ Decrypts the given message using the given private key.

    Args:
      cipherText (bytes): The cipher content to decrypt.
      privateKey (Key): The key to use.

    Returns:
      bytes: The decrypted content.
    """

    cipher = PKCS1_OAEP.new(privateKey)
    return cipher.decrypt(cipherText)

  def sign(self, message, privateKey):
    """ Signs the given message using the given private key.

    Args:
      message (bytes): The content to sign.
      privateKey (Key): The private key to use.

    Returns:
      bytes: A digest representing the derived signature.
    """

    signer = PKCS1_v1_5.new(privateKey)
    digest = SHA512.new()
    digest.update(message)
    return signer.sign(digest)

  def signObject(self, obj, uri, published = None):
    """ Returns a signature for the given Object using the given private key.

    The signature is a cryptographic hash of the object.json with the revision
    appended.

    Args:
      obj (Object): The object to sign.
      uri (str): The identity to sign as.
      published (datetime): The datetime to say the signature was published.
                            Defaults to the system date.

    Returns:
      str: The identifer of the verify key.
      bytes: A digest representing the derived signature.
      datetime: The time that the signature was created or the value of published.
    """

    signingKey = self.signingKeyFor(uri)
    privateKey = self.keys.importKey(signingKey.key)

    if published is None:
      import datetime
      published = datetime.datetime.now()

    token = self.keys.tokenFor(obj, uri, published)
    signature = self.sign(token, privateKey)

    # Can then check if a key is part of the master key
    verifyingKey = self.keys.verifyingKeyFor(uri, signingKey.id)
    publicKey    = self.keys.importKey(verifyingKey)

    # Verify that the public can check the signature
    verified = self.keys.verifyObject(obj, signature, uri, signingKey.id, published)
    if not verified:
      raise Exception("Could not verify the signature based on the verification key.")

    return signingKey.id, signature, published

  def signBuild(self, obj, uri, task, buildHash, built, published=None):
    """ Returns a signature representing the given build.

    Args:
      obj (Object): The object whose build is given.
      uri (str): The identity to sign as.
      task (Object): The build task.
      buildHash (str): The hash of the built binaries.
      built (datetime): When the build occurred.
      published (datetime): When the build was signed (default: now)

    Returns:
      bytes: A digest representing the derived signature.
      datetime: The time that the signature was created or the value of published.
      str: The verify key identifier.
    """

    signingKey = self.signingKeyFor(uri)
    privateKey = self.keys.importKey(signingKey.key)

    if published is None:
      import datetime
      published = datetime.datetime.now()

    tag = task.id + task.uid + task.revision + buildHash + built.isoformat() + "Z"

    token = self.keys.tokenFor(obj, uri, published, tag = tag)
    signature = self.sign(token, privateKey)

    # Can then check if a key is part of the master key
    verifyingKey = self.keys.verifyingKeyFor(uri, signingKey.id)
    publicKey    = self.keys.importKey(verifyingKey)

    # Verify that the public can check the signature
    verified = self.keys.verifyTag(obj, signature, uri, signingKey.id, tag, published)
    if not verified:
      raise Exception("Could not verify the signature based on the verification key.")

    return signature, published, signingKey.id

  def signTag(self, obj, uri, tag, published = None):
    """ Returns a signature representing the application of the given tag.

    Args:
      obj (Object): The object to sign.
      uri (str): The identity to sign as.
      tag (str): The tag being applied to the object.
      published (datetime): The datetime to say the signature was published.
                            Defaults to the system date.

    Returns:
      bytes: A digest representing the derived signature.
      datetime: The time that the signature was created or the value of published.
    """

    signingKey = self.signingKeyFor(uri)
    privateKey = self.keys.importKey(signingKey.key)

    if published is None:
      import datetime
      published = datetime.datetime.now()

    token = self.keys.tokenFor(obj, uri, published, tag = tag)
    signature = self.sign(token, privateKey)

    # Can then check if a key is part of the master key
    verifyingKey = self.keys.verifyingKeyFor(uri, signingKey.id)
    publicKey    = self.keys.importKey(verifyingKey)

    # Verify that the public can check the signature
    verified = self.keys.verifyTag(obj, signature, uri, signingKey.id, tag, published)
    if not verified:
      raise Exception("Could not verify the signature based on the verification key.")

    return signature, published

  def store(self, obj, signature, identity, verifyKeyID, published):
    """ Stores the given signature for the given object tied to the identity.

    Args:
      obj (Object): The object that has been signed.
      signature (bytes): The signature.
      identity (str): The Identity URI of the signer.
      verifyKeyID (str): The verification key id.
      published (datetime): The datetime to say the signature was published.

    Returns:
      SignatureRecord: The record of the signature.
    """

    # Create a signature in the database if it doesn't already exist

    try:
      return self.datastore.write.createSignature(obj.id, obj.revision, identity, signature, verifyKeyID, published)
    except DataNotUniqueError as e:
      raise KeySignatureExistsError(obj.id, obj.revision, identity)

  def trust(self, trustee, trusted, signature):
    """ Stores a proof that the given identity trusts the second identity.

    Args:
      trustee (str): The Identity URI of the person acting upon the trust.
      privateKey (Key): The acting Identity's private key.
      trusted (str): The Identity URI of the Identity being trusted.

    Returns:
      IdentityVoucherRecord: The record of the trust.
    """

    # Store that signature
    return self.datastore.write.createIdentityVoucher(trustee, trusted, signature)

class KeySignatureExistsError(KeyError):
  """ An attempt to store a signature resulted in a collision.
  """

  def __init__(self, uuid, revision, uri):
    self.uri      = uri
    self.uuid     = uuid
    self.revision = revision

  def __str__(self):
    return "Signature already exists when signing '%s@%s' as %s" % (self.uuid, self.revision, self.uri)
