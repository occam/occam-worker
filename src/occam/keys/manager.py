# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os

from occam.log     import loggable
from occam.manager import manager, uses

from Crypto           import Random
from Crypto.PublicKey import RSA
from Crypto.Cipher    import PKCS1_OAEP
from Crypto.Signature import PKCS1_v1_5
from Crypto.Hash      import SHA512, SHA384, SHA256, SHA, MD5

from occam.objects.manager import ObjectManager

@loggable
@manager("keys")
@uses(ObjectManager)
class KeyManager:
  """ This OCCAM manager handles signing and encryption keys for accounts.

  When objects are published or tagged, they can be signed. When you request a
  particular version or revision of an object, they can be requested with a
  particular signature. The object can then be retrieved only when verified
  against the signature that represents that identity.

  Servers can also be a signing authority.

  Server private keys verify Account private keys.

  A SignatureRecord holds the signed content of a particular object revision.

  The database records consist of the Identity, which represents an actor within
  the system. The Identity contains a public key. There is an IdentityKey which
  represents the private key. This could be lost (perhaps purposely) and
  recovered by gathering a set of KeyShard records. There is, associated with
  the Identity, a SigningKey (private) which has an associated VerifyKey
  (public).

  The Identity is referred by its URI. Considering Identity is decentralized in
  the best of cases, the URI itself is based on something that does not refer
  directly to the source (such as a domain). Instead, the URI refers to the
  identifying key, which is the private key used to sign the signing keys for
  that identity. The URI is the hash of the public key that corresponds to this
  private key. The URI itself is signed by the private key upon its creation.

  If this private identifying key is lost, then the identity is also lost and
  must be abandoned by its owner. This person must then create a new identity
  and resign their past work under their new identity and, through some social
  mechanism, tell others of this circumstance.
  """

  def newKeyPair(self):
    """ Returns a tuple containing new public and private keys respectively.

    Returns:
      tuple: (public key, private key), which are both PEM encoded utf-8 strings.
    """

    random_generator = Random.new().read
    key = RSA.generate(4096, random_generator)
    publicKey  = key.publickey().exportKey('PEM', pkcs=1)
    privateKey = key.exportKey('PEM', pkcs=1)

    return (publicKey.decode('utf-8'), privateKey.decode('utf-8'),)

  def randomHex(self, length):
    """ Generates a random hexadecimal string of the specified length.

    Args:
      length (int): The number of characters required in the returned string.

    Returns:
      str: A utf-8 encoded string representing a random hexadecimal string.
    """

    import binascii
    return binascii.hexlify(os.urandom(length)).decode('utf-8')

  def importKey(self, key):
    """ Imports and returns an instantiation of the given PEM string.
    """

    return RSA.importKey(key)

  def exportKey(self, key, format):
    """ Returns a key represented with the given format.

    The possible formats are "OpenSSH", "PEM" (unencrypted), and "DER".

    Args:
      key (RSAKey): The imported key.
      format (str): The format to return.
      
    Returns:
      str: The key in the given format.
    """

    try:
      ret = key.exportKey(format, pkcs=1)
    except ValueError:
      raise KeyFormatError(format)

    return ret

  def identityFor(self, uri):
    """ Retrieves the public identifying key for the given Identity URI.

    This key is used for verification of verifying keys (public signing keys)
    for use by the outside world in investigating trust.

    Returns:
      publicKey: The public key for this Person.
    """

    ret = self.datastore.retrieveIdentity(uri)

    if ret is None:
      raise KeyIdentityUnknownError(uri)

    ret = ret.key
    if not isinstance(ret, str):
      ret = ret.decode('utf-8')

    return ret

  def idFor(self, publicKey):
    """ Encodes the given public key. 
    """

    # Create a URI by hashing the public key with a multihash
    from occam.storage.plugins.ipfs_vendor.multihash import multihash
    hashedBytes = multihash.encode(publicKey, multihash.SHA2_256)

    from occam.storage.plugins.ipfs_vendor.base58 import b58encode
    return b58encode(bytes(hashedBytes))

  def verifyingKeysFor(self, uri):
    """ Retrieves the verifying keys for the given Identity URI.

    Raises:
      KeyIdentityUnknownError: No such identity is known.

    Returns:
      list: A list of each VerifyKeyRecord known for this identity.
    """

    ret = self.datastore.retrieveVerifyKeys(uri)

    if not ret:
      raise KeyIdentityUnknownError(uri)

    return ret

  def verifyingKeyFor(self, uri, id):
    """ Retrieves the public signing key for the given Identity URI.

    Arguments:
      uri (str): The Identity URI
      id (str): The identifier for the verify key

    Raises:
      KeyIdentityUnknownError: No such identity is known.

    Returns:
      publicKey: The public signing key for this Person.
    """

    ret = self.datastore.retrieveVerifyKey(uri, id)

    if ret is None:
      raise KeyIdentityUnknownError(uri)

    return ret.key

  def keyTokenFor(self, id, verifyingKey, published):
    """ Yields the sign payload for the given key.
    """
    return (id + published.isoformat() + "Z" + verifyingKey).encode('utf-8')

  def verifyKey(self, id, verifyingKey, published, signature, publicKey):
    """ Determines if the verifying and public key belong to the same identity.

    Returns:
      bool: True when the verifyingKey matches the publicKey.
    """

    token = self.keyTokenFor(id, verifyingKey, published)
    return self.verify(token, signature, publicKey)

  def encrypt(self, message, publicKey):
    """ Encrypts the given message using the given public key.

    Args:
      message (bytes): The message to encrypt.
      publicKey (Key): The key to use.

    Returns:
      bytes: The encrypted content.
    """

    cipher = PKCS1_OAEP.new(publicKey)
    return cipher.encrypt(message)

  def verify(self, message, signature, publicKey):
    """ Verifies the given message against the signature and given public key.

    Args:
      message (bytes): The content to verify.
      signature (bytes): A previously generated signature.
      publicKey (Key): The public key to use.
    
    Returns:
      bool: True if the signature matches the message.
    """

    signer = PKCS1_v1_5.new(publicKey)
    digest = SHA512.new()
    digest.update(message)
    return signer.verify(digest, signature)

  def tokenFor(self, obj, uri, published, tag = None):
    """ Returns the signable token for the given object and identity.
    """
    
    return self.objects.retrieveFileFrom(obj, "object.json") \
         + obj.revision.encode('utf-8') \
         + uri.encode('utf-8') \
         + (published.isoformat() + "Z").encode('utf-8') + (tag or "").encode('utf-8')

  def verifyObject(self, obj, signature, uri, id, published):
    """ Verifies that the given object was signed.

    Args:
      obj (Object): The object to verify.
      signature (bytes): A previously generated signature.
      uri (str): The identity to use.
      published (datetime): The time the signature was created.
    
    Returns:
      bool: True if the signature matches the object.
    """

    publicKey = self.importKey(self.verifyingKeyFor(uri, id))

    token = self.tokenFor(obj, uri, published)

    return self.verify(token, signature, publicKey)

  def verifyBuild(self, obj, verifyKeyId, signature, uri, task, buildHash, published, built):
    """ Verifies that the given build was signed.
    """

    publicKey = self.importKey(self.verifyingKeyFor(uri, verifyKeyId))

    tag = task.id + task.uid + task.revision + buildHash + built.isoformat() + "Z"

    token = self.tokenFor(obj, uri, published, tag = tag)

    return self.verify(token, signature, publicKey)

  def verifyTag(self, obj, signature, uri, id, tag, published):
    """ Verifies that the given tag was signed.

    Args:
      obj (Object): The object to verify.
      signature (bytes): A previously generated signature.
      uri (str): The identity to use.
      id (str): The identifier for the verify key.
      tag (str): The tag name.
      published (datetime): The time the signature was created.
    
    Returns:
      bool: True if the signature matches the object.
    """

    publicKey = self.importKey(self.verifyingKeyFor(uri, id))

    token = self.tokenFor(obj, uri, published, tag = tag)

    return self.verify(token, signature, publicKey)

  def signaturesFor(self, obj, identity=None):
    """ Gets all signatures for the given object.

    Args:
      obj (Object): The respective Object.
      identity (str): The Identity URI to inquire. Defaults to any attached
                      to the given object.

    Raises:
      KeySignatureNotFoundError: No such signature record could be found.

    Returns:
      SignatureRecord: An existing record of the signing.
    """

    signatures = self.datastore.retrieveSignatures(obj.id, obj.revision, identity or obj.identity)

    if not signatures:
      raise KeySignatureNotFoundError(obj.id, obj.revision, identity or obj.identity)

    return signatures

  def trustDistanceBetween(self, trustee, trusted):
    """ Determines if there is an existing and known trust relationship.

    Args:
      trustee (str): The Identity URI of the acting entity.
      trusted (str): The Identity URI of the actor to gauge the trust.

    Returns:
      float: The score of the relationship as a distance between the trustee and
             trusted. The value is between 0.0 and 1.0. 0 means there is no such
             relationship. 1 is a direct trust.
    """

    # Obviously a person trusts themselves as much as any other person
    if trustee == trusted:
      return 1.0

    # Gets the best known voucher and, with it, a trust score
    voucher = self.datastore.retrieveIdentityRelationship(trustee, trusted)

    ret = 0
    if voucher is not None:
      ret = pow(0.5, voucher.distance - 1)

    return ret

class KeyError(Exception):
  """ Base class for all key/identity errors.
  """

class KeyFormatError(KeyError):
  """ Unknown key format when importing or exporting keys.
  """

  def __init__(self, format):
    self.format = format

  def __str__(self):
    return "Unknown key format '%s'" % (self.format)

class KeyIdentityUnknownError(KeyError):
  """ A requested identity could not be found.
  """

  def __init__(self, uri):
    self.uri = uri

  def __str__(self):
    return "Unknown identity '%s'" % (self.uri)

class KeySignatureNotFoundError(KeyError):
  """ The requested signature was not known.
  """

  def __init__(self, uuid, revision, uri):
    self.uri      = uri
    self.uuid     = uuid
    self.revision = revision

  def __str__(self):
    return "Signature does not exist for '%s@%s' as %s" % (self.uuid, self.revision, self.uri)
