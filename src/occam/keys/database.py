# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sql

from occam.databases.manager import uses, datastore

@datastore("keys")
class KeysDatabase:
  """ Manages the database interactions for the Key component.
  """

  def queryIdentity(self, uri, key = None):
    """ Returns a query that returns the IdentityRecord for the given Identity.
    """

    identities = sql.Table("identities")

    if key:
      query = identities.select(identities.__getattr__(key))
    else:
      query = identities.select()

    query.where = (identities.uri == uri)
    return query

  def retrieveIdentity(self, uri):
    """ Returns the public IdentityRecord for the given Identity.
    """

    from occam.keys.records.identity import IdentityRecord

    session = self.database.session()

    query = self.queryIdentity(uri)

    self.database.execute(session, query)
    
    row = self.database.fetch(session)
    if row:
      row = IdentityRecord(row)

    return row

  def queryVerifyKey(self, uri, id):
    """ Returns a query that returns the public VerifyKey for the given Identity.
    """

    verifyKeys = sql.Table("verify_keys")

    return verifyKeys.select(where = (verifyKeys.id == id) & (verifyKeys.uri == uri))

  def retrieveVerifyKey(self, uri, id):
    """ Returns the public VerifyKey for the given Identity.
    """

    from occam.keys.records.verify_key import VerifyKeyRecord

    session = self.database.session()

    query = self.queryVerifyKey(uri, id)

    self.database.execute(session, query)
    
    row = self.database.fetch(session)
    if row:
      row = VerifyKeyRecord(row)

    return row

  def queryVerifyKeys(self, uri):
    """ Returns a query that returns all public VerifyKey records for the given Identity.
    """

    verifyKeys = sql.Table("verify_keys")

    return verifyKeys.select(where = (verifyKeys.uri == uri))

  def retrieveVerifyKeys(self, uri):
    """ Returns all public VerifyKey records for the given Identity.
    """

    from occam.keys.records.verify_key import VerifyKeyRecord

    session = self.database.session()

    query = self.queryVerifyKeys(uri)

    self.database.execute(session, query)
    
    rows = self.database.many(session, size=100)
    if rows:
      rows = [VerifyKeyRecord(row) for row in rows]

    return rows

  def querySignatures(self, id, revision, identity):
    """ Returns a query that returns the signature for the given object and Identity.
    """

    signatures = sql.Table("signatures")

    query = signatures.select()
    query.where = (signatures.uid          == id) & \
                  (signatures.revision     == revision) & \
                  (signatures.identity_uri == identity)

    return query

  def retrieveSignatures(self, id, revision, identity):
    """ Returns the signature for the given object and Identity.
    """

    from occam.keys.records.signature import SignatureRecord

    session = self.database.session()

    query = self.querySignatures(id, revision, identity)

    self.database.execute(session, query)
    
    rows = self.database.many(session, size=100)
    if rows:
      rows = [SignatureRecord(row) for row in rows]

    return rows

  def queryIdentityRelationship(self, trusteeURI, trustedURI):

    identityVouchers = sql.Table("identity_vouchers")

    # NOTE:
    # We don't need distance = 2 or 3 if we found one at 1...
    # We might be able to short circuit this query so it doesn't do all of that
    # other junk.

    # Gather trust relationships at a distance of 1 (direct connections)
    query = identityVouchers.select(sql.As(sql.Literal(trusteeURI), "uri"),
                                    sql.As(sql.Literal(trustedURI), "trusted_identity_uri"),
                                    sql.As(sql.Literal(1), "distance"))
    query.where = (identityVouchers.uri == trusteeURI) & \
                  (identityVouchers.trusted_identity_uri == trustedURI)

    # Gather trust relationships at a distance of 2
    subQuery = identityVouchers.select(sql.As(sql.Literal(trusteeURI), "uri"),
                                       sql.As(sql.Literal(trustedURI), "trusted_identity_uri"),
                                       sql.As(sql.Literal(2), "distance"))
    subSubQuery = identityVouchers.select(identityVouchers.uri)
    subSubQuery.where = (identityVouchers.trusted_identity_uri == trustedURI)

    subQuery.where = (identityVouchers.uri == trusteeURI) & \
                     (identityVouchers.trusted_identity_uri.in_(subSubQuery))

    query = query | subQuery

    # Gather trust relationships at a distance of 3
    subQuery = identityVouchers.select(sql.As(sql.Literal(trusteeURI), "uri"),
                                       sql.As(sql.Literal(trustedURI), "trusted_identity_uri"),
                                       sql.As(sql.Literal(3), "distance"))

    subSubQuery = identityVouchers.select(identityVouchers.uri)

    subSubSubQuery = identityVouchers.select(identityVouchers.uri)
    subSubSubQuery.where = (identityVouchers.trusted_identity_uri == trustedURI)

    subSubQuery.where = (identityVouchers.trusted_identity_uri.in_(subSubSubQuery))

    subQuery.where = (identityVouchers.uri == trusteeURI) & \
                     (identityVouchers.trusted_identity_uri.in_(subSubQuery))

    query = query | subQuery

    return query

  def retrieveIdentityRelationship(self, trusteeURI, trustedURI):
    """ Returns an IdentityVoucherRecord augmented with a score.
    """

    from occam.keys.records.identity_voucher import IdentityVoucherRecord

    session = self.database.session()

    query = self.queryIdentityRelationship(trusteeURI, trustedURI)

    self.database.execute(session, query)
    
    row = self.database.fetch(session)
    if row:
      row = IdentityVoucherRecord(row)
      row.distance = row._data['distance']

    return row
