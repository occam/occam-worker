# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.databases.manager import table

@table("signatures")
class SignatureRecord:
  """ Stores a generated signature for a particular revision of an Object.
  """

  schema = {

    # Primary Key

    # The signature
    "signature": {
      "type": "string",
      "length": 512,
      "primary": True
    },

    # Attributes

    # The ID of the object
    "uid": {
      "type": "string",
      "length": 36
    },

    # The revision hash of the object
    "revision": {
      "type": "string",
      "length": 128
    },

    # A timestamp for the request (can be whatever, honestly)
    "published": {
      "type": "datetime",
    },

    # The origin server
    
    # The signing Person
    "identity_uri": {
      "type": "string",
      "length": 256
    },

    # The verify key
    "verify_key_id": {
      "type": "string",
      "length": 256
    },

    # Constraints
    "relationship": {
      "constraint": {
        "type": "unique",
        "columns": ["identity_uri", "verify_key_id", "uid", "revision"],
        "conflict": "replace"
      }
    }
  }
