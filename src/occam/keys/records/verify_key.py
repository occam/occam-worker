# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.databases.manager import table

@table("verify_keys")
class VerifyKeyRecord:
  """ 
  """

  schema = {

    # Primary Key

    # The multihash of the key
    "id": {
      "type": "string",
      "length": 256,
      "primary": True
    },

    # Foreign Keys

    # The associated identity
    "uri": {
      "foreign": {
        "key": "uri",
        "table": "identities"
      }
    },

    # Attributes

    # The id of the key this one revokes
    "revokes": {
      "type": "string",
      "length": 256
    },

    # The actual key (PEM)
    "key": {
      "type": "text"
    },

    # A timestamp for the request (can be whatever, honestly)
    "published": {
      "type": "datetime",
    },

    # The signature for this key
    "signature": {
      "type": "string",
      "length": 512
    }
  }
