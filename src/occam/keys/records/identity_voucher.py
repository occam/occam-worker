# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.databases.manager import table

@table("identity_vouchers")
class IdentityVoucherRecord:
  """ Identity Vouchers are records of trust from one identity to another.
  """

  schema = {

    # Foreign Keys

    # The trusted identity
    "trusted_identity_uri": {
      "foreign": {
        "key": "uri",
        "table": "identities"
      }
    },

    # The trustee
    "uri": {
      "foreign": {
        "key": "uri",
        "table": "identities"
      }
    },

    # Attributes

    # The signature proof of this relationship
    # It is the signed public key of the trusted identity.
    "signature": {
      "type": "string",
      "length": 512
    },

    # Constraints
    "relationship": {
      "constraint": {
        "type": "unique",
        "columns": ["trusted_identity_uri", "uri"],
        "conflict": "replace"
      }
    }
  }
