# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.databases.manager import table

@table("identity_keys")
class IdentityKeyRecord:
  """ These are so-called "master" keys which are used to create signing keys.

  They are called, more appropriately, Identity Keys in this system since, with
  these, an identity can be impersonated. It is important, therefore, that
  these keys be as secure and private as possible.

  In an ideal context, you would create this, shard the key, and destroy the
  representation. That is, this record would not be stored. You need the key
  seldomly; generally this is only needed when a new signing key is created.
  Often times, months pass before such a key is needed.

  In this case, sharding the key, and only keeping some pieces (if any) can be
  a good strategy for obscuring the secret key which requires collusion in
  order to circumvent.

  In a less-ideal world, this record can be kept to hold the Private Key used
  to generate new keys for use in signing objects and actions across the
  network.

  This record only contains the PEM text of the key and no other referencing
  information. You would also need the IdentityRecord in order to associate any
  particular private key to its owner.
  """

  schema = {

    # Primary Key (hash of public key)

    "uri": {
      "type": "string",
      "length": 256,
      "primary": True
    },

    # Point to the current signing key
    "signing_key_id": {
      "foreign": {
        "key":   "id",
        "table": "signing_keys"
      }
    },

    # Attributes

    # The actual key (PEM)
    "key": {
      "type": "text"
    }
  }
