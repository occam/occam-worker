# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import sys

# To get the local hostname

from occam.log import loggable

from occam.manager import manager
from occam.config import Config

from urllib.request import Request, URLopener, urlopen, HTTPError
from http.client import HTTPConnection
from urllib.parse import urlparse, quote

class HTTPSConnection(HTTPConnection):
  pass

@loggable
@manager("network")
class NetworkManager:
  """
  This OCCAM manager handles downloading resources from the network.
  """

  def __init__(self):
    """
    Initialize the network manager.
    """

    self.ports = {}
    self.portStart = 30001
    self.portEnd = 31000

    # Ensure that the port manager path is created
    self.portPath = self.configuration.get('paths', {}).get('ports', os.path.join(Config.root(), "ports"))
    if not os.path.exists(self.portPath):
      os.mkdir(self.portPath);

  def hostname(self):
    """
    Returns the local hostname.
    """

    import socket
    return socket.gethostname()

  def ip4(self):
    """ Returns the local ip address.
    """

    import socket
    # This is a frustratingly difficult problem.
    # From: https://stackoverflow.com/questions/166506/finding-local-ip-addresses-using-pythons-stdlib
    try:
      ret = (([ip for ip in socket.gethostbyname_ex(socket.gethostname())[2] if not ip.startswith("127.")] or [[(s.connect(("8.8.8.8", 53)), s.getsockname()[0], s.close()) for s in [socket.socket(socket.AF_INET, socket.SOCK_DGRAM)]][0][1]]) + ["127.0.0.1"])[0]
    except:
      ret = None

    return ret

  def ip6(self):
    """ Returns the local ipv6 address.
    """

    import socket
    #return socket.getaddrinfo(socket.gethostname(), 443, socket.AF_INET6)[0][4][0]
    try:
      ret = (([[(s.connect(("2001:4860:4860::8888", 53, 0, 0)), s.getsockname()[0], s.close()) for s in [socket.socket(socket.AF_INET6, socket.SOCK_DGRAM)]][0][1]]) + ["::1/128"])[0]
    except:
      ret = None
      
    return ret

  def parseURL(self, url):
    """ Retrieves the urlparts of the given url.

    Returns:
      urllib.parse.ParseResult: An object namespace that holds several
        properties that correspond to the parts of the url.
        Those properties are "scheme", "netloc", "path, "params", "query", and
        "fragment". More information can be found in the python documentation
        for urllib.parse.
    """

    return urlparse(url)

  def getFTP(self, url):
    """
    Retrieves the given document over FTP.
    """

    # For pulling network http resources
    from ftplib import FTP

    urlparts = urlparse(url)
    path = os.path.dirname(urlparts.path)
    filename = os.path.basename(urlparts.path)

    ftp = FTP(urlparts.hostname)
    NetworkManager.Log.write(ftp.login())

    # Force binary mode
    NetworkManager.Log.write(ftp.sendcmd("TYPE I"))

    # Change directory
    NetworkManager.Log.write(ftp.cwd(path))

    # Retieve file
    socket, size = ftp.ntransfercmd('RETR %s' % (filename))

    # We must maintain the ftp connection and the socket for the incoming buffer
    class Socket:
      def __init__(self, socket, ftp):
        self.socket = socket
        self.ftp    = ftp

      def read(self, *args):
        return self.socket.recv(*args)

    # Return the buffer as a byte stream
    return Socket(socket, ftp), 'application/octet-stream', size

  def getHTTP(self, url, accept, scheme=None, suppressError=False, doNotVerify=False, cert=None, headers=None):
    """ Retrieves the given document over HTTP.
    """

    data = None

    response = None
    content_type = None

    request = Request(url)
    if scheme is not None:
      request.type = scheme
    request.add_header('Accept', accept)
    request.add_header('User-Agent', 'occam')

    for k, v in (headers or {}).items():
      request.add_header(k, v)

    ctx = None

    try:
      if doNotVerify:
        import ssl

        ctx = ssl.create_default_context()
        ctx.check_hostname = False
        ctx.verify_mode = ssl.CERT_NONE

        response = urlopen(request, cafile=cert, context=ctx)
      else:
        response = urlopen(request, cafile=cert)

      content_type = dict(response.info()).get('Content-Type')

      try:
        content_type = content_type[0:content_type.index(';')]
      except:
        pass
    except HTTPError as e:
      if e.code == 404:
        if not suppressError:
          NetworkManager.Log.error("cannot find resource: HTTP code %s" % (e.code))
      else:
        if not suppressError:
          NetworkManager.Log.error("cannot open resource: HTTP code %s" % (e.code))
    except:
      if not suppressError:
        NetworkManager.Log.error("cannot open resource: Other error.")

    try:
      size = int(response.headers.get('Content-Length'))
    except:
      size = 0

    return response, content_type, size

  def get(self, url, accept='application/json', scheme=None, suppressError=False, doNotVerify=False, cert=None, headers=None):
    """
    Retrieves the given document over HTTP or FTP.
    """

    urlparts = urlparse(url)

    if scheme is None:
      if urlparts.scheme == "":
        scheme = 'http'
      else:
        scheme = urlparts.scheme

    if scheme == "ftp":
      return self.getFTP(url)
    else:
      return self.getHTTP(url, accept, scheme, suppressError, doNotVerify, cert, headers=headers)

  def getJSON(self, url, accept="application/json", scheme=None, suppressError=False, cert=None, doNotVerify=False, headers=None):
    import codecs
    import json

    response, content_type, size = self.get(url, accept, scheme, suppressError, cert=cert, doNotVerify=doNotVerify, headers=headers)

    if response is None:
      return None

    reader = codecs.getreader('utf-8')
    data = json.load(reader(response))

    return data

  def post(self, url, data={}, accept='application/json'):
    pass

  def isGitAt(self, url, cert=None):
    """
    Returns True when the given url points to a git repository.
    """
    from occam.git_repository import GitRepository
    return GitRepository.isAt(url, cert=cert)

  def isObjectAt(self, url):
    pass

  def isURL(self, url):
    import re
    return re.match(r'^[a-z]+://', url)

  def allocatePort(self):
    """ Allocates a free port from the ranged configured on the system.

    This ensures that ports are not used more than once by an Occam
    process or virtual machine.
    """

    ret = None
    for port in range(self.portStart, self.portEnd):
      if not port in self.ports:
        path = os.path.join(self.portPath, str(port))
        if not os.path.exists(path):
          f = open(str(path), 'w+')
          f.close()

          self.ports[port] = True
          ret = port

          break

    return ret

  def freePort(self, port):
    """ Deallocates a port that was previously allocated by a call to allocatePort.
    """

    if port in self.ports:
      path = os.path.join(self.portPath, str(port))
      if os.path.exists(path):
        os.remove(path)

      del self.ports[port]

    return True
