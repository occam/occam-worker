# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.log        import Log
from occam.group      import Group
from occam.object     import Object

import os
import json

class Person(Object):
  """ This class represents an OCCAM person.
  """

  @staticmethod
  def create(path, name, uuid=None):
    """ Creates a person.
    """

    from uuid import uuid4

    Object.create(path, name, "person", uuid=str(uuid4()), root=None, build=False, belongsTo=None, createPath=False)
    return Person(path, root=None)

  def __init__(self, path, revision="HEAD", root=None, identity=None):
    """ Instantiates a Person wrapping an existing person at the given path.
    
    It can take any path and will search for the person in parent paths until
    finding a person object.
    """

    super(path=path, revision=revision, root=root)

    self.identity = identity
