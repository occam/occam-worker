# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os

from occam.log     import loggable
from occam.manager import manager, uses

from occam.storage.manager import StorageManager
from occam.keys.manager    import KeyManager

@loggable
@manager("builds")
@uses(StorageManager)
@uses(KeyManager)
class BuildManager:
  """ This OCCAM manager handles retrieval of builds.
  """

  def retrieveAll(self, obj):
    """ Retrieve a set of builds for the given object.

    Args:
      obj (Object): A resolved Object.

    Returns:
      list: One or more BuildRecord objects representing the known past builds.
    """

    return self.datastore.retrieveBuilds(obj.id, obj.revision)

  def retrieve(self, obj, task):
    """ Retrieve the BuildRecord for the given object and task.

    Returns:
      BuildRecord: The build record (or None if not found) for this build
    """

    return self.datastore.retrieveBuild(obj.id, obj.revision, task.id, task.revision)

  def retrieveDirectoryFrom(self, uid, revision, buildId, subpath):
    """ Pulls out a stream for retrieving the given file for this object.
    """

    items = self.storage.retrieveDirectory(uid, revision, subpath, buildId = buildId)

    if 'items' in items:
      for item in items['items']:
        if not 'mime' in item:
          item['mime'] = self.storage.mimeTypeFor(item['name'])

    return items

  def retrieveFileStatFrom(self, uid, revision, buildId, subpath):
    """ Pulls out the file status for the given path for this object.
    """

    ret = self.storage.retrieveFileStat(uid, revision, subpath, buildId = buildId)
    ret["mime"] = self.storage.mimeTypeFor(subpath)
    return ret

  def retrieveFileFrom(self, uid, revision, buildId, subpath, start=0, length=None):
    """ Pulls out a stream for retrieving the given file for this object.
    """

    return self.storage.retrieveFile(uid, revision, subpath, buildId = buildId)

  def logFor(self, obj, task):
    """ Retrieves the build log for the given object and build task.
    """

    return self.storage.buildLogFor(obj.uid, task.id, obj.revision)

  def verify(self, obj, task, identity, verifyKeyId, signature, published, signed):
    """ Verifies the build based on the files and signature.

    Args:
      obj (Object): The Object that would be built.
      task (Object): The Object representing the build task.
      identity (str): The actor that signed the build.
      verifyKeyId (str): The key used to sign.
      signature (bytes): The signature.
      published (datetime): When the object was built.
      signed (datetime): When the object was signed.
    """

    # Hash the build
    buildHash = self.retrieveHash(obj.uid, obj.revision, task.id)

    return self.keys.verifyBuild(obj, verifyKeyId, signature, identity, task, buildHash, signed, published)

  def retrieveHash(self, uid, revision, buildId):
    """ Hashes the given build.
    """

    # Recursively get the contents of the build and hash them to a standard
    import hashlib
    mainDigest = hashlib.sha256()

    def filehash(uid, revision, buildId, filepath):
      blocksize = 64*1024
      sha = hashlib.sha256()
      try:
        fp = self.retrieveFileFrom(uid, revision, buildId, filepath)
      except:
        return sha.hexdigest()
      while True:
        data = fp.read(blocksize)
        if not data:
          break
        sha.update(data)
      return sha.hexdigest() 

    def hashDirectory(uid, revision, buildId, items, path):
      # Sort items
      items = sorted(items, key = lambda x: x['name'])
      for item in items:
        filepath = os.path.join(path, item['name'])
        if item.get('type') == "tree":
          subItems = self.retrieveDirectoryFrom(uid, revision, buildId, filepath)['items']
          hashDirectory(uid, revision, buildId, subItems, filepath)
        else:
          sha = filehash(uid, revision, buildId, filepath)
          size = item['size']
          hashdata = '%s,%s,%s\n' % (size, sha, filepath)
          mainDigest.update(hashdata.encode('utf-8'))

    hashdata = """\
%%%% HASHDEEP-1.0
%%%% size,sha256,filename
##
## $ occam
##
"""
    mainDigest.update(hashdata.encode('utf-8'))

    data = self.retrieveDirectoryFrom(uid, revision, buildId, "/")
    hashDirectory(uid, revision, buildId, data['items'], "/")

    return mainDigest.hexdigest()
