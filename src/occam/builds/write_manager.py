# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.log     import loggable
from occam.manager import manager, uses

import os

from occam.objects.manager    import ObjectManager
from occam.builds.manager     import BuildManager
from occam.storage.manager    import StorageManager
from occam.network.manager    import NetworkManager
from occam.system.manager     import SystemManager
from occam.keys.write_manager import KeyWriteManager

@loggable
@manager("builds.write", reader=BuildManager)
@uses(ObjectManager)
@uses(BuildManager)
@uses(StorageManager)
@uses(NetworkManager)
@uses(SystemManager)
@uses(KeyWriteManager)
class BuildWriteManager:
  """ This manages the storage of new builds of objects.
  """

  def store(self, identity, object, task, buildPath, elapsed, buildLogPath=None):
    """ Stores the given build given the build task.

    Args:
      identity (str): The URI of the actor that built the object.
      task (Object): The task that built this object.
      buildPath (str): The path on disk of the resulting build.
      elapsed (int): The time in milliseconds of the build.
      buildLogPath (str): The filename of the build log.

    Returns:
      BuildRecord: The record of the build.
    """

    # Get the system configuration
    system = self.system.retrieve()

    # Retrieve the host/port that built the object
    host = system.host or self.network.hostname()
    port = system.port or 9292

    # Get the task manifest
    taskInfo = self.objects.infoFor(task)

    # Retrieve the built object from the task manifest
    builtObjectInfo = taskInfo.get('builds')

    backend = taskInfo.get('backend')

    # Retrieve identifiers for that object
    id       = builtObjectInfo.get('id')
    uid      = builtObjectInfo.get('uid')
    revision = builtObjectInfo.get('revision')

    # Store the build in the file store
    self.storage.pushBuild(uid, revision=revision, buildId=task.id, path=buildPath, logPath=buildLogPath)

    # Get the build hash
    buildHash = self.builds.retrieveHash(object.uid, object.revision, task.id)

    import datetime
    published = datetime.datetime.utcnow()

    # Generate a signature
    signature, signed, verifyKeyId = self.keys.write.signBuild(object, identity, task, buildHash, published)

    # Store a record of the build
    return self.datastore.write.createBuild(id            = id,
                                            uid           = uid,
                                            revision      = revision,
                                            buildId       = task.id,
                                            buildUid      = task.uid,
                                            buildRevision = task.revision,
                                            backend       = backend,
                                            elapsed       = elapsed,
                                            host          = host,
                                            port          = port,
                                            identity      = identity,
                                            verifyKeyId   = verifyKeyId,
                                            signed        = signed,
                                            signature     = signature,
                                            published     = published)
