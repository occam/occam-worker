# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sql
import sql.aggregate

from occam.log import loggable
from occam.databases.manager import uses, datastore

@loggable
@datastore("builds")
class BuildDatabase:
  """ Manages the database interactions for keeping track of builds.
  """

  def queryBuilds(self, id, revision, taskId=None, taskRevision=None):
    builds = sql.Table("builds")

    query = builds.select()
    query.where = (builds.id == id) & (builds.revision == revision)

    if taskId is not None and taskRevision is not None:
      query.where = query.where & (builds.build_id == taskId) & (builds.build_revision == taskRevision)

    return query

  def retrieveBuilds(self, id, revision):
    from occam.builds.records.build import BuildRecord

    session = self.database.session()

    query = self.queryBuilds(id, revision)

    self.database.execute(session, query)
    return [BuildRecord(x) for x in self.database.many(session, size=100)]

  def retrieveBuild(self, id, revision, taskId, taskRevision):
    from occam.builds.records.build import BuildRecord

    session = self.database.session()

    query = self.queryBuilds(id, revision, taskId, taskRevision)

    self.database.execute(session, query)

    ret = self.database.fetch(session)

    if ret is not None:
      ret = BuildRecord(ret)

    return ret
