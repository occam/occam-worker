# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sql

from occam.databases.manager import uses, datastore

from occam.builds.database import BuildDatabase

@datastore("builds.write", reader=BuildDatabase)
class BuildWriteDatabase:
  """ Manages the database interactions for recording new builds.
  """

  def createBuild(self, id,
                        uid,
                        revision,
                        buildId,
                        buildUid,
                        buildRevision,
                        backend,
                        elapsed,
                        host,
                        port,
                        identity,
                        verifyKeyId, 
                        published,
                        signed,
                        signature):

    from occam.builds.records.build import BuildRecord

    session = self.database.session()

    r = BuildRecord()

    r.id             = id
    r.uid            = uid
    r.revision       = revision
    r.build_id       = buildId
    r.build_uid      = buildUid
    r.build_revision = buildRevision
    r.backend        = backend
    r.elapsed        = elapsed
    r.host           = host
    r.port           = port
    r.identity_uri   = identity
    r.verify_key_id  = verifyKeyId
    r.published      = published
    r.signed         = signed
    r.signature      = signature

    self.database.update(session, r)
    self.database.commit(session)

    return r
