# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.databases.manager import table

@table("builds")
class BuildRecord:
  schema = {

    # Foreign keys

    "id": {
      "foreign": {
        "table": "objects",
        "key": "id"
      }
    },

    # The identifier of the build (which is the task id)
    "build_id": {
      "foreign": {
        "table": "objects",
        "key": "id"
      }
    },

    # Normalized metadata

    "uid": {
      "type": "string",
      "length": 256
    },

    # The task object's uid
    "build_uid": {
      "type": "string",
      "length": 256
    },

    # The task object's revision
    "build_revision": {
      "type": "string",
      "length": 128
    },

    # The backend type
    "backend": {
      "type": "string",
      "length": 32
    },

    # The revision of the object that was built
    "revision": {
      "type": "string",
      "length": 128
    },

    # A timestamp for its creation (can be whatever, honestly)
    "published": {
      "type": "datetime",
    },

    # The build time in seconds
    "elapsed": {
      "type": "integer"
    },
    
    # The signing Person
    "identity_uri": {
      "type": "string",
      "length": 256
    },

    # The host that built this object
    "host": {
      "type": "string",
      "length": 256
    },

    # The port of the machine that built this object
    "port": {
      "type": "integer"
    },

    # The verify key
    "verify_key_id": {
      "type": "string",
      "length": 256
    },

    # A timestamp for the signature
    "signed": {
      "type": "datetime",
    },

    # A signature for this build
    "signature": {
      "type": "string",
      "length": 512,
    }
  }
