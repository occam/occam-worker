# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.log    import Log
from occam.object import Object

from types import SimpleNamespace

from occam.commands.manager import command, option, argument

from occam.objects.manager import ObjectManager
from occam.builds.manager  import BuildManager

from occam.manager import uses

@command('builds', 'view',
  category      = 'Build Management',
  documentation = "View build log or files within a build.")
@argument("object", type="object", nargs="?", help="A buildable object")
@argument("task",   type="object", help="The build task")
@option("-j", "--json", dest   = "to_json",
                        action = "store_true",
                        help   = "returns result as a json document")
@option("-s", "--start",  action  = "store",
                          dest    = "start",
                          type    = int,
                          default = 0,
                          help    = "the byte position to start reading from")
@option("-l", "--length", action  = "store",
                          dest    = "length",
                          type    = int,
                          help    = "the number of bytes to read")
@uses(ObjectManager)
@uses(BuildManager)
class BuildsViewCommand:
  def viewBuild(self, obj, task):
    Log.header("Viewing build log")

    data = self.builds.logFor(obj, task)

    if data is not None:
      Log.pipe(data)

    return 0

  def viewBuildData(self, obj, task, path):
    data = self.builds.retrieveFileFrom(obj.uid, obj.revision, task.id, path, start = self.options.start, length = self.options.length)

    Log.pipe(data, length=self.options.length)

    return 0

  def do(self):
    if self.options.object is None:
      obj = self.objects.resolve(SimpleNamespace(id       = "+",
                                                 revision = None,
                                                 version  = None,
                                                 uid      = None,
                                                 path     = None,
                                                 index    = None,
                                                 link     = None,
                                                 identity = None),
                                 person = self.person)
    else:
      obj = self.objects.resolve(self.options.object,
                                 person = self.person)

    if obj is None:
      Log.error("Could not find the object.")
      return -1

    task = self.objects.resolve(self.options.task,
                                person = self.person)

    if task is None:
      Log.error("Could not find the task.")
      return -1

    if self.options.task.path:
      return self.viewBuildData(obj, task, self.options.task.path)

    return self.viewBuild(obj, task)
