# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.log    import Log
from occam.object import Object

from occam.commands.manager import command, option, argument

from occam.objects.manager import ObjectManager
from occam.builds.write_manager  import BuildWriteManager
from occam.manifests.manager import ManifestManager, BuildRequiredError
from occam.jobs.manager      import JobManager

from occam.manager import uses

@command('builds', 'new',
  category      = 'Build Management',
  documentation = "Starts a build of the given object.")
@argument("object", type="object", nargs="?")
@uses(ObjectManager)
@uses(BuildWriteManager)
@uses(ManifestManager)
@uses(JobManager)
class BuildsNewCommand:
  def do(self):
    if self.person is None:
      Log.error("Must be authenticated to build.")
      return -1

    if self.options.object is None:
      obj = Object(path=".")
    else:
      obj = self.objects.resolve(self.options.object,
                                 person = self.person)

    Log.header("Starting build")

    if obj is None:
      # Cannot resolve the object
      Log.error("Could not find the object.")
      return -1

    # Get a task
    resolved  = False
    penalties = {}

    task = self.manifests.build(obj, id = obj.id, revision = obj.revision, local = False, penalties = penalties, person = self.person)
    tasks = [task]
    resolved = True

    for task in reversed(tasks):
      #if self.options.task_only:
      if False:
        import json

        ret = {
          "id":       task.id,
          "uid":      task.uid,
          "revision": task.revision
        }

        Log.output(json.dumps(ret))
        return 0

      taskInfo = self.objects.infoFor(task)
      originalTaskId = task.id
      taskInfo['id'] = task.id
      opts = self.jobs.deploy(taskInfo, revision = task.revision, local=False, person = self.person, interactive=False)
      report = self.jobs.execute(*opts)

      elapsed = report['time']
      buildPath = report.get('paths').get(taskInfo.get('builds').get('index')).get('buildPath')

      self.builds.write.store(self.person.identity, obj, task, buildPath, elapsed)

    return 0
