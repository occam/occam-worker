# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.log    import Log
from occam.object import Object

import json
from types import SimpleNamespace

from occam.commands.manager import command, option, argument

from occam.objects.manager import ObjectManager
from occam.builds.manager  import BuildManager

from occam.manager import uses

@command('builds', 'verify',
  category      = 'Build Management',
  documentation = "Verifies the integrity of a build in the store.")
@argument("object", type="object", nargs="?", help="A buildable object")
@argument("task",   type="object", help="The build task")
@option("-j", "--json", dest   = "to_json",
                        action = "store_true",
                        help   = "returns result as a json document")
@uses(ObjectManager)
@uses(BuildManager)
class BuildsVerifyCommand:
  def do(self):
    if self.options.object is None:
      obj = self.objects.resolve(SimpleNamespace(id       = "+",
                                                 revision = None,
                                                 version  = None,
                                                 uid      = None,
                                                 path     = None,
                                                 index    = None,
                                                 link     = None,
                                                 identity = None),
                                 person = self.person)
    else:
      obj = self.objects.resolve(self.options.object,
                                 person = self.person)

    if obj is None:
      Log.error("Could not find the object.")
      return -1

    task = self.objects.resolve(self.options.task,
                                person = self.person)

    if task is None:
      Log.error("Could not find the task.")
      return -1

    # Retrieve the build record
    build = self.builds.retrieve(obj, task)

    if build is None:
      Log.error("Could not find a record of this build.")
      return -1

    # Verify the build
    identity    = build.identity_uri
    published   = build.published
    signature   = build.signature
    verifyKeyId = build.verify_key_id
    signed      = build.signed

    Log.output(json.dumps(self.builds.verify(obj, task, identity, verifyKeyId, signature, published, signed)))
