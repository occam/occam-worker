# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json
import datetime

from occam.commands.manager import command, option, argument

from occam.object import Object
from occam.log    import Log

from occam.manager import uses

from occam.objects.manager       import ObjectManager
from occam.jobs.manager          import JobManager
from occam.builds.write_manager  import BuildWriteManager

@command('builds', 'job-done',
  category      = 'Build Management',
  documentation = "Respond to a finished build job.")
@argument("job_id", type=str, help = "The identifier for the job.")
@uses(ObjectManager)
@uses(JobManager)
@uses(BuildWriteManager)
class BuildsJobDoneCommand:
  def do(self):
    """ Performs the 'builds job-done' command.
    """

    # Get the job
    job_id = self.options.job_id
    job = self.jobs.retrieve(job_id = self.options.job_id)[0]

    # Get the identity of the job actor
    person = self.jobs.personFor(job)

    # If the job failed (or cancelled), then do not commit the build
    if job.status == "failed":
      return 0

    # Get the task executed
    task = self.jobs.taskForJob(job_id)

    if task is None:
      # Cannot find the task
      Log.error("Cannot find the referenced task.")
      return -1

    # Retrieve the task manifest
    taskInfo = self.objects.infoFor(task)

    # Retrieve the object from the task
    objectInfo = taskInfo.get('builds')
    object = self.objects.retrieve(id       = objectInfo.get('id'),
                                   revision = objectInfo.get('revision'),
                                   person   = person)

    if object is None:
      # We cannot find the object that was built
      Log.error("Cannot find the referenced object.")
      return -1

    owner = self.objects.ownerFor(object, person = person)

    # Get the eventual build path
    buildPath = self.jobs.storage.buildPathFor(owner.uid,
                                               revision = objectInfo.get('revision'),
                                               buildId  = task.info.get("id"),
                                               create   = True)

    start_time  = job.start_time  or datetime.datetime.utcnow()
    finish_time = job.finish_time or datetime.datetime.utcnow()
    elapsed     = (job.finish_time-job.start_time).total_seconds()

    buildLogPath = self.jobs.logPathFor(job.id)

    self.builds.write.store(person.identity, owner, task, buildPath, elapsed, buildLogPath)
