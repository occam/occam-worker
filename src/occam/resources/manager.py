# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import json

from occam.config import Config
from occam.log    import loggable
from occam.object import Object

from occam.manager import manager, uses

from occam.storage.manager   import StorageManager

@loggable
@uses(StorageManager)
@manager("resources")
class ResourceManager:
  """ This OCCAM manager handles resource installation.
  
  This is the process OCCAM uses to resolve "install" sections of any
  Object.

  When a resource is newly pulled from source, the uid and revision/hash
  are not known. So, it is pulled to a temporary place near where the
  resource would be stored and then moved when the name is known. An Object
  and Resource record is created as necessary.

  For files, the resource data is a binary blob. Other types of resources
  are also possible. Git resources are directories that are maintained by
  the git data structure. In these cases, the resource is versioned by its
  own mechanism. Files and binary blobs, on the other hand, are versioned
  through an external mechanism: a hash.

  Therefore, each repository plugin can decide how it handles versioning.
  The FileResource plugin, for example, will be given a path to its
  resource data as stored by the StorageManager and will return a path
  to the requested revision as ``<base path>/<revision hash>``. Whereas a
  GitResource will simple parrot the ``<base path>`` as revisions are kept
  as metadata within the git resource rooted at that path.

  This is why, as strange as it may look when viewing one over the others,
  the resource plugin asks for a path and then repeats that path back to
  the other subroutines within the resource implementation. A FileResource,
  when :meth:`~occam.resources.plugins.file.FileResource.retrieve` is called, 
  passing it ``<base path>/<resource hash>`` allows it to form a direct
  path to the resource at ``<base path>/<resource hash>/data`` whereas the
  GitResource gets passed ``<base path>`` and remotely executes git to pull
  out the file data with ``git show`` using the ``<base path>`` as the
  working directory.

  Examples of different types of Resources are available in plugins. The
  FileResource is an example of a binary blob. The TarResource inherits the
  functionality of a File but adds archival and directory traversal within the
  resource. GitResource shows you a versioned resource that is self-maintained.
  DockerResource gives an example of a resource that is self-stored using some
  external tooling and is not installed as part of an Object's local filesystem.
  """

  handlers = {}

  def __init__(self):
    """ Initialize the resource manager.
    """

    import occam.resources.plugins.file
    import occam.resources.plugins.tar
    import occam.resources.plugins.git
    import occam.resources.plugins.mercurial
    import occam.resources.plugins.svn
    import occam.resources.plugins.zip
    import occam.resources.plugins.docker
    import occam.resources.plugins.iso

  @staticmethod
  def register(resourceType, handlerClass):
    """ Adds a new resource type.
    """
    ResourceManager.handlers[resourceType] = handlerClass

  def handlerFor(self, resourceType):
    """ Returns an instance of a handler for the given type.
    """

    if not resourceType in self.handlers:
      # TODO: warning?
      resourceType = 'file'

    return self.handlers[resourceType]()

  def uidTokenFor(self, type, name, source):
    """ Returns the reference UID token for the Resource Object.
    """

    # type, name, source

    return ("type:%s\nname:%s\nsource:%s" % (type, name, source)).encode('utf-8')

  def uidFor(self, type, name, source):
    """ Returns the reference UID for the Resource Object.
    """

    # Base58(SHA256(type, name, source))
    token = self.uidTokenFor(type, name, source)

    # Create a URI by hashing the public key with a multihash
    from occam.storage.plugins.ipfs_vendor.multihash import multihash
    hashedBytes = multihash.encode(token, multihash.SHA2_256)

    from occam.storage.plugins.ipfs_vendor.base58 import b58encode
    uri = b58encode(bytes(hashedBytes))
    
    return uri

  def idTokenFor(self, type, name, source, identity):
    """ Returns the ID token for the Resource Object.
    """

    # uid, identity, type, name, source

    uid = self.uidFor(type, name, source)
    return ("uid:%s\nidentity:%s\ntype:%s\nname:%s\nsource:%s" % (uid, identity, type, name, source)).encode('utf-8')

  def idFor(self, type, name, source, identity):
    """ Returns the ID for the Resource Object.
    """

    # Base58(SHA256(uid, identity, type, name, source))

    token = self.idTokenFor(type, name, source, identity)

    # Create a URI by hashing the public key with a multihash
    from occam.storage.plugins.ipfs_vendor.multihash import multihash
    hashedBytes = multihash.encode(token, multihash.SHA2_256)

    from occam.storage.plugins.ipfs_vendor.base58 import b58encode
    uri = b58encode(bytes(hashedBytes))
    
    return uri

  def infoFor(self, id=None, uid=None):
    """ Returns the information for the given resource.
    """

    if uid is None and id is None:
      raise ValueError("uid or id must be given.")

    row = self.retrieveResource(uid=uid, id=id)

    if row:
      return {
        "id": row.id,
        "uid": row.uid,
        "identity": row.identity_uri,
        "revision": row.revision,
        "name": row.name,
        "source": row.source,
        "type": "resource",
        "subtype": row.resource_type
      }
    else:
      return None

  def pathFor(self, uid, revision, handler=None):
    """ Returns the path the object of the given uid would be stored.
    """

    if handler is None:
      obj = self.datastore.retrieveResource(uid=uid)

      resourceType = None

      if not obj is None:
        resourceType = obj.resource_type

      if resourceType is None:
        return None

      handler = self.handlerFor(resourceType)

    storagePath = self.storage.resourcePathFor(uid, revision)
    return handler.pathFor(uid, storagePath, revision)

  def install(self, resourceInfo, path):
    """ Installs the given resource to the given path.
    """

    if resourceInfo is None:
      return

    # Get resource Type
    resourceType = resourceInfo.get('subtype', 'file')
    handler = self.handlerFor(resourceType)

    basePath = path

    # Default destination:
    resourceInfo['to'] = resourceInfo.get('to', 'package')

    # Do not allow a 'to' path with a relative ..
    if ".." in resourceInfo['to']:
      ResourceManager.Log.error("Resource's destination path contains a '..' which is not allowed.")
      return False

    path = os.path.join(basePath, resourceInfo['to'])

    id  = resourceInfo.get('id')
    uid = resourceInfo.get('uid')
    revision = resourceInfo.get('revision')

    resourcePath = None
    if not revision is None and not uid is None:
      storagePath = self.storage.resourcePathFor(uid, revision)
      if handler.exists(uid, storagePath, revision):
        resourcePath = handler.pathFor(uid, storagePath, revision)
        ResourceManager.Log.noisy("Installing resource from %s" % (resourcePath))

    if not resourcePath is None:
      ret = handler.install(uid, revision, resourcePath, resourceInfo, destination=os.path.realpath(path))

      if ret:
        handler.actions(uid, revision, resourcePath, resourceInfo, destination=os.path.realpath(basePath))

      return ret

    ResourceManager.Log.error("Failed to find resource")
    return False

  def installAll(self, objectInfo, path):
    """ Returns an array of Objects for pulled objects.
    
    This array, like pullAll, correspond directly to the list of
    resources to install in the given Object's 'install' section. This method
    will install the resources to the given path.
    """

    resources = objectInfo.get('install', [])

    for resource in resources:
      if resource.get('type') != 'resource':
        continue

      new_obj = self.install(resource, path)

      if 'install' in resource:
        self.installAll(resource, path)

    return resources

  def cloneAll(self, obj):
    """ Returns an array of new resource infos for pulled objects.
    
    This array corresponds
    directly to the list of resources to install in the given Object's 'install'
    section. It will clone the resource when possible and update that section to
    point to the cloned version. Otherwise, it will leave the resource info
    alone.
    """

    ret = []

    objectInfo = obj.objectInfo()

    resources = objectInfo.get('install', [])

    if not isinstance(resources, list):
      resources = [resources]

    for resourceInfo in resources:
      new_info = self.clone(resourceInfo)
      ret.append(new_info)

    return ret

  def clone(self, resourceInfo):
    """
    Will clone the given resource, if necessary. If not necessary, will just
    return the resource as is. Generally, this is useful for cloning objects
    that have a git repository attached. When you clone that object, you mean
    to modify the code, so you must also fork the repositories.
    """

    install_type = resourceInfo.get('type', 'object')
    uid          = resourceInfo.get('id')
    revision     = resourceInfo.get('revision')
    source       = resourceInfo.get('source')
    name         = resourceInfo.get('name')
    to           = resourceInfo.get('to', 'package')

    handler = self.handlerFor(install_type)
    if handler.clonable():
      newResourceInfo = handler.clone(uid, revision, name, source, to)
      newInfo = resourceInfo.copy()

      # Overwrite identifying tags in the new resource info structure
      for tag in ['id', 'revision']:
        newInfo[tag] = newResourceInfo.get(tag, newInfo.get(tag))

      return newInfo
    else:
      return resourceInfo

  def retrieveFile(self, id, uid, revision, resourceType, start=0, length=None):
    """ Pulls out a stream for retrieving the resource data for this object.
    """

    handler = self.handlerFor(resourceType)
    path = self.pathFor(uid, revision, handler=handler)
    return handler.retrieve(uid, revision, path)

  def retrieveFileStat(self, id, uid, revision, resourceType):
    """ Pulls out the file status for the resource object.
    """

    handler = self.handlerFor(resourceType)
    path = self.pathFor(uid, revision, handler=handler)
    ret = handler.stat(uid, revision, path)

    ret["from"] = {
      "type": "resource",
      "subtype": resourceType,
      "traversable": hasattr(handler, "traversable") and handler.traversable(uid, revision, path),
      "id": id,
      "uid": uid,
      "revision": revision,
      "path": path
    }

    return ret

  def retrieveRelativeDirectory(self, id, uid, revision, resourceType, actions, installPath, path):
    """ Returns a directory listing for a resource relative to an object.

    Args:
      id (str): The identifier for the resource.
      uid (str): The base identifier for the resource.
      revision (str): The hash for the resource.
      resourceType (str): The type of resource.
      installPath (str): The path the resource is to be installed within the object.
      path (str): The path within the object to retrieve.

    Returns:
      list: A list of directory information (including item lists) for the given path within an object.
    """

    data = {"items": []}

    ret = []

    destinationPath = os.path.normpath(os.path.join("/", os.path.normpath(os.path.dirname(installPath))))
    installationPath = os.path.normpath(os.path.join("/", os.path.normpath(installPath)))

    resourceInfo = self.infoFor(uid = uid, id = id)

    if actions.get('postUnpack') != "delete":
      if destinationPath == path:
        item = self.retrieveFileStat(id, uid, revision, resourceType)

        if item:
          item["name"] = os.path.basename(installPath)
          data['items'].append(item)
          ret.append((resourceInfo, data, None,))

    data = {"items": []}

    if actions.get('postUnpack') != "delete":
      if path.startswith(installationPath):
        item = self.retrieveFileStat(id, uid, revision, resourceType)

        # When the resource is a 'tree' type (like a git repository) or
        # it is 'traversable' (a file that contains files), we can
        # go /into/ that resource when the path specifies it

        # This works when we want to list the object such as
        # <ID>@<REVISION>/<RESOURCE NAME>/file.txt
        if item and (item["type"] == "tree" or item.get("from", {}).get("traversable")):
          # List the directory within the resource
          subPath = path[len(installationPath):]
          data['items'].extend(self.retrieveDirectoryFrom(id, uid, revision, resourceType, subPath)['items'])
          ret.append((resourceInfo, data, subPath,))

    data = {"items": []}

    unpackPath = actions.get('unpack')
    if unpackPath:
      unpackPath = os.path.normpath(os.path.join("/", unpackPath))

    comparePath = path
    if not path.startswith == "/":
      comparePath = "/" + path
    if unpackPath and os.path.commonpath([unpackPath,comparePath]) == unpackPath:
      internalPath = path[len(unpackPath)-1:]

      try:
        items = self.retrieveDirectoryFrom(id, uid, revision, resourceType, internalPath)
      except:
        items = {}

      # Incorporate the resource provenance to each entry:
      for item in items.get('items', []):
        if 'from' in items:
          item['from'] = items['from']

      data['items'].extend(items.get('items', []))

      ret.append((resourceInfo, data, internalPath,))

    return ret

  def retrieveDirectoryFrom(self, id, uid, revision, resourceType, subpath):
    """ Pulls out a stream for retrieving the given file for this object.
    """

    handler = self.handlerFor(resourceType)
    path = self.pathFor(uid, revision, handler=handler)
    data = handler.retrieveDirectory(uid, revision, path, subpath) or {}

    data['items'] = data.get('items', [])

    if not isinstance(data.get('items'), list):
      data['items'] = []

    if 'items' in data:
      for item in data['items']:
        if not 'mime' in item:
          item['mime'] = self.storage.mimeTypeFor(item['name'])

    data['from'] = {
      "type": "resource",
      "subtype": resourceType,
      "id": id,
      "uid": uid,
      "revision": revision,
      "path": subpath,
    }

    return data

  def retrieveResource(self, resourceType=None, id=None, uid=None, revision=None, source=None):
    return self.datastore.retrieveResource(resourceType=resourceType, id=id, uid=uid, revision=revision, source=source)

  def retrieveFileStatFrom(self, id, uid, revision, resourceType, subpath):
    """ Pulls out the file status for the given path for this object.
    """

    handler = self.handlerFor(resourceType)
    path = self.pathFor(uid, revision, handler=handler)
    return handler.retrieveFileStat(uid, revision, path, subpath)

  def retrieveFileFrom(self, id, uid, revision, resourceType, subpath, start=0, length=None):
    """ Pulls out a stream for retrieving the given file for this object.
    """
    handler = self.handlerFor(resourceType)
    path = self.pathFor(uid, revision, handler=handler)
    return handler.retrieveFile(uid, revision, path, subpath, start=start, length=length)

  def retrieveHistory(self, id, uid, revision, resourceType):
    """ Returns the known history of this resource and any related resources.
    """

    handler = self.handlerFor(resourceType)
    path = self.pathFor(uid, revision, handler=handler)
    ret = handler.retrieveHistory(uid, revision, path)

    if ret:
      return ret

    resources = self.datastore.retrieveResources(resourceType = resourceType,
                                                 id           = id,
                                                 uid          = uid)

    return list(map(lambda row:
      {
        "revision": row.revision,
        "name": row.name,
        "source": row.source,
        "type": "resource",
        "subtype": row.resource_type
      },
      resources))

def resource(name):
  """
  This decorator will register the given class as a resource.
  """

  def register_resource(cls):
    ResourceManager.register(name, cls)
    return cls

  return register_resource
