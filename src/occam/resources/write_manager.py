# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import json

from occam.config import Config
from occam.log    import loggable
from occam.object import Object

from occam.manager import manager, uses

from occam.resources.manager import ResourceManager
from occam.storage.manager   import StorageManager
from occam.databases.manager import DataNotUniqueError

@loggable
@uses(StorageManager)
@uses(ResourceManager)
@manager("resources.write", reader=ResourceManager)
class ResourceWriteManager:
  """ This OCCAM manager handles resource installation. This is the process
  OCCAM uses to resolve "install" sections of any Object.

  When a resource is newly pulled from source, the uid and revision/hash
  are not known. So, it is pulled to a temporary place near where the
  resource would be stored and then moved when the name is known. An Object
  and Resource record is created as necessary.
  """

  def pull(self, resourceInfo, identity, rootPath = None):
    """ Returns the object for the specfied resource, creating and storing it if
    necessary.
    """

    install_type = resourceInfo.get('subtype', 'file')
    uid          = resourceInfo.get('uid')
    id           = resourceInfo.get('id')
    revision     = resourceInfo.get('revision')
    source       = resourceInfo.get('source')
    name         = resourceInfo.get('name')
    to           = resourceInfo.get('to', 'package')
    headers      = resourceInfo.get('headers', {})

    destinationPath = os.path.join(rootPath, to)

    if id is None:
      if source:
        id  = self.resources.idFor(install_type, name, source, identity)
        uid = self.resources.uidFor(install_type, name, source)
      else:
        from uuid import uuid4
        tmpSource = to or str(uuid4())
        id  = self.resources.idFor(install_type, name, tmpSource, identity)
        uid = self.resources.uidFor(install_type, name, tmpSource)
    elif uid is None:
      uid = self.resources.uidFor(install_type, name, source)

    handler = self.resources.handlerFor(install_type)

    # Detect if the resource exists
    path = self.storage.resourcePathFor(uid)

    # Create a place to put the resource, if it doesn't exist
    # (We don't know what the 'revision' hash will be...
    new = False
    if path is None:
      path = self.storage.resourcePathFor(uid, create=True)
      ResourceWriteManager.Log.noisy("Creating a new resource in %s" % (path))
      new  = True

    # TODO: check for updates and preserve knowledge of changes
    if os.path.exists(destinationPath):
      if new:
        ResourceWriteManager.Log.write("Resource exists in path, but not in store. Using this instead of source.")
      else:
        ResourceWriteManager.Log.write("Resource exists in path, updating store.")
    else:
      destinationPath = None

    if revision and handler.exists(uid, path, revision):
      return id, uid, revision, [], handler.pathFor(uid, path, revision)

    # Pull the resource
    revision, subDependencies, data_path = handler.pull(uid, revision, name, source, to, path, headers=headers, existingPath=destinationPath)
    ResourceWriteManager.Log.noisy("Storing path %s" % data_path)

    # Store the resource
    self.storage.pushResource(uid, revision, data_path)

    # Create a resource record
    try:
      self.datastore.createResource(resourceType = install_type,
                                    id           = id,
                                    uid          = uid,
                                    name         = name,
                                    identity_uri = identity,
                                    revision     = revision,
                                    source       = source)
    except DataNotUniqueError as e:
      pass

    return id, uid, revision, subDependencies, data_path

  def pullAll(self, objectInfo, identity, rootPath = None):
    """ Returns an array of Objects for pulled objects.
    
    This array corresponds
    directly to the list of resources to install in the given Object's 'install'
    section.
    """

    ret = []

    resources = objectInfo.get('install', [])

    if not isinstance(resources, list):
      resources = [resources]

    for resourceInfo in resources:
      if resourceInfo.get('type') != "resource":
        ret.append({})
        continue

      id, uid, revision, subDependencies, data_path = self.pull(resourceInfo, identity, rootPath=rootPath)

      if uid is None:
        ResourceManager.Log.error("Could not pull %s resource." % (resourceInfo.get('type', 'unknown')))
        new_info = {}
      else:
        new_info = resourceInfo.copy()
        new_info.update({
                'id': id,
               'uid': uid,
          'revision': revision
        })

        # Add the dependencies to the resource itself (if they are new)
        if subDependencies:
          saved = []
          for newDependency in subDependencies:
            found = False
            for oldDependency in resourceInfo.get('install', []):
              if oldDependency.get('id') == newDependency.get('id') and oldDependency.get('revision') == newDependency.get('revision'):
                found = True

            if not found:
              saved.append(newDependency)

          new_info['install'] = new_info.get('install', []) + saved

        # Recurse for the dependencies
        if len(new_info.get('install', [])) > 0:
          subResources = self.pullAll(new_info, identity, rootPath=rootPath)

          # Add dependencies to install section
          new_info['install'] = [subResource.get('info', {}) for subResource in subResources]

      ret.append({
        'info': new_info,
        'path': data_path
      })

    return ret

  def update(self):
    """ Updates the given resource via the source.
    
    Pulls in any new information or changes from the source or from node mirrors.
    Returns True if there are any updated changes.
    """

    return False

  def commitAll(self, obj, objInfo):
    """
    This is given a local object. Will go through the object's resources in
    its 'install' section and for each decide if the value currently on disk in
    the object's path has changed. It will return an updated resource list
    containing new revisions while storing the changes in the store.
    """

    resources = objInfo.get('install', [])
    if not isinstance(resources, list):
      resources = [resources]

    dirty = False

    ret = []

    for resourceInfo in resources:
      # Look at that directory (if it exists) and check to see
      # if the resource has changed

      path = os.path.join(obj.path, resourceInfo.get('to', 'package'))

      newResourceInfo, changed = self.commit(resourceInfo, path)
      if changed:
        ret.append(newResourceInfo)
        dirty = True
      else:
        ret.append(resourceInfo)

    return ret, dirty

  def commit(self, resourceInfo, path):
    """
    """

    install_type = resourceInfo.get('type', 'object')
    uid          = resourceInfo.get('id')
    revision     = resourceInfo.get('revision')
    source       = resourceInfo.get('source')
    name         = resourceInfo.get('name')

    # Call the resource handler's commit method
    handler = self.resources.handlerFor(install_type)

    if not os.path.exists(path):
      return resourceInfo, False

    newResourceInfo, dirty = handler.commit(uid, revision, name, source, path)

    # Create a new resource tag based on the old
    newInfo = resourceInfo.copy()

    # Update revision
    newInfo['revision'] = newInfo.get('revision', '')
    newInfo['revision'] = newResourceInfo.get('revision', newInfo['revision'])

    # Return the updated resource tag
    return newInfo, dirty
