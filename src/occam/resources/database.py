# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sql

from occam.databases.manager import uses, datastore

@datastore("resources")
class ResourceDatabase:
  """ Manages the database interactions for tracking resource objects.
  """

  def queryResource(self, resourceType=None, id=None, uid=None, revision=None, source=None):
    """ Returns a query to retrieve any ResourceRecord for the given parameters.

    Returns:
      sql.Query: The formed query.
    """

    resources = sql.Table('resources')

    query = resources.select()
    query.where = sql.Literal(True)

    if not resourceType is None:
      query.where = (resources.resource_type == resourceType)

    if not id is None:
      query.where = query.where & (resources.id == id)

    if not uid is None:
      query.where = query.where & (resources.uid == uid)

    if not source is None:
      query.where = (resources.source == source)

    if not revision is None:
      query.where = query.where & (resources.revision == revision)

    return query

  def retrieveResource(self, resourceType=None, id=None, uid=None, revision=None, source=None):
    """ Retrieves the database record that describes the instantiation of the given
    resource given its resourceType, id/uid, and revision.

    If the resource is not known specifically for this revision, None is
    returned.
    """

    from occam.resources.records.resource import ResourceRecord
    session = self.database.session()

    query = self.queryResource(resourceType = resourceType,
                               id           = id,
                               uid          = uid,
                               revision     = revision,
                               source       = source)

    self.database.execute(session, query)

    ret = self.database.fetch(session)
    if not ret is None:
      ret = ResourceRecord(ret)

    return ret

  def retrieveResources(self, resourceType=None, id=None, uid=None, revision=None, source=None):
    """ Retrieves all ResourceRecords for the given parameters.
    """

    from occam.resources.records.resource import ResourceRecord
    session = self.database.session()

    query = self.queryResource(resourceType = resourceType,
                               id           = id,
                               uid          = uid,
                               revision     = revision,
                               source       = source)

    self.database.execute(session, query)

    rows = self.database.many(session, size = 100)
    return [ResourceRecord(x) for x in rows]

  def createResource(self, id, uid, revision, resourceType, name, identity_uri, source=None):
    """ Adds a record for this instantiation of a resource.

    Adds the record which will be attached to
    the given object record. The new record will record the source of the data
    for the original resource and the revision (hash, etc) for the resource
    as given by the handler.
    """

    # Get a Database instance
    session = self.database.session()

    # Retrieve any existing resource
    resource = self.retrieveResource(resourceType = resourceType,
                                     id           = id,
                                     uid          = uid,
                                     revision     = revision,
                                     source       = source)

    # If the record doesn't exist, create it
    if resource is None:
      from occam.resources.records.resource import ResourceRecord
      resource = ResourceRecord()

      # Tag the revision or hash for lookups later
      resource.revision = revision
      resource.resource_type = resourceType
      resource.id = id
      resource.uid = uid
      resource.name = name
      resource.revision = revision
      resource.source = source
      resource.identity_uri = identity_uri

      # Commit
      self.database.update(session, resource)
      self.database.commit(session)

    return resource
