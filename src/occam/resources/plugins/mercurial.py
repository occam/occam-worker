# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os

from occam.object import Object
from occam.log    import loggable
from occam.git_repository import GitRepository as GitRepo

from occam.resources.manager import resource, ResourceManager

from occam.network.manager import NetworkManager

from occam.manager import uses

import hglib

@resource('mercurial')
@uses(NetworkManager)
@loggable
class MercurialResource:
  """ This is an OCCAM resource engine for handling repeatable Mercurial retrieval
  and storage.
  """

  def pathFor(self, uuid, path, revision=None):
    """ Returns the path to the stored mercurial repository on disk (if it would exist)
    for the given uuid.
    """

    # The repository is stored in the base path
    return path

  def actions(self, uuid, revision, path, resourceInfo, destination):
    """ Performs any actions that may be required upon installation.
    """

    return None

  def install(self, uuid, revision, path, resourceInfo, destination):
    """ Installs the mercurial repository via the given uuid and revision to the given path.
    """

    hgPath = path
    MercurialResource.Log.noisy("Cloning mercurial repository at revision %s from %s to %s" % (revision, hgPath, destination))

    hglib.clone(hgPath, destination, updaterev=GitRepo.hexFromMultihash(revision))
    # reset to given revision

    return True

  def pull(self, uuid, revision, name, source, to, path, cert=None, headers=None, existingPath=None):
    """ Retrieves and stores the given mercurial resource according to the given resource info.
    
    Returns a reference of the Object in the store. The certificate path
    necessary to retrieve the resource over HTTPS can be given.
    """

    name = name or "Mercurial Repository"

    git = None
    gitPath = None
    dependencies = []
    git_obj = None

    # Create local repository
    MercurialResource.Log.write("Pulling resource from %s" % (source))

    # Place it in:
    hgPath = path
    if os.path.exists(hgPath):
      import shutil
      shutil.rmtree(hgPath)
    cloned = hglib.clone(source, hgPath)
    cloned = hglib.open(hgPath)

    revision = cloned.tip()[1].decode('utf-8')
    revision = GitRepo.hexToMultihash(revision)
    dependencies = None

    return revision, dependencies, path

  def exists(self, uuid, path, revision=None):
    """ Returns True if a mercurial repository containing the given revision is stored
    in the object store. If revision is not specified, it just looks for the
    presence of the mercurial repository.
    """

    hgPath = self.pathFor(uuid, path, revision)
    hgPath = os.path.join(hgPath, '.hg')
    return os.path.exists(hgPath)

  def update(self, uuid, source):
    """ Updates the mercurial repository object stored in the store.
    """

    # Git updates just pull down any new commits into a random branch name.
    # These commits will be preserved by the branching semantics of git.
    # We can then return a new HEAD, if there is one.

    info = obj.objectInfo()
    object_type = info['type']

    if info.get('storable') == False:
      return

    uuid = info['id']
    path = self.git.objects.pathFor(uuid, 'git')

    # Get the repository's actual file path in the git store
    repo_path = os.path.join(path, info['file'])

    # Interface to Git for this repository in the git store
    store_git = GitRepo(repo_path)

    GitResource.Log.noisy("fetching from %s to %s" % (git.path, repo_path))

    # Create random remote and branch names
    remote_name = str(uuid4())
    new_branch_name = str(uuid4())

    # Add the remote to the local git repository
    GitResource.Log.noisy("adding remote %s -> %s" % (remote_name, git.path))
    store_git.addRemote(remote_name, git.path)

    # Fetch local branches
    store_git.fetchRemoteBranches(remote_name)

    # Checkout that branch from the remote
    GitResource.Log.noisy("checking out %s from %s/%s" % (new_branch_name, remote_name, git.branch()))
    store_git.checkoutBranch(new_branch_name, git.branch(), remote_name)

    # Remove generated remote name
    store_git.rmRemote(remote_name)

    # Done

  def currentRevision(self, path):
    """ Returns the current revision of the object.
    
    This is used to detect changes.
    When this revision differs from the one an object current is using during
    a commit, then it may be updated in that object. For instance, in an 'occam
    update' command.
    """

    hg = hglib.open(path)
    if hg is None:
      return None

    return GitRepo.hexToMultihash(hg.tip())

  def retrieveDirectory(self, uuid, revision, path, subpath):
    """ Retrieves the directory listing of the given revision.
    """

    return GitRepo(path).retrieveDirectory(subpath)

  def retrieveFileStat(self, uuid, revision, path, subpath):
    """ Retrieves the file status information for the given path in the given revision.
    """

    return GitRepo(path).retrieveFileStat(subpath)

  def retrieveFile(self, uuid, revision, path, subpath):
    """ Retrieves the file data found within the resource at the given revision.
    """

    return GitRepo(path).retrieveFile(subpath)

  def stat(self, uuid, revision, path):
    """ Retrieves the file status of the resource.
    """

    # Git repositorites are directories:

    return {
      "name": "repository",
      "type": "tree"
    }

  def retrieve(self, uuid, revision, path):
    """ Retrieves the resource data.
    """

    # Mercurial repositories are directory based, they are not retrievable.
    return None

  def retrieveHistory(self, uid, revision, path):
    return None

  def traversable(self, uid, revision, path):
    """ Returns True since repositories are always treated as a directory.
    """

    return True
