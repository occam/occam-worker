# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json
import os

import occam.resources.plugins.vendor.isoparser as isoparser

from occam.resources.manager import resource

from occam.log import loggable

from occam.resources.plugins.file import FileResource

@loggable
@resource("application/x-iso9660-image")
@resource("iso")
@resource("bin")
class IsoResource(FileResource):
  """ This is an Occam resource engine for handling 'compressed' iso files.
  """

  def actions(self, uuid, revision, path, resourceInfo, destination):
    """ Performs any actions that may be required upon installation.

    In this case, the unpack action.
    """

    path = os.path.join(path, "data")

    if os.path.exists(path):
      actions = resourceInfo.get('actions', {})
      if not isinstance(actions, dict):
        actions = {}

      if resourceInfo and "unpack" in actions:
        # TODO: go through and extract every file (probably unwise??)
        pass

      return True

    return False

  def _pullEntryInfo(self, isoRecord):
    """ Internal method to form the item entry for a given tarinfo.
    """

    type = "file"
    if isoRecord.is_directory:
      type = "tree"
    #if tarinfo.issym():
    #  type = "link"

    # Go through SUSP entries
    mode  = None
    mtime = None
    atime = None
    uid   = None
    gid   = None
    for susp_entry in isoRecord.susp_entries:
      if hasattr(susp_entry, 'mode'):
        mode = susp_entry.mode
      if hasattr(susp_entry, 'modify'):
        mtime = susp_entry.modify
      if hasattr(susp_entry, 'access'):
        atime = susp_entry.access
      if hasattr(susp_entry, 'uid'):
        uid = susp_entry.uid
      if hasattr(susp_entry, 'gid'):
        gid = susp_entry.gid

    item = {"name":  isoRecord.name.decode('utf-16be'),
            "size":  isoRecord.length,
            "type":  type,
            "uid":   uid,
            "gid":   gid,
            "mode":  mode,
            "mtime": mtime}

    # TODO: understand how ISO files keep track of links (tarfile given below)
    #if type == "link":
    #  item['target'] = tarinfo.linkname

    return item

  def retrieveDirectory(self, uuid, revision, path, subpath):
    """ Retrieves the directory listing of the given revision.
    """

    if subpath.startswith("/"):
      subpath = subpath[1:]

    if subpath and subpath.endswith("/"):
      subpath = subpath[:-1]

    ret = []
    path = os.path.join(path, "data")
    iso = isoparser.parse(path)

    #try:
    directoryRecord = iso.root
    if subpath != "":
      directoryRecord = iso.record(*subpath.encode('utf-8').split(b"/"))
    for info in directoryRecord.children:
      ret.append(self._pullEntryInfo(info))
    #except:
    #  pass

    return {"items": ret}

  def retrieveFileStat(self, uuid, revision, path, subpath):
    """ Retrieves the file status information for the resource at the given revision.
    """

    if subpath.startswith("/"):
      subpath = subpath[1:]

    path = os.path.join(path, "data")
    iso = isoparser.parse(path)

    info = None
    try:
      info = iso.record(*subpath.encode('utf-8').split(b"/"))
      info = self._pullEntryInfo(info)
    except:
      raise FileNotFoundError()

    if info is None:
      raise FileNotFoundError()

    return info

  def retrieveFile(self, uuid, revision, path, subpath, start=0, length=None):
    """ Retrieves the file found within the resource at the given revision.
    """

    # If it is a directory, yield the tree
    stat = self.retrieveFileStat(uuid, revision, path, subpath)
    if stat['type'] == "tree":
      import io
      return io.BytesIO((json.dumps(stat) + "\n").encode('utf-8'))

    path = os.path.join(path, "data")
    iso = isoparser.parse(path)

    if subpath.startswith("/"):
      subpath = subpath[1:]

    info = None
    try:
      info = iso.record(*subpath.encode('utf-8').split(b"/"))
    except:
      raise FileNotFoundError()

    if info is None:
      raise FileNotFoundError()

    ret = info.get_stream()
    ret.seek(start)

    return ret

  def traversable(self, uid, revision, path):
    """ ISO files are traversable, usually.
    """

    return True
