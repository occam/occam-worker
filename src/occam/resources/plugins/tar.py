# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Feature Request:
# We should allow an encoding to be specified as resource metadata. Some
# tar files might be old enough that they are in weird encodings. Generally
# utf-8 systems are what we will be using, so it's not really a problem unless
# it is truly non-ascii that is expected.

import json
import os
import tarfile

from occam.resources.manager import resource

from occam.log    import loggable

from occam.resources.plugins.file import FileResource

@loggable
@resource("application/gzip")
@resource("application/x-bzip2")
@resource("application/x-tar")
@resource("application/x-lzip")
@resource("application/x-lzip-compressed-tar")
@resource("application/x-lzma")
@resource("application/x-xz")
class TarResource(FileResource):
  """ This is an OCCAM resource engine for handling compressed tar files.
  """

  def actions(self, uuid, revision, path, resourceInfo, destination):
    """ Performs any actions that may be required upon installation.

    In this case, the unpack action.
    """

    path = os.path.join(path, "data")

    if os.path.exists(path):
      actions = resourceInfo.get('actions', {})
      if not isinstance(actions, dict):
        actions = {}

      if resourceInfo and "unpack" in actions:
        TarResource.Log.write("unpacking file")
        unpackPath = actions['unpack']
        unpackPath = os.path.join(destination, unpackPath)

        try:
          tar = tarfile.open(path, "r")
          tar.extractall(unpackPath)
        except:
          # TODO: add, potentially, an occam object call to do this
          os.system('tar xvf "%s" -C "%s" > /dev/null' % (path, unpackPath))

      if resourceInfo and "postUnpack" in actions:
        if actions["postUnpack"] == "delete" and 'to' in resourceInfo:
          # Delete the tar file from its destination
          TarResource.Log.write("deleting tar file")
          os.unlink(os.path.join(unpackPath, resourceInfo.get('to')))

      return True

    return False

  def _pullEntryInfo(self, tarinfo, subpath=""):
    """ Internal method to form the item entry for a given tarinfo.
    """

    type = "file"
    if tarinfo.issym():
      type = "link"
    if tarinfo.isdir():
      type = "tree"

    item = {"name":  tarinfo.name[len(subpath):],
            "size":  tarinfo.size,
            "type":  type,
            "uname": tarinfo.uname,
            "gname": tarinfo.gname,
            "uid":   tarinfo.uid,
            "gid":   tarinfo.gid,
            "mode":  tarinfo.mode,
            "mtime": tarinfo.mtime}

    if type == "link":
      item['target'] = tarinfo.linkname

    return item

  def _generateCache(self, uuid, revision, path):
    """ Generates a directory cache for the tar file, if it doesn't exist.
    """

    cachepath = os.path.join(path, "cache")

    if not os.path.exists(cachepath):
      os.mkdir(cachepath)
    else:
      return

    # Go through each file and create the metadata file for it
    root = {'items': [], 'dirs': {}}

    tarpath = os.path.join(path, "data")
    tar = tarfile.open(tarpath, "r")

    for info in tar:
      name = info.name

      # Split into directories
      subpaths = name.split("/")
      filename = subpaths[-1]
      subpaths = subpaths[:-1]

      current = root
      for subpath in subpaths:
        current['dirs'][subpath] = current['dirs'].get(subpath, {'dirs': {}, 'items': []})
        current = current['dirs'][subpath]

      basepath = "/".join(subpaths) + "/"
      if basepath == "/":
        basepath = ""
      current['items'].append(self._pullEntryInfo(info, basepath))

    def generate(data, subpath):
      if not os.path.exists(subpath):
        os.mkdir(subpath)

      cachefile = os.path.join(subpath, "dir.json")
      with open(cachefile, "w+") as f:
        json.dump({"items": data["items"]}, f)

      for k, v in data["dirs"].items():
        # TODO: ensure subpath can't break out of its root path
        generate(v, os.path.join(subpath, k))

    generate(root, cachepath)

  def retrieveDirectory(self, uuid, revision, path, subpath):
    """ Retrieves the directory listing of the given revision.
    """

    if subpath.startswith("/"):
      subpath = subpath[1:]

    if subpath and not subpath.endswith("/"):
      subpath = subpath + "/"

    # Generate directory structure for the entire file, if needed
    self._generateCache(uuid, revision, path)

    # Look for the cache file, if it is there, use it
    cachefile = os.path.join(path, "cache", subpath, "dir.json")

    ret = []

    if os.path.exists(cachefile):
      with open(cachefile, "r") as f:
        try:
          ret = json.load(f)["items"]
        except:
          ret = []
    elif os.path.exists(os.path.join(path, "cache")):
      raise FileNotFoundError()

    if ret == []:
      path = os.path.join(path, "data")
      tar = tarfile.open(path, "r")

      for info in tar:
        if info.name.startswith(subpath) and not "/" in info.name[len(subpath):]:
          ret.append(self._pullEntryInfo(info, subpath))

    return {"items": ret}

  def retrieveFileStat(self, uuid, revision, path, subpath):
    """ Retrieves the file status information for the resource at the given revision.
    """

    if subpath.startswith("/"):
      subpath = subpath[1:]

    # Look for the cache file, if it is there, use it
    cachefile = os.path.join(path, "cache", os.path.dirname(subpath), "dir.json")
    if os.path.exists(cachefile):
      with open(cachefile, "r") as f:
        try:
          direntries = json.load(f)["items"]
          for direntry in direntries:
            if direntry['name'] == os.path.basename(subpath):
              return direntry
        except:
          pass

    path = os.path.join(path, "data")
    tar = tarfile.open(path, "r")

    info = None
    try:
      entry = tar.next()
      while entry is not None:
        entry = tar.next()
        if entry.name == subpath:
          info = entry
          break
    except:
      raise FileNotFoundError()

    if info is None:
      raise FileNotFoundError()

    return self._pullEntryInfo(info)

  def retrieveFile(self, uuid, revision, path, subpath, start=0, length=None):
    """ Retrieves the file found within the resource at the given revision.
    """

    # If it is a directory, yield the tree
    stat = self.retrieveFileStat(uuid, revision, path, subpath)
    if stat['type'] == "tree":
      import io
      return io.BytesIO((json.dumps(stat) + "\n").encode('utf-8'))

    path = os.path.join(path, "data")
    tar = tarfile.open(path, "r")

    if subpath.startswith("/"):
      subpath = subpath[1:]

    info = None
    try:
      # Right now, we look for the first occurrence
      # Annoyingly tar can append without indicating this anywhere because of
      # its original purpose as being a tape-storage device.

      # We should add a flag to the tar resource metadata when this is an
      # actual issue, because of the shear slowdown for large tar files when
      # the entire member list must be scanned. (doesn't help late added files
      # regardless)
      entry = tar.next()
      while entry is not None:
        entry = tar.next()
        if entry.name == subpath:
          info = entry
          break
    except:
      raise FileNotFoundError()

    if info is None:
      raise FileNotFoundError()

    # We could save the header data and search that ourselves to get around
    # The duplicates issue

    ret = tar.extractfile(info)
    ret.read(start)
    return ret

  def traversable(self, uid, revision, path):
    """ Returns True since tar files are traversable.
    """

    return True
