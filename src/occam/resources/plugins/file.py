# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os

from occam.resources.manager import resource

from occam.network.manager   import NetworkManager

from occam.log    import loggable

from occam.manager import uses

@loggable
@uses(NetworkManager)
@resource('file')
class FileResource:
  """ This is an OCCAM resource engine for handling repeatable file retrieval.
  """

  def pathFor(self, uuid, path, revision=None):
    """ Returns the path to the stored file on disk (if it would exist) for the
    given file revision.
    """

    filePath = path

    if revision:
      filePath = os.path.join(filePath, revision)

    return filePath

  def actions(self, uuid, revision, path, resourceInfo, destination):
    """ Performs any actions that may be required upon installation.
    """

    return None

  def install(self, uuid, revision, path, resourceInfo, destination):
    """ Installs the file stored as the given uuid and revision to the given path.

    Arguments:
      uuid (str):          The UUID of the resource.
      revision (str):      The revision of the resource
      path (str):          The storage path where the resource will be pulled.
      resourceInfo (dict): The resource metadata.
      destination (str):   The destination path to install the resource.
    """

    filePath = path

    installPath = resourceInfo.get('to')

    if installPath is None:
      return False

    # ensure path exists
    dirname = os.path.dirname(os.path.realpath(destination))
    try:
      os.makedirs(dirname)
    except OSError as exc:  # Python >2.5
      import errno
      if exc.errno == errno.EEXIST and os.path.isdir(dirname):
        pass
      else:
        raise

    if not filePath is None:
      filePath = os.path.join(filePath, "data")
      # TODO: error if destination path already exists??
      try:
        FileResource.Log.noisy("copying file from %s to %s" % (filePath, destination))
        import shutil
        shutil.copyfile(filePath, destination)
      except:
        FileResource.Log.error("Failed to install file resource.")
        return False

      return True

    return False

  def pull(self, uuid, revision, name, source, to, path, cert=None, headers=None, existingPath=None):
    """ Retrieves and stores the given file resource according to the given resource
    info.
    
    Returns:
      A reference of the Object in the store.
    """

    import hashlib # For digesting the file

    name = name or "File Download"

    file_obj = None

    filehash = revision

    # Download file
    FileResource.Log.write("Downloading resource from %s" % (source))

    if existingPath:
      reader = open(existingPath, "rb")
      size = os.stat(existingPath).st_size
    else:
      reader, _, size = self.network.get(source, accept='*/*', cert=cert, doNotVerify=True, headers=headers)

    if reader is None:
      return None, []

    filePath = os.path.join(path, 'data')

    bytesTotal = size

    if not bytesTotal:
      bytesTotal = 1

    bytesSoFar = 0
    percentage = 0

    FileResource.Log.writePercentage("Downloading resource from source (size: %s)" % (bytesTotal))

    digest = hashlib.sha256()

    with open(filePath, 'wb+') as f:
      bytesRead = 1
      while bytesRead > 0:
        chunk = reader.read(8196)
        bytesRead = len(chunk)
        bytesSoFar += bytesRead
        digest.update(chunk)
        f.write(chunk)
        # TODO: division by zero
        FileResource.Log.updatePercentage((bytesSoFar / bytesTotal) * 100)
    f.close()

    # Create a URI by hashing the public key with a multihash
    from occam.storage.plugins.ipfs_vendor.multihash import multihash
    hashedBytes = bytearray([multihash.SHA2_256, len(digest.digest())])
    hashedBytes.extend(digest.digest())

    from occam.storage.plugins.ipfs_vendor.base58 import b58encode
    filehash = b58encode(bytes(hashedBytes))

    # The File revision is the hash of the file?
    # Create the object for the file. Store the file and hash.
    # Use the object revision to find the file??

    # Store the data to the proper place
    path = os.path.join(path, filehash)

    if not os.path.exists(path):
      os.mkdir(path)

    # Move the file
    import shutil
    shutil.move(filePath, os.path.join(path, "data"))

    return filehash, [], path

  def exists(self, uuid, path, revision=None):
    """ Returns True if a file of the given revision is stored in the object store.

    If revision is not specified, it just looks for the presence of any
    revisions.
    """
    filePath = self.pathFor(uuid, path, revision)

    # If the revision is None, then it doesn't exist
    if revision is None:
      return None

    return os.path.exists(filePath)

  def update(self, uuid, source):
    """
    """

    # File update can be handled by looking at cache info and content-length
    # We should pull a HEAD and check the headers
    # When we find a new version, we should pull it down and give back the
    # revision.

    return False

  def clonable(self):
    """ Files are not clonable.
    """

    return False

  def clone(self, uuid, revision, name, source, to):
    """ Files are not cloned. This just returns True.
    """

    return True

  def currentRevision(self, path):
    """ Returns the current revision of the object.
    
    This is used to detect changes.
    When this revision differs from the one an object current is using during
    a commit, then it may be updated in that object. For instance, in an 'occam
    update' command.
    """

  def commit(self, uuid, revision, name, source, path):
    """ Will commit the resource at the current path to the store as an updated
    revision if it has changed.
    
    It will return the updated resource info.
    """

    return {}, False

  def retrieveDirectory(self, uuid, revision, path, subpath):
    """ Retrieves the directory listing of the given revision.
    """

    return None

  def retrieveFileStat(self, uuid, revision, path, subpath):
    """ Retrieves the file status information for the resource at the given revision.
    """

    return None

  def retrieveFile(self, uuid, revision, path, subpath, start=0, length=None):
    """ Retrieves the file found within the resource at the given revision.
    """

    return None

  def stat(self, uuid, revision, path):
    """ Retrieves resource specific metadata for the resource at the given revision.
    """

    import stat

    path = os.path.join(path, "data")
    result = os.lstat(path)
    mode   = result.st_mode

    return {
      "name": "data",
      "size": result.st_size,
      "type": "file",
      "mode": mode
    }

  def retrieve(self, uuid, revision, path, start=0, length=None):
    """ Retrieve the resource.
    """

    filePath = os.path.join(path, "data")

    ret = open(filePath, "rb")

    ret.seek(start)
    return ret

  def retrieveHistory(self, uid, revision, path):
    return None

  def traversable(self, uid, revision, path):
    """ Returns True only if the file may be treated as a directory.

    Typical files return False here, but this can be overwritten to return True.
    """

    return False
