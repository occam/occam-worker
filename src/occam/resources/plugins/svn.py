# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os

from occam.object import Object
from occam.log    import loggable
from occam.git_repository import GitRepository as GitRepo

from occam.resources.manager import resource, ResourceManager

from occam.network.manager import NetworkManager

from occam.manager import uses

import svn.local
import svn.remote

@resource('svn')
@uses(NetworkManager)
@loggable
class SVNResource:
  """ This is an OCCAM resource engine for handling repeatable SVN retrieval
  and storage.
  """

  def copyPath(self, src, dst, symlinks=False, ignore=None):
    import shutil
    if not os.path.exists(dst):
      os.makedirs(dst)
    for item in os.listdir(src):
      s = os.path.join(src, item)
      d = os.path.join(dst, item)
      if os.path.isdir(s):
        self.copyPath(s, d, symlinks, ignore)
      else:
        if not os.path.exists(d) or os.stat(s).st_mtime - os.stat(d).st_mtime > 1:
          shutil.copy2(s, d)

  def hashPath(self, hashpath, path):
    """ Returns a hash representing the given content in the given path.
    """

    # Derived from:
    # https://gist.github.com/techtonik/5175896
    # (public domain, by anatoly techtonik <techtonik@gmail.com>)

    import os
    import os.path as osp
    import hashlib

    mainDigest = hashlib.sha256()

    def filehash(filepath):
      blocksize = 64*1024
      sha = hashlib.sha256()
      with open(filepath, 'rb') as fp:
        while True:
          data = fp.read(blocksize)
          if not data:
            break
          sha.update(data)
      return sha.hexdigest() 

    def sortedWalk(top, topdown=True, onerror=None):
      """ Modification of the standard 'os.walk' routine to recurse in sorted order.
      """

      from os.path import join, isdir, islink

      names = os.listdir(top)
      names.sort()
      dirs, nondirs = [], []

      for name in names:
        if isdir(os.path.join(top, name)):
          dirs.append(name)
        else:
          nondirs.append(name)

      if topdown:
        yield top, dirs, nondirs
      for name in dirs:
        path = join(top, name)
        if not os.path.islink(path):
          for x in sortedWalk(path, topdown, onerror):
            yield x
      if not topdown:
        yield top, dirs, nondirs

    with open(hashpath, 'w+') as hashfile:
      hashfile.write("""\
%%%% HASHDEEP-1.0
%%%% size,sha256,filename
##
## $ occam
##
""")

      for root, dirs, files in sortedWalk(path):
        for fpath in [osp.join(root, f) for f in files]:
          size = osp.getsize(fpath)
          sha = filehash(fpath)
          name = osp.relpath(fpath, path)
          hashdata = '%s,%s,%s\n' % (size, sha, name)
          hashfile.write(hashdata)
          mainDigest.update(hashdata.encode('utf-8'))

    hash = mainDigest.hexdigest()

    # Create a URI by hashing the public key with a multihash
    from occam.storage.plugins.ipfs_vendor.multihash import multihash
    hashedBytes = multihash.encode(hash, multihash.SHA2_256)

    from occam.storage.plugins.ipfs_vendor.base58 import b58encode
    hash = b58encode(bytes(hashedBytes))

    return hash

  def pathFor(self, id, path, revision=None):
    """ Returns the path to the stored SVN repository on disk (if it would exist)
    for the given id.
    """

    # The repository is stored in the base path
    return path

  def actions(self, uuid, revision, path, resourceInfo, destination):
    """ Performs any actions that may be required upon installation.
    """

    return None

  def install(self, uuid, revision, path, resourceInfo, destination):
    """ Installs the SVN repository via the given uuid and revision to the given path.
    """

    requestedHash = revision.split(':')[1]

    revisionPath = os.path.join(path, requestedHash)
    if not os.path.exists(revisionPath):
      return False

    svnPath = revisionPath
    SVNResource.Log.noisy("Cloning SVN repository at revision %s from %s to %s" % (revision, svnPath, destination))

    self.copyPath(svnPath, destination)

    return True

  def pull(self, uuid, revision, name, source, to, path, cert=None, headers=None, existingPath=None):
    """ Retrieves and stores the given SVN resource according to the given resource info.
    
    Returns a reference of the Object in the store. The certificate path
    necessary to retrieve the resource over HTTPS can be given.
    """

    try:
      requestedHash = revision.split(':')[1]

      revisionPath = os.path.join(path, requestedHash)
      if os.path.exists(revisionPath):
        return revision, [], path

    except:
      pass
    revision = revision.split(':')[0]

    if len(revision) > 0 and revision[0] == "r":
      revision = revision[1:]

    name = name or "SVN Repository"

    # Create local repository
    SVNResource.Log.write("Pulling resource %s @ %s from %s" % (uuid, revision or "latest", source))

    # Place it in:
    svnPath = path

    # TODO: remove this
    #if os.path.exists(svnPath):
    #  import shutil
    #  shutil.rmtree(svnPath)

    # Generate a temporary directory for this checkout
    # Thanks to https://stackoverflow.com/questions/2257441/random-string-generation-with-upper-case-letters-and-digits-in-python

    import string
    import random

    tmp = ''.join(random.choices(string.ascii_uppercase + string.digits, k=20))
    svnPath = os.path.join(svnPath, "_" + tmp)

    repository = svn.remote.RemoteClient(source)
    repository.checkout(svnPath, revision = revision)

    if not revision:
      # Determine the current revision
      revision = svn.local.LocalClient(svnPath).info()['commit_revision']

    # Remove the svn directory
    import shutil
    shutil.rmtree(os.path.join(svnPath, ".svn"))

    hashFile = os.path.join(path, tmp + ".txt")
    revisionHash = self.hashPath(hashFile, svnPath)

    # Determine the actual hashed revision and report that
    revision = revision + ":" + revisionHash

    # Update the directory
    os.rename(svnPath, os.path.join(os.path.dirname(svnPath), revisionHash))
    os.rename(hashFile, os.path.join(os.path.dirname(svnPath), revisionHash + ".txt"))

    return revision, [], path

  def exists(self, uuid, path, revision=None):
    """ Returns True if a SVN repository containing the given revision is stored
    in the object store. If revision is not specified, it just looks for the
    presence of the SVN repository.
    """

    requestedHash = revision.split(':')[1]

    revisionPath = os.path.join(path, requestedHash)

    return os.path.exists(revisionPath)

  def update(self, uuid, source):
    """ Updates the SVN repository object stored in the store.
    """
    pass

  def currentRevision(self, path):
    """ Returns the current revision of the object.
    
    This is used to detect changes.
    When this revision differs from the one an object current is using during
    a commit, then it may be updated in that object. For instance, in an 'occam
    update' command.
    """

    repo = svn.local.LocalClient(path)
    if repo is None:
      return None

    return repo.info()['commit_revision']

  def retrieveDirectory(self, uuid, revision, path, subpath):
    """ Retrieves the directory listing of the given revision.
    """

    return GitRepo(path).retrieveDirectory(subpath)

  def retrieveFileStat(self, uuid, revision, path, subpath):
    """ Retrieves the file status information for the given path in the given revision.
    """

    return GitRepo(path).retrieveFileStat(subpath)

  def retrieveFile(self, uuid, revision, path, subpath):
    """ Retrieves the file data found within the resource at the given revision.
    """

    return GitRepo(path).retrieveFile(subpath)

  def stat(self, uuid, revision, path):
    """ Retrieves the file status of the resource.
    """

    # Git repositorites are directories:

    return {
      "name": "repository",
      "type": "tree"
    }

  def retrieve(self, uuid, revision, path):
    """ Retrieves the resource data.
    """

    # Mercurial repositories are directory based, they are not retrievable.
    return None

  def retrieveHistory(self, uid, revision, path):
    return None

  def traversable(self, uid, revision, path):
    """ Returns True since repositories are always treated as a directory.
    """

    return True
