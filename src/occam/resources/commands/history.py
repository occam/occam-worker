# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.commands.manager import command, option, argument

from occam.object import Object
from occam.log    import Log

from occam.manager import uses

from occam.resources.manager import ResourceManager
from occam.discover.manager  import DiscoverManager

@command('resources', 'history',
  category      = 'Resource Inspection',
  documentation = "Displays the revision history for the object.")
@argument("object", type = "object", help = "The resource object ID to view the history of.")
@option("-j", "--json", dest   = "to_json",
                        action = "store_true",
                        help   = "returns result as a json document")
@uses(ResourceManager)
@uses(DiscoverManager)
class ResourcesHistoryCommand:
  def do(self):
    # Get the object to list
    obj = None
    data = None

    # Query for resource object by id
    resource = None
    if self.options.object and self.options.object.id:
      # Check id
      resource = self.resources.infoFor(id=self.options.object.id)

      # Check uid
      if not resource:
        resource = self.resources.infoFor(uid=self.options.object.id)

    if resource is None:
      if self.options.object:
        # TODO: handle resource federation (dir listing)
        data = self.discover.retrieveHistory(self.options.object, person = self.person)

      if data is None:
        Log.error("cannot find resource with id %s" % (self.options.object.id))
        return -1

    if data is None:
      try:
        data = self.resources.retrieveHistory(id           = resource["id"],
                                              uid          = resource["uid"],
                                              revision     = resource["revision"],
                                              resourceType = resource["subtype"])
      except IOError:
        data = []

    import json
    Log.output(json.dumps(data))

    return 0
