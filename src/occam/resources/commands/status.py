# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.log import Log

from occam.commands.manager import command, option, argument

from occam.resources.manager   import ResourceManager
from occam.discover.manager    import DiscoverManager

from occam.manager import uses

@command('resources', 'status',
  category      = 'Resource Inspection',
  documentation = "Returns information about the resource data or a file within the resource.")
@argument("object", type = "object")
@option("-j", "--json", dest   = "to_json",
                        action = "store_true",
                        help   = "returns result as a json document")
@uses(ResourceManager)   # For pulling resource objects
@uses(DiscoverManager)
class ResourcesStatusCommand:
  """ This class handles retrieving knowledge about a resource or a file within it.
  """

  def do(self):
    # Get the object to list
    resource = None
    ret = None

    # Check id
    if self.options.object and self.options.object.id:
      resource = self.resources.infoFor(id=self.options.object.id)

      # Check uid
      if not resource:
        resource = self.resources.infoFor(uid=self.options.object.id)

      # Last ditch effort: ask the federation
      if resource is None:
        # TODO: Get the "RESOURCE" status
        ret = self.discover.status(self.options.object, person = self.person)

    if resource is None and ret is None:
      Log.error("cannot find resource with id %s" % (self.options.object.id))
      return -1

    if ret is None and self.options.object and self.options.object.path:
      # Return file stat
      ret = self.resources.retrieveFileStatFrom(resource["id"],
                                                resource["uid"],
                                                revision     = self.options.object.revision,
                                                resourceType = resource["subtype"],
                                                subpath      = self.options.object.path)
    elif ret is None:
      # Return resource stat
      ret = self.resources.retrieveFileStat(resource["id"],
                                            resource["uid"],
                                            revision     = self.options.object.revision,
                                            resourceType = resource["subtype"])

    if self.options.to_json:
      import json
      Log.output(json.dumps(ret))
    else:
      Log.output(str(ret))

    return 0
