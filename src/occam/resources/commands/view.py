# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# occam view dd44fcce-5274-11e5-b1d4-dc85debcef4e (defaults to object.json)
# occam view dd44fcce-5274-11e5-b1d4-dc85debcef4e/object.json (same as default)
# occam view dd44fcce-5274-11e5-b1d4-dc85debcef4e/configs/0/input.json
# occam view dd44fcce-5274-11e5-b1d4-dc85debcef4e@abc124/configs/0/input.json

import json
import os

from occam.commands.manager import command, option, argument

from occam.object import Object
from occam.log    import Log

from occam.key_parser import KeyParser

from occam.manager import uses

from occam.resources.manager import ResourceManager
from occam.discover.manager  import DiscoverManager

@command('resources', 'view',
  category      = 'Resource Inspection',
  documentation = "Retrieves data representing or found within the resource object.")
@argument("objects", type = "object", nargs = '+', help = "A list of object identifiers of the form <id>@<revision>/<path>. Without a path, the resource data itself is returned.")
@option("-s", "--start",  action  = "store",
                          dest    = "start",
                          type    = int,
                          default = 0,
                          help    = "the byte position to start reading from")
@option("-l", "--length", action  = "store",
                          dest    = "length",
                          type    = int,
                          help    = "the number of bytes to read")
@option("-c", "--compress", action = "store",
                            dest   = "compress",
                            help   = "compress the directory or file with the given type ('tgz', 'zip')")
@uses(ResourceManager)
@uses(DiscoverManager)
class ResourcesViewCommand:
  def printResource(self, resource, revision, path):
    if self.options.compress == "zip":
      import zipstream
      z = zipstream.ZipFile()

      # This iterator can chunk the file contents
      # But we lose the file stat because the zipstream cannot
      # handle adding that information
      class ChunkIterator:
        def __init__(self, f):
          self.__f = f

        def __iter__(self):
          return self

        def __next__(self):
          if self.__f is None:
            raise StopIteration

          if isinstance(self.__f, bytes):
            ret = self.__f
            self.__f = None
          else:
            ret = self.__f.read(1024)

          if len(ret) == 0:
            raise StopIteration

          return ret

      class OccamFileIterator:
        def __init__(self, resources, obj, curPath, person):
          self.__obj = obj
          self.__curPath = curPath
          self.__person = person

          self.resources = resources

        def __iter__(self):
          return ChunkIterator(
            self.resources.retrieveFileFrom(
              resource["id"],
              resource["uid"],
              revision     = revision,
              resourceType = resource["subtype"],
              subpath      = self.__curPath))

      stat = self.resources.retrieveFileStatFrom(
        resource["id"],
        resource["uid"],
        revision     = revision,
        resourceType = resource["subtype"],
        subpath      = path)

      if stat and stat["type"] == "tree":
        # Go through the directory and pull each file
        def packPath(z, path):
          # Get directory listing
          listing = self.resources.retrieveDirectoryFrom(
            resource["id"],
            resource["uid"],
            revision     = revision,
            resourceType = resource["subtype"],
            subpath      = path)

          for item in listing.get('items'):
            curPath = os.path.join(path, item.get('name'))
            if item.get('type') == 'tree':
              packPath(z, curPath)
            else:
              z.write_iter(curPath, OccamFileIterator(self.resources, obj, curPath, person = self.person))

        packPath(z, path)
      else:
        z.write_iter(path, ChunkIterator(data))

      for zipdata in z:
        Log.pipe(zipdata)
    else:
      if path == "":
        # The default behavior is to pull the resource data
        data = self.resources.retrieveFile(resource["id"],
                                           resource["uid"],
                                           revision     = revision,
                                           resourceType = resource["subtype"],
                                           start        = self.options.start,
                                           length       = self.options.length)
      else:
        # Gather data from the given path, if possible
        data = self.resources.retrieveFileFrom(resource["id"],
                                               resource["uid"],
                                               revision     = revision,
                                               resourceType = resource["subtype"],
                                               subpath      = path,
                                               start        = self.options.start,
                                               length       = self.options.length)

      if data is None:
        raise Exception("Could not locate data.")
        return -1

      Log.pipe(data, length=self.options.length)

  def do(self):
    obj = None
    path = ""

    for object in self.options.objects:
      # Query for object by id
      if object is None:
        Log.error("the object's given id is invalid")
        return -1

      resource = None
      if object and object.id:
        # Check id
        resource = self.resources.infoFor(id=object.id)

        # Check uid
        if not resource:
          resource = self.resources.infoFor(uid=object.id)

      if resource is None:
        # Discover
        # TODO: handle resource federation (retrieving data)
        #resource = self.discover.retrieveFile(object, person = self.person)
        #Log.error("cannot find object with id %s" % (object.id))
        #return -1
        pass

      if object.path:
        path = object.path

      self.printResource(resource, object.revision, path)
      return 0
