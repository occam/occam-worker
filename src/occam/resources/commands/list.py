# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# occam list dd44fcce-5274-11e5-b1d4-dc85debcef4e  # defaults to listing "/"
# occam list dd44fcce-5274-11e5-b1d4-dc85debcef4e/ # same as above
#
# occam list dd44fcce-5274-11e5-b1d4-dc85debcef4e/configs/0
#
# occam list dd44fcce-5274-11e5-b1d4-dc85debcef4e@abc123/configs/0

from occam.commands.manager import command, option, argument

from occam.manager import uses

from occam.resources.manager import ResourceManager
from occam.discover.manager  import DiscoverManager

from occam.object import Object
from occam.log import Log

import os

@command('resources', 'list',
  category      = 'Resource Inspection',
  documentation = "Displays a file listing within a path within the given resource object.")
@argument("object", type = "object")
@option("-j", "--json", dest   = "to_json",
                        action = "store_true",
                        help   = "returns result as a json document")
@option("-l", "--long", dest   = "list_details",
                        action = "store_true",
                        help   = "lists the metadata for each file in a table listing")
@uses(ResourceManager)
@uses(DiscoverManager)
class ResourcesListCommand:
  """ The list command will view a directory within a given resource object.
  """

  def do(self):
    # Get the object to list
    path = "/"
    data = None

    if self.options.object and not self.options.object.path is None:
      path = self.options.object.path

    path = os.path.normpath(path)

    resource = None
    if self.options.object and self.options.object.id:
      # Check id
      resource = self.resources.infoFor(id=self.options.object.id)

      # Check uid
      if not resource:
        resource = self.resources.infoFor(uid=self.options.object.id)

    if resource is None:
      if self.options.object:
        # TODO: handle resource federation (dir listing)
        data = self.discover.retrieveDirectory(self.options.object, person = self.person)

      if data is None:
        Log.error("cannot find resource with id %s" % (self.options.object.id))
        return -1

    # 'ls' the directory structure
    if data is None:
      data = {'items': []}

      # Lists resources as well
      # This will show the file listing as it would be seen when the resources are
      # installed into the object.
      try:
        data = self.resources.retrieveDirectoryFrom(resource["id"], resource["uid"],
                                                    revision = self.options.object.revision,
                                                    resourceType = resource["subtype"],
                                                    subpath = path)
      except IOError:
        Log.error("Path not found.")
        raise Exception("Path not found.")
        return -1

    if self.options.to_json:
      import json

      if self.options.list_details:
        Log.output(json.dumps(data['items']))
      else:
        Log.output(json.dumps([item['name'] for item in data['items']]))
      return 0

    for i, item in enumerate(data['items']):
      if i > 0:
        Log.output("  ", end="", padding="")
      Log.output(item['name'], end="", padding="")

    Log.output("\n", end="", padding="")
    return 0
