# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os

from occam.log import loggable

from occam.manager import manager, uses

from occam.objects.write_manager  import ObjectWriteManager
from occam.versions.write_manager import VersionWriteManager
from occam.manifests.manager      import ManifestManager
from occam.jobs.manager           import JobManager
from occam.notes.manager          import NoteManager
from occam.keys.write_manager     import KeyWriteManager, KeySignatureExistsError

@loggable
@manager("ingest")
@uses(ObjectWriteManager)
@uses(VersionWriteManager)
@uses(ManifestManager)
@uses(JobManager)
@uses(NoteManager)
@uses(KeyWriteManager)
class IngestManager:
  """
  """

  handlers = {}

  def __init__(self):
    import importlib

    # Look at configuration at import the plugins specified
    pluginList = self.configuration.get('plugins', [])
    for pluginName in pluginList:
      importlib.import_module(pluginName)

  @staticmethod
  def register(name, handlerClass):
    """ Registers the given class as an importable package with the given name.
    """

    IngestManager.handlers[name] = handlerClass

  def handlerFor(self, packageType):
    """ Returns an instance of a handler for the given package type.
    """

    if not packageType in self.handlers:
      return None

    subConfig = self.configurationFor(packageType)

    # Create a driver instance
    instance = IngestManager.handlers[packageType](subConfig)

    return instance

  def configurationFor(self, packageType):
    """ Returns the configuration for the given package type.
    Returns the configuration for the given storage that is found within the
    occam configuration (config.yml) under the "stores" section under the
    given storage backend name.
    """

    config = self.configuration
    subConfig = config.get(packageType, {})

    return subConfig

  def pull(self, packageType, packageName, person, **kwargs):
    """ Invokes the given plugin to ingest the given package.
    """

    return self._pull(packageType, packageName, person = person, limit = 10, **kwargs)

  def _pull(self, packageType, packageName, person, limit = 10, **kwargs):
    limit = limit - 1
    if limit == 0:
      return None

    values = self.handlerFor(packageType).query(person.identity, packageName, **kwargs)

    if values is None:
      return None

    if isinstance(values, list) or isinstance(values, tuple):
      objectInfo, options = values
    else:
      objectInfo, options = values, {}

    # Check to see if we already have the object
    # If so, don't continue
    tag = options.get("tag")
    obj = self.objects.retrieve(id = self.objects.idFor(obj=None, identity = person.identity, objectInfo = objectInfo), version = tag, person = person)

    if obj is not None:
      print("already know about this object")
      return None

    # Create the object
    IngestManager.Log.write("Creating %s %s" % (objectInfo.get('type'), objectInfo.get('name')))
    obj = self.objects.write.create(objectInfo.get('name', packageName), objectInfo.get('type', packageType), info = objectInfo)

    # Clone the object to a particular place to complete the ingestion
    localObject = self.objects.temporaryClone(obj, person = person)[0]

    # Gather resources
    self.objects.write.pullResources(localObject, person.identity)

    if 'files' in options:
      files = options['files']
      for path in files:
        with open(path, 'rb') as f:
          self.objects.write.addFileTo(localObject, os.path.basename(path), f.read())

    self.objects.write.commit(localObject, person.identity, message="Adds file content")
    self.objects.write.store(localObject, person.identity)

    objectInfo = self.objects.infoFor(localObject)

    report = None
    if 'run' in options:
      report = "run"
    if 'build' in options:
      report = "build"

    innerLimit = 10
    while report and innerLimit > 0:
      # Run the object and pass along the stdout
      if report == "run":
        IngestManager.Log.header("Running for {}".format(packageName))
        task = self.manifests.run(localObject, person = person)
      else:
        IngestManager.Log.header("Building for {}".format(packageName))
        task = self.manifests.build(localObject, person = person)

      import io, json
      output = io.BytesIO(b"")
      taskInfo = self.objects.infoFor(task)
      originalTaskId = task.id
      opts = self.jobs.deploy(taskInfo, task.revision, person = person, stdout = output)
      runReport = self.jobs.execute(*opts)

      if report == "build":
        elapsed = runReport['time']
        buildPath = runReport.get('paths').get(taskInfo.get('builds').get('index')).get('buildPath')

        self.builds.write.store(person.identity, localObject, task, buildPath, elapsed)

      # TODO: look at the stdout in the object's output
      dataLength = output.tell()
      output.seek(0)
      data = output.read(dataLength).decode('utf-8')

      values = self.handlerFor(packageType).report(person.identity, report, objectInfo, packageName, data, **kwargs)

      if values is None:
        break

      if isinstance(values, list) or isinstance(values, tuple):
        objectInfo, options = values
      else:
        objectInfo, options = values, {}

      obj = self.objects.write.create(objectInfo.get('name', packageName), objectInfo.get('type', packageType), info = objectInfo)

      # Clone the object to a particular place to complete the ingestion
      localObject = self.objects.temporaryClone(obj, person = person)[0]

      # Gather resources
      self.objects.write.pullResources(localObject, identity = person.identity)

      files = []
      if 'files' in options:
        files = options['files']
        for path in files:
          with open(path, 'rb') as f:
            self.objects.write.addFileTo(localObject, os.path.basename(path), f.read())

      report = None
      if 'run' in options:
        report = "run"
      if 'build' in options:
        report = "build"

      if 'ingest' in options:
        IngestManager.Log.write("Ingesting: {}".format([x.get('packageName') for x in options['ingest']]))
        for subObject in options['ingest']:
          IngestManager.Log.write("Recursively ingesting {}".format(subObject.get('packageName')))
          self.pull(person = person, **subObject)

      innerLimit = innerLimit - 1

      self.objects.write.commit(localObject, identity = person.identity, message="Adds file content")
      self.objects.write.store(localObject, identity = person.identity)

      if 'tag' in options:
        tag = options.get("tag")

    if tag:
      # Tag the version in the note store
      try:
        signature, published = self.keys.write.signTag(localObject, person.identity, tag)
        IngestManager.Log.write("Signed version tag as %s" % (person.identity))
      except KeySignatureExistsError as e:
        pass

      self.versions.write.update(localObject,
                                 tag = tag,
                                 identity  = person.identity,
                                 published = published,
                                 signature = signature)

    return objectInfo

def package(name):
  """ This decorator will register the given class as a package type.
  """

  def register_package(cls):
    IngestManager.register(name, cls)
    cls = loggable("IngestManager")(cls)

    def init(self, subConfig):
      self.configuration = subConfig

    cls.__init__ = init

    return cls

  return register_package
