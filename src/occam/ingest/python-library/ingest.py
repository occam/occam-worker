import os, sys, json
import importlib

# First, we check for setup/install-time dependencies
# We do this by parsing the setup.py script
# If there is something interesting, we have to stall the parsing of the normal
#   dependencies...

f = open("_output", "w+")

ignoreExit = False

try:
  if not sys.argv[1].endswith(".whl") and (len(sys.argv) < 3 or sys.argv[2] != "--ignore-setup-dependencies"):
    import setuptools
    def setup_hook(*args, **kwargs):
      global ignoreExit
      if "setup_requires" in kwargs:
        f.write(json.dumps(kwargs))
        ignoreExit = True
        exit(0)

    # Update setup() to call our function instead
    setuptools.setup = setup_hook

    # Import (and thus execute) setup.py
    sys.path.append(os.path.realpath(os.path.join(os.curdir, sys.argv[1])))
    os.chdir(sys.argv[1])
    try:
      importlib.import_module("setup")
    except SystemExit:
      if ignoreExit:
        exit(0)
    os.chdir(os.path.realpath(os.path.join(os.curdir, "..")))

  # Pip bases
  from pip.download import PipSession
  from pip.req      import InstallRequirement, RequirementSet
  from pip.index    import PackageFinder

  from pip._vendor import pkg_resources

  # Wheel support
  from pip.wheel import WheelCache, WheelBuilder

  # Gather paths
  build_dir = os.path.join(os.curdir, "_build")
  download_dir = os.path.join(os.curdir)
  src_dir = sys.argv[1]

  # Generate a Pip Session
  session = PipSession()

  # Initialize the requirement set
  requirement_set = RequirementSet(
    build_dir           = build_dir,
    src_dir             = src_dir,
    download_dir        = download_dir,
    ignore_installed    = True,
    ignore_dependencies = True,
    force_reinstall     = True,
    session             = session,
    pycompile           = False,
    isolated            = True,
    wheel_cache         = None,
    require_hashes      = False,
  )

  # Gather the package finder
  finder = PackageFinder(find_links = [], format_control = False, index_urls = [], session = session)

  # Add the requested package
  requirement_set.add_requirement(InstallRequirement.from_line(sys.argv[1], None))

  # Gather dependency list
  requirement_set.prepare_files(finder)

  # Get the requested package itself (it is the first in this list)
  req_to_install = requirement_set.requirements.values()[0]

  # Pull out the requirements list
  try:
      # Is non-wheel package
      dist = req_to_install.get_dist()
  except:
      # Is a wheel
      dist = list(pkg_resources.find_distributions(req_to_install.source_dir))[0]

  parsed = [str(x) for x in dist.requires([])]

  # Output the list
  f.write(json.dumps(parsed))
except SystemExit as e:
  pass

f.close
