# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.commands.manager import command, option, argument

from occam.manager import uses

from occam.objects.manager    import ObjectManager
from occam.resources.manager  import ResourceManager
from occam.ingest.manager     import IngestManager

from occam.object import Object
from occam.log import Log

import os

@command('ingest', 'python-library',
  category      = 'Importing Other Packages',
  documentation = "Imports a given python package as an Occam object.")
@argument("pythonLibraryName", type = str)
@argument("version", nargs="?")
@option("-p", "--python-version", type = str, dest = "python_version", help = "The python version to target.", default = "3")
@uses(ObjectManager)
@uses(ResourceManager)
@uses(IngestManager)
class PythonLibraryCommand:
  """ The python-library command will import a python library.
  """

  def do(self):
    if self.person is None:
      Log.error("Must be authenticated to store objects")
      return -1

    libraryName = self.options.pythonLibraryName

    Log.done("imported %s" % (libraryName))

    self.ingest.pull("python-library", libraryName, person = self.person, pythonVersion = self.options.python_version)

    return 0
