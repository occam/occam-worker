# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import ipaddress

from occam.object import Object
from occam.log    import loggable

from occam.manager import manager, uses

from occam.storage.manager       import StorageManager
from occam.nodes.manager         import NodeManager
from occam.keys.write_manager    import KeyWriteManager
from occam.network.manager       import NetworkManager
from occam.objects.write_manager import ObjectWriteManager

@loggable
@manager("discover")
@uses(StorageManager)
@uses(NodeManager)
@uses(NetworkManager)
@uses(KeyWriteManager)
@uses(ObjectWriteManager)
class DiscoverManager:
  """ This manages mechanisms for finding objects in the world at large. This
      gives the discovery capability to any module that needs it.

      Discovery is sometimes passed into routines that might require it. For
      instance, routines that might lookup subobjects that are linked might
      want a discovery agent to use to do that lookup. You can provide this
      manager as that agent.

      It is possible that down the line we will allow alternative agents or
      extensions that will enable different modes of discovery through
      services or various archive or publication venues that are outside of
      the federation.
  """

  handlers = {}
  handlerImpl = []
  instantiated = {}

  def __init__(self):
    import occam.discover.plugins.ipfs

  @staticmethod
  def register(networkName, handlerClass):
    """ Adds a new discover backend type.

    Arguments:
      handlerClass (object): The handler that implements the discovery interface.
    """

    DiscoverManager.handlers[networkName] = {
      'class': handlerClass
    }

  def handlerFor(self, networkName):
    """ Returns an instance of a handler for the given name.
    """

    if not networkName in DiscoverManager.handlers:
      DiscoverManager.Log.error("discovery network backend %s not known" % (networkName))
      return None

    # Instantiate a storage backend if we haven't seen it before
    if not networkName in DiscoverManager.instantiated:
      # Pull the configuration (from the 'stores' subpath and keyed by the name)
      subConfig = self.configurationFor(networkName)

      # Create a driver instance
      instance = DiscoverManager.handlers[networkName]['class'](subConfig)

      # If there is a problem detecting the backend, cancel
      # This will set the value in the instantiations to None
      # TODO: cache this in the stores configuration? or detection file?
      if hasattr(instance, 'detect') and callable(instance.detect) and not instance.detect():
        instance = None

      # Set the instance
      DiscoverManager.instantiated[networkName] = instance

    if DiscoverManager.instantiated[networkName] is None:
      # The driver could not be initialized
      return None

    return DiscoverManager.instantiated[networkName]

  def defaultBackend(self):
    """ Returns the default discovery backend.
    """

    return self.configuration.get("default", "ipfs")

  def configurationFor(self, networkName):
    """ Returns the configuration for the given discovery network.
    
    This configuration is found within the occam configuration (config.yml)
    under the "discover" section under the given plugin name.
    """

    config = self.configuration
    subConfig = config.get(networkName, {})

    return subConfig

  def daemon(self):
    """ Starts a discovery daemon.
    """

    # Start an IPFS daemon

  def announce(self, id, token, backend=None):
    """ Announces a particular object id on the federation.
    """

    backend = backend or self.defaultBackend()

    handler = self.handlerFor(backend)
    if not handler:
      return

    handler.announce(id, token)

  def announceIdentity(self, id, publicKey, backend=None):
    """ Announces the public key with the identifier.
    """

    backend = backend or self.defaultBackend()

    handler = self.handlerFor(backend)
    if not handler:
      return

    handler.announceIdentity(id, publicKey)

  def announceViewer(self, type, subtype, backend=None):
    """ Announces that there is a viewer for the given type and subtype.
    """

    backend = backend or self.defaultBackend()

    handler = self.handlerFor(backend)
    if not handler:
      return

    id    = self.viewerIdFor(type, subtype, backend=backend)
    token = self.viewerTokenFor(type, subtype, backend=backend)

    handler.announce(id, token)

  def announceEditor(self, type, subtype, backend=None):
    """ Announces that there is a editor for the given type and subtype.
    """

    backend = backend or self.defaultBackend()

    handler = self.handlerFor(backend)
    if not handler:
      return

    id    = self.editorIdFor(type, subtype, backend=backend)
    token = self.editorTokenFor(type, subtype, backend=backend)
    handler.announce(id, token)

  def retrieveToken(self, id, backend=None):
    """
    """

    backend = backend or self.defaultBackend()

    handler = self.handlerFor(backend)
    if not handler:
      return None

    ret = handler.retrieveToken(id)

    return ret

  def nodesWithId(self, id, backend=None, limit=10):
    """
    """

    backend = backend or self.defaultBackend()

    handler = self.handlerFor(backend)
    if not handler:
      return []

    nodes = handler.search(id)

    ret = []

    for address in nodes:
      if ipaddress.ip_address(address).is_private:
        continue
      hostURL = "https://%s:9292" % (address)

      node = self.nodes.search(hostURL)
      if node is None:
        node = self.nodes.discover(hostURL, untrusted=True, quiet=True)

      ret.append(node)

      if len(ret) == limit:
        break

    return ret

  def retrieveJSON(self, option, person = None, backend=None):
    """
    """

    data = self.retrieveFile(option, person, backend)
    if data:
      import json
      import codecs
      try:
        reader = codecs.getreader('utf-8')
        data = json.load(reader(data))
      except:
        pass

    return data

  def retrieveFile(self, option, person = None, backend=None):
    """
    """

    nodes = self.nodesWithId(option.id, backend=backend)

    if not nodes:
      return None

    return self.nodes.retrieveFileFrom(nodes[0], option, person=person)

  def retrieveDirectory(self, option, person = None, backend=None):
    """
    """

    nodes = self.nodesWithId(option.id, backend=backend)

    if not nodes:
      return None

    return self.nodes.retrieveDirectoryFrom(nodes[0], option, person=person)

  def editorIdFor(self, type, subtype, backend):
    """
    """

    if type is None:
      type = ""
    if subtype is None:
      subtype = ""

    backend = backend or self.defaultBackend()

    handler = self.handlerFor(backend)
    if not handler:
      return None

    return handler.editorIdFor(type, subtype)

  def editorTokenFor(self, type, subtype, backend):
    """
    """

    if type is None:
      type = ""
    if subtype is None:
      subtype = ""

    backend = backend or self.defaultBackend()

    handler = self.handlerFor(backend)
    if not handler:
      return None

    return handler.editorTokenFor(type, subtype)

  def editorsFor(self, type, subtype = None, person=None, backend=None):
    """ Discovers editors for the given type and optional subtype.
    """

    id = self.editorIdFor(type, subtype, backend=backend)

    nodes = self.nodesWithId(id, backend=backend)

    if not nodes:
      return []

    data = self.nodes.editorsFor(nodes[0], type, subtype, person=person)

    ret = []

    from occam.objects.records.object import ObjectRecord
    for editor in data:
      # Create a editor/object record with the data
      editor['identity_uri'] = editor.get('identity')
      ret.append(ObjectRecord(editor))

    return ret

  def viewerIdFor(self, type, subtype, backend):
    """
    """

    if type is None:
      type = ""
    if subtype is None:
      subtype = ""

    backend = backend or self.defaultBackend()

    handler = self.handlerFor(backend)
    if not handler:
      return None

    return handler.viewerIdFor(type, subtype)

  def viewerTokenFor(self, type, subtype, backend):
    """
    """

    if type is None:
      type = ""
    if subtype is None:
      subtype = ""

    backend = backend or self.defaultBackend()

    handler = self.handlerFor(backend)
    if not handler:
      return None

    return handler.viewerTokenFor(type, subtype)

  def viewersFor(self, type, subtype = None, person=None, backend=None):
    """ Discovers viewers for the given type and optional subtype.
    """

    id = self.viewerIdFor(type, subtype, backend=backend)

    nodes = self.nodesWithId(id, backend=backend)

    if not nodes:
      return []

    data = self.nodes.viewersFor(nodes[0], type, subtype, person=person)

    ret = []

    from occam.objects.records.object import ObjectRecord
    for viewer in data:
      # Create a viewer/object record with the data
      viewer['identity_uri'] = viewer.get('identity')
      ret.append(ObjectRecord(viewer))

    return ret

  def taskFor(self):
    """ Discovers any existing tasks for the given properties.
    """

  def providersFor(self, type, subtype = None, person=None, backend=None):
    """ Discovers providers for the given environment and architecture.
    """

    nodes = self.nodesWithId(option.id, backend=backend)

    if not nodes:
      return None

    return self.nodes.viewersFor(nodes[0], type, subtype, person=person)

  def status(self, option, person = None, backend=None):
    """
    """

    nodes = self.nodesWithId(option.id, backend=backend)

    if not nodes:
      return None

    return self.nodes.statusFrom(nodes[0], option, person=person)

  def history(self, option, person = None, backend=None):
    """
    """

    nodes = self.nodesWithId(option.id, backend=backend)

    if not nodes:
      return None

    return self.nodes.historyFrom(nodes[0], option, person=person)

  def resolve(self, option, person = None):
    """ Returns an Object based on a CLI object argument.

    If it is given a URL, it attempts to discover the object through the normal
    means. The scheme of the url will depict the storage layer to query.

    If it is not a URL, but an Occam uuid and revision, it will search for the
    object within the federation and discover it through the normal channels.
    """

    if option is None:
      return None

    if self.network.isURL(option.id):
      parts = self.network.parseURL(option.id)
      objInfo = self.storage.retrieve(parts.scheme, parts.netloc)

      ret = None
    else:
      # TODO: check the normal local repository and then react only if the
      #       object is not found.
      ret = self.objects.resolve(option, person = person)
      if ret is None:
        ret = self.discover(option.id, revision = option.revision)

    return ret

  def retrieveIdentity(self, uri, person=None, backend=None):
    """ Discovers the given identity.
    """

    # Retrieve public key (IPFS)
    publicKey = self.retrieveToken(uri, backend=backend)

    if not publicKey:
      print("cannot find identity")
      return None

    # Discover this identity
    identity = self.keys.write.discover(uri, publicKey)
    if not identity:
      # The KeyManager rejected this identity
      return None

    # Retrieve everything else
    nodes = self.nodesWithId(uri, backend=backend)

    if not nodes:
      return None

    info = self.nodes.identityFrom(nodes[0], uri, person=person)

    import base64, datetime

    # Look at known signing keys (and their signatures)
    # Verify each of them and commit them
    for verifyKey in info.get('verifyingKeys', []):
      # {
      #   'key': { 'data': '', 'encoding': 'base64' },
      #   'signature': { 'data': '', 'encoding': 'base64' }
      # }
      try:
        keyInfo = verifyKey.get('key')
        id = keyInfo.get('id')
        published = datetime.datetime.strptime(keyInfo.get('published'), "%Y-%m-%dT%H:%M:%S.%fZ")
        key = None

        if keyInfo.get('format') == "PEM" and keyInfo.get('encoding') == "base64":
          key = base64.b64decode(keyInfo.get('data')).decode('utf-8')

        signatureInfo = verifyKey.get('signature')
        signature = None

        if signatureInfo.get('format') == "PKCS1_v1_5" and signatureInfo.get('encoding') == "base64":
          signature = base64.b64decode(signatureInfo.get('data'))
      except Exception as e:
        # Invalid key if there is any strange encoding error
        continue

      verifyKey = self.keys.write.discoverKey(uri, id, key, signature, published, None, publicKey)

    return info

  def discover(self, uuid, revision=None):
    """
    Uses the various storage backends to hopefully find and retrieve the object
    information. Returns the Object that has been discovered.
    """

    DiscoverManager.Log.noisy("Attempting to discover object %s@%s" % (uuid, revision))

    hosts = self.storage.discover(uuid, revision=revision)

    if len(hosts) > 0:
      # Start asking the hosts for object invocations
      for host in hosts:
        hostURL = "https://%s:9292" % (host)

        # TODO: Optionally, we can also start discovering the nodes via:
        #   self.nodes.discover(hostURL)  # ... for host in hosts

        # TODO: We can maybe select from the list any nodes we already know
        #   or trust

        node = self.nodes.search(hostURL)
        if node is None:
          node = self.nodes.discover(hostURL, untrusted=True, quiet=True)

        if not node is None:
          # Create empty placeholder for Object
          path = self.objects.write.createPathFor(uuid)
          DiscoverManager.Log.write("creating object")

          # Add entry to database (if needed)
          objInfo = self.nodes.pullObjectInfoFrom(node, uuid, revision=revision)
          revision = objInfo.get('revision', revision)

          # Pull Invocation Metadata
          obj = Object(path=None, uuid=uuid, revision=revision, info=objInfo)
          self.nodes.pullInvocationDataFrom(node, obj, ['builds', 'output', 'generated', 'stores/ipfs', 'partialTasks', 'tasks', 'versions'])

          # Store object
          self.objects.write.store(obj)

          return Object(path = obj.path, uuid=uuid, revision=revision)

def network(name):
  """ This decorator will register the given class as a discovery backend.
  """

  def register_network(cls):
    DiscoverManager.register(name, cls)
    cls = loggable("DiscoverManager")(cls)

    def init(self, subConfig):
      self.configuration = subConfig

    cls.__init__ = init

    return cls

  return register_network
