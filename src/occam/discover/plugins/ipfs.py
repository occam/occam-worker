# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os

from occam.config import Config

from occam.manager import uses

from occam.discover.manager import network
from occam.network.manager  import NetworkManager

import subprocess

@network('ipfs')
@uses(NetworkManager)
class IPFS:
  """ Implements the logic to make use of IPFS distributed/federated storage.
  """

  daemon = None

  def detect(self):
    """ Returns True when IPFS is available.
    """
    
    try:
      p = self.invokeIPFS([])
      code = p.wait()
    except:
      code = -1

    return code == 0

  @staticmethod
  def path():
    """ Returns the path to the IPFS binary.
    """

    return (os.path.realpath("%s/../../../../../go-ipfs" % (os.path.realpath(__file__))))

  def ipfsMountPath(self):
    return os.path.join(self.storePath(), "ipfs-mount")

  def ipnsMountPath(self):
    return os.path.join(self.storePath(), "ipns-mount")

  def IPFSIsDaemonRunning(self):
    if IPFS.daemon:
      return True

    ipfsMountPath = self.ipfsMountPath()
    ipnsMountPath = self.ipnsMountPath()

    if os.path.exists(os.path.join(ipnsMountPath, "local")):
      return True

    return False

  def IPFSStartDaemon(self):
    try:
      if IPFS.daemon:
        return IPFS.daemon
    except:
      pass

    ipfsMountPath = self.ipfsMountPath()
    ipnsMountPath = self.ipnsMountPath()

    if self.IPFSIsDaemonRunning():
      return

    if not os.path.exists(ipfsMountPath):
      try:
        os.mkdir(ipfsMountPath)
      except:
        pass

    if not os.path.exists(ipnsMountPath):
      try:
        os.mkdir(ipnsMountPath)
      except:
        pass

    IPFS.Log.noisy("Starting IPFS Daemon (%s, %s)" % (ipfsMountPath, ipnsMountPath))
    IPFS.daemon = self.invokeIPFS(["daemon"]) #, "--mount", "--mount-ipfs", ipfsMountPath, "--mount-ipns", ipnsMountPath], ipfsPath=self.storePath())

    # Wait until the files are mounted
    #while not os.path.exists(os.path.join(ipnsMountPath, "local")):
    #  IPFS.daemon.poll()
    #  if not IPFS.daemon.returncode is None:
    #    break

    return IPFS.daemon

  def IPFSStopDaemon(self):
    if IPFS.daemon:
      import signal
      try:
        IPFS.daemon.send_signal(signal.SIGINT)
      except:
        pass
      IPFS.daemon = None

  def IPFSNodeHashToAddresses(self, nodeHash):
    """ Yields a set of IP addresses for the given node hash.
    """

    ipfsPath = self.storePath()

    p = self.invokeIPFS(["dht", "findpeer", nodeHash], ipfsPath=ipfsPath)
    addresses = p.stdout.read().decode('utf-8').strip().split("\n")
    p.wait()

    # Go through and pull out IP
    # They are in the form:
    # /ip4/<ip addr>/tcp/<port>
    ret = []
    for address in addresses:
      if address.startswith("/ip4/"):
        # Pull out IP address
        address = address[5:address.index('/', 5)]
        if address != "127.0.0.1" and address != "0.0.0.0":
          ret.append(address)

    return ret

  def storePath(self):
    """ Returns the path to the ipfs instance.
    """

    return self.configuration.get('path', os.path.join(Config.root(), "ipfs"))

  @staticmethod
  def popen(command, stdout=subprocess.DEVNULL, stdin=None, stderr=subprocess.DEVNULL, cwd=None, env=None):
    return subprocess.Popen(command, stdout=stdout, stdin=stdin, stderr=stderr, cwd=cwd, env=env)

  def invokeIPFS(self, args, stdout=subprocess.PIPE, stdin=None, stderr=subprocess.DEVNULL, ipfsPath=None, env=None):
    """ Internal method to invoke IPFS with the given arguments
    """

    if env is None:
      env = {}
    if not 'IPFS_PATH' in env and not ipfsPath is None:
      env['IPFS_PATH'] = ipfsPath

    env['PATH'] = os.getenv('PATH')

    command = ['%s/ipfs' % (IPFS.path())] + args
    return IPFS.popen(command, stdout=stdout, stdin=stdin, stderr=stderr, env=env)

  def announce(self, id, token):
    """ Announces a token.
    """

    IPFS.Log.write("Pushing token for object %s" % (id))

    # Write an advertisement token
    p = self.invokeIPFS(["block", "put"], stdin=subprocess.PIPE, ipfsPath=self.storePath())
    result = p.communicate(input=token)

    # Pin the hash maybe
    return id

  def announceIdentity(self, id, publicKey):
    """ Announces a token.
    """

    # This is technically the same as announcing anything in IPFS
    # Since the identity uris are IPFS hashes.

    IPFS.Log.write("Pushing public key for identity %s" % (id))

    # Write an advertisement
    p = self.invokeIPFS(["block", "put"], stdin=subprocess.PIPE, ipfsPath=self.storePath())
    p.communicate(input=publicKey.encode('utf-8'))

    # Pin the hash maybe
    return id

  def editorIdFor(self, type, subtype):
    """ Returns a discoverable key for the given editor type/subtype.
    """

    token = self.editorTokenFor(type, subtype)

    # Hash the token

    from occam.storage.plugins.ipfs_vendor.multihash import multihash
    hashedBytes = multihash.encode(token, multihash.SHA2_256)

    from occam.storage.plugins.ipfs_vendor.base58 import b58encode
    return b58encode(bytes(hashedBytes))

  def editorTokenFor(self, type, subtype):
    """ Returns a discoverable token for the given editor type/subtype.
    """

    return ("occam-editor\ntype=%s\nsubtype=%s\n" % (type, subtype)).encode('utf-8')

  def viewerIdFor(self, type, subtype):
    """ Returns a discoverable key for the given viewer type/subtype.
    """

    token = self.viewerTokenFor(type, subtype)

    # Hash the token

    from occam.storage.plugins.ipfs_vendor.multihash import multihash
    hashedBytes = multihash.encode(token, multihash.SHA2_256)

    from occam.storage.plugins.ipfs_vendor.base58 import b58encode
    return b58encode(bytes(hashedBytes))

  def viewerTokenFor(self, type, subtype):
    """ Returns a discoverable token for the given viewer type/subtype.
    """

    return ("occam-viewer\ntype=%s\nsubtype=%s\n" % (type, subtype)).encode('utf-8')

  def providerTokenFor(self, environment, architecture, targetEnvironment, targetArchitecture):
    """ Returns a discoverable token for the given provider env/arch.
    """

    token = "occam-viewer\nenvironment=%s\narchitecture=%s\ntargetEnvironment=%s\ntargetArchitecture=%s\n" % (
      environment,
      architecture,
      targetEnvironment,
      targetArchitecture
    )

    # Hash the token

    from occam.storage.plugins.ipfs_vendor.multihash import multihash
    hashedBytes = multihash.encode(token, multihash.SHA2_256)

    from occam.storage.plugins.ipfs_vendor.base58 import b58encode
    return b58encode(bytes(hashedBytes))

  def taskTokenFor(self, id, environment, architecture):
    """
    """

  def retrieveToken(self, id):
    """ Returns the value associated with the given id.
    """

    IPFS.Log.noisy("attempting to retrieve %s" % (id))

    daemon = self.IPFSStartDaemon()

    # TODO: add timeout to configuration
    p = self.invokeIPFS(["block", "get", id, "--timeout", "1s"], ipfsPath=self.storePath())
    ret = p.stdout.read()
    p.communicate()

    self.IPFSStopDaemon()

    return ret

  def search(self, id):
    """ Returns whether or not the given object exists on the network.
    """

    IPFS.Log.noisy("attempting to discover %s" % (id))

    daemon = self.IPFSStartDaemon()

    # TODO: add timeout to configuration
    p = self.invokeIPFS(["dht", "findprovs", id, "--timeout", "1s"], ipfsPath=self.storePath())
    hosts = p.stdout.read().decode('utf-8').strip().split("\n")
    p.wait()
    if hosts[0] == "":
      hosts = hosts[1:]

    ret = []
    for host in hosts:
      addresses = self.IPFSNodeHashToAddresses(host)
      if len(addresses) > 0:
        IPFS.Log.noisy("Found %s at %s" % (id, addresses))
      ret.extend(addresses)

    if len(hosts) == 0:
      IPFS.Log.noisy("%s not found" % (id))

    self.IPFSStopDaemon()

    return ret

  def status(self, id):
    """
    """

    # Look up nodes
    addresses = self.search(id)

    # TODO: sort by trust
    for address in addresses:
      # Query each node
      print(address)

    return {}
