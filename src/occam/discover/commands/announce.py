# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json
import os

from occam.commands.manager import command, option, argument

from occam.object import Object
from occam.log    import Log

from occam.manager import uses

from occam.objects.manager  import ObjectManager
from occam.discover.manager import DiscoverManager

@command('discover', 'announce',
  category      = 'Discovery',
  documentation = "Announces the given object.")
@uses(ObjectManager)
@uses(DiscoverManager)
@argument("object", type = "object", help = "the object to announce")
class DiscoverAnnounceCommand:
  def do(self):
    obj = self.objects.resolve(self.options.object, person = self.person, allowNone = True)

    if obj is None:
      Log.error("Object not found")
      return -1

    token = self.objects.idTokenFor(obj, obj.identity)
    self.discover.announce(obj.id, token)

    objectInfo = self.objects.infoFor(obj)

    # Announce each viewer
    objectInfo['views'] = objectInfo.get('views', [])

    if not isinstance(objectInfo['views'], list):
      objectInfo['views'] = [objectInfo['views']]

    for viewer in objectInfo.get('views', []):
      self.discover.announceViewer(viewer.get('type'), viewer.get('subtype'))

    for editor in objectInfo.get('edits', []):
      self.discover.announceEditor(editor.get('type'), editor.get('subtype'))

    return 0
