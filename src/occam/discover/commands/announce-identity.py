# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json
import os

from occam.commands.manager import command, option, argument

from occam.object import Object
from occam.log    import Log

from occam.manager import uses

from occam.objects.manager  import ObjectManager
from occam.keys.manager     import KeyManager
from occam.discover.manager import DiscoverManager

@command('discover', 'announce-identity',
  category      = 'Discovery',
  documentation = "Announces the given identity.")
@uses(ObjectManager)
@uses(KeyManager)
@uses(DiscoverManager)
@argument("object", type = "object", help = "the identity to announce")
class DiscoverAnnounceIdentityCommand:
  def do(self):
    # Retrieve the public key for the identity
    publicKey = self.keys.identityFor(self.options.object.id)

    # Announce the key on the network
    self.discover.announceIdentity(self.options.object.id, publicKey)

    return 0
