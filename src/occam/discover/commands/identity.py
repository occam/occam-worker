# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json
import os

from occam.commands.manager import command, option, argument

from occam.object import Object
from occam.log    import Log

from occam.manager import uses

from occam.discover.manager import DiscoverManager

@command('discover', 'identity',
  category      = 'Discovery',
  documentation = "Retrieves information about the given identity.")
@uses(DiscoverManager)
@argument("object", type = "object", help = "the identifier")
class DiscoverIdentityCommand:
  def do(self):
    self.discover.retrieveIdentity(self.options.object.id)

    return 0
