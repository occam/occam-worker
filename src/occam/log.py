# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys
import os

from io import BytesIO

# json logging
# {
#   "type": "normal",
#   "message": "...",
#   "id": "...",
#   "params": { }
# }
# {
#   "type": "warning",
#   "message": "...",
#   "id": "...",
#   "params": { }
# }

# Log types:

# normal   - average, useful log info about what's going on / what was requested
# debug    - noisy info that is less useful for the day to day. for devs
# error    - error conditions!
# warning  - warning condition
# external - from other subprocesses we don't control
# done     - when things finish successfully

class Log:
  class Header:
    INVALID = 0
    STATUS  = 1

  class Action:
    INVALID = 0

  class Type:
    # Invalid or unknown type
    INVALID  = None

    # Normal everyday messages (useful!) (*)
    # This is just the normal output. This may have an ID when the logged
    # output is known. For instance, we might see a "normal" log type with
    # an id of 1234 which might correspond to 'building object Foo'. It will
    # have a params key 'object' that describes the object the line says we
    # are building.
    NORMAL   = "normal"

    # Debugging messages (not useful. for devs) (*)
    # May contain an ID and some parameters, but probably won't have i18n
    # support.
    DEBUG    = "debug"

    # Error messages (oh no!) (!)
    ERROR    = "error"

    # Warning messages (oh meh?? maybe oh no?) (!)
    WARNING  = "warning"

    # External messages (docker being noisy) ([])
    # These messages generally do not have ids. They are stdout/stderr of a
    # subprocess that OCCAM spawns to do some related task or run a VM.
    EXTERNAL = "external"

    # Completion messages (yay. success!!) (*)
    # These messages will appear when a command completes successfully. They
    # will have an associated DoneType for an ID.
    DONE     = "done"

  class DoneTypes:
    INVALID = 0 # Invalid or unknown type

    RUN     = 1 # Completed a run task
    BUILD   = 2 # Completed a build task
    CONSOLE = 3 # Completed a console task

  # Metadata per-thread about the logging destination
  threadInformation = {}

  @staticmethod
  def threadInfo():
    from threading import get_ident

    threadID = get_ident()

    if not threadID in Log.threadInformation:
      # Default Log settings
      Log.threadInformation[threadID] = {
        "first_line":     False,
        "verbose":        False,
        "store_output":   False,
        "errorMessage":   "",
        "stored_output":  BytesIO(),
        "percentMessage": "",
        "withinPercent":  False,
        "jsonLogging":    False
      }

    return Log.threadInformation[threadID]

  @staticmethod
  def clear():
    """ Clears the stored output.
    """

    Log.threadInfo()["stored_output"] = BytesIO()

  @staticmethod
  def print(message, end="\n", file=sys.stdout):
    if Log.threadInfo()["withinPercent"]:
      print(file=sys.stderr)
      Log.threadInfo()["withinPercent"] = False

    #print(message, file=file, end=end)
    if isinstance(message, str):
      message = message.encode('utf-8')
    file.buffer.write(message)
    file.buffer.write(end.encode('utf-8'))
    if (file == sys.stdout):
      sys.stdout.flush()
    if (file == sys.stderr):
      sys.stderr.flush()

  @staticmethod
  def initialize(options, storeOutput = False, stdout = None):
    if stdout is None:
      stdout = sys.stdout.buffer

    if options.verbose:
      Log.threadInfo()["verbose"] = True
    if options.logtype == "json":
      Log.threadInfo()["jsonLogging"] = True
    Log.threadInfo()["store_output"] = storeOutput
    Log.threadInfo()["stdout"] = stdout

    Log.clear()

  @staticmethod
  def header(message):
    if Log.threadInfo()["jsonLogging"]:
      Log.writeJSON("header", message)
    else:
      if Log.threadInfo()["first_line"]:
        Log.print("", file=sys.stderr)

      Log.first_line = True
      Log.print(message, file=sys.stderr)

  @staticmethod
  def output(message, padding="  ", end="\n"):
    if Log.threadInfo()["store_output"]:
      Log.threadInfo()["stored_output"].write((str(message) + end).encode('utf8'))
    else:
      Log.print(padding, end="", file=sys.stderr)
      sys.stderr.flush()
      Log.print(message, end=end)

  @staticmethod
  def pipe(data, length=None):
    if isinstance(data, str):
      data = data.encode('utf-8')
    if isinstance(data, bytes):
      if length:
        data = data[0:length]
      if Log.threadInfo()["store_output"]:
        Log.threadInfo()["stored_output"].write(data)
      else:
        Log.threadInfo()["stdout"].write(data)
        if Log.threadInfo()["stdout"] is sys.stdout.buffer:
          sys.stdout.flush()
    else:
      bytesRead = 0
      while (length is None or length != bytesRead):
        amount = 1000
        if length:
          amount = min(1000, length)

        buff = data.read(amount)

        if length and (len(buff) + bytesRead > length):
          buff = buff[0:length-bytesRead]

        bytesRead += len(buff)

        if len(buff) == 0:
          break

        if Log.threadInfo()["store_output"]:
          Log.threadInfo()["stored_output"].write(buff)
        else:
          Log.threadInfo()["stdout"].write(buff)
          if Log.threadInfo()["stdout"] is sys.stdout.buffer:
            sys.stdout.flush()

  def storedOutput():
    output = Log.threadInfo()["stored_output"]
    Log.threadInfo()["stored_output"] = BytesIO()

    output.seek(0, 0)
    return output

  def errorMessage():
    return Log.threadInfo()["errorMessage"]

  @staticmethod
  def writeJSON(type, message, params={}, id="0", context=None):
    import json

    Log.print(json.dumps({
      "type": type,
      "message": message,
      "id": str(id),
      "params": params,
      "context": context
    }), file=sys.stderr, end="\n")

  @staticmethod
  def write(message, end="\n", context=None):
    if Log.threadInfo()["jsonLogging"]:
      Log.writeJSON("normal", message, context=context)
    else:
      if context:
        message = "%s: %s" % (context, message)
      Log.print(" \x1b[1;37m*\x1b[0m %s" % (message), file=sys.stderr, end=end)

  @staticmethod
  def external(message, source):
    if Log.threadInfo()["jsonLogging"]:
      Log.writeJSON("external", message, params={"source": source})
    else:
      Log.print(" \x1b[1;37m[\x1b[0m%s\x1b[1;37m]\x1b[0m: %s" % (source, message), file=sys.stderr)

  @staticmethod
  def noisy(message, end="\n", context=None):
    if Log.threadInfo()["verbose"]:
      if Log.threadInfo()["jsonLogging"]:
        Log.writeJSON("debug", message, context=context)
      else:
        if context:
          message = "%s: %s" % (context, message)
        Log.print(" \x1b[38;5;240m* %s\x1b[0m" % (message), file=sys.stderr, end=end)

  @staticmethod
  def warning(message, context=None):
    if Log.threadInfo()["jsonLogging"]:
      Log.writeJSON("warning", message, context=context)
    else:
      if context:
        message = "%s: %s" % (context, message)
      Log.print(" \x1b[1;33m! Warning: %s\x1b[0m" % (message), file=sys.stderr)

  @staticmethod
  def error(message, context=None):
    Log.threadInfo()["errorMessage"] = str(message)
    if Log.threadInfo()["jsonLogging"]:
      Log.writeJSON("error", message, context=context)
    else:
      if context:
        message = "%s: %s" % (context, message)
      Log.print(" \x1b[1;31m! Error: %s\x1b[0m" % (message), file=sys.stderr)

  @staticmethod
  def done(message):
    if Log.threadInfo()["jsonLogging"]:
      Log.writeJSON("done", message)
    else:
      Log.print(" \x1b[1;32m* Done: %s\x1b[0m" % (message), file=sys.stderr)

  @staticmethod
  def event(message, values={}):
    # Only happens in JSON logging (or when verbose mode is set)
    if Log.threadInfo()["jsonLogging"]:
      Log.writeJSON("event", message, params=values)
    elif Log.verbose:
      Log.print(" \x1b[38;5;240m+ Event: %s\x1b[0m" % (message), file=sys.stderr)

  @staticmethod
  def writePercentage(message, context=None):
    Log.threadInfo()["percentMessage"] = message

    if context:
      Log.write("%s: [   %%] %s" % (context, message), end='')
    else:
      Log.write("[   %] " + message, end='')

    Log.threadInfo()["withinPercent"] = True

  @staticmethod
  def updatePercentage(percent, message=None, context=None):
    message = message or Log.threadInfo()["percentMessage"]

    percentCode = str(int(percent)).rjust(3, ' ')
    if message:
      Log.threadInfo()["withinPercent"] = False
      print("\x1b[G", file=sys.stderr, end ='')
      sys.stderr.flush()
      Log.writePercentage(message, context)
      print("\x1b[K", file=sys.stderr, end ='')
      sys.stderr.flush()

    pos = 5
    if context:
      pos = pos + len(context) + 2

    lastPos = pos + 6 + len(message)

    print("\x1b[%dG%s%%]\x1b[%dG" % (pos, percentCode, lastPos), file=sys.stderr, end='')
    sys.stderr.flush()

  @staticmethod
  def outputBlock(text, padding="  "):
    for line in text.split('\n'):
      printed = ""
      for word in line.split(' '):
        if printed != "" and len(printed) + len(word) > (80 - len(padding)):
          Log.output("%s" % (printed), padding="")
          printed = ""
        printed += word + " "
      Log.output("%s" % (printed), padding="")

def loggable(context):
  def _loggable(cls, context):
    """
    Decorator that you can add to a class to give it the ability to log with
    context. It will add a Log class to your given class that contains methods
    that connect to the main logger.

    @loggable
    class Foo:
      ... etc ...

      def bar():
        # Prints "Foo: message"
        Foo.Log.write("message")

      def bar():
        # Prints "Foo: message" when in verbose mode
        Foo.Log.noisy("message")

      def baz():
        # Prints "Error: Foo: message"
        Foo.Log.error("message")

      def bop():
        # Prints "Warning: Foo: message"
        Foo.Log.warning("message")

    """

    class LogWrapper:
      def write(message, end="\n"):
        Log.write(message, end=end, context=context)

      def noisy(message, end="\n"):
        Log.noisy(message, end=end, context=context)

      def error(message):
        Log.error(message, context=context)

      def warning(message):
        Log.warning(message, context=context)

      def header(message):
        Log.header(message)

      def writePercentage(message):
        Log.writePercentage(message, context=context)

      def updatePercentage(percent, message=None):
        Log.updatePercentage(percent, message, context=context)

      def storedOutput():
        return Log.storedOutput()

      def errorMessage():
        return Log.errorMessage()

    cls.Log = LogWrapper
    return cls

  if isinstance(context, str):
    def _loggableContext(cls):
      return _loggable(cls, context + "[" + cls.__name__ + "]")

    return _loggableContext
  else:
    return _loggable(context, context.__name__)
