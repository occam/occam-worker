# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sql

from occam.log import loggable

from occam.databases.manager import uses, datastore
from occam.objects.database import ObjectDatabase

@loggable
@uses(ObjectDatabase)
@datastore("backends")
class BackendDatabase:
  """ Manages the database interactions for the EnvironmentMap and backend support.
  """

  def retrieveCapabilityRecordsFor(self, db_object):
    """ Returns the CapabilityRecords for the given object.
    """

    session = self.database.session()

    capabilities = sql.Table("capabilities")

    query = capabilities.select()

    query.where = (capabilities.internal_object_id == db_object.id)
    self.database.execute(session, query)

    from occam.backends.records.capability import CapabilityRecord

    return [CapabilityRecord(x) for x in self.database.many(session)]

  def retrieveProviderRecordsFor(self, db_object):
    """ Returns the ProviderRecords for the given object.
    """

    session = self.database.session()

    providers = sql.Table("providers")

    query = providers.select()

    query.where = (providers.internal_object_id == db_object.id)
    self.database.execute(session, query)

    from occam.backends.records.provider import ProviderRecord

    return [ProviderRecord(x) for x in self.database.many(session)]

  def retrieveProvidersFor(self, environment=None, architecture=None, capability=None):
    """ Returns rows of objects that provide the given environment and architecture.
    """

    session = self.database.session()

    objects   = sql.Table('objects')
    providers = sql.Table('providers')

    subquery = providers.select(providers.internal_object_id)
    subquery.where = (providers.environment  == environment)  \
                   & (providers.architecture == architecture)

    query = objects.select(where = objects.id.in_(subquery))

    self.database.execute(session, query)

    from occam.objects.records.object import ObjectRecord

    return [ObjectRecord(x) for x in self.database.many(session, size=10)]

  def retrieveBackendsFor(self, environment, architecture):
    """ Returns a list of strings indicating the backends for the given env/arch pair.

    Uses the Environment Map to look up which backends are possible endpoints
    for the given environment/architecture pair. This lets the system or a
    person know what possible options there are to run the object.
    """

    # Look up all EnvironmentMaps that point to backends and report which exist.
    session = self.database.session()

    targets   = sql.Table('environment_targets')
    maps      = sql.Table('environment_maps')
    providers = sql.Table('providers')

    # Find the providers for the given environment and architecture
    subsubquery = providers.select(providers.id)
    subsubquery.where = (providers.environment  == environment)  \
                      & (providers.architecture == architecture)

    # Find the joined table for the environment map
    subquery = maps.select(maps.environment_target_id)
    subquery.where = (maps.provider_id.in_(subsubquery))

    # Finally, find the environment target entry that has a 'backend' field
    # which indicates it is mapping to a native backend and not another
    # object.
    query = targets.select(targets.backend)
    query.where = (targets.backend != None) \
                & (targets.id.in_(subquery))

    self.database.execute(session, query)

    backends = set([])

    [backends.add(x['backend']) for x in self.database.many(session, size=10)]

    return list(backends)

  def updateProviders(self, db_object, objectInfo):
    """ This method will update or add Provider/Capability records for the given object record.
    """

    # Go through the providers list and add/get provider records
    # For each env/arch provider, add to environment map

    session = self.database.session()

    # Store a token for any Provider of an environment/architecture
    if not 'provides' in objectInfo:
      return []

    db_providers    = self.retrieveProviderRecordsFor(db_object)
    db_capabilities = self.retrieveCapabilityRecordsFor(db_object)

    provides = objectInfo.get('provides', {})

    # Just assume it is a list of different pairs
    if not isinstance(provides, list):
      provides = [provides]

    # Keep track of all the Provider records
    ret = []

    for providerInfo in provides:
      # Create a ProviderRecord if it doesn't already exist
      db_provider = self.retrieveOrCreateProvider(db_object, providerInfo, db_providers)

      # Create the CapabilityRecords if they do not already exist
      self.retrieveOrCreateCapabilities(db_object, providerInfo, db_capabilities)

      # We will return the providers
      ret.append(db_provider)

    return ret

  def updateEnvironmentMap(self, id, revision, objectInfo, backends):
    """ Adds the object as a possible backend provider and adds EnvironmentMap normalization records.
    """

    session = self.database.session()

    rows = self.objects.retrieveObjects(id=id)

    if len(rows) > 0:
      db_object = rows[0]
    else:
      return None

    # Query/Create provider
    db_providers = self.updateProviders(db_object, objectInfo)

    for db_provider in db_providers:
      environment  = db_provider.environment
      architecture = db_provider.architecture

      # What this provider maps to (what it itself runs on)
      targetEnvironment  = objectInfo.get('environment')
      targetArchitecture = objectInfo.get('architecture')

      BackendDatabase.Log.write("Updating Environment Map... %s-%s" % (environment, architecture))

      if targetEnvironment is not None and targetArchitecture is not None:
        # Look up environment map entries that provide the environment/architecture
        # for this providing object.
        environmentMap = self.retrieveEnvironmentMapFor(environment=targetEnvironment, architecture=targetArchitecture)

        # Point this object to each and every one.
        for environmentTarget in environmentMap:
          if not environmentTarget.backend is None:
            BackendDatabase.Log.noisy("Object mapped to backend %s" % (environmentTarget.backend))
          else:
            BackendDatabase.Log.noisy("Object mapped to %s-%s" % (environmentTarget.environment, environmentTarget.architecture))
          self.insertEnvironmentMapping(db_provider=db_provider, environmentTarget=environmentTarget)

      # TODO: Look up providers that require our providing environment and add the environment target
      # TODO: For each of those providers, lookup the environment maps that reach them and add ourselves as well

      # Add ourselves
      self.insertEnvironmentMapping(db_provider=db_provider, environment=targetEnvironment, architecture=targetArchitecture)

      # Add possible backends
      for backend in backends:
        self.insertEnvironmentMapping(db_provider=db_provider, backend=backend)

      # For now, we will recursively search the map
      # We don't really need to do this
      # TODO: move to a 'repair' command option to rescan and recreate the Environment Map
      #stuff = self.providerPath(targetEnvironment, targetArchitecture)
      #if not stuff is None:
      #  for thing in stuff:
      #    if isinstance(thing, Object):
      #      self.store(thing)

  def insertEnvironmentMapping(self, db_provider=None, environmentTarget=None, environment=None, architecture=None, backend=None):
    """ Maps a Provider record to an EnvironmentTarget through an EnvironmentMap.
    """

    session = self.database.session()

    # Add ourselves, if it doesn't exist already
    found = False
    if environmentTarget is None:
      environmentTargets = self.retrieveEnvironmentMapFor(db_provider=db_provider)

      for target in environmentTargets:
        if environment is None and architecture is None and not backend is None:
          if target.backend == backend:
            found = True
        else:
          if target.environment == environment and target.architecture == architecture:
            found = True
    else:
      # Look up existing EnvironmentMap from provider -> environmentTarget
      backend      = environmentTarget.backend
      environment  = environmentTarget.environment
      architecture = environmentTarget.architecture

      maps    = sql.Table('environment_maps')
      targets = sql.Table('environment_targets')

      query = maps.select(where = (maps.provider_id == db_provider.id) & (maps.environment_target_id == environmentTarget.id))

      self.database.execute(session, query)
      db_environmentMap = self.database.fetch(session)

      if not db_environmentMap is None:
        found = True

    if found:
      if environment is None and architecture is None and not backend is None:
        BackendDatabase.Log.write("Mapping already exists from %s-%s to %s" % (db_provider.environment, db_provider.architecture, backend))
      else:
        BackendDatabase.Log.write("Mapping already exists from %s-%s to %s-%s" % (db_provider.environment, db_provider.architecture, environment, architecture))

    if not found:
      if environment is None and architecture is None and not backend is None:
        BackendDatabase.Log.write("Adding environment target %s-%s -> %s" % (db_provider.environment, db_provider.architecture, backend))
      else:
        BackendDatabase.Log.write("Adding environment target %s-%s -> %s-%s" % (db_provider.environment, db_provider.architecture, environment, architecture))

      if environmentTarget:
        db_environmentTarget = environmentTarget
      else:
        from occam.backends.records.environment_target import EnvironmentTargetRecord

        db_environmentTarget = EnvironmentTargetRecord()
        if environment is None and architecture is None and not backend is None:
          db_environmentTarget.backend = backend
        else:
          db_environmentTarget.environment = environment
          db_environmentTarget.architecture = architecture

        self.database.update(session, db_environmentTarget)

      # Commit the EnvironmentTarget record to get an id
      self.database.commit(session)

      from occam.backends.records.environment_map import EnvironmentMapRecord
      db_environmentMap = EnvironmentMapRecord()
      db_environmentMap.environment_target_id = db_environmentTarget.id
      db_environmentMap.provider_id = db_provider.id
      self.database.update(session, db_environmentMap)

      self.database.commit(session)

  def retrieveEnvironmentMapFor(self, environment=None, architecture=None, db_provider=None):
    """ Returns the set of EnvironmentTarget records for the given environment and architecture.
    
    This is a collection of possible environment/architecture or backends that can be used
    to provide the given pair.
    """

    targets = sql.Table('environment_targets')
    maps    = sql.Table('environment_maps')

    db_providers = []

    if not db_provider is None:
      db_providers = [db_provider.id]
    else:
      providers = sql.Table('providers')
      db_providers = providers.select(providers.id)
      db_providers.where = (providers.environment == environment) & (providers.architecture == architecture)

    session = self.database.session()

    maps_query = maps.select(maps.environment_target_id)
    maps_query.where = (maps.provider_id.in_(db_providers))

    query = targets.select()
    query.where = (targets.id.in_(maps_query))

    self.database.execute(session, query)
    from occam.backends.records.environment_target import EnvironmentTargetRecord

    return [EnvironmentTargetRecord(x) for x in self.database.many(session)]

  def backendCanSupport(self, backend, environment, architecture):
    """
    Returns True when the given backend can support the given
    environment/architecture pair. Returns False otherwise. Uses the Environment
    Map to make this decision.
    """

    # Look up any EnvironmentMap that points to the given backend
    # Thank goodness I'm writing pure SQL because an ORM would be silly here

    session = self.database.session()
    
    # Look up an EnvironmentMap that points to the goal pair
    targets   = sql.Table('environment_targets')
    maps      = sql.Table('environment_maps')
    providers = sql.Table('providers')

    providerQuery = providers.select(providers.id)
    providerQuery.where = (providers.environment == environment) & (providers.architecture == architecture)

    mapQuery = maps.select(maps.environment_target_id)
    mapQuery.where = (maps.provider_id.in_(providerQuery))
    
    query = targets.select(targets.backend)
    query.where = (targets.backend == backend) & (targets.id.in_(mapQuery))

    self.database.execute(session, query)
    row = self.database.fetch(session)

    return row is None

  def canSupport(self, environment, architecture, environmentGoal=None, architectureGoal=None):
    """ Returns True when the given environment/architecture pair has a possible
    route to a native backend or the given goal.
    """

    if not environmentGoal is None or not architectureGoal is None:
      session = self.database.session()
      
      # Look up an EnvironmentMap that points to the goal pair
      targets   = sql.Table('environment_targets')
      maps      = sql.Table('environment_maps')
      providers = sql.Table('providers')

      providerQuery = providers.select(providers.id)
      providerQuery.where = (providers.environment == environment) & (providers.architecture == architecture)

      mapQuery = maps.select(maps.environment_target_id)
      mapQuery.where = (maps.provider_id.in_(providerQuery))
      
      query = targets.select(targets.backend)
      query.where = (targets.environment == environmentGoal) & (targets.architecture == architectureGoal) & (targets.id.in_(mapQuery))

      self.database.execute(session, query)
      row = self.database.fetch(session)

      return row is not None
    else:
      # Look up any EnvironmentMap that points to a backend
      return len(self.retrieveBackendsFor(environment=environment, architecture=architecture)) > 0

    return False
  
  def retrieveOrCreateProvider(self, db_object, providesInfo, db_providers):
    """ This method returns a ProviderRecord for the given provider information.

    It will create the record if it doesn't already exist.
    """

    # TODO: it should remove any that now don't exist

    for db_provider in db_providers:
      if db_provider.environment == providesInfo.get("environment") and db_provider.architecture == providesInfo.get("architecture"):
        return db_provider

    from occam.backends.records.provider import ProviderRecord

    session = self.database.session()

    db_provider = ProviderRecord()
    db_provider.environment  = providesInfo.get("environment")
    db_provider.architecture = providesInfo.get("architecture")
    db_provider.internal_object_id = db_object.id
    db_provider.revision = db_object.revision

    self.database.update(session, db_provider)
    self.database.commit(session)

    db_providers.append(db_provider)

    return db_provider

  def retrieveOrCreateCapabilities(self, db_object, providesInfo, db_capabilities):
    """ This method returns a set of CapabilityRecords for the given provider information.

    It will create any that do not already exist.
    """

    ret = []

    from occam.backends.records.capability import CapabilityRecord

    session = None

    # TODO: it should remove any that now don't exist
    for capability in providesInfo.get('capabilities', []):
      for db_capability in db_capabilities:
        if db_capability.capability != capability:
          # Create a new CapabilityRecord in the database
          if not sesson:
            session = self.database.session()

          db_capability = CapabilityRecord()
          db_capability.capability = capability
          db_capability.internal_object_id = db_object.id

          self.database.update(session, db_capability)

          db_capabilities.append(db_capability)

        ret.append(db_capability)

    if session:
      self.database.commit(session)

    return ret
