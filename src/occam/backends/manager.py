# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os

from occam.log    import loggable
from occam.object import Object
from occam.config import Config

from occam.manager import uses, manager

from occam.objects.manager   import ObjectManager

@loggable
@uses(ObjectManager)
@manager("backends")
class BackendManager:
  """
  This OCCAM manager handles backend invocations. A backend is a service that
  OCCAM can use to build and run objects. For instance, a backend can wrap a
  virtual machine such that objects can be built and run within that
  environment. Some objects may be able to run on most backends, some on just
  a few, and some backends are decidedly relegated to run older archived
  programs.

  The ManifestManager creates the tasks it wishes to run and the BackendManager
  receives those tasks and invokes the backend system to build or run the
  objects represented by those tasks. The ManifestManager creates tasks by
  using the BackendManager to determine which objects can be executed and
  which need to be used together to run something. It is a two step process:
  1. Determine the task (virtual machine environment) to run something. and 2.
  Create and run that virtual machine (or reuse one that already exists)

  The Environment Map is a data structure that normalizes the relationships
  among environmental objects. That is, for objects that maintain an environment
  for which other objects can be executed (emulators, operating systems, etc,)
  the Environment Map contains a mapping between each environment/architecture
  and every possible environment/architecture it maps to. It also maps every
  known environment/architecture to known backends.

  That is, the Environment Map can tell you when given "dos" and "x86" all of
  the possible environments ("linux" on "x86-64") that this object can
  potentially run upon. Or any backends ("docker", "kvm") that it can run
  upon.

  The system uses this Environment Map to determine how to best create
  virtual machines to run objects::

    occam manifest run bd4e25a0-5389-11e6-8a01-f23c910a26c8@2ed1d22cb3bd3f6dd40da2765e31a18f161e966c

  This command will use this ManifestManager and BackendManager, making use of
  the Environment Map, to generate a possible task that will run the given
  object at the given revision::

    occam manifest run bd4e25a0-5389-11e6-8a01-f23c910a26c8@2ed1d22cb3bd3f6dd40da2765e31a18f161e966c --targetEnvironment javascript --targetArchitecture html

  This will generate a task manifest that will tell a browser component how to
  run this object within an HTML/javascript environment. Again, it uses the
  Environment Map to quickly know how to map environments such as "dos/x86" to
  "javascript/html" and whether or not it is possible. It can make that
  decision almost immediately::

    occam objects run bd4e25a0-5389-11e6-8a01-f23c910a26c8@2ed1d22cb3bd3f6dd40da2765e31a18f161e966c

  This command will run that object using the task it generates.

  To query backends some object can run use the `manifest` command with the
  ``--backends``/``-b`` flag::

    occam manifest run --environment dos --architecture x86 --backends

  This will list the backends that are able to run an object originally meant
  to be executed in a DOS environment.

  See 'manifest' usage for more examples and information.
  """

  handlers = {}

  def __init__(self):
    """
    Initialize the backend manager.
    """

    import occam.backends.plugins.docker

    self.handlers = BackendManager.handlers

  @staticmethod
  def register(backendName, handlerClass):
    """
    Adds a new backend type.
    """
    BackendManager.handlers[backendName] = handlerClass

  def isAvailable(self, backendName):
    """ Returns True when the backendName is available on this node.
    """

    return backendName in self.handlers

  def handlerFor(self, backendName):
    """
    Returns an instance of a handler for the given type.
    """

    if not backendName in self.handlers:
      BackendManager.Log.error("backend %s not known" % (backendName))
      return None

    return self.handlers[backendName]()

  def isBuilt(self, task):
    """
    Returns whether or not the object represented in the given task hsa been
    built already by any machine. Returns True if the object has been build and
    False otherwise.
    """

    # Gather metadata and ask the backend.
    # If metadata cannot be found, then assume it is not built.
    # If it is found, ask the backend.

    return False

  def initialize(self):
    """
    """

    ret = {}
    for k, v in BackendManager.handlers.items():
      info = self.handlerFor(k).initialize()
      if not info is None:
        ret[k] = info

    return ret

  def build(self, task):
    """
    Builds the object within the generated task. Each buildable object has a
    'build' section in its metadata. This lists everything the ManifestManager
    needs to build a task for the build environment of this object.

    This function will take a task and, based on its backend, build that object
    for use in other build/run tasks.

    The built task will be marked in an invocation record.
    """

    if not "backend" in task:
      return None

    # Return the success of the build and any metadata from the backend handler.

    # Store that metadata in the invocation record related to this backend type.

    #objs = self.manifests.rakeTaskObjects(task)
    #for obj in objs:
      # Submit a build task, if we need it
    #  if 'build' in obj:
    #    buildTask = self.manifests.taskFor(obj, section="build")
    #    self.manifests.printTask(buildTask)

        #if not self.isBuilt(buildTask):
        #  self.occam.manifests.build(buildTask)

    #self.manifests.printTask(task)

    return

  def run(self, task, paths=[], network = None, stdout = None, stdin = None):
    """
    Runs the object represented by the given task. Each runnable object has a
    'run' section in its metadata. This lists everything the ManifestManager
    needs to build a task for the environment needed to run the object.

    This function will take a task and, based on its backend, run that object.

    The run will be marked in an invocation record along with information about
    the runtime or any failures. Other invocations of this object on other
    machines may use that information to make decisions about how to best run
    the object in the future.
    """

    BackendManager.Log.noisy("Running with %s" % (task.get('backend')))
    backend = self.handlerFor(task.get('backend'))

    return backend.run(task, paths, network, stdout, stdin)

  def terminate(self, task):
    """ Terminates the running task.
    """

    backend = self.handlerFor(task.get('backend'))

    if backend is None:
      return

    return backend.terminate(task)

  def providersFor(self, environment = None, architecture = None, capability = None):
    """ Returns rows of objects that provide the given environment and architecture.
    """

    return self.datastore.retrieveProvidersFor(environment, architecture, capability)

  def backendsFor(self, environment, architecture):
    """ Returns a list of strings indicating the backends for the given env/arch pair.

    Uses the Environment Map to look up which backends are possible endpoints
    for the given environment/architecture pair. This lets the system or a
    person know what possible options there are to run the object.
    """

    return self.datastore.retrieveBackendsFor(environment, architecture)

  def providerPath(self, environment, architecture, environmentGoal=None, architectureGoal=None, person=None, limit = 10):
    """ Provides an array of providers that will map the given environment and
    architecture to a native backend.
    
    The last item in the array will be a
    native backend instance (if goals aren't given, otherwise it will be an
    object that yields that goal environment.) Every other item will, in turn,
    be an object that provides one step of an environment.

    For instance, if we want to run a "dos" object that runs on "x86", we will
    invoke this command with environment and architecture given accordingly.
    The native environment might be "linux" on "ARM", so we must find a set
    of objects that will give us that environment. This method will return the
    hypothetical array of objects that will get us there. The first object
    might be DOSBox, which provides the "dos"/"x86" environment but runs on
    "ubuntu:14.04"/"x86-64". So the next object in the array might be an
    ubuntu distribution providing "ubuntu:14.04"/"x86-64". So we will need
    something to run that distribution, therefore the next object might be
    VirtualBox, providing the "*"/"x86-64" environment. Let's assume we can
    run VirtualBox natively. In this case, our search is complete.

    If this returns None, then the path cannot be made.
    """

    if limit <= 0:
      return None

    ret = []

    from occam.objects.records.object import ObjectRecord

    BackendManager.Log.noisy("Looking up provider path from %s %s to %s %s" % (environment, architecture, environmentGoal, architectureGoal))

    providers = list(self.providersFor(environment, architecture))

    # Look for backends that can handle this object
    for k, v in self.handlers.items():
      if v.canProvide(environment, architecture):
        backend = v()
        providers.append(backend)

    # Look for secondary objects that can handle this object
    for provider in providers:
      if not isinstance(provider, ObjectRecord):
        BackendManager.Log.noisy("Trying backend %s" % (provider.name()))
        currentEnvironment  = environment
        currentArchitecture = architecture
      else:
        BackendManager.Log.noisy("Trying object %s to reach %s/%s" % (provider.name, environmentGoal, architectureGoal))
        currentEnvironment  = provider.environment
        currentArchitecture = provider.architecture

      # Look for successful path endpoints:
      if environmentGoal is None and architectureGoal is None:
        # We are looking for absolutely ANY path to a backend (not an Object)
        if not isinstance(provider, ObjectRecord):
          BackendManager.Log.noisy("Path successful: Found a backend matching our goal.")
          ret.append(provider)
          return ret
      elif (environmentGoal is None or currentEnvironment == environmentGoal) and (architectureGoal is None or currentArchitecture == architectureGoal):
        # We found an object that meets our goal
        BackendManager.Log.noisy("Found %s @ %s" % (provider.name, provider.revision))
        provider = self.objects.retrieve(id=provider.id, revision=provider.revision)
        ret.append(provider)
        return ret

      if not isinstance(provider, ObjectRecord):
        # We hit a backend, but this is not our goal
        BackendManager.Log.noisy("Path not successful: Hit a backend, but not our goal.")
        continue

      # Get a path from this point to a backend or goal
      result = self.providerPath(currentEnvironment, currentArchitecture, environmentGoal, architectureGoal, limit = limit - 1, person = person)

      # If we can't get a path from this point to a goal, then this point is not in our path
      if result is None:
        BackendManager.Log.noisy("Path not successful: Can't find a path from %s/%s to %s/%s" % (environment, architecture, environmentGoal, architectureGoal))
        continue

      # Otherwise, append the path and succeed
      provider = self.objects.retrieve(id=provider.id, revision=provider.revision, person=person)

      # TODO: handle this failure better
      if provider:
        ret.append(provider)
        ret.extend(result)
        return ret

      # Otherwise we keep trying...

    # Cannot find a route
    return None

  def store(self, uuid, revision, objectInfo):
    """ Records the given object as a possible backend provider.

    Looks at the object given and, if it is a provider, will add this object to
    the Environment Map. The Environment Map consists of records that when given
    an environment/architecture pair yields all possible environment/architectures
    that the given object can run upon.

    If the object is an emulator for ARM64, for instance, and the emulator runs
    on ubuntu or x86-64, then this function will figure out what things run
    ubuntu on x86-64... what do those things run upon... and exhaust all
    possibilities. It will record the map between this Provider record and
    each possible environment/architecture pair.

    Now, whenever we have an ARM64 application, we can quickly know exactly what
    possible routes we have to emulator or virtualize the application.

    In the end: it normalizes the pathfinding algorithm.
    """

    targetEnvironment  = objectInfo.get('environment')
    targetArchitecture = objectInfo.get('architecture')

    backends = self.providing(environment=targetEnvironment, architecture=targetArchitecture)
    ret = self.datastore.updateEnvironmentMap(uuid, revision, objectInfo, backends = backends)

    return ret

  def backendCanSupport(self, backend, environment, architecture):
    """
    Returns True when the given backend can support the given
    environment/architecture pair. Returns False otherwise. Uses the Environment
    Map to make this decision.
    """

    return self.datastore.backendCanSupport(backend, environment, architecture)

  def canSupport(self, environment, architecture, environmentGoal=None, architectureGoal=None):
    """ Returns True when the given environment/architecture pair has a possible
    route to a native backend or the given goal.
    """

    return self.datastore.canSupport(environment, architecture, environmentGoal, architectureGoal)

  def providing(self, environment, architecture):
    """ Returns names of all backends that can manage the given environment and
    architecture pair.
    
    Returns an array of strings. Returns an empty array
    when the given environment and architecture cannot run directly on an
    available.
    """

    # Go through the available backends and find one that provides the
    # requested environment/architecture combination

    ret = []

    for k, v in self.handlers.items():
      if v.canProvide(environment, architecture):
        ret.append(k)

    return ret

def backend(name, priority=0):
  """ This decorator will register the given class as a backend.
  """
  def register_backend(cls):
    BackendManager.register(name, cls)
    cls = loggable("BackendManager")(cls)

    return cls

  return register_backend
