# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.databases.manager import table

@table('environment_targets')
class EnvironmentTargetRecord:
  """ Holds a record to a particular environment/architecture or backend target.

  These records are used to map every possible environment/architecture pair to
  any and all possible environment/architecture or native backend service that
  could possibly run them.

  It is a complex data structure but it helps quickly determine if a particular
  object can run on a particular machine. That is, whether or not a path can be
  made between the desired object to run, through any set of providers, and
  finally to a backend. It memoizes each and every lookup.
  """

  schema = {

    # Primary Key

    "id": {
      "type": "integer",
      "primary": True
    },

    # Attributes

    "backend": {
      "type": "string",
      "length": 128
    },

    "environment": {
      "type": "string",
      "length": 128
    },

    "architecture": {
      "type": "string",
      "length": 128
    },
  }
