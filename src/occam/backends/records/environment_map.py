# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.databases.manager import table

@table('environment_maps')
class EnvironmentMapRecord:
  """ Maps a Provider to an EnvironmentTarget.

  This join helps quickly find paths between objects we might want to run, and
  objects that help us run them. It ultimately helps us create paths between
  such objects and the native resources on our machine such as Virtualization
  and Containerization services.
  """

  schema = {

    # Primary Key

    "id": {
      "type": "integer",
      "primary": True
    },

    # Foreign Key to Provider

    "provider_id": {
      "foreign": {
        "table": "providers",
        "key":   "id"
      }
    },

    # Foreign Key to EnvironmentTarget

    "environment_target_id": {
      "foreign": {
        "table": "environment_targets",
        "key":   "id"
      }
    }
  }
