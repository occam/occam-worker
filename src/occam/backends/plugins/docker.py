# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.log import Log

from occam.backends.manager import backend

import sys
import codecs
import select
import subprocess
import os
import glob
import time
import json
import shutil
import base64

# runCommandFor(obj)

# for tasks:
#   runcommand will run a series of lines for combined elements
#   runcommands may be cached as a Configuration + Input pair
#   volumes will be determined from those too

#   Object needs the ability to pull all depend(a|e)nt objects
#   such that this backend can build/append volumes.

#   that would be: all 'input', 'output', 'with', and itself
#   well, 'with' would just impose an input relationship, but whatever

@backend('docker')
class Docker:
  """ Implements the logic to build/run objects as Docker containers.
  """

  def initialize(self):
    pass

  @staticmethod
  def provides():
    """ Returns a list of all possible environments and architecture pairs that
    can be created and used by this backend.
    """

    # Generally, we should look up docker-container environment objects.

    # Look up docker-container objects and list the possible objects as
    # environments.

    # Search for type 'docker-container' and look at ['metadata']['docker']
    # field for 'environment' and 'architecture'

    # Record those in the database as possible environments

    # We only have to rake the object pool when we see that the number of
    # docker-container objects has changed. (although it may not see
    # updates)

    return [
      ['docker', 'x86-64']
    ]

  @staticmethod
  def name():
    return "docker"

  @staticmethod
  def canProvide(environment, architecture):
    """
    Returns True if this backend can build/run objects with the given
    environment and architecture.
    """

    return [environment, architecture] in Docker.provides()

  @staticmethod
  def mountCommandForSoftLink(hostPath, access='ro', mounted=None):
    """
    Returns the list of command tokens (['-v', 'path:path:ro', ...]) to use to
    generate the docker command to mount in the given file represented by path
    and all files it links to.
    """

    mounted = mounted or {}

    command = []
    if os.path.exists(hostPath):
      if not hostPath in mounted:
        command += ['-v', '%s:%s:%s' % (hostPath, hostPath, access)]
        mounted[hostPath] = True

      if os.path.islink(hostPath):
        curPath = os.path.dirname(hostPath)
        nextPath = os.readlink(hostPath)
        nextPath = os.path.join(curPath, nextPath)
        command += Docker.mountCommandForSoftLink(nextPath, access, mounted)
    return command

  def mountNVIDIADriver(self, mounted=None):
    """
    Returns the list of command tokens (['-v', 'path:path:ro', ...]) to use to
    generate the docker command to mount in the NVIDIA driver if it exists on
    the system.
    """

    # TODO: use ld.so.conf
    SEARCH_LIST=self.occam.system.libraryPaths() + ["/usr/lib/xorg/modules", "/usr/lib/xorg/modules/drivers", "/usr/lib/xorg/modules/extensions", "/usr/lib/x86_64-linux-gnu/vdpau"]
    FILE_LIST=["libnvidia*", "libnvcu*", "libwfb*", "nvidia_drv*", "libglx*"]

    mounted = mounted or {}

    command = []
    command += Docker.mountCommandForSoftLink("/usr/lib/libGL.so", "ro", mounted)
    command += Docker.mountCommandForSoftLink("/usr/lib/libGL.so.1", "ro", mounted)

    for pattern in FILE_LIST:
      for root in SEARCH_LIST:
        for path in glob.iglob(os.path.join(root, pattern)):
          command += Docker.mountCommandForSoftLink(path, "ro", mounted)

    return command

  def mergeVariables(self, a, b):
    if 'LD_LIBRARY_PATH' in a and 'LD_LIBRARY_PATH' in b:
      a['LD_LIBRARY_PATH'] = "%s:%s" % (a['LD_LIBRARY_PATH'], b['LD_LIBRARY_PATH'])
      del b['LD_LIBRARY_PATH']
    a.update(b)
    return a

  # TODO: purgeBuiltImage
  def purge(self, obj):
    """
    Deletes any local cached copy of the built object.
    """

    # TODO: this D: D: D:
    return False

  def exists(self, obj):
    """ Returns True if the given object exists as a cached docker volume.
    """

    name = self.imageNameFor(obj)
    command = ['docker', 'inspect', '%s' % (name)]

    return self.executeDockerCommand(command, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL) == 0

  def detect(self):
    """
    Returns True when docker is available.
    """

    # Just check that docker is in PATH and runs
    # Even when it does exist, we check the error code to see if the docker
    # service is running. It should post a non-zero when the docker service
    # has failed.

    command = ['docker']

    return self.executeDockerCommand(command) == 0

  def canRun(self, obj):
    """
    Returns True when the given object can be run on this backend.
    """

    objInfo = self.objects.infoFor(obj)
    environment  = objInfo.get('environment')
    architecture = objInfo.get('architecture')

    return Docker.canProvide(environment, architecture)

  # TODO: export
  def export(self, obj):
    """
    """
    return False

  def pull(self, url, uuid, cert=None, revision=None):
    """
    Pulls the container representing the given object from an existing node.
    Returns True when a container is pulled and False otherwise.
    """

    return

    # Get the docker resource URL at the other node
    nodeToken = self.occam.nodes.partialFromURL(url).replace(',', ':')
    resourceURL = "https://%s/resources/docker-container/%s/%s" % (nodeToken, uuid, revision)

    # Tell that server our intent (so it caches the docker container)
    Log.noisy("pinging intent to pull container at %s" % (resourceURL))
    response, content_type, size = self.occam.network.get(resourceURL)
    if response is None:
      return False

    # form docker's prefered resource URI
    source = nodeToken + "/" + uuid + ":" + revision

    # Add certificate to global certs list for Docker (needs permission!)
    # TODO: what the hell does docker do on Windows. especially wrt ':' since
    #       those chars aren't allowed in filenames
    if cert:
      localCertPath = os.path.join('/', 'etc', 'docker', 'certs.d')

      localCertPath = os.path.join(localCertPath, nodeToken)
      if not os.path.exists(localCertPath):
        os.mkdir(localCertPath)

      localCertFile = os.path.join(localCertPath, 'ca.crt')

      # Copy node certificate to this location
      shutil.copyfile(cert, localCertFile)

    self.pullExternal(source)

    # Create occam/{uuid}:{revision} tag
    self.tag(uuid, source, revision=revision)

    # Create localhost:5000/{uuid}:{revision} tag
    self.tag(uuid, source, revision=revision, namespace='localhost:5000')

    if cert:
      # Remove certificate
      os.remove(localCertFile)
      os.rmdir(localCertPath)

    return True

  def safeName(self, name):
    """ Returns a Docker-safe name from the given name.
    """

    from uuid import uuid5, NAMESPACE_URL
    return str(uuid5(NAMESPACE_URL, "occam://docker/%s" % (name)))

  def run(self, task, paths, network, stdout, stdin=None):
    """
    """

    if stdout is None:
      import sys
      stdout = sys.stdout.buffer

    # First, ensure that the base container is built
    baseContainer = task['running'][0]['process'][0]
    baseContainerId = baseContainer.get('id')
    baseContainerName = "occam/base-%s" % (self.safeName(baseContainerId))

    command = ['docker', 'inspect', baseContainerName]
    baseContainerExists = self.executeDockerCommand(command, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL) == 0

    if not baseContainerExists:
      # Ensure there is a scratch container
      os.system("tar cv --files-from /dev/null | docker import - scratch")

      # Build the base container
      baseContainerPath = paths[baseContainer.get('index')]['path']
      command = ['docker', 'build', '--rm', '-t', baseContainerName, "."]
      baseContainerExists = self.executeDockerCommand(command, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL, cwd = baseContainerPath) == 0

    portArgs = []
    if 'runs' in task and task['runs']['id'] == 'be0b3af2-6740-11e7-b0e8-e06995729391':
      portArgs = ['-p', '30000:5900']

    if 'runs' in task and task['runs']['id'] == '8c9cc84e-6e3b-11e7-a9cd-e06995729391':
      portArgs = ['-p', '30000:5900']

    command = ['docker', 'run'] + portArgs

    command.append("-i")
    command.append("-t")

    # Run as the current user
    command = command + ['--user', '%s:%s' % (str(os.getuid()), str(os.getgid()))]

    # Add a temp folder
    #command = command + ["--mount", "type=tmpfs,destination=/tmp"]
    command = command + ["--tmpfs", "/tmp:rw,exec,size=787448k,mode=1777"]

    # Docker Environment
    environment = task.get('running')[0]['process'][0]

    # Mount task volume
    command.append('-v')
    command.append('%s:/home/occam:rw' % paths['home'])
    command.append('-v')
    command.append('%s:/home/occam/task:rw' % paths['task'])

    # Add root volumes
    rootPath = paths.get('root')
    if rootPath:
      for subPath in os.listdir(rootPath):
        command.append('-v')
        command.append('%s:%s' % (os.path.join(rootPath, subPath), os.path.join("/", subPath)))

    # Add network
    for port in network.get('ports', []):
      command.append("-p")
      command.append("%s:%s" % (port['port'], port['bind']))

    # Add each volume

    for k, pathInfo in paths.items():
      if isinstance(pathInfo, dict):
        command.append('-v')
        command.append('%s:%s:ro' % (pathInfo.get('path'), pathInfo.get('mount')))

        if 'localPath' in pathInfo:
          command.append('-v')
          command.append('%s:%s:rw' % (pathInfo.get('localPath'), pathInfo.get('localMount')))

        if 'buildPath' in pathInfo:
          command.append('-v')
          command.append('%s:%s:rw' % (pathInfo.get('buildPath'), pathInfo.get('buildMount')))

    # Form environment

    env = environment.get('run', {}).get('env', {})

    for k, v in env.items():
      command.append('-e')
      command.append("%s=%s" % (k, v))

    # Working directory
    command.append('-w')
    command.append("/home/occam")

    command.append('--rm=true')

    # Add some security holes
    command.extend(self.capabilitiesFor(task))
    command.extend(self.securityOptionsFor(task))

    # TODO: is this name good enough when there might be multiple run phases?
    command.append("--name")
    command.append("occam-%s" % (self.safeName(task.get('id'))))

    command.append(baseContainerName)

    runCommand = environment.get('run', {}).get('command')

    if runCommand is None:
      # We cannot run this
      return False

    if not isinstance(runCommand, list):
      runCommand = [runCommand]

    command.extend(runCommand)

    pty = False
    if task.get('interactive', False):
      stdout = None
      pty = True

    return self.executeDockerCommand(command, cwd=paths['task'], interactive=True, stdout = stdout, stdin = stdin, pty = True)

  def signal(self, task, signal):
    """ Gives the given signal to the given task, if it is running.

    Uses 'docker kill' to send a signal to the docker process.
    """

    name = "occam-%s" % (self.safeName(task.get('id')))

    sigx = [None, "HUP", "INT", "QUIT", "ILL", "TRAP", "ABRT", "EMT", "FPE",
            "KILL", "BUS", "SEGV", "SYS", "PIPE", "ALRM", "TERM", "USR1", "USR2",
            "CHLD", "PWR", "WINCH", "URG", "POLL", "STOP", "TSTP", "CONT", "TTIN",
            "TTOU", "VTALRM", "PROF", "XCPU", "XFSZ", "WAITING", "LWP", "AIO"]

    # The default is KILL
    if signal < 0 or signal >= len(sigx):
      signal = 9

    signalString = sigx[signal] or "KILL"

    command = ["docker", "kill", name, "--signal", signalString]

    return self.executeDockerCommand(command)

  def terminate(self, task):
    """ Gives the given signal to the given task, if it is running.

    Uses 'docker kill' to send a SIGKILL (or other signal) to the docker process.
    """

    return self.signal(task, 9)

  def pullExternal(self, source):
    """
    Pulls the container from the given source found on a non-occam network.
    """

    command = ['docker', 'pull', source]
    code = subprocess.Popen(command).communicate()

  def securityOptionsFor(self, obj):
    """
    Returns the security options needed to run the given object.
    """

    ret = []

    #apparmor = self.occam.capabilities.appArmor()
    apparmor = True

    if apparmor:
      # TODO: better apparmor options?
      ret += ["--security-opt", "seccomp:unconfined", "--security-opt", "apparmor:unconfined"]

    return ret

  def capabilitiesFor(self, obj):
    """
    Returns the capability list needed to run the given object.
    """
    ret = []

    # Allows ptrace
    ret += ["--cap-add", "SYS_PTRACE"]

    return ret

  def executeDockerCommand(self, command, cwd=None, stdout=None, stderr=None, stdin=None, wait=True, interactive=False, pty = False):
    Log.noisy("Invoking native command: %s %s" % ("(interactive)" if interactive else "", ' '.join(command)))

    parseToLog = False
    parseTo = None
    if stdout is not None and stdout != subprocess.PIPE and stdout != subprocess.DEVNULL:
      parseTo = stdout
      parseToLog = True
      stdout = subprocess.PIPE
      stderr = subprocess.PIPE

    if not interactive:
      if stdout is None:
        # Capture stdout and replay it on the log (as stderr)
        stdout = subprocess.PIPE
        parseToLog = True

      if stderr is None:
        # Capture stderr and replay it on the log
        stderr = subprocess.PIPE
        parseToLog = True

    host  = None
    guest = None
    old_tty = None

    def update_size(signum, frame):
      # Get the terminal size of the real terminal, set it on the pty
      try:
        buf = array.array('h', [0, 0, 0, 0])
        fcntl.ioctl(PTY.STDOUT_FILENO, termios.TIOCGWINSZ, buf, True)
        fcntl.ioctl(host, termios.TIOCSWINSZ, buf)

        # Pass along signal
        process.send_signal(signal.SIGWINCH)
      except:
        pass

    if pty:
      import array
      import termios
      import pty as PTY
      import tty
      import signal
      import fcntl

      # save original tty setting then set it to raw mode
      try:
        old_tty = termios.tcgetattr(sys.stdin)
        tty.setraw(sys.stdin.fileno())
      except:
        pass

      # open pseudo-terminal to interact with subprocess
      host, guest = PTY.openpty()

      stdout = guest
      stderr = guest
      stdin  = guest

      signal.signal(signal.SIGWINCH, update_size)

      update_size(signal.SIGWINCH, None)

    process = subprocess.Popen(command, cwd=cwd, stdin=stdin, stdout=stdout, stderr=stderr, start_new_session = pty)

    if pty and guest:
      os.close(guest)
    elif not interactive and process.stdin:
      process.stdin.close()

    utfDecoder = codecs.getreader('utf-8')

    totalRead = 0

    if wait:
      if parseToLog or pty:
        log = b""

        closed = []
        readers = []
        if pty:
          readers = [host, sys.stdin]
          closed.append(sys.stdin)
        else:
          stdout = process.stdout
          stderr = process.stderr
          readers = [stdout, stderr]

        while True:
          readable, writable, exceptional = select.select(readers, [], [])

          numRead = 0

          for reader in readable:
            read = None
            if reader is sys.stdin and host:
              d = os.read(sys.stdin.fileno(), 10240)
              os.write(host, d)
            elif host and reader is host:
              try:
                read = os.read(host, 10240)
              except OSError as e:
                pass

              if read:
                if parseTo is not None:
                  parseTo.write(read)
                  if parseTo is sys.stdout.buffer:
                    parseTo.flush()
                else:
                  os.write(sys.stdout.fileno(), read)

              if read is None or len(read) == 0:
                closed.append(reader)
                readers.remove(reader)
            else:
              read = reader.read(1)
              log += read
              if (read == b"\n"):
                if parseTo is not None:
                  parseTo.write(log)
                  if parseTo is sys.stdout.buffer:
                    parseTo.flush()
                else:
                  Log.external(log, 'docker')
                log = b""

              numRead += len(read)
              totalRead += len(read)

              if len(read) == 0:
                closed.append(reader)
                readers.remove(reader)

          if len(readable) == 0 and process.poll() is not None:
            break

          if numRead == 0 and len(closed) == 2:
            break

        if len(log) > 0:
          if parseTo is not None:
            parseTo.write(log)
          else:
            Log.external(log, 'docker')

      process.communicate()

      if pty and old_tty:
        # restore tty settings back
        try:
          termios.tcsetattr(sys.stdin, termios.TCSADRAIN, old_tty)
        except:
          pass

      return process.returncode
    else:
      return process

  def rootBasepath(self):
    """ Returns the root basepath for the docker filesystem.
    """

    return {
      "separator": "/",
      "mount": "/occam/{{ id }}-{{ revision }}",
      "cwd": "{{ paths.localMount }}/local",
      "taskLocal": "/home/occam/task",
      "local": "/home/occam/task/objects/{{ index }}",
    }
