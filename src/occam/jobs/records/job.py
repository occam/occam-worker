# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.databases.manager import table

@table('jobs')
class JobRecord:
  schema = {
    # Primary Key

    "id": {
      "type": "integer",
      "primary": True
    },

    # Status

    "status": {
      "type":    "string",
      "length":  10,
      "values":  ["started", "queued", "finished", "errored"],
      "default": "queued"
    },

    "kind": {
      "type":   "string",
      "length": 10,
      "values": ["build", "run"]
    },

    "priority": {
      "type": "integer",
      "default": 10
    },

    # Scheduler Metainformation

    "scheduler": {
      "type": "string",
      "length": 20
    },

    "scheduler_identifier": {
      "type": "string",
      "length": 128
    },

    # Path

    "path": {
      "type": "string",
      "length": 256
    },

    # Person Identifier

    "person_uid": {
      "type": "string",
      "length": 48
    },

    # Task Identifier

    "task_uid": {
      "type": "string",
      "length": 48
    },

    "task_revision": {
      "type": "string",
      "length": 128
    },

    "task_name": {
      "type": "string",
      "length": 128
    },

    "task_backend": {
      "type": "string",
      "length": 20
    },

    # Interactive?
    "interactive": {
      "type": "boolean",
      "default": False
    },

    # Time/Date

    "queue_time": {
      "type": "datetime"
    },

    "start_time": {
      "type": "datetime"
    },

    "finish_time": {
      "type": "datetime"
    },

    # Initialize (a component to call before the job starts)

    "initialize": {
      "type": "string",
      "length": 20
    },

    "initialize_tag": {
      "type": "string",
      "length": 48
    },

    # Finalize (a component to call after the job completes)

    "finalize": {
      "type": "string",
      "length": 20
    },

    "finalize_tag": {
      "type": "string",
      "length": 48
    },
  }
