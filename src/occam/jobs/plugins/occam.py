# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.jobs.manager import scheduler

@scheduler("occam")
class OccamScheduler:
  """ The native Occam scheduler.
  """

  def __init__(self, configuration):
    """ Initializes the driver.
    """

  def queue(self, command, outputPath):
    """ Queues the given occam command.

    The given outputPath is a filename of the log file for the task. It will
    be the occam command's stdout when invoked.
    """
