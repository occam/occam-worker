# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import shlex

from occam.config     import Config
from occam.log        import loggable

from occam.manager import manager, uses

from occam.storage.manager   import StorageManager
from occam.backends.manager  import BackendManager
from occam.system.manager    import SystemManager
from occam.notes.manager     import NoteManager
from occam.objects.manager   import ObjectManager
from occam.network.manager   import NetworkManager

from occam.objects.write_manager import ObjectWriteManager

@loggable
@uses(StorageManager)
@uses(BackendManager)
@uses(SystemManager)
@uses(NoteManager)
@uses(NetworkManager)
@uses(ObjectManager)
@uses(ObjectWriteManager)
@manager("jobs")
class JobManager:
  """ This OCCAM manager handles dispatching and controlling running tasks.
  """

  drivers = {}

  @staticmethod
  def register(adapter, driverClass):
    """ Adds a new scheduler driver to the possible drivers.
    """

    JobManager.drivers[adapter] = driverClass

  def driverFor(self, adapter):
    """ Returns an instance of a driver for the given scheduler backend.
    """

    if not adapter in self.drivers:
      # TODO: error?
      adapter = 'occam'

    return self.drivers[adapter](self.configuration)

  def __init__(self):
    """ Initialize the job manager.
    """

    self._driver = None

  def connect(self):
    """ Initializes a job scheduler connection.
    """

    # Determine the database type
    if self._driver is None:
      adapter = self.configuration.get('adapter', 'occam')

      # Attempt to find that database driver
      import importlib

      try:
        importlib.import_module("occam.jobs.plugins.%s" % (adapter))
      except ImportError:
        pass

      self._driver = self.driverFor(adapter)

    return self._driver

  def updateRunSection(self, task, runSection, root = None):
    """ Modifies the given task to replace the run section with the given run section.
    """
    if root is None:
      root = task

    # For this object, add its install paths to root
    if 'index' in task and task['index'] == root['runs']['index']:
      task['run'] = task.get('run', {})
      task['run'].update(runSection)

      if 'cwd' in runSection:
        task['paths']['cwd'] = runSection['cwd']

      return root

    for wire in task.get('inputs', []) + [{"connections": task.get('running', [{}])[0].get('process', [])}]:
      for inputObject in wire.get('connections', []):
        # Recursively find paths to subobjects
        self.updateRunSection(inputObject, runSection, root)

    return root

  def writeTask(self, task, taskPath):
    import json

    # Create an stdin file/socket to communicate to the object
    stdinPath = os.path.join(taskPath, "stdin")
    if not os.path.exists(stdinPath):
      with open(stdinPath, "w+") as f:
        pass

    # Output the whole task to path/object.json
    taskManifestPath = os.path.join(taskPath, "task", "object.json")

    if not os.path.exists(taskManifestPath):
      f = open(taskManifestPath, "w+")
      json.dump(task, f, indent=2, separators=(',', ': '))
      f.close()

  def writeNetwork(self, network, taskPath):
    import json

    # Output the whole task to path/object.json
    networkManifestPath = os.path.join(taskPath, "network.json")

    if not os.path.exists(networkManifestPath):
      f = open(networkManifestPath, "w+")
      json.dump(network, f, indent=2, separators=(',', ': '))
      f.close()

  def _deploy(self, task, taskPath, cwd=None, pathsFor=[], buildObject=None, buildPath=None, runSection = None, person = None, stdin = None, stdout = None, interactive=False):
    """ Deploys the given task.
    """

    # Ask the StorageManager for paths for each object at its revision
    paths = self.pathsForTask(task,
                              buildObject    = buildObject,
                              buildPath      = buildPath,
                              person         = person)

    rootPath = os.path.join(taskPath, "task", "root")
    self.installTask(rootPath, task, task, paths)

    for k, v in paths.items():
      for item in pathsFor:
        if item.get('id') == v.get('id') and item.get('revision') == v.get('revision'):
          v['path'] = item['path']

      if v.get('index') == task.get('runs', task.get('builds', {})).get('index'):
        if not cwd is None:
          v['localPath']  = cwd
          localMount  = task.get('runs', task.get('builds', {})).get('paths', {}).get('localMount')
          localMounts = task.get('runs', task.get('builds', {})).get('paths', {}).get('local')
          if len(localMounts) > 0:
            localMount = localMounts.get(task.get('provides').get('architecture'), {}).get(task.get('provides').get('environment'), localMount)
          v['localMount'] = os.path.join(localMount, "local")

        # Install stdin file if we have data
        if stdin is not None:
          runningTaskLocal = os.path.join(taskPath, "objects", str(v['index']))
          if not os.path.exists(runningTaskLocal):
            try:
              os.makedirs(runningTaskLocal)
            except OSError as exc:  # Python >2.5
              if exc.errno == errno.EEXIST and os.path.isdir(runningTaskLocal):
                pass
              else:
                raise
          stdinPath = os.path.join(runningTaskLocal, "stdin")
          with open(stdinPath, "w+") as f:
            f.write(stdin)

    paths['root'] = rootPath
    paths['task'] = os.path.join(taskPath, "task")
    paths['home'] = taskPath

    network = self.installNetwork(task)

    self.writeTask(task, taskPath)
    self.writeNetwork(network, taskPath)

    return (task, paths, network, stdout)

  def execute(self, task, paths, network, stdout, ignoreStdin = False):
    import time
    import datetime

    # Keep track of date
    date = datetime.datetime.utcnow().isoformat()

    taskPath = paths['home']

    # Get stdin stream
    stdin = None
    #if not ignoreStdin:
    #  stdinPath = os.path.join(taskPath, "stdin")
    #  stdin = open(stdinPath, "rb")

    # Run that task (keep track of time)
    elapsed = time.perf_counter()
    exitCode = self.backends.run(task, paths, network, stdout, stdin)
    elapsed = time.perf_counter() - elapsed

    self.uninstallNetwork(network)

    self.cleanUpTaskPath(task, taskPath)

    # Look at the run.json if it exists
    runReport = self.pullRunReport(task, taskPath)

    return {
      'time': elapsed,
      'date': date,
      'code': exitCode,
      'runReport': runReport,
      'paths': paths,
      'id': task.get('id')
    }

  def updateTaskWithRuntimeRequests(self, task, interactive, root = None):
    """ Inserts runtime tags into each provider in the task.

    The interactive parameter indicates that the task wants to run as an
    interactive process.

    The root parameter should be None as it is only used when the function
    recurses.
    """

    if root is None:
      root = task

    if interactive:
      task['interactive'] = interactive

    for subA in task.get('running', []):
      for subB in subA.get('process', []):
        if "provides" in subB and "run" in subB:
          self.updateTaskWithRuntimeRequests(subB, interactive, root)

  def deployBuild(self, task, revision=None, local=False, runSection=None, person=None, uuid = None, interactive=False):
    """ Deploys a build task.
    """

    import datetime
    import json

    originalTaskId = task.get('id')

    if uuid is None:
      from uuid import uuid1
      uuid = str(uuid1())

    task['id'] = uuid

    # TODO: instantiate a writable version of the object for the build
    #       delete that if it isn't local
    # TODO: record the run parameters and build information in the metadata
    taskPath = self.createTaskPath(task)
    self.updateTaskWithRuntimeRequests(task, interactive)

    taskRootPath = os.path.join(taskPath, "task")
    if not os.path.exists(taskRootPath):
      os.mkdir(taskRootPath)

    self.prepareTaskPath(task, taskRootPath, local=local, person=person)

    objectInfo = task.get('builds')
    if local:
      # Local objects get built inside the local directory they are in
      # They get placed in a ".occam/local" path.
      if not os.path.exists(os.path.join(os.getcwd(), ".occam")):
        os.mkdir(os.path.join(os.getcwd(), ".occam"))

      buildPath = os.path.join(os.getcwd(), ".occam", "local")
      if not os.path.exists(buildPath):
        os.mkdir(buildPath)

      # Write local copy of the build task
      localTaskPath  = os.path.join(buildPath, "object.json")
      with open(localTaskPath, "w+") as f:
        f.write(json.dumps(task))

      buildPath = os.path.join(buildPath, "build")
      if not os.path.exists(buildPath):
        os.mkdir(buildPath)

      deployInfo = self._deploy(task, taskPath, cwd=os.getcwd(), pathsFor=[
          {'id': objectInfo.get('id'), 'revision': objectInfo.get('revision'), 'path': os.getcwd()}
        ], buildPath=buildPath, runSection = runSection, person = person, interactive=interactive)
    else:
      owner_uid = self.notes.resolveOwner(objectInfo.get('uid'))
      buildPath  = self.storage.buildPathFor(owner_uid, revision=objectInfo.get('revision'), buildId=originalTaskId, create=True)
      deployInfo = self._deploy(task, taskPath, runSection = runSection, buildPath = buildPath, person = person, interactive=interactive)

    return deployInfo

  def deployRun(self, task, revision=None, arguments = None, command = None, local = False, runSection = None, person = None, stdin = None, stdout = None, uuid = None, taskPath = None, interactive=False, cwd=None):
    """ Deploys a run task.
    """

    if uuid is None:
      from uuid import uuid1
      uuid = str(uuid1())

    task['id'] = uuid

    runs_uuid = task.get('runs', {}).get('id')
    runs_revision = task.get('runs', {}).get('revision')

    runningObject = self.getObjectFromTask(task, runs_uuid, runs_revision)

    # Add arguments
    if runningObject is None:
      JobManager.Log.error("Task does not contain the object requested to run.")
      # TODO: raise exception
      return -1

    if command:
      runningObject['run'] = runningObject.get('run', {})
      runningObject['run']['command'] = shlex.split(command)

    if 'run' in runningObject and not arguments is None:
      runningObject['run']['arguments'] = arguments

      if 'command' in runningObject['run']:
        runningObject['run']['command'].extend(arguments)

    if not taskPath:
      taskPath = self.createTaskPath(task)

    if runSection:
      task = self.updateRunSection(task, runSection)

    self.updateTaskWithRuntimeRequests(task, interactive)

    taskRootPath = os.path.join(taskPath, "task")
    if not os.path.exists(taskRootPath):
      os.mkdir(taskRootPath)

    self.prepareTaskPath(task, taskRootPath, person=person)
    if local:
      # If we are building a local object, the built version goes in .occam/local
      # Therefore, mount that path for the running object

      if not os.path.exists(os.path.join(os.getcwd(), ".occam")):
        os.mkdir(os.path.join(os.getcwd(), ".occam"))

      buildPath = os.path.join(os.getcwd(), ".occam", "local")
      if not os.path.exists(buildPath):
        buildPath = None
      else:
        buildPath = os.path.join(buildPath, "build")
        if not os.path.exists(buildPath):
          buildPath = None

      ret = self._deploy(task, taskPath, cwd=os.getcwd(), buildPath=buildPath, runSection = runSection, person = person, stdin = stdin, stdout = stdout, interactive=interactive)
    else:
      ret = self._deploy(task, taskPath, cwd=cwd, runSection = runSection, person = person, stdin = stdin, stdout = stdout, interactive=interactive)

    return ret

  def pullRunReport(self, task, taskPath):
    """ Returns the array of run sections found within a run.json if it exists.
    """

    objectIndex = task.get('runs', task.get('builds', {}))['index']

    runReportPath = os.path.join(taskPath, "task", "objects", str(objectIndex), "run.json")

    if not os.path.exists(runReportPath):
      return []

    import json

    ret = []

    with open(runReportPath) as f:
      try:
        ret = json.load(f)
      except:
        ret = []

    return ret

  def create(self, task, taskId, revision, person=None, initialize=None, initializeTag=None, finalize=None, finalizeTag=None, interactive=False, existingPath=None):
    import datetime

    personId = None
    if person:
      personId = person.id

    kind = ""
    if 'builds' in task and 'type' in task and task['type'] == "task":
      kind = "build"
    elif 'type' in task and task['type'] == "task":
      kind = "run"

    # TODO: handle other schedulers
    return self.datastore.createJob(taskId = taskId or task.get('id'),
                                    taskBackend = task.get('backend', ''),
                                    taskName = task.get('name', "NoName"),
                                    taskRevision = revision,
                                    path = existingPath,
                                    initialize = initialize,
                                    initializeTag = initializeTag,
                                    finalize = finalize,
                                    finalizeTag = finalizeTag,
                                    scheduler = "occam",
                                    personId = personId,
                                    status = "queued",
                                    kind = kind,
                                    queueTime = datetime.datetime.now())

  def launchDaemon(self):
    """ Retrieves an instance of the daemon class.
    """

    from occam.jobs.daemon import JobDaemon
    JobDaemon().start()

  def retrieve(self, status = None, kind = None, task = None, job_id = None):
    """ Retrieves a list of JobRecord items for the given criteria.
    """

    return self.datastore.retrieveJobs(status = status, kind = kind, task_id = task, id = job_id)

  def pull(self):
    """ Atomically retrieves a job to run.

    This will pull a job while also marking it as 'started'.

    Returns:
      JobRecord: An available Job or None if no jobs are on the queue.
    """

    return self.datastore.pullJob()

  def finish(self, job):
    """ Marks the job as complete.
    """

    import datetime

    self.datastore.updateJob(job.id, status = "finished",
                                     finishTime = datetime.datetime.now())

  def failure(self, job_or_job_id):
    """ Marks the job as a failure.
    """

    import datetime

    job = self.jobFromId(job_or_job_id)

    self.datastore.updateJob(job.id, status = "failed",
                                     finishTime = datetime.datetime.now())

  def personFor(self, job):
    """ Gets the Person that queued this task.
    """

    person = None
    if job.person_uid:
      person = self.objects.retrieve(job.person_uid)
      person.roles = []

    return person

  def run(self, job, interactive=False):
    # Get who is running this task
    person = None
    if job.person_uid:
      person = self.objects.retrieve(job.person_uid)
      # # TODO: don't do this
      person.roles = ["administrator"]

    # Get the task itself
    taskObject = self.taskForJob(job, person)

    task = None
    if taskObject:
      task = self.objects.infoFor(taskObject)

    if task is None:
      # Error
      job.status = "failed"
      return -1

    taskPath = None
    if job.path:
      # Deploy within an existing path
      taskPath = os.path.realpath(os.path.join(job.path, ".."))

    # Deploy each phase
    task, paths, network, stdout = self.deploy(task,
                                               revision    = taskObject.revision,
                                               person      = person,
                                               taskPath    = taskPath,
                                               interactive = interactive)

    # Set the job's path
    if paths:
      self.updatePath(job, paths['task'])

      # Execute the task
      run = self.execute(task, paths, network, stdout)

      # Deploy secondary phases
      for runSection in run['runReport']:
        data = self.deploy(task,
                           revision    = taskObject.revision,
                           runSection  = runSection,
                           person      = person,
                           uuid        = run['id'],
                           taskPath    = taskPath,
                           interactive = interactive)
        run = self.execute(*data)

    return task, paths, network, stdout

  def deploy(self, task, revision, local=False, arguments = None, command = None, person = None, runSection = None, stdin = None, stdout = None, uuid = None, taskPath = None, interactive=False):
    """ Deploys the given task.

    Returns:
      int: The exit code of the deployed process.
    """

    # Determine if it is a build or run task.

    if not task.get('builds') is None:
      return self.deployBuild(task, revision=revision, local=local, runSection = runSection, person = person, interactive=interactive)
    elif not task.get('runs') is None:
      return self.deployRun(task, revision=revision, local=local, arguments = arguments, command = command, person = person, runSection = runSection, stdin = stdin, stdout = stdout, uuid = uuid, taskPath = taskPath, interactive=interactive)
    else:
      # We don't understand this task
      return [None, None, None, None]

  def signal(self, task, signal = 9):
    """ Submits the given signal to the running task.
    """

    if task is None:
      return None

    self.backends.kill(task, signal)

  def terminate(self, job):
    """ Terminates the given job.
    """

    task = self.taskInfoForJob(job.id)
    if task is None:
      return None
      # Have the backend kill the task
    ret = self.backends.terminate(task)
    if ret != 0:
      pid = self.pidForJob(job_id = job.id)
      if pid:
        import os
        import signal
        os.killpg(pid, signal.SIGKILL)
    self.failure(job.id)
    return 0

  def createTaskPath(self, task):
    """ Creates a path for the running task.
    """

    taskPath = self.createPathFor(task['id'], "runs")

    return taskPath

  def createPathFor(self, uuid, subPath='runs'):
    """ Creates a path to store an object with the given uuid.
    """

    if uuid is None:
      return None

    code = uuid
    code1 = code[0:2]
    code2 = code[2:4]

    path = self.configuration.get('paths', {}).get(subPath, os.path.join(Config.root(), subPath))

    if path is None:
      return None

    if not os.path.exists(path):
      os.mkdir(path)
    path = os.path.join(path, code1)
    if not os.path.exists(path):
      os.mkdir(path)
    path = os.path.join(path, code2)
    if not os.path.exists(path):
      os.mkdir(path)
    path = os.path.join(path, uuid)
    if not os.path.exists(path):
      os.mkdir(path)

    return path

  def cleanUpTaskPath(self, task, path, root=None):
    """
    """

    if root is None:
      root = task

    # Move stdout
    # Look at running task
    runningObject = root.get('runs', root.get('builds'))
    runningTaskLocal = os.path.join(path, "task", "objects", str(runningObject['index']))

    stdoutPath = os.path.join(runningTaskLocal, "stdout")
    stderrPath = os.path.join(runningTaskLocal, "stderr")

    for oldPath in [stdoutPath, stderrPath]:
      if os.path.exists(oldPath):
        i = 0
        while True:
          newPath = oldPath + ".%s" % (str(i))
          i += 1

          if not os.path.exists(newPath):
            break

        os.rename(oldPath, newPath)

  def prepareTaskPath(self, task, path, root=None, build=None, local=False, person=None):
    """
    """

    import json

    if build is None:
      build = 'builds' in task

    if root is None:
      root = task

    # Create a path that represents contents in root
    rootPath = os.path.join(path, "root")
    if not os.path.exists(rootPath):
      os.mkdir(rootPath)

    # Create a path that holds local data for the objects
    objectsPath = os.path.join(path, "objects")
    if not os.path.exists(objectsPath):
      os.mkdir(objectsPath)

    # Create an events file/socket to communicate any actions of objects
    eventsPath = os.path.join(path, "events")
    if not os.path.exists(eventsPath):
      with open(eventsPath, "w+") as f:
        pass

    # For this object, add it's object path
    if 'index' in task and ('run' in task or (build and task['index'] == root['builds']['index'])):
      objectPath = os.path.join(path, "objects", str(task['index']))
      if not os.path.exists(objectPath):
        os.mkdir(objectPath)

      inputsPath  = os.path.join(objectPath, "inputs")
      outputsPath = os.path.join(objectPath, "outputs")

      inputSymlinkPath  = os.path.join(objectPath, "input")
      outputSymlinkPath = os.path.join(objectPath, "output")

      localPath = os.path.join(objectPath, "local")

      # When this is a build task, there is also a place for
      # built version of objects to go.
      if build and task['index'] == root['builds']['index']:
        buildPath = os.path.join(objectPath, "build")

        if not os.path.exists(buildPath):
          os.mkdir(buildPath)

        # Instantiate the object in the local path
        if not local:
          self.storage.instantiate(task['uid'], task['revision'], localPath)
          buildObject = self.objects.retrieve(task['id'], revision=task['revision'], person = person)

          self.objects.install(buildObject, localPath, build = True)

      if not os.path.exists(inputsPath):
        os.mkdir(inputsPath)

      if not os.path.exists(outputsPath):
        os.mkdir(outputsPath)

      if not os.path.exists(localPath):
        os.mkdir(localPath)

      for idx, outputInfo in enumerate(task.get('outputs', [])):
        subOutputPath = os.path.join(outputsPath, str(idx))

        if not os.path.exists(subOutputPath):
          os.mkdir(subOutputPath)

        # Allow for at least one output
        subSubOutputPath = os.path.join(subOutputPath, "0")

        if not os.path.exists(subSubOutputPath):
          os.mkdir(subSubOutputPath)

        #for outputIndex, outputInfo in enumerate(wire):
        #  subSubOutputPath = os.path.join(subOutputPath, str(outputIndex))
        #  if not os.path.exists(subSubOutputPath):
        #    os.mkdir(subSubOutputPath)

      # Link inputs to their object contents when applicable
      for idx, wire in enumerate(task.get('inputs', [])):
        subInputPath = os.path.join(inputsPath, str(idx))

        if not os.path.exists(subInputPath):
          os.mkdir(subInputPath)

        for inputIndex, inputInfo in enumerate(wire.get('connections', [])):
          subSubInputPath = os.path.join(subInputPath, str(inputIndex))

          if inputInfo.get('id') and inputInfo.get('revision') and inputInfo.get('paths'):
            if not os.path.lexists(subSubInputPath):
              os.symlink(inputInfo['paths'].get('mount'), subSubInputPath)
          elif not os.path.exists(subSubInputPath):
            os.mkdir(subSubInputPath)

      # Link ../input to ../inputs/0/0 as a convenience
      if not os.path.lexists(inputSymlinkPath):
        os.symlink(os.path.join("inputs", "0", "0"), inputSymlinkPath)

      # Link ../output to ../outputs/0/0 as a convenience
      if not os.path.lexists(outputSymlinkPath):
        os.symlink(os.path.join("outputs", "0", "0"), outputSymlinkPath)

      # TODO: link inputs to output paths when appropriate

      subTaskManifestPath = os.path.join(objectPath, "task.json")

      # Place the task component into this path
      f = open(subTaskManifestPath, "w+")
      json.dump(task, f, indent=2, separators=(',', ': '))
      f.close()

    # For each object, create the directory it runs in
    for wire in task.get('inputs', []) + [{'connections': task.get('running', [{}])[0].get('process', [])}]:
      for obj in wire.get('connections', []):
        self.prepareTaskPath(obj, path, root=root, build=build, local=local, person = person)

  def pathsForTask(self, task, buildObject=None, root=None, create=False, buildPath=None, person=None):
    """ Returns the paths to each object given in a task.
    """

    if root is None:
      root = task

    ret = {}

    for wire in task.get('inputs', []) + [{"connections": task.get('running', [{}])[0].get('process', [])}]:
      for inputObject in wire.get('connections', []):
        if 'index' in inputObject:
          id = inputObject.get('id')
          owner_id = inputObject.get('owner', {}).get('id', id)
          revision = inputObject.get('revision')
          buildId  = inputObject.get('buildId')
          obj = self.objects.retrieve(id = owner_id, revision = revision, person = person)
          #ownerInfo = inputObject.get('owner', self.objects.infoFor(obj))
          ownerInfo = {}

          # This determines if the build should be a thing or not
          # Local builds (and subsequent runs) mess this up so much!

          # TODO: use this when it is a local run!
          built = not inputObject.get('index') == root.get('builds', root.get('runs', {})).get('index') and 'build' in ownerInfo
          #built = not inputObject.get('index') == root.get('builds', {}).get('index') and 'build' in ownerInfo

          path = None
          if not obj is None:
            if built:
              # Discover the built version of this object (or build it?? or report that we need to build it??)
              buildIds = self.notes.retrieveBuildIds(owner_id, revision)

              # For every build we know, we attempt to find that build's content
              if buildIds:
                for build in buildIds:
                  build = build["id"]
                  path = self.objects.buildPathFor(obj, build)

                  if path is not None:
                    buildId = build
                    break
            elif not buildId is None:
              path = self.objects.buildPathFor(obj, buildId)
            else:
              path = self.objects.instantiate(obj)

          if path is None:
            JobManager.Log.warning("hmm can't find an object %s %s %s" % (owner_id, revision, buildId))
            raise Exception("failure")

          mountPath  = inputObject.get('paths', {}).get('mount')
          mountPaths = inputObject.get('paths', {}).get('volume', {})

          if mountPaths:
            mountPath = mountPaths.get(root.get('provides').get('architecture'), {}).get(root.get('provides').get('environment'), mountPath)

          pathInfo = {
            "id": owner_id,
            "revision": revision,
            "index": inputObject.get('index'),
            "mount": mountPath,
            "path": path
          }

          if 'builds' in root:
            # This is a build task.
            if root['builds'].get('index') == inputObject.get('index'):
              localMount  = root.get('builds', {}).get('paths', {}).get('localMount')
              localMounts = root.get('builds', {}).get('paths', {}).get('local')
              if len(localMounts) > 0:
                localMount = localMounts.get(task.get('provides').get('architecture'), {}).get(task.get('provides').get('environment'), localMount)
              localPath = localMount
              localPath = os.path.join(localPath, "build")

              pathInfo["buildMount"] = localPath
              if buildPath is None:
                # Object would have already been discovered?
                # TODO: Assess the truth of the above haha
                buildPath = self.storage.buildPathFor(owner_id, revision=revision, buildId=root.get('id'), create=True)

              pathInfo["buildPath"] = buildPath
          elif 'runs' in root:
            # This is a run task
            if root['runs'].get('index') == inputObject.get('index'):
              if not buildPath is None:
                pathInfo["path"] = buildPath

          ret[inputObject.get('index')] = pathInfo

          # Recursively find paths to subobjects
          ret.update(self.pathsForTask(inputObject, buildObject=buildObject, root=root, create=create, buildPath=buildPath, person = person))

    return ret

  def installNetwork(self, task, root = None, output = None):
    """ Instantiates the network knowledge for this object.

    This will allocate ports when necessary.
    """

    if root is None:
      root = task

    if output is None:
      output = {
        "ports": []
      }

    # Look for a network section
    if 'network' in task:
      for port in task['network'].get('ports', []):
        if 'bind' in port:
          systemPort = self.network.allocatePort();
          JobManager.Log.write("allocating system port %s to %s" % (systemPort, port.get('bind', 0)))
          port['port'] = systemPort
          output['ports'].append(port)

    for wire in task.get('inputs', []) + [{'connections': task.get('running', [{}])[0].get('process', [])}]:
      for inputObject in wire.get('connections', []):
        # Recursively install subobjects
        self.installNetwork(inputObject, root, output)

    return output

  def uninstallNetwork(self, network):
    """ Unallocates resources allocated by installNetwork.
    """

    for portInfo in network.get('ports', []):
      if 'port' in portInfo:
        systemPort = portInfo['port']
        JobManager.Log.write("freeing system port %s" % (systemPort))
        self.network.freePort(systemPort)

  def installTask(self, rootPath, task, root=None, paths={}):
    """
    """

    if root is None:
      root = task

    # For this object, add its install paths to root
    if 'init' in task and 'index' in task:
      pathInfo = paths.get(task.get('index'))

      if pathInfo:
        JobManager.Log.noisy("Instantiating %s %s %s@%s" % (task.get('type', 'object'), task.get('name', 'unnamed'), task['id'], task.get('version', task['revision'])))
        # For each install object that is a local path, symlink to its mount path
        if not ("builds" in root and root["builds"]["id"] == task["id"] and root["builds"]["revision"] == task["revision"]):
          mountPath  = task.get('paths', {}).get('mount')
          mountPaths = task.get('paths', {}).get('volume', {})

          if len(mountPaths) > 0:
            mountPath = mountPaths.get(root.get('provides').get('architecture'), {}).get(root.get('provides').get('environment'), mountPath)

          # Link files into the system
          for linkInfo in task['init'].get('link', []):
            # TODO: check if this is a resource (it has an 'id' and 'revision')
            # this would be added as a dependency.

            if not 'id' in linkInfo:
              installPath = os.path.join(mountPath, linkInfo.get('source'))
              installDestination = linkInfo.get('to')
              localPath = os.path.join(pathInfo['path'], linkInfo.get('source'))

              # Create symlinks for each item in the task
              self.linkTaskDirectory(rootPath, localPath, installPath, installDestination)

          # Copy files into the system
          for copyInfo in task['init'].get('copy', []):
            if not 'id' in copyInfo:
              installPath = os.path.join(mountPath, copyInfo.get('source'))
              installDestination = copyInfo.get('to')
              localPath = os.path.join(pathInfo['path'], copyInfo.get('source'))

              # Create symlinks for each item in the task
              self.copyTaskDirectory(rootPath, localPath, installPath, installDestination)

    for wire in task.get('inputs', []) + [{'connections': task.get('running', [{}])[0].get('process', [])}]:
      for inputObject in wire.get('connections', []):
        # Recursively find paths to subobjects
        self.installTask(rootPath, inputObject, root, paths)

  def copyTaskDirectory(self, rootPath, localPath, mountPath, destinationPath, recurse=True):
    """ Copies the files from the local path to the given destination.
    """

    # Ensure that the destination path exists
    localDestinationPath = os.path.join(rootPath, destinationPath[1:])

    import errno
    import shutil

    #try:
    if os.path.exists(localPath):
      if os.path.isdir(localPath):
        files = os.listdir(localPath)

        if os.path.lexists(localDestinationPath) and os.path.islink(localDestinationPath):
          os.remove(localDestinationPath)

        if not os.path.exists(localDestinationPath):
          try:
            os.makedirs(localDestinationPath)
          except OSError as exc:  # Python >2.5
            if exc.errno == errno.EEXIST and os.path.isdir(localDestinationPath):
              pass
            else:
              raise

        for filename in files:
          copyDestination = os.path.join(localDestinationPath, filename)
          copySource = os.path.join(localPath, filename)
          if os.path.isdir(copySource):
            if recurse:
              self.copyTaskDirectory(rootPath, copySource, None, os.path.join(destinationPath, filename))
          else:
            if os.path.lexists(copyDestination):
              os.remove(copyDestination)
            shutil.copy(copySource, copyDestination)
      else:
        copyDestination = localDestinationPath
        copySource = localPath

        try:
          os.makedirs(os.path.dirname(localDestinationPath))
        except FileExistsError:
          pass

        shutil.copy(copySource, copyDestination)
    #except FileNotFoundError as e:
    #  JobManager.Log.error("Could not link files within the object to the VM filesystem: %s" % (e.strerror))
    #  return None

  def linkTaskDirectory(self, rootPath, localPath, mountPath, destinationPath, recurse=True):
    """ Creates symlinks at destination that point to the mounted paths by walking the
    localPath.

    The localPath gives a local view on what will be seen inside the
    virtual machine. We can duplicate that structure and point them to each file
    with a path relative to the virtual machine (mountPath)
    """

    # Ensure that the destination path exists
    localDestinationPath = os.path.join(rootPath, destinationPath[1:])

    import errno

    #try:
    if os.path.exists(localPath):
      if not os.path.islink(localPath) and os.path.isdir(localPath):
        files = os.listdir(localPath)

        if os.path.lexists(localDestinationPath) and os.path.islink(localDestinationPath):
          os.remove(localDestinationPath)

        if not os.path.exists(localDestinationPath):
          try:
            os.makedirs(localDestinationPath)
          except OSError as exc:  # Python >2.5
            if exc.errno == errno.EEXIST and os.path.isdir(localDestinationPath):
              pass
            else:
              raise

        for filename in files:
          localSymlink = os.path.join(localDestinationPath, filename)
          localFilePath = os.path.join(localPath, filename)

          # We always replace the symlink with a new one
          if os.path.lexists(localSymlink) and os.path.islink(localSymlink):
            os.remove(localSymlink)

          # If the file to copy in is a directory, then override any link
          # and then recurse into that directory
          if not os.path.islink(localFilePath) and os.path.isdir(localFilePath):
            # TODO: intelligently detect when we can just symlink the directory
            if recurse:
              self.linkTaskDirectory(rootPath, os.path.join(localPath, filename), os.path.join(mountPath, filename), os.path.join(destinationPath, filename))
          else:
            destination = os.path.join(mountPath, filename)

            # We won't replace a directory with a symlink
            if not os.path.exists(localSymlink):
              os.symlink(destination, localSymlink)
      else:
        destination  = mountPath
        localSymlink = localDestinationPath

        try:
          os.makedirs(os.path.dirname(localDestinationPath))
        except FileExistsError:
          pass

        if not os.path.lexists(localSymlink):
          os.symlink(destination, localSymlink)
    #except FileNotFoundError as e:
    #  JobManager.Log.error("Could not link files within the object to the VM filesystem: %s" % (e.strerror))
    #  return None

  def getObjectFromTask(self, task, uuid, revision):
    """ Finds the first instance of the given object in the task.
    """

    for obj in task.get('inputs', []) + task.get('running', [{}])[0].get('process', []):
      if obj.get('id') == uuid and obj.get('revision') == revision:
        return obj
      else:
        ret = self.getObjectFromTask(obj, uuid, revision)
        if not ret is None:
          return ret

    return None

  def jobFromId(self, job_or_job_id):
    if isinstance(job_or_job_id, int) or isinstance(job_or_job_id, str):
      jobs = self.retrieve(job_id = job_or_job_id)

      job = None
      if len(jobs) != 0:
        return jobs[0]
    else:
      return job_or_job_id

    return None

  def taskForJob(self, job_or_job_id, person = None):
    """ Returns the task for the given job as the given Person.
    """

    job = self.jobFromId(job_or_job_id)

    if job is None:
      return None

    # Get the task itself
    uuid       = job.task_uid
    revision   = job.task_revision
    return self.objects.retrieve(uuid, revision=revision, person=person)

  def networkInfoFileForJob(self, job_or_job_id):
    """ Returns the stream for the network manifest for the given job, if it exists.
    """

    job = self.jobFromId(job_or_job_id)

    if job is None:
      return None

    if not job.path:
      return None

    networkInfoPath = os.path.join(job.path, "..", "network.json")

    if not os.path.exists(networkInfoPath):
      return None

    return open(networkInfoPath, "rb")

  def networkInfoForJob(self, job_or_job_id):
    """ Returns the instantiated network manifest for the running job, if it exists.
    """

    import json

    f = self.networkInfoFileForJob(job_or_job_id)

    networkInfo = {}

    try:
      networkInfo = json.load(f)
    except:
      pass

    return networkInfo

  def taskInfoFileForJob(self, job_or_job_id):
    """ Returns the stream for the task manifest for the given job, if it exists.
    """

    job = self.jobFromId(job_or_job_id)

    if job is None:
      return None

    if not job.path:
      return None

    taskObjectPath = os.path.join(job.path, "object.json")

    if not os.path.exists(taskObjectPath):
      return None

    return open(taskObjectPath, "rb")

  def taskInfoForJob(self, job_or_job_id):
    """ Returns the instantiated task for the running job, if it exists.
    """

    import json
    import codecs
    from collections import OrderedDict

    f = self.taskInfoFileForJob(job_or_job_id)

    taskInfo = {}

    try:
      taskInfo = json.loads(codecs.decode(f.read(), 'utf8'), object_pairs_hook=OrderedDict)
    except:
      pass

    return taskInfo

  def taskIdForJob(self, job_or_job_id):
    """ Returns the instantiated id for the running job, if it exists.
    """

    taskInfo = self.taskInfoForJob(job_or_job_id)

    if taskInfo is None:
      return None

    return taskInfo.get('id')

  def pidForJob(self, job_id):
    """ Returns the process id for the given job, if it is running.

    Returns:
      process_id (int): The process id for the job runner or None if the job is
                        not running.
    """

    metadata = self.metadataFor(job_id)

    if metadata is None:
      return None

    # Get the running job's process id
    pid = metadata.get("pid")

    # Get the command name for the given process
    cmd_check = None
    try:
      with open(os.path.join("/", "proc", str(pid), "cmdline")) as f:
        cmd_check = f.read()
    except:
      pass

    # We use endswith since it may append the full path to the initial process
    # name, which we may not know
    if cmd_check is None or metadata is None or not metadata.get("check", "").endswith(cmd_check):
      return None

    return pid

  def jobRunning(self, job_id):
    """ Returns True when the given job is current running.
    """

    # Check the metadata file for the pid and cmd name
    # Check for the presence of the pid with the cmd name
    # Return True if that checks out

    return self.pidForJob(job_id) is not None

  def updatePath(self, job, path):
    """ Updates the path of the given job.
    """

    self.datastore.updateJob(job.id, path = path)

    return job

  def metadataFor(self, job_id):
    """ Returns the metadata for the given job.
    """

    import json
    from occam.config import Config

    basePath = Config.root()

    try:
      ret = json.loads(open(os.path.join(basePath, "job-daemon", "%s.json" % (job_id)), "r").read())
    except:
      ret = None

    return ret

  def eventLogFor(self, job_id):
    rows = self.retrieve(job_id = job_id)
    if len(rows) == 0:
      return None

    job = rows[0]
    if not job.path:
      return None

    eventPath = os.path.join(job.path, "events")
    if not os.path.exists(eventPath):
      return None

    return open(eventPath, "rb")

  def writeTo(self, job_id, data, person):
    """ Writes the given data to the given job.
    """

    pid = self.pidForJob(job_id)

    if not pid:
      return None

    with open("/proc/%s/fd/0" % (pid), "wb") as f:
      f.write(data)

  def logPathFor(self, job_id):
    from occam.config import Config

    basePath = Config.root()

    return os.path.join(basePath, "job-daemon", "%s.log" % (job_id))

  def logFor(self, job_id):
    from occam.config import Config

    basePath = Config.root()

    try:
      ret = open(self.logPathFor(job_id), "rb")
    except:
      ret = None

    return ret

  def outputsFor(self, job, wireIndex = None, itemIndex = None):
    """ Retrieves a list of JobOutputRecord objects for the given job.
    """

    return self.datastore.retrieveJobOutputs(job.id, wireIndex  = wireIndex,
                                                     itemIndex  = itemIndex)

  def outputsForAll(self, jobIds, wireIndex = None, itemIndex = None, finishTime = None):
    """ Retrieves a list of JobOutputRecord objects for the given list of jobs.
    """

    jobs = self.datastore.retrieveJobOutputsForAll(jobIds,
                                                   wireIndex  = wireIndex,
                                                   itemIndex  = itemIndex,
                                                   finishTime = finishTime)

    # Organize into a list of lists
    ret = []

    lastJobId = None
    for jobOutput in jobs:
      if jobOutput.job_id != lastJobId:
        ret.append([])
        lastJobId = jobOutput.job_id

      ret[-1].append(jobOutput)

    return ret

  def createJobOutput(self, job, object, wireIndex, itemIndex):
    """ Adds a record of the output object for this job.

    Args:
      job (JobRecord): The job that produced the output.
      object (Object): The object that was produced and stored.
      wireIndex (int): The index of the output wire.
      itemIndex (int): The position of the item on that wire.

    Returns:
      JobOutputRecord: The saved record.
    """

    JobManager.Log.write("Adding job record for %s %s %s" % (job.id, wireIndex, itemIndex))

    return self.datastore.createJobOutput(id             = job.id,
                                          objectId       = object.id,
                                          objectRevision = object.revision,
                                          wireIndex      = wireIndex,
                                          itemIndex      = itemIndex)

  def rakeOutput(self, task, taskPath, generators = None, root = None, identity=None, job=None):
    """ Given a run report produced by a call to deploy, gather the generated output.
    """

    # Look at the possible outputs

    if root is None:
      root = task

    if 'generator' in task:
      if generators is None:
        generators = []

      generators.append(task['generator'])

    outputs = []

    for wire in task.get('inputs', []) + [{"connections": task.get('running', [{}])[0].get('process', [])}]:
      for inputObject in wire.get('connections', []):
        if 'index' in inputObject:
          outputs.extend(self.rakeOutput(inputObject, taskPath, generators=generators, root=root, identity=identity, job=job))


    if 'index' in task and 'outputs' in task:
      objectPath = os.path.join(taskPath, "task", "objects", str(task['index']))
      for wireIndex, wire in enumerate(task.get('outputs', [])):
        outputPath = os.path.join(objectPath, "outputs", str(wireIndex))

        # Create DB entry for output of the node pin
        new_output={}
        new_output["output_index"] = wireIndex
        new_output["output_count"] = 0
        new_output["objects"]=[]

        # Look for the existence of any new outputs
        # Remember, the first one is assumed, but if it is empty, ignore
        # Ignore all empty outputs
        i = 0
        while os.path.exists(os.path.join(outputPath, str(i))):
          subOutputPath = os.path.join(outputPath, str(i))
          itemIndex = i
          i += 1

          # Is this directory empty?
          if len(os.listdir(subOutputPath)) == 0:
            continue

          # Rake this object
          outputInfo = wire.copy()

          subOutputJSON = os.path.join(subOutputPath, "object.json")
          if os.path.exists(subOutputJSON):
            definedInfo = {}
            try:
              with open(subOutputJSON, "r") as f:
                import json
                definedInfo = json.load(f)
            except:
              JobManager.Log.warning("Could not open the object.json of the next output.")
              definedInfo = {}

            outputInfo.update(definedInfo)

          # Set defaults
          outputInfo['type'] = outputInfo.get('type', 'object')
          outputInfo['name'] = outputInfo.get('name', 'output')

          # Handle schema
          if 'schema' in wire and outputInfo['schema'] == wire['schema'] and isinstance(wire['schema'], str):
            outputInfo['schema'] = {
              "id": task['id'],
              "name": wire.get('name'),
              "type": "application",
              "subtype": "application/json",
              "revision": task['revision'],
              "file": wire['schema']
            }

          # Add generator relation (The generator is the object, and any passed along)
          outputInfo['generator'] = [{
            "name": task.get('name'),
            "type": task.get('type'),
            "id":   task.get('id'),
            "uid":  task.get('uid'),
            "revision": task.get('revision')
          }]

          for generator in (generators or []):
            outputInfo['generator'].append(generator)

          # Add
          JobManager.Log.write("Raking %s %s" % (outputInfo['type'], outputInfo['name']))

          outputObject = self.objects.write.create(name        = outputInfo['name'],
                                                   object_type = outputInfo['type'],
                                                   subtype     = outputInfo.get('subtype'),
                                                   path        = subOutputPath,
                                                   info        = outputInfo,
                                                   createPath  = False)
          self.objects.write.store(outputObject, identity)

          if job:
            # Add the output records
            self.createJobOutput(job, outputObject, wireIndex, itemIndex)

          # Add note for each generator
          for generator in outputInfo['generator']:
            self.addObjectToOutputs(outputObject, generator)

          new_output["output_count"] += 1
          out_object = {}
          out_object["output_index"] = wireIndex
          out_object["object_id"] = outputObject.id
          out_object["object_uid"] = outputObject.uid
          out_object["object_revision"] = outputObject.revision
          new_output["objects"].append(out_object)
        outputs.append(new_output)
    return outputs

  def addObjectToOutputs(self, object, recipient):
    objectInfo = self.objects.infoFor(object)
    self.notes.store(
      recipient.get('id'),
      'outputs',
      key="object",
      value={
        "name":     objectInfo['name'],
        "type":     objectInfo['type'],
        "id":       object.id,
        "uid":      object.uid,
        "revision": object.revision,
      },
      revision=recipient.get('revision'),
      append=True)
    return

def scheduler(name: str):

  """ This decorator will register a possible scheduler backend.
  """

  def _scheduler(cls):
    JobManager.register(name, cls)
    return cls

  return _scheduler
