# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sql
import sql.aggregate

from occam.log import loggable
from occam.databases.manager import uses, datastore

@loggable
@datastore("jobs")
class JobDatabase:
  """ Manages the database interactions for job management.
  """

  def queryJobs(self, id=None, ids=None, status=None, kind=None, task_id=None, finishTime=None, key=None):
    """ Returns a instantiated query object for retrieving job records.

    Args:
      id (str): The identifier for the job.
      ids (list): A list of identifiers for jobs.
      status (str): The job status to look for.
      kind (str): The registered job 'kind', for instance 'build'.
      task_id (str): The object ID for the running task.

    Returns:
      sql.Query: The query to be executed.
    """

    jobs = sql.Table("jobs")
    if key:
      query = jobs.select(jobs.__getattr__(key))
    else:
      query = jobs.select()

    query.where = sql.Literal(True)

    if id is not None:
      query.where = query.where & (jobs.id == id)

    if ids is not None:
      query.where = query.where & (jobs.id.in_(ids))

    if task_id is not None:
      query.where = query.where & (jobs.task_uid == task_id)

    if kind is not None:
      query.where = query.where & (jobs.kind == kind)

    if status is not None:
      query.where = query.where & (jobs.status == status)

    if finishTime is not None:
      query.where = query.where & (jobs.finish_time < finishTime)

    return query

  def retrieveJobs(self, id=None, status=None, kind=None, task_id=None):
    """ Returns a set of JobRecord objects that match the given criteria.

    Args:
      id (str): The identifier for the job.
      status (str): The job status to look for.
      kind (str): The registered job 'kind', for instance 'build'.
      task_id (str): The object ID for the running task.

    Returns:
      list: A set of JobRecord objects.
    """

    # One option must be used
    if id is None and status is None and kind is None and task_id is None:
      raise ValueError

    # Create a session
    from occam.jobs.records.job import JobRecord
    session = self.database.session()

    # Retrieve the query
    query = self.queryJobs(id, status=status, kind=kind, task_id=task_id)

    # Execute the query
    self.database.execute(session, query)

    # Return the result
    return [JobRecord(x) for x in self.database.many(session, size=100)]

  def pullJob(self):
    """ Will atomically return a JobRecord for the next job to run.

    This will pull a job while also marking it as 'started'.

    Returns:
      JobRecord: An available Job or None if no jobs are on the queue.
    """

    import sql
    import datetime

    from occam.jobs.records.job import JobRecord

    jobs = sql.Table('jobs')

    session = self.database.session()

    # Find queued jobs with no dependencies
    query = jobs.select()
    query.where = (jobs.status == "queued")

    self.database.execute(session, query)

    rows = self.database.many(session)
    # For each one, attempt to switch the status
    # If successful, then return that job
    for row in rows:
      row = JobRecord(row)
      row.status = "started"
      row.start_time = datetime.datetime.today()

      # Update the job's db row
      query = jobs.update(where   = (jobs.status == "queued") & (jobs.id == row.id),
                          columns = [jobs.status, jobs.start_time],
                          values  = [row.status, row.start_time])

      # Count the number of updated rows (should be 1)
      count = self.database.execute(session, query)
      self.database.commit(session)

      # If count was 0, then it didn't update, otherwise, return the job:
      if count == 1:
        # Successfully updated the job record
        # Return the record
        row.status = "started"
        return row

    # If there are no jobs, then return None
    return None

  def updateJob(self, id, path=None, status=None, finishTime=None):
    """ Updates the given job record.

    Args:
      id (str): The job identifier.
      path (str): When not None, updates the job path.

    Returns:
      bool: True if the record was updated and False otherwise.
    """

    import sql
    jobs = sql.Table("jobs")

    # Create a session
    session = self.database.session()

    # Set up columns and values tuples
    cols = []
    vals = []

    if path is not None:
      cols.append(jobs.path)
      vals.append(path)

    if status is not None:
      cols.append(jobs.status)
      vals.append(status)

    if finishTime is not None:
      cols.append(jobs.finish_time)
      vals.append(finishTime)

    query = jobs.update(where = (jobs.id == id), columns = cols, values = vals)

    count = self.database.execute(session, query)
    self.database.commit(session)

    if count == 1:
      return True

    # Did not update
    return False

  def createJob(self, taskBackend=None,
                      taskName=None,
                      taskId=None,
                      taskRevision=None,
                      initialize=None,
                      initializeTag=None,
                      finalize=None,
                      finalizeTag = None,
                      scheduler = None,
                      personId = None,
                      status = None,
                      kind = None,
                      queueTime = None,
                      path = None):
    from occam.jobs.records.job import JobRecord

    # Create a session
    session = self.database.session()

    # Create an empty record
    job = JobRecord()

    # Start filling out the record
    if taskBackend:
      job.task_backend = taskBackend

    if taskName:
      job.task_name = taskName

    if taskRevision:
      job.task_revision = taskRevision

    if taskId:
      job.task_uid = taskId

    if path:
      job.path = path

    if scheduler:
      job.scheduler = scheduler

    if personId:
      job.person_uid = personId

    if status:
      job.status = status

    if kind:
      job.kind = kind

    if queueTime:
      job.queue_time = queueTime

    if initialize:
      job.initialize = initialize

    if initializeTag:
      job.initialize_tag = initializeTag

    if finalize:
      job.finalize = finalize

    if finalizeTag:
      job.finalize_tag = finalizeTag

    # Submit the record
    self.database.update(session, job)
    self.database.commit(session)

    return job

  def createJobOutput(self, id, objectId, objectRevision, wireIndex, itemIndex):
    """ Adds a record of the output object for this job.

    Args:
      id (str): The identifier for the job that produced the output.
      objectId (str): The id for the object that was produced and stored.
      objectRevision (str): The revision for the object that was produced and stored.
      wireIndex (int): The index of the output wire.
      itemIndex (int): The position of the item on that wire.

    Returns:
      JobOutputRecord: The saved record.
    """
    # Create a session
    from occam.jobs.records.job_output import JobOutputRecord
    session = self.database.session()

    record = JobOutputRecord()

    record.job_id            = id
    record.object_uid        = objectId
    record.object_revision   = objectRevision
    record.output_index      = wireIndex
    record.output_item_index = itemIndex

    # Submit the record
    self.database.update(session, record)
    self.database.commit(session)

    return record

  def queryJobOutputs(self, id, wireIndex=None, itemIndex=None):
    """ Retrieves a query to find all known outputs for the given job.

    Args:
      id (str): The identifier of the job to query.
      wireIndex (int): The index of the wire to filter.
      itemIndex (int): The index of the item to filter.

    Returns:
      sql.Query: The respective query.
    """

    outputs = sql.Table("job_outputs")

    query = outputs.select()
    query.where = (outputs.job_id == id)

    if itemIndex:
      query.where = query.where & (outputs.output_item_index == itemIndex)

    if wireIndex:
      query.where = query.where & (outputs.output_index == wireIndex)

    return query

  def retrieveJobOutputs(self, id, wireIndex=None, itemIndex=None):
    """ Retrieves all known outputs for the given job.

    Args:
      id (str): The identifier of the job to query.
      wireIndex (int): The index of the wire to filter.
      itemIndex (int): The index of the item to filter.

    Returns:
      list: A list of JobOutputRecord objects.
    """

    # Create a session
    from occam.jobs.records.job_output import JobOutputRecord
    session = self.database.session()

    # Retrieve the query
    query = self.queryJobOutputs(id, wireIndex = wireIndex,
                                     itemIndex = itemIndex)

    # Execute the query
    self.database.execute(session, query)

    # Return the result
    return [JobOutputRecord(x) for x in self.database.many(session, size=100)]

  def queryJobOutputsForAll(self, ids, wireIndex=None, itemIndex=None, finishTime=None):
    """ Retrieves a query to find all known outputs for the given job.

    Args:
      ids (list): A list of identifiers for the jobs to query.
      wireIndex (int): The index of the wire to filter.
      itemIndex (int): The index of the item to filter.

    Returns:
      sql.Query: The respective query.
    """

    outputs = sql.Table("job_outputs")

    query = outputs.select(order_by = sql.Asc(outputs.job_id))

    if finishTime:
      subQuery = self.queryJobs(ids = ids, finishTime = finishTime, key = "id")
      query.where = (outputs.job_id.in_(subQuery))
    else:
      query.where = (outputs.job_id.in_(ids))

    if itemIndex:
      query.where = query.where & (outputs.output_item_index == itemIndex)

    if wireIndex:
      query.where = query.where & (outputs.output_index == wireIndex)

    return query

  def retrieveJobOutputsForAll(self, ids, wireIndex=None, itemIndex=None, finishTime=None):
    """ Retrieves all known outputs for the given set of jobs.

    Args:
      ids (list): A list of identifiers for the jobs to query.
      wireIndex (int): The index of the wire to filter.
      itemIndex (int): The index of the item to filter.

    Returns:
      list: A list of JobOutputRecord objects.
    """

    # Create a session
    from occam.jobs.records.job_output import JobOutputRecord
    session = self.database.session()

    # Retrieve the query
    query = self.queryJobOutputsForAll(ids, wireIndex  = wireIndex,
                                            itemIndex  = itemIndex,
                                            finishTime = finishTime)

    # Execute the query
    self.database.execute(session, query)

    # Return the result
    return [JobOutputRecord(x) for x in self.database.many(session, size=100)]
