# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import datetime

from occam.log import Log

from occam.commands.manager import command, option, argument

from occam.manager import uses

from occam.objects.manager   import ObjectManager
from occam.jobs.manager      import JobManager

@command('jobs', 'status',
    category = "Job Management",
    documentation = "Lists information about the given job.")
@argument("job_id", type=str, help = "The identifier for the job you wish to list.")
@option("-j", "--json",   action  = "store_true",
                          dest    = "to_json",
                          help    = "Report the output in JSON format.")
@uses(JobManager)
class ListCommand:
  """ Lists all tasks within the job queue.
  """

  def do(self):
    Log.header("Listing job %s" % (self.options.job_id))

    jobs = self.jobs.retrieve(job_id = self.options.job_id)

    if len(jobs) == 0:
      Log.error("Job not found.")
      return -1

    job = jobs[0]

    ret = {
      "id": job.id,
      "task": {
        "id": job.task_uid,
        "revision": job.task_revision,
        "backend": job.task_backend
      },
      "kind": job.kind,
      "running": self.jobs.jobRunning(self.options.job_id),
      "status": job.status,
      "scheduler": job.scheduler
    }

    if job.queue_time:
      ret["queueTime"] = job.queue_time.isoformat()

    if job.start_time:
      ret["startTime"] = job.start_time.isoformat()

    if job.finish_time:
      ret["finishTime"] = job.finish_time.isoformat()

    if self.options.to_json:
      import json
      Log.output(json.dumps(ret))
    else:
      Log.output(str(ret))

    return 0
