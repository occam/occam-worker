# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.log import Log

from occam.commands.manager import command, option, argument

from occam.manager import uses

from occam.objects.manager   import ObjectManager
from occam.jobs.manager      import JobManager

@command('jobs', 'cancel',
    category = "Job Management",
    documentation = "Stops the given job")
@argument("job_id", type=str, help = "The identifier for the job you wish to cancel.")
@uses(JobManager)
@uses(ObjectManager)
class CancelCommand:
  """ Cancels the given job.
  """

  def do(self):
    Log.header("Cancelling job %s" % (self.options.job_id))
    jobs = self.jobs.retrieve(job_id=self.options.job_id)
    if len(jobs) > 0:
      return self.jobs.terminate(jobs[0])

    return -1
