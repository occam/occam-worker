# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import datetime

from occam.log import Log

from occam.commands.manager import command, option, argument

from occam.manager import uses

from occam.objects.manager   import ObjectManager
from occam.jobs.manager      import JobManager

@command('jobs', 'list',
    category = "Job Management",
    documentation = "Lists tasks queued.")
@option("-s", "--status", action  = "store",
                          dest    = "status",
                          help    = "The status of the jobs to list.",
                          default = "queued")
@option("-j", "--json",   action  = "store_true",
                          dest    = "to_json",
                          help    = "Report the output in JSON format.")
@uses(JobManager)
class ListCommand:
  """ Lists all tasks within the job queue.
  """

  def do(self):
    Log.header("Listing %s tasks" % (self.options.status))

    jobs = self.jobs.retrieve(status = self.options.status)

    if self.options.to_json:
      import json

      # Convert dates to ISO8601:
      for job in jobs:
        for k, v in job._data.items():
          if isinstance(v, datetime.datetime):
            job._data[k] = v.isoformat()

      # Output the data for all of the job records
      Log.output(json.dumps(list(map(lambda x: x._data, jobs))))
    else:
      for job in jobs:
        time = job.finish_time

        if job.status == "queued":
          time = job.queue_time
        if job.status == "started":
          time = job.start_time

        Log.write("%s: %s %s %s at %s" % (job.id, job.kind, job.task_uid, job.status, time.isoformat()))

    return 0
