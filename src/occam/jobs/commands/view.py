# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.log import Log

from occam.commands.manager import command, option, argument

from occam.manager import uses

from occam.objects.manager   import ObjectManager
from occam.jobs.manager      import JobManager

@command('jobs', 'view',
    category = "Job Management",
    documentation = "Views the log for a given job")
@argument("job_id", type=str, help = "The identifier for the job you wish to view.")
@option("-t", "--tail",    action  = "store_true",
                           default = False,
                           help    = "Will spool contents as they happen.")
@option("-f", "--from",    action  = "store",
                           dest    = "position",
                           type    = int,
                           default = 0,
                           help    = "The byte position to start reading from.")
@option("-e", "--events",  action  = "store_true",
                           dest    = "events",
                           default = False,
                           help    = "View the event log.")
@option("-k", "--task",    action  = "store_true",
                           dest    = "task",
                           default = False,
                           help    = "Pull the job task manifest.")
@option("-n", "--network", action  = "store_true",
                           dest    = "network",
                           default = False,
                           help    = "Pull the job network manifest.")
@uses(JobManager)
class ViewCommand:
  """ Views the logs for a given job.
  """

  def do(self):
    Log.header("Viewing log for job %s" % (self.options.job_id))

    jobs = self.jobs.retrieve(job_id = self.options.job_id)

    if len(jobs) == 0:
      Log.error("Job not found.")
      return -1

    job = jobs[0]

    try:
      if self.options.task:
        logFile = self.jobs.taskInfoFileForJob(job.id)
      elif self.options.network:
        logFile = self.jobs.networkInfoFileForJob(job.id)
      elif self.options.events:
        logFile = self.jobs.eventLogFor(job.id)
      else:
        logFile = self.jobs.logFor(job.id)
    except OSError as e:
      Log.warning("File not found.")
      return 0;
    if logFile is None:
      Log.warning("File not found.")
      return 0

    logFile.seek(self.options.position)

    while(True):
      Log.pipe(logFile)

      if not self.options.tail or not self.jobs.jobRunning(job.id):
        break

    return 0
