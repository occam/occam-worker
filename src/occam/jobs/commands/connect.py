# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import select, os, sys
import tty
import termios

from occam.log import Log

from occam.commands.manager import command, option, argument

from occam.manager import uses

from occam.objects.manager   import ObjectManager
from occam.jobs.manager      import JobManager

@command('jobs', 'connect',
    category = "Job Management",
    documentation = "Connects your terminal to the given job")
@argument("job_id", type=str, help = "The identifier for the job you wish to connect to.")
@uses(JobManager)
class ConnectCommand:
  """ Connects the terminal to the given job.
  """

  def do(self):
    job_id = self.options.job_id

    old_tty = None

    # Tail the log
    logFile = None
    readers = [sys.stdin]

    while self.jobs.jobRunning(job_id):
      if not logFile:
        logFile = self.jobs.logFor(job_id)
        if logFile:
          # save original tty setting then set it to raw mode
          try:
            old_tty = termios.tcgetattr(sys.stdin)
            tty.setraw(sys.stdin.fileno())
          except:
            pass

          # append to watch list
          readers.append(logFile)

      r, _, _ = select.select(readers, [], [])

      if sys.stdin in r:
        d = os.read(sys.stdin.fileno(), 10240)
        self.jobs.writeTo(self.options.job_id, d, person = self.person)

      if logFile and logFile in r:
        Log.pipe(logFile)

    # restore tty settings back
    if old_tty:
      termios.tcsetattr(sys.stdin, termios.TCSADRAIN, old_tty)

    return 0
