# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.log import Log

from occam.commands.manager import command, option, argument

from occam.manager import uses

from occam.objects.manager   import ObjectManager
from occam.jobs.manager      import JobManager

@command('jobs', 'run',
    category = "Job Management",
    documentation = "Runs the given job")
@argument("job_id", type=str, help = "The identifier for the job you wish to run.")
@option("-i", "--interactive", action = "store_true",
                               help   = "Deploys the job to run as an interactive process")
@uses(JobManager)
@uses(ObjectManager)
class RunCommand:
  """ Immediately runs the given job.
  """

  def do(self):
    Log.header("Running job %s" % (self.options.job_id))

    jobs = self.jobs.retrieve(job_id = self.options.job_id)

    if len(jobs) == 0:
      Log.error("Job not found.")
      return -1

    job = jobs[0]

    person = self.jobs.personFor(job)

    opts = self.jobs.run(job, interactive=self.options.interactive)

    if opts == -1 or opts[0] is None:
      return 0

    # Rake output
    Log.write("Raking Output")
    task = self.jobs.taskForJob(job, person = person)
    self.jobs.rakeOutput(task       = self.objects.infoFor(task),
                         taskPath   = opts[1]['home'],
                         generators = [],
                         identity   = person.identity,
                         job        = job)

    return 0
