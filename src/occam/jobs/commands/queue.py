# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.log import Log

from occam.commands.manager import command, option, argument

from occam.manager import uses

from occam.objects.manager   import ObjectManager
from occam.jobs.manager      import JobManager

@command('jobs', 'queue',
    category = "Job Management",
    documentation = "Queues a task to run.")
@argument("object", type="object")
@option("-i", "--interactive", action = "store_true",
                               help   = "Queues the job to run as an interactive process")
@option("-j", "--json",     dest    = "to_json",
                            action  = "store_true",
                            help    = "returns result as a json document")
@uses(ObjectManager)
@uses(JobManager)
class QueueCommand:
  """ Queues a task to run in the background.
  """

  def do(self):
    Log.header("Queuing a task")

    # Retrieve the task
    task = self.objects.resolve(self.options.object, person = self.person)

    if task is None:
      Log.error("Cannot find task %s" % (self.options.object.id))
      return -1

    taskInfo = self.objects.infoFor(task)
    taskInfo['id'] = task.id

    Log.write("Queuing: %s" % (taskInfo.get('name', 'unnamed')))

    job = self.jobs.create(taskInfo, task.id, revision=task.revision, person=self.person, interactive=self.options.interactive)
    self.jobs.launchDaemon()

    if self.options.to_json:
      import json

      ret = {
        "id": job.id
      }

      Log.output(json.dumps(ret))
    else:
      Log.write("job id: ", end = "")
      Log.output(str(job.id), padding = "")

    return 0
