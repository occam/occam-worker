# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os

from occam.log import loggable

@loggable
class Config:
  data = None

  @staticmethod
  def root(occamPath = None):
    path = occamPath

    if path is None:
      # Path can be specified as OCCAM_ROOT env var
      path = os.getenv("OCCAM_ROOT")

    if path is None:
      # Default path is ~/.occam
      path = os.path.realpath(os.path.join(os.path.expanduser("~"), ".occam"))

    return path

  @staticmethod
  def path(occamPath = None):
    """ Returns the path to the configuration file.
    """

    path = Config.root(occamPath)

    # Look at config.yml
    config_path = os.path.join(path, 'config.yml')

    return config_path

  @staticmethod
  def create(path):
    """ Creates a new configuration file replacing the one that exists if necessary.
    """

    # Create base directory if necessary
    if not os.path.exists(path):
      Config.Log.noisy("Creating directory %s" % (Config.root()))
      os.mkdir(Config.root())

    config_path = Config.path(path)

    # Create from default config
    default_config_path = os.path.join(os.path.dirname(__file__), '..', '..', 'config.yml.sample')

    import shutil
    shutil.copyfile(default_config_path, config_path)

    # Generate secrets
    f = open(config_path)
    config_data = f.read()
    f.close()

    # Create new secrets
    import binascii

    secret = binascii.hexlify(os.urandom(32))
    config_data = config_data.replace("%secret1%", secret.decode('ascii'))

    secret = binascii.hexlify(os.urandom(32))
    config_data = config_data.replace("%secret2%", secret.decode('ascii'))

    f = open(config_path, 'w+')
    f.write(config_data)
    f.close()

  @staticmethod
  def load(options=None, path=None):
    """ Returns the configuration file.
    """

    if Config.data is None:
      # Look at config.yml
      path = Config.root(path)
      config_path = Config.path(path)

      if not os.path.exists(config_path):
        Config.create(path)

      Config.Log.noisy("Reading configuration file at %s" % (config_path))
      config_file = open(config_path)

      import yaml
      Config.data = yaml.safe_load(config_file)

      # Work with environments
      # TODO: DEPRECATED
      if "production" in Config.data:
        Config.data = Config.data["production"]

      # Default paths
      Config.data['paths'] = Config.data.get('paths') or {}
      paths = Config.data['paths']

      # TODO: just clean this up like an adult
      paths['objects']    = paths.get('objects')    or os.path.join(path, "objects")
      paths['local']      = paths.get('local')      or os.path.join(path, "local")
      paths['git']        = paths.get('git')        or os.path.join(path, "git")
      paths['docker']     = paths.get('docker')     or os.path.join(path, "docker")
      paths['hg']         = paths.get('hg')         or os.path.join(path, "hg")
      paths['invocation'] = paths.get('invocation') or os.path.join(path, "invocation")
      paths['svn']        = paths.get('svn')        or os.path.join(path, "svn")
      paths['builds']     = paths.get('builds')     or os.path.join(path, "builds")
      paths['runs']       = paths.get('runs')       or os.path.join(path, "runs")
      paths['store']      = paths.get('store')      or os.path.join(path, "store")
      paths['jobs']       = paths.get('jobs')       or os.path.join(path, "jobs")
      paths['nodes']      = paths.get('nodes')      or os.path.join(path, "nodes")
      paths['ports']      = paths.get('ports')      or os.path.join(path, "ports")
      paths['binaries']   = paths.get('binaries')   or os.path.join(path, "binaries")

    return Config.data

def configurable(defaults):
  def _configurable(cls):
    return cls

  return _configurable
