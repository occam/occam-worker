# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.citations.manager import CitationManager, citationFormat

from occam.manager import uses

@citationFormat("harvard")
class Harvard:
  """ This plugin generates Harvard citations.
  """

  def generate(self, info):
    """ Returns a string representing the Harvard for the given citation keys.
    """

    ret = ""

    if 'title' in info:
      ret = ret + info['title'] + ". "

    if 'year' in info:
      ret = ret + "(" + str(info['year']) + "). "

    if 'publisher' in info:
      ret = ret + info['publisher'] + "."

    ret = ret + "\n"

    return ret
