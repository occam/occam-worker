# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.commands.manager import command, option, argument

from occam.manager import uses

from occam.objects.manager   import ObjectManager
from occam.citations.manager import CitationManager

from occam.object import Object
from occam.log import Log

import os

@command('citations', 'view',
  category      = 'Metadata',
  documentation = "Generates a citation in the given format.")
@argument("object", type = "object")
@option("-f", "--format", type = str,
                          dest = "format",
                          help = "The citation format to generate.",
                          default = "bibtex")
@option("-o", "--output", type = str,
                          dest = "output",
                          help = "The output format to generate. (text, html, markdown)",
                          default = "text")
@uses(ObjectManager)
@uses(CitationManager)
class ViewCommand:
  """ Generates citations.
  """

  def do(self):
    obj = self.objects.resolve(self.options.object, self.person)

    if obj is None:
      Log.error("Cannot resolve the given object.")
      return -1

    citation = self.citations.view(obj, type = self.options.format, output = self.options.output)

    if citation is None:
      Log.error("Format {} is not known.".format(self.options.format))
      return -1

    Log.output(citation, padding="")
    return 0
