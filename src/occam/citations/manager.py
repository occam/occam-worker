# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os

from occam.log     import loggable
from occam.manager import manager, uses

from occam.objects.manager import ObjectManager

@loggable
@manager("citations")
@uses(ObjectManager)
class CitationManager:
  """ This OCCAM manager handles the generation of citations for objects.
  """

  handlers = {}

  def __init__(self):
    import importlib

    # Look at configuration at import the plugins specified
    pluginList = self.configuration.get('plugins', [])
    for pluginName in pluginList:
      importlib.import_module(pluginName)

    import mistune

    class TextRenderer(mistune.Renderer):
      def block_code(self, code, lang):
        return code

      def block_quote(self, text):
        return text

      def block_html(self, html):
        return html

      def header(self, text, level, raw=None):
        return text

      def hrule(self):
        return ""

      def list(self, body, ordered=True):
        return body

      def list_item(self, text):
        return text

      def paragraph(self, text):
        return text

      def table(self, header, body):
        return body

      def table_row(self, content):
        return content

      def table_cell(self, content, **flags):
        return content

      def autolink(self, link, **kwargs):
        return link

      def codespan(self, text):
        return text

      def double_emphasis(self, text):
        return text

      def emphasis(self, text):
        return text

      def image(self, src, title, alt_text):
        return alt_text

      def linebreak(self):
        return "\n"

      def newline(self):
        return "\n"

      def link(self, link, title, content):
        return content

      def strikethrough(self, text):
        return text

      def text(self, text):
        return text

      def inline_html(self, text):
        return text

      def footnote_ref(self, key, index):
        return key + ": " + index

      def footnote_item(self, key, text):
        return key + ": " + text

      def footnotes(self, text):
        return text

    self.markdown_html = mistune.Markdown(escape = True)
    self.markdown_text = mistune.Markdown(renderer=TextRenderer())

  @staticmethod
  def register(name, handlerClass):
    """ Registers the given class as an importable plugin with the given name.
    """
    CitationManager.handlers[name] = handlerClass

  def handlerFor(self, plugin):
    """ Returns an instance of a handler for the given plugin type.
    """

    if not plugin in self.handlers:
      return None

    subConfig = self.configurationFor(plugin)

    # Create a driver instance
    instance = CitationManager.handlers[plugin](subConfig)

    return instance

  def configurationFor(self, plugin):
    """ Returns the configuration for the given plugin type.
    Returns the configuration for the given storage that is found within the
    occam configuration (config.yml) under the "stores" section under the
    given storage backend name.
    """

    config = self.configuration
    subConfig = config.get(plugin, {})

    return subConfig

  def view(self, object, type="bibtex", output="text"):
    """ Returns a string representing the citation for the given object in the given format.
    """

    import datetime

    plugin = self.handlerFor(type)

    markdown = None
    if plugin:
      info = self.objects.infoFor(object)
      history = self.objects.retrieveHistoryFor(object)[0]

      if 'citation' in info:
        info = info['citation']
      else:
        # From:
        # https://stackoverflow.com/questions/969285/how-do-i-translate-a-iso-8601-datetime-string-into-a-python-datetime-object
        import re
        timestamp = history['date']
        conformed_timestamp = re.sub(r"[:]|([-](?!((\d{2}[:]\d{2})|(\d{4}))$))", '', timestamp)

        # split on the offset to remove it. use a capture group to keep the delimiter
        split_timestamp = re.split(r"[+|-]",conformed_timestamp)
        main_timestamp = split_timestamp[0]
        if len(split_timestamp) == 3:
          sign = split_timestamp[1]
          offset = split_timestamp[2]
        else:
          sign = None
          offset = None

        output_datetime = datetime.datetime.strptime(main_timestamp +"Z", "%Y%m%dT%H%M%SZ" )

        info = {
          "title":   info['name'],
          "year":    output_datetime.year,
          "month":   output_datetime.month,
          "day":     output_datetime.day,
          "version": "rev " + object.revision
        }

        if 'organization' in info:
          info['publisher'] = info['organization']

        authors = info.get('authors', [])
        if not isinstance(authors, list):
          authors = [authors]

      markdown = plugin.generate(info).strip()

    ret = markdown
    if output == "html":
      ret = self.markdown_html(markdown).strip()
    elif output == "text":
      ret = self.markdown_text(markdown).strip()

    return ret

def citationFormat(name):
  """ This decorator will register the given class as a package type.
  """

  def register_package(cls):
    CitationManager.register(name, cls)
    cls = loggable("CitationManager")(cls)

    def init(self, subConfig):
      self.configuration = subConfig

    cls.__init__ = init

    return cls

  return register_package
