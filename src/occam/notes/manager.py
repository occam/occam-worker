# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import json
import datetime

from occam.config  import Config
from occam.semver  import Semver
from occam.manager import manager, uses

from occam.log import loggable

from occam.network.manager import NetworkManager

@loggable
@uses(NetworkManager)
@manager("notes")
class NoteManager:
  """ Maintains auxiliary information about objects.

  Objects have a versioned history and some metadata at each point in time
  which is indexed by a revision. These two features, id and revision,
  point to an Object in space and time. However, there are pieces of
  information that may describe an object as a whole. These are organized
  using this component.

  For instance, builds of an Object are kept isolated from the Object's
  history. When an Object is built, a record is kept that described the
  provenance of the build (what the task looked like) and that is stored
  in a piece of metadata called "builds". This module allows you to
  append such a record.

  Each note is recorded with a category. The category then has notes per each
  revision known for the Object. For every revision, there exists a key/value
  store. Since this system often makes use of untrusted values from this store,
  the keys point to a list of possible values, each value recording their
  source (host/port/etc) that can be used to verify the information.

  This allows other nodes in the system to provide alternate information that
  over time can be used to derive new methods of building or interaction with
  existing artifacts in the store.

  One example is the builds category, as discussed. The builds category, for a
  given revision, can have a key for "tasks" to list the UUID for any task
  manifests that have been used to build the artifact. Obviously, many systems
  may build the object in various ways independently. This will then be an
  aggregation of different tasks that can grow over time. A system that does
  not yet know how to build an artifact may look over this list and retrieve
  each task and, based on the specifics of the task such as the available
  hardware or virtualization technology, make use of it.

  In this example, this is knowledge building. Over time, systems can see an
  artifact and acquire aggregate knowledge (notes) that can be used to discover
  and use auxiliary artifacts to build/run/interact with new objects.
  """

  def path(self):
    """ Returns the path to the local note store.
    """

    return self.configuration.get('path', os.path.join(Config.root(), 'notes'))

  def pathFor(self, id, category):
    """ Returns the path to the note data for the given object and category.
    """

    if id is None:
      return None

    # Find the full path to the stored invocation metadata
    # Ex: .occam/notes/2-code/2-code/2-code/2-code/full-code

    code = id
    codes = [code[0:2], code[2:4], code[4:6], code[6:8]]

    path = self.path()

    if not os.path.exists(path):
      os.mkdir(path)

    for codePart in codes:
      path = os.path.join(path, codePart)
      if not os.path.exists(path):
        os.mkdir(path)

    path = os.path.join(path, id)
    if not os.path.exists(path):
      os.mkdir(path)

    subCategories = category.split('/')

    for subCategory in subCategories:
      path = os.path.join(path, subCategory)
      if not os.path.exists(path):
        os.mkdir(path)

    return path

  def filePathFor(self, id, category, revision):
    """ Returns the exact filename of the note data for the given object and category.
    """

    if revision is None:
      revision = "values"

    return os.path.join(self.pathFor(id, category), "%s.json" % (revision))

  def retrieveObjectList(self):
    """ Retrieves an exhaustive list of all objects known.
    """

    import glob

    path = self.path()

    # Walk the path for object ids
    return map(lambda x: os.path.basename(x), glob.iglob(os.path.join(path, "*", "*", "*", "*", "*")))

  def revisionListFor(self, id, category):
    """ Returns a list of known revisions with data for the given object and category.
    """

    path = self.pathFor(id, category)

    # Return a list of all .json files in this directory
    ret = []
    for item in os.listdir(path):
      if item.endswith(".json"):
        path, ext = os.path.splitext(item)
        ret.append(os.path.basename(path))

    return ret

  def dataFor(self, id, category):
    """ Returns the notes for this object from the given category.
    """

    # Open and parse each note
    revisions = self.revisionListFor(id, category)

    ret = {}

    for revision in revisions:
      data = self.retrieve(id, category, revision)

      if not data is None:
        ret[revision] = data

    # Return the data
    return ret

  def retrieveValues(self, id, category, revision, key = None):
    values = self.retrieve(id, category, revision, key)

    ret = []

    for value in (values or []):
      ret.append(value.get('value'))

    return ret

  def retrieve(self, id, category, revision, key = None, fallback = True):
    """ Returns the key/values for a given category and optionally a key.

    When a revision is given, it will look up revision specific values. If those
    are not found, it will fallback to looking at revision-less values if `fallback` is True.

    When revision is None, it will look for revision-less values which are set
    by calling store with revision set to None.
    """

    filePath = self.filePathFor(id, category, revision)

    try:
      f = open(filePath, 'r')
      data = json.load(f)
      f.close()
    except:
      data = None

    if data and key:
      return data.get(key, None)

    if data is None and revision is not None and fallback:
      return self.retrieve(id, category, None, key = key)

    # Ensure notes are a dict
    if not isinstance(data, dict):
      # Abort!
      if not fallback or revision is None:
        return None
      else:
        return self.retrieve(id, category, None, key = key)

    return data

  def merge(self, id, category, items):
    """ Stores the result of merging the given revisioned list to the object in the given category.

    The items will consist of a key/value dict containing keys for every known revision.
    """

    # Retrieve invocation data
    data = self.dataFor(id, category)

    # Go through and merge data items
    for revision, value in items.items():
      # The 'value' is the dictionary of key/value pairs.
      # Each value of a key/value pair is a list.
      # We will merge the lists such that only unique items exist
      if not revision in data:
        data[revision] = value
      else:
        for key, subItems in value.items():
          if not key in data[revision]:
            data[revision][key] = subItems
          else:
            for subItem in subItems:
              if not subItem in data[revision][key]:
                data[revision][key].append(subItem)

      # Update the data
      self.store(id, category, revision, data[revision])

    # Returns invocation data
    return data

  def store(self, id, category, key, value, revision=None, source=None, append=False):
    """ Adds the given item to the given category within the stored invocation record.

    Examples:

      This would add that the given object has generated an object with id <id>
      seen with the revision "fedcba". The given object was at revision "abcdef"
      when it generated the new object::

        store(id, "generated", "object", {
          "id":       "<id>",
          "revision": "fedcba",
          "name":     "foo",
          "type":     "application/json"
        })

      The items in the invocation file have arbitrary structure. All categories
      are arrays, but you can place any data within them.
    """

    # Retrieve invocation data
    data = self.retrieve(id, category, revision)

    # Ensure that revision exists
    if data is None:
      data = {}

    ret = data

    if not key in data:
      data[key] = []

    # Ensure data chunk is list
    if not isinstance(data[key], list):
      # Abort!
      return None

    # If there is no source, the source is ourselves
    if source is None:
      source = self.network.hostname()

    # Append the data item if not already there
    # TODO: better checks for the same data
    #       maybe even allow a comparator to be given
    exists = False
    remove = None
    if not append:
      for index, token in enumerate(data[key]):
        if value == token.get('value'):
          # Just add ourselves as a vouch if we are trying to store
          # the same value someone else already computed
          if not source in token.get('vouch', []):
            token['vouch'] = token.get('vouch', []) + [source]

          exists = True
        elif source in token.get('vouch', []):
          token['vouch'].remove(source)
          if len(token['vouch']) == 0:
            # Remove this entry
            remove = index

      if not remove is None:
        del data[key][index]

    if not exists or append:
      # Append the data
      data[key].append({
        "vouch": [source],
        "source": source,
        "published": datetime.datetime.utcnow().isoformat(),
        "value": value
      })

    # Update the invocation
    filePath = self.filePathFor(id, category, revision)

    try:
      f = open(filePath, 'w+')
      json.dump(data, f)
      f.close()
    except:
      data = None

    # Returns invocation data
    return ret

  def retrieveBestRevision(self, id):
    """ Determines the best revision for this object for the purposes of running
    the object.
    """

    revisions = self.revisionListFor(id, "stores/local")

    revision = None

    if len(revisions) > 0:
      revision = revisions[0]

    return revision

  def retrieveBuildIds(self, id, revision):
    """ Retrieves the build ids for the given object at the given revision.
    """

    # Look up builds for this revision
    builds = self.retrieve(id, category="builds", key="ids", revision=revision)
    if builds is None or len(builds) == 0:
      return None

    return [build["value"] for build in builds]

  def retrieveBuild(self, id, revision):
    """ Retrieves a build task for the requested object representing a previous build
    of this object.

    Returns None when no build is known.
    """

    # Look up builds for this revision
    buildId = self.retrieveBuildId(id, revision)

    if buildId is None:
      return None

    buildTask = self.retrieveInvocation(id, category="builds", key="build-%s" % (buildId), revision = revision)

    return buildTask

  def resolveOwner(self, id):
    """ This function returns the owning id for the given id.

    If there is no owning id, id is returned.

    If there is an owning id, that id is returned.
    """

    ownerData = self.retrieve(id, "owner", revision = None) or {}

    owner_id = id
    for record in ownerData.get('id', []):
      if 'value' in record:
        owner_id = record.get('value')
        break

    return owner_id

  def delete(self, id):
    """ Removes all records about a particular object.
    """
    path = os.path.realpath(os.path.join(self.pathFor(id, "foo"), ".."))
    NoteManager.Log.write("Deleting notes for %s" % (id))
    import shutil
    shutil.rmtree(path)

  def resolveVersion(self, id, version, versions = None, penalties = None):
    """ This function will return the revision of the given version string.
    
    The version string can be a parsable identifier or a full semver string. It
    works the same as npm's semver.

    Returns None when a version within the requested version range cannot be
    found.
    """

    if version is None:
      return None, None

    ranges = Semver.parseVersion(version)

    # Look up versions

    versionData = self.retrieve(id, "versions", revision = None)

    if versionData is None:
      versionData = {}

    if versions is None:
      versions = list(versionData.keys())

    currentVersion = Semver.resolveVersion(version, versions)

    # Go and pick the best (most-likely) revision
    revision = None
    if currentVersion:
      for revisionItem in versionData[currentVersion]:
        value = revisionItem.get('value')
        if not penalties or not value in penalties.get(id, []):
          revision = value

    if revision is None and currentVersion in versions:
      # Negate this version and try again
      versions.remove(currentVersion)
      return self.resolveVersion(id, version, versions=versions, penalties=penalties)

    return revision, currentVersion
