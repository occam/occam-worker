# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.commands.manager import command, option, argument

from occam.manager import uses

from occam.notes.manager     import NoteManager
from occam.objects.manager   import ObjectManager

from occam.object import Object
from occam.log import Log

import os

@command('notes', 'list',
  category      = 'Object Inspection',
  documentation = "Displays notes for the given object.")
@argument("object", type = "object", nargs = '?')
@argument("category")
@option("-j", "--json", dest   = "to_json",
                        action = "store_true",
                        help   = "returns result as a json document")
@uses(NoteManager)
@uses(ObjectManager)
class ListCommand:
  """ Shows a list of all recorded revisions for the given object and category.
  """

  def do(self):
    if self.options.object is None:
      # Use local object
      obj, root = self.objects.retrieveLocal(".")
    else:
      # Query for object by id
      obj = self.objects.resolve(self.options.object, person = self.person)
    if obj is None:
      Log.error("cannot find object with id %s" % (self.options.object.id))
      return -1

    revisions = self.notes.revisionListFor(obj.uuid, "stores/local")

    if self.options.to_json:
      import json
      Log.output(json.dumps(revisions))

      return 0

    for i, item in enumerate(revisions):
      if i > 0:
        Log.output("  ", end="", padding="")
      Log.output(item, end="", padding="")

    Log.output("\n", end="", padding="")
    return 0
