# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.commands.manager import command, option, argument

from occam.manager import uses

from occam.notes.manager     import NoteManager
from occam.objects.manager   import ObjectManager

from occam.object import Object
from occam.log import Log

import os
import json

@command('notes', 'view',
  category      = 'Object Inspection',
  documentation = "Displays notes for the given object.")
@argument("object", type = "object")
@argument("category")
@argument("key", nargs="?")
@option("-j", "--json", dest   = "to_json",
                        action = "store_true",
                        help   = "returns result as a json document")
@uses(NoteManager)
@uses(ObjectManager)
class ViewCommand:
  """ Retrieves a set of notes for the given object and category.
  """

  def do(self):
    if self.options.object is None:
      # Use local object
      obj, root = self.objects.retrieveLocal(".")
    else:
      # Query for object by id
      obj = self.objects.resolve(self.options.object, person = self.person)
    if obj is None:
      Log.error("cannot find object with id %s" % (self.options.object.id))
      return -1

    data = self.notes.retrieve(obj.id, self.options.category, obj.revision, key = self.options.key)
    if data is None:
      data = []

    Log.output(json.dumps(data or []))
    return 0
