# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.databases.manager import table

@table("accounts")
class AccountRecord:
  """ Stores information about a Person that can be authenticated on this node.
  """

  schema = {

    # Primary Key

    "id": {
      "type": "integer",
      "primary": True
    },

    # Foreign Key to Person Object

    "person_id": {
      "foreign": {
        "table": "objects",
        "key":   "id"
      }
    },

    # The private identity
    "identity_key_id": {
      "foreign": {
        "key": "id",
        "table": "identity_keys"
      }
    },

    # Attributes

    # This username for this Account. Must be unique on this node.
    "username": {
      "type": "string",
      "length": 128,
      "unique": True
    },

    # The hashed/salted password for this Account.
    "hashed_password": {
      "type": "string",
      "length": 128
    },

    # Whether or not the account is active. Inactive accounts cannot log on or
    # generate authentication tokens until verified by an administrator.
    "active": {
      "type": "boolean",
      "default": True
    },

    # Semi-colon delimited (;) list of default roles for new Accounts.
    "roles": {
      "type": "text"
    },

    # The email associated for this account
    "email": {
      "type": "string",
      "length": 128
    },

    # Whether or not the email was verified
    "email_verified": {
      "type": "boolean",
      "default": False
    },

    # The email verification token
    "email_token": {
      "type": "string",
      "length": 128
    },

    # Whether or not the email is displayed
    "email_public": {
      "type": "boolean",
      "default": False
    },

    # Every Account has a secret that is used to create authentication tokens
    # If the secret is updated, you have revoked existing tokens.
    "secret": {
      "type": "string",
      "length": 128
    },

    # This is a particular token that allows the reset of a password when
    # it is sent to some alternate trusted channel (such as email)
    "reset_token": {
      "type": "string",
      "length": 128
    },

    # The expiration data for the token.
    "reset_token_expiration": {
      "type": "datetime"
    },

    # The time in which the account was created.
    "published": {
      "type": "datetime"
    },

    # The time in which the account was updated.
    "updated": {
      "type": "datetime"
    },
  }
