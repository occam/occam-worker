# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.databases.manager import table

@table('memberships')
class MembershipRecord():
  """ This joins the Person table with the Person table to connect members of groups.
  """

  schema = {

    # Primary Key

    "id": {
      "type": "integer",
      "primary": True
    },

    # Foreign Key to member Person Object

    "person_object_id": {
      "foreign": {
        "table": "objects",
        "key":   "id"
      }
    },

    # Foreign Key to owning Person Object

    "base_person_object_id": {
      "foreign": {
        "table": "objects",
        "key":   "id"
      }
    },

    # The reference count for how many times the Person is a member
    # It starts at 1

    "reference_count": {
      "type": "integer",
      "default": 0
    },

    # Whether or not the membership is private

    "private": {
      "type": "boolean",
      "default": False
    },
  }
