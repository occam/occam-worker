# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Account records are authentication records. They are attached to a Person
# which is a general account of a particular Person/Organization in the
# federation. If a Person has an Account record, they can authenticate to
# the current node. A Person may have an Account on multiple clusters in
# the federation, enabling them to log in to several different nodes.

# When you authenticate to the system, you generate a token. In this case it
# is an HMAC-SHA256 generated using a random 64 byte key. When an Account is
# created (see AccountManager.addAuthentication) that key is generated along
# with an RSA key. (The RSA key is not used for node authentication)

# For terminal access, the login token is implied by the owner of that shell.
# The token is generated upon a "login" command and stored in the shell
# owner's OCCAM home under 'auth.json'. This file is read when any command is
# used and treated as the current Person. (see AccountManager.currentPerson)

# For other access (or to force a particular Person,) a Person UUID and token
# can be passed to any OCCAM command using the "--as" and "--token" (-A, -T)
# arguments. (See CommandManager.execute and CommandManager.parser) In these
# cases, AccountManager.currentPerson is called with those instead of the keys
# found in the auth.json file. This is used for the socket connections and
# authenticated clients. For example, an interactive web server.

# Passwords are used to authenticate, as well. They are used to generate the
# token. Passwords often have less entropy than tokens, so they are point of
# least resistance to break authentication. Stronger authentication might be
# implemented later, such as an external secure authenticator, or multi-factor
# authentication.

# Passwords are stored in the database as a salted hash using bcrypt (can be
# configured to add new algorithms later) and uses a configurable number of
# rounds. Although all passwords are assumed to use the same number of rounds.
# If rounds were to change, all passwords would have to be reset somehow.
# There is no plan on how to do this.

import os

from occam.workset import Workset
from occam.config  import Config

from occam.manager import uses, manager

from occam.system.manager        import SystemManager
from occam.objects.write_manager import ObjectWriteManager
from occam.databases.manager     import DatabaseManager
from occam.permissions.manager   import PermissionManager
from occam.keys.manager          import KeyManager

@uses(SystemManager)
@uses(ObjectWriteManager)
@uses(DatabaseManager)
@uses(PermissionManager)
@uses(KeyManager)
@manager("accounts")
class AccountManager:
  """ Manages Accounts and People within the node or federation.
  """

  def addPerson(self, name, identity, subtypes=[]):
    """ Create a new person.
    """

    from occam.person import Person

    info = None
    if subtypes:
      info = {
        "subtype": subtypes
      }

    person = self.objects.write.create(name, object_type = "person", info=info)

    self.objects.write.store(person, identity)

    # Assign identity (TODO: move this)
    session = self.database.session()
    db_person_objects = self.retrievePeople(id=person.id)

    db_person_object = None
    if len(db_person_objects) > 0:
      db_person_object = db_person_objects[0]

    if db_person_object:
      db_person_object.identity_uri = identity
      self.database.update(session, db_person_object)
      self.database.commit(session)

    # Make Person objects public accessible
    personInfo = self.objects.infoFor(person)
    self.permissions.update(id = person.id, canRead=True, canWrite=False, canClone=False)

    # Make Person object writable by that person
    self.permissions.update(id = person.id, person_id=person.id, canRead=True, canWrite=True, canClone=False)

    return person

  def addMember(self, group, member, person = None):
    """ Adds an existing Person as a member under another Person

    Args:
        group (Person): The Occam object representing the group to be amended.
        member (Person): The Occam object representing the person to be added.
        person (Person): The Person who is adding the member to the group.

    Returns:
        MembershipRecord: The record reflecting the membership. None if the membership could not be created.
    """

    if self.objects.objectPositionWithin(group, member) is None:
      # Add the item to the object itself
      self.objects.write.addObjectTo(group, subObject=member, create=False)

      # Create the Membership record for access control purposes
      row = self.datastore.insertMembership(group, member)
    else:
      row = self.datastore.retrieveMembershipFor(group, member)

    # Return the MembershipRecord
    return row

  def updateVisibility(self, group, member, private):
    """ Updates the visibility of the given member of the given group.

    Returns the updated MembershipRecord.
    """

  def removeMember(self, group, member):
    """ Removes an existing member, given as a Person, of its base Person.
    """

    self.objects.write.removeObjectFrom(group, member)

    self.datastore.deleteMembership(group, member)

  def retrieveMembershipsFor(self, member):
    """ Retrieves the list of groups for the given member Object.
    """

    return self.datastore.retrieveMembershipObjectsFor(member)

  def retrieveMembershipsOf(self, group):
    """ Retrieves the list of members for the given group.
    """

    return self.datastore.retrieveMembershipObjectsOf(group)

  def addWorkset(self, path, name):
    """ Create a new workset within this occam instance at the given local path.
    """

    path = os.path.realpath(path)

    workset = Workset.create(path, name)

    self.objects.write.store(workset)

    return Workset(path)

  def retrievePersonObject(self, account_db_object):
    """ Retrieves the augmented (PersonRecord/ObjectRecord) for the given AccountRecord.
    """

    import sql
    session = self.database.session()

    objects = sql.Table('objects')
    people  = sql.Table('people')

    join = people.join(objects)
    join.condition = (join.right.id == people.id) & (people.id == account_db_object.person_id)

    query = join.select()

    self.database.execute(session, query)
    record = self.database.fetch(session)

    if record is None:
      return None

    from occam.objects.records.person import PersonRecord
    from occam.objects.records.object import ObjectRecord
    return (PersonRecord(record), ObjectRecord(record))

  def retrievePeople(self, id=None, name=None, keyword=None, email=None):
    import sql

    session = self.database.session()

    objects = sql.Table('objects')
    people  = sql.Table('people')

    subquery = objects.select(objects.id)
    if name is not None:
      if subquery.where:
        subquery.where = subquery.where | (objects.name == name)
      else:
        subquery.where = (objects.name == name)

    if id is not None:
      if subquery.where:
        subquery.where = subquery.where | (objects.id == id)
      else:
        subquery.where = (objects.id == id)

    query = people.select()
    query.where = (people.id.in_(subquery))

    if email is not None:
      query.where = query.where | (people.email == email)

    self.database.execute(session, query)

    records = self.database.many(session, size=10)

    from occam.objects.records.person import PersonRecord

    return [PersonRecord(record) for record in records]

  def retrieveAccount(self, username=None, email=None, account_id=None, person_id=None):
    import sql

    session = self.database.session()

    accounts = sql.Table('accounts')

    query = accounts.select()

    if username is not None:
      if query.where is not None:
        query.where = query.where | (accounts.username == username)
      else:
        query.where = (accounts.username == username)

    if email is not None:
      if query.where is not None:
        query.where = query.where | (accounts.email == email)
      else:
        query.where = (accounts.email == email)

    if person_id is not None:
      # Find Account by person_id
      people = sql.Table('people')

      query.where = (accounts.person_id == person_id)

    if account_id:
      query.where = (accounts.id == account_id)

    self.database.execute(session, query)

    record = self.database.fetch(session)

    if record is None:
      return None

    from occam.accounts.records.account import AccountRecord

    return AccountRecord(record)

  def authTokenPath(self):
    auth_token_path = os.path.join(Config.root(), "auth.json")

    return auth_token_path

  def currentAuthentication(self):
    auth_token_path = self.authTokenPath()

    if os.path.exists(auth_token_path):
      import json

      try:
        with open(auth_token_path, 'r') as f:
          return json.load(f)
      except:
        pass

    return {}

  def removeAuthentication(self):
    auth_token_path = self.authTokenPath()

    if os.path.exists(auth_token_path):
      os.remove(auth_token_path)

  def generateToken(self, account_db_object, db_person_object, type="person"):
    import jwt

    token = {}
    token[type] = db_person_object[0].id

    secret = account_db_object.secret

    encoded = jwt.encode(token, secret, algorithm="HS256").decode('utf-8')

    return encoded

  def generateAuthentication(self, account_db_object):
    auth_token_path = self.authTokenPath()

    personObject = self.retrievePersonObject(account_db_object)

    token = self.generateToken(account_db_object, personObject)

    auth_token = {
      'account': account_db_object.id,
      'person':  personObject[0].id,
      'token':   token
    }

    import json
    with open(auth_token_path, 'w+') as f:
      f.write(json.dumps(auth_token))

    return auth_token

  def decodeToken(self, token):
    """ Returns the decoded token information.
    """
    import jwt

    decoded = {}
    try:
      decoded = jwt.decode(token.encode('utf-8'), verify=False, algorithms=['HS256'])
    except:
      decoded = {}

    return decoded

  def currentPerson(self, token=None):
    import jwt

    account_db_object = None

    if token is None:
      auth_token = self.currentAuthentication()

      if 'account' in auth_token and 'person' in auth_token:
        token      = auth_token.get('token')
      else:
        return None

      account_db_object = self.retrieveAccount(account_id = auth_token.get('account'))
      person_uuid = auth_token['person']
    else:
      try:
        unverified = jwt.decode(token.encode('utf-8'), verify=False, algorithms=['HS256'])
      except:
        unverified = {}
      account_db_object = self.retrieveAccount(person_id = unverified.get('person'))

    if account_db_object is None:
      return None

    # Retrieve the PersonObject
    person_db, object_db = self.retrievePersonObject(account_db_object)

    # Authenticate the token
    try:
      payload = jwt.decode(token.encode('utf-8'), account_db_object.secret, algorithms=['HS256'])
    except:
      payload = {}

    if payload.get('person'):
      person = self.objects.retrieve(id=payload.get('person'))
      if person is None:
        return None

      person.roles = (account_db_object.roles or ";;").split(';')[1:-1]
      person.identity = (person_db.identity_uri)
      person.account = account_db_object

      return person

    return None

  def currentPersonId(self):
    auth_token_path = self.authTokenPath()

    if os.path.exists(auth_token_path):
      auth_token = json.load(open(auth_token_path, 'r'))

      if 'person' in auth_token:
        person_uid = auth_token['person']
        db_person = self.searchPeople(uuid=person_uid).first()
        if not db_person is None:
          return db_person.id

    return None

  def authenticate(self, username=None, email=None, password=None):
    account = self.retrieveAccount(username=username, email=email)

    if account is None:
      return False

    if account:
      import bcrypt   # For password hashing
      return bcrypt.hashpw(password.encode('utf-8'), account.hashed_password.encode('utf-8')) == account.hashed_password.encode('utf-8')
    else:
      return False

  def rolesFor(self, account_db):
    """ Returns the roles for the given account.
    """

    # Gather existing roles (they are ; delimited)
    roles = (account_db.roles or ";;")[1:-1].split(';')

    # Ensure an empty list is properly handled
    if len(roles) == 1 and roles[0] == "":
      roles = []

    return roles

  def addRoles(self, account_db, newRoles=[]):
    """ Adds the given roles to the given AccountRecord
    """

    roles = self.rolesFor(account_db)

    # Append given roles
    roles.extend(newRoles)

    return self.commitRoles(account_db, roles)

  def removeRoles(self, account_db, newRoles=[]):
    """ Adds the given roles to the given AccountRecord
    """

    roles = self.rolesFor(account_db)

    # Get the difference in the role set
    roles = list(set(roles) - set(newRoles))

    return self.commitRoles(account_db, roles)

  def commitRoles(self, account_db, roles=[]):
    # Remove duplicates
    roles = list(set(roles))

    # Remove empty roles
    roles = list(filter(None, roles))

    # String-ify
    account_db.roles = ";%s;" % (';'.join(roles))

    # Commit changes to database
    session = self.database.session()
    self.database.update(session, account_db)
    self.database.commit(session)

    return roles

  def addAuthentication(self, person_id, username, password, identityKey, roles=[]):
    from occam.accounts.records.account import AccountRecord

    # Get system admin record
    system = self.system.retrieve()

    # Set password (if exists)
    account = AccountRecord()

    # Accounts are active by default, unless moderation says otherwise
    if system.moderate_accounts:
      # We are moderating accounts
      account.active = False
    else:
      # We allow all accounts
      account.active = True

    # Default to no roles
    account.roles = ";;"

    # First account defaults to administrator role
    import sql
    accounts = sql.Table('accounts')

    session = self.database.session()

    query = accounts.select(sql.aggregate.Count(sql.Literal(1)))
    self.database.execute(session, query)
    accountCount = next(iter(self.database.fetch(session).values()))

    if accountCount == 0:
      if not 'administrator' in roles:
        roles.append('administrator')

    # Add the roles designated by the admin table
    default_roles = ((system.default_roles or ";;")[1:-1] or "").split(';')
    for default_role in default_roles:
      if not default_role in roles:
        roles.append(default_role)

    roles = list(set(roles))

    # Add the semi-colon delimited list of roles
    # TODO: roles can be broken if they contain a ';'
    #       but maybe this is not a problem.
    account.roles = ';' + ';'.join(roles) + ';'

    # Add username
    account.username = username

    account.person_id = person_id

    # Create password auth
    account.hashed_password = self.hashPassword(password)

    account.identity_key_id = identityKey.uri

    account.secret = self.keys.randomHex(64)

    self.database.update(session, account)
    self.database.commit(session)

  def hashPassword(self, password):
    """ Generates a password hash for the given string.
    """

    rounds = 12

    config_info = self.configuration
    rounds = config_info.get('passwords', {}).get('rounds', rounds)

    import bcrypt   # For password hashing

    return bcrypt.hashpw(password.encode('utf-8'), bcrypt.gensalt(rounds, prefix=b"2a")).decode('utf-8')

  def updatePassword(self, account, password):
    """ Updates the password for the given account.
    """

    session = self.database.session()

    # Create password auth
    account.hashed_password = self.hashPassword(password)

    self.database.update(session, account)
    self.database.commit(session)

  def authorsFor(self, uuid):
    """ Returns a PersonRecord list for all People that are authors of the given object.
    """

    import sql

    authorships = sql.Table('authorships')
    objects     = sql.Table('objects')

    subsubquery = objects.select(objects.id, where = (objects.uid == uuid))

    subquery = authorships.select(authorships.person_object_id, where = (authorships.internal_object_id.in_(subsubquery)))

    query = objects.select(where = (objects.id.in_(subquery)))

    session = self.database.session()
    self.database.execute(session, query)

    from occam.objects.records.object import ObjectRecord
    return [ObjectRecord(x) for x in self.database.many(session)]

  def collaboratorsFor(self, uuid):
    """ Returns a PersonRecord list for all People that are collaborators of the given object.
    """

    import sql

    collaboratorships = sql.Table('collaboratorships')
    objects           = sql.Table('objects')

    subsubquery = objects.select(objects.id, where = (objects.uid == uuid))

    subquery = collaboratorships.select(collaboratorships.person_object_id, where = (collaboratorships.internal_object_id.in_(subsubquery)))

    query = objects.select(where = (objects.id.in_(subquery)))

    session = self.database.session()
    self.database.execute(session, query)

    from occam.objects.records.object import ObjectRecord
    return [ObjectRecord(x) for x in self.database.many(session)]

  def removeAuthorship(self, uuid, person_uuid):
    """ Removes the given authorship.
    """

    import sql
    session = self.database.session()

    authorships = sql.Table('authorships')
    objects     = sql.Table('objects')

    subquery1 = objects.select(objects.id, where = (objects.uid == uuid))
    subquery2 = objects.select(objects.id, where = (objects.uid == person_uuid))

    query = authorships.delete()
    query.where = (authorships.internal_object_id.in_(subquery1)) & (authorships.person_object_id.in_(subquery2))

    self.database.execute(session, query)
    self.database.commit(session)

  def removeCollaboratorship(self, uuid, person_uuid):
    """ Removes the given collaboratorship.
    """

    import sql
    session = self.database.session()

    collaboratorships = sql.Table('collaboratorships')
    objects           = sql.Table('objects')

    subquery1 = objects.select(objects.id, where = (objects.uid == uuid))
    subquery2 = objects.select(objects.id, where = (objects.uid == person_uuid))

    query = collaboratorships.delete()
    query.where = (collaboratorships.internal_object_id.in_(subquery1)) & (collaboratorships.person_object_id.in_(subquery2))

    self.database.execute(session, query)
    self.database.commit(session)

  def addAuthorship(self, id, person_id):
    """ Adds the given Person as an author of the given Object.
    """

    objects = self.objects.search(id = id)
    if len(objects) == 0:
      return None

    db_obj = objects[0]

    people = self.objects.search(id = person_id)
    if len(people) == 0:
      return None

    db_person = people[0]

    from occam.accounts.records.authorship import AuthorshipRecord

    import sql
    session = self.database.session()

    authorships = sql.Table('authorships')

    query = authorships.select()
    query.where = (authorships.internal_object_id == db_obj.id) & (authorships.person_object_id == db_person.id)

    self.database.execute(session, query)

    items = [AuthorshipRecord(x) for x in self.database.many(session)]

    # Do not add a record if it already exists
    if len(items) > 0:
      return items[0]

    # Add the record
    authorshipRecord = AuthorshipRecord()

    authorshipRecord.internal_object_id = db_obj.id
    authorshipRecord.person_object_id   = db_person.id

    self.database.update(session, authorshipRecord)
    self.database.commit(session)

    return authorshipRecord

  def addCollaboratorship(self, id, person_id):
    """ Adds the given Person as an collaborator of the given Object.
    """

    objects = self.objects.search(id = id)
    if len(objects) == 0:
      return None

    db_obj = objects[0]

    people = self.objects.search(id = person_id)
    if len(people) == 0:
      return None

    db_person = people[0]

    from occam.accounts.records.collaboratorship import CollaboratorshipRecord

    import sql
    session = self.database.session()

    collaboratorships = sql.Table('collaboratorships')

    query = collaboratorships.select()
    query.where = (collaboratorships.internal_object_id == db_obj.id) & (collaboratorships.person_object_id == db_person.id)

    self.database.execute(session, query)

    items = [CollaboratorshipRecord(x) for x in self.database.many(session)]

    # Do not add a record if it already exists
    if len(items) > 0:
      return items[0]

    collaboratorshipRecord = CollaboratorshipRecord()

    collaboratorshipRecord.internal_object_id = db_obj.id
    collaboratorshipRecord.person_object_id   = db_person.id

    session = self.database.session()
    self.database.update(session, collaboratorshipRecord)
    self.database.commit(session)

    return collaboratorshipRecord
