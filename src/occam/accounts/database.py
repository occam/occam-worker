# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sql

from occam.databases.manager import uses, datastore

from occam.objects.database import ObjectDatabase

@uses(ObjectDatabase)
@datastore("accounts")
class AccountDatabase:
  """ Manages the database interactions for the Account component.
  """

  def queryMembershipsFor(self, person, key = None, keyAs = None, table = None):
    """ Retrieves a query the returns a row for each MembershipRecord associated with the given Person Object.
    """

    if keyAs is None:
      keyAs = key

    memberships = sql.Table("memberships")

    if table:
      memberships = table

    from occam.object import Object
    if isinstance(person, Object):
      objectQuery = self.objects.queryObjectRecordFor(person, key = "id")
    else:
      objectQuery = person

    if key is None:
      query = memberships.select()
    else:
      query = memberships.select(memberships.__getattr__(key).as_(keyAs))

    query.where = (memberships.person_object_id.in_(objectQuery))
    query.where = query.where & (memberships.reference_count > 0)

    return query

  def queryMembershipFor(self, group, member):
    """ Retrieves a query for the specific row pertaining to the MembershipRecord
    associated with the given Person group and Person member.
    """

    memberships = sql.Table("memberships")

    baseObjectQuery = self.objects.queryObjectRecordFor(group, key = "id")

    query = self.queryMembershipsFor(member, table = memberships)
    query.where = query.where & (memberships.base_person_object_id.in_(baseObjectQuery))

    return query

  def queryMembershipsOf(self, person, key = None, keyAs = None):
    """ Retrieves a query the returns a row for each MembershipRecord associated with the given Person Object.
    """

    if keyAs is None:
      keyAs = key

    memberships = sql.Table("memberships")

    from occam.object import Object
    if isinstance(person, Object):
      objectQuery = self.objects.queryObjectRecordFor(person, key = "id")
    else:
      objectQuery = person

    if key is None:
      query = memberships.select()
    else:
      query = memberships.select(memberships.__getattr__(key).as_(keyAs))

    query.where = (memberships.base_person_object_id.in_(objectQuery))
    query.where = query.where & (memberships.reference_count > 0)

    return query

  def retrieveMembershipsFor(self, person):
    """ Retrieves a list of each MembershipRecord associated with the given Person Object.
    """

    from occam.accounts.records.membership import MembershipRecord

    session = self.database.session()

    query = self.queryMembershipsFor(person)

    self.database.execute(session, query)

    records = self.database.many(session, size=1000)
    return [MembershipRecord(x) for x in records]

  def retrieveMembershipObjectsFor(self, person):

    from occam.accounts.records.membership import MembershipRecord

    session = self.database.session()

    query = self.queryMembershipsFor(person, key = "base_person_object_id", keyAs = "id")
    return self.objects.retrieveObjectsForIds(query)

  def retrieveMembershipsOf(self, person):
    """ Retrieves a list of each MembershipRecord that has the given Person Object as a member.
    """

    from occam.accounts.records.membership import MembershipRecord

    session = self.database.session()

    query = self.queryMembershipsOf(person)

    self.database.execute(session, query)

    records = self.database.many(session, size=1000)
    return [MembershipRecord(x) for x in records]

  def retrieveMembershipObjectsOf(self, person):

    from occam.accounts.records.membership import MembershipRecord

    session = self.database.session()

    query = self.queryMembershipsOf(person, key = "person_object_id", keyAs = "id")
    return self.objects.retrieveObjectsForIds(query)

  def retrieveMembershipFor(self, group, member):
    """ Returns the MembershipRecord for the given group and member.
    """

    from occam.accounts.records.membership import MembershipRecord

    session = self.database.session()

    query = self.queryMembershipFor(group, member)

    self.database.execute(session, query)
    record = self.database.fetch(session)
    if record:
      record = MembershipRecord(record)

    return record

  def insertMembership(self, group, member):
    import sql.conditionals

    # Create Membership record to link group to the member

    # Then, create a Membership record to link that member to all owners of that group
    # Or, increment the record if it already exists (since you can be a member
    #   of a particular subgroup more than once through different intermediaries)
    # Poll until the record is created/updated.
    
    # This SQL query will perform an upsert with an increment in SQLite3
    #"with new (person_object_id, base_person_object_id) as ( values(%, %) ) insert or replace into memberships (id, person_object_id, base_person_object_id, reference_count) select old.id, new.person_object_id,  new.base_person_object_id, (old.reference_count+1) from new left join memberships as old on new.person_object_id = old.person_object_id and new.base_person_object_id = old.base_person_object_id"

    # Or??
    #"begin; insert into blah if not exists(); update blah = blah + 1; commit;"

    # Or this:
    #"insert or replace into memberships (id, person_object_id, base_person_object_id, reference_count) values ((coalesce((select id from memberships where person_object_id = 0 and base_person_object_id = 1), NULL)), 0, 1, coalesce((select reference_count from memberships where person_object_id = 0 and base_person_object_id = 1), 0) + 1);"

    # We go for, instead however, a two command transaction, for ease of implementation and to conform with python-sql
    # and the most portable SQL:

    session  = self.database.session()

    memberships = sql.Table("memberships")

    objectQuery = self.objects.queryObjectRecordFor(member, key = "id")
    baseObjectQuery = self.objects.queryObjectRecordFor(group, key = "id")
    baseMembershipQuery = self.queryMembershipsFor(group).select(memberships.base_person_object_id)

    # Insert the initial record for the given member and group

    query = memberships.insert()
    query.columns = [memberships.person_object_id, memberships.base_person_object_id]
    query.values = sql.Select(columns = [objectQuery, baseObjectQuery])
    query.values.where = sql.operators.Not(sql.operators.Exists(memberships.select(sql.Literal(1), where = (memberships.person_object_id.in_(objectQuery)) & (memberships.base_person_object_id.in_(baseObjectQuery)))))

    # INSERT INTO memberships (person_object_id, base_person_object_id) VALUES
    #   (SELECT <objectQuery.id>, <baseObjectQuery.id> WHERE NOT EXISTS
    #     (SELECT 1 FROM 'memberships' WHERE 
    #        memberships.person_object_id IN <objectQuery> AND memberships.base_person_object_id IN <baseObjectQuery>))
    # (IN is used in the final clause because the sql query creation is messed up when it is '=')
    self.database.execute(session, query)

    # Insert the extra records for membership in the group's groups

    # We need to reference the table a different way so it gets a new alias (that is, it is assigned a different "AS x")
    membershipsB = sql.Table("memberships")
    query = memberships.insert()
    query.columns = [memberships.person_object_id, memberships.base_person_object_id]
    subQuery = memberships.select(objectQuery, memberships.base_person_object_id)
    subQuery.where = (memberships.person_object_id.in_(baseObjectQuery))
    subQuery.where = subQuery.where & sql.operators.Not(sql.operators.Exists(membershipsB.select(sql.Literal(1), where = (membershipsB.person_object_id.in_(objectQuery)) & (membershipsB.base_person_object_id == (memberships.base_person_object_id)))))
    query.values = subQuery

    # INSERT INTO memberships (person_object_id, base_person_object_id) VALUES
    #   (SELECT <objectQuery.id>, base_person_object_id AS 'a' WHERE
    #     a.person_object_id = <baseObjectQuery.id> AND NOT EXISTS
    #       (SELECT 1 FROM 'memberships' AS 'b' WHERE b.person_object_id IN <objectQuery> AND
    #         b.base_person_object_id = a.base_person_object_id))
    self.database.execute(session, query)

    # Increment the records

    query = memberships.update(columns = [memberships.reference_count], values=[memberships.reference_count + sql.Literal(1)])

    query.where = (memberships.person_object_id.in_(objectQuery))
    query.where = query.where & ((memberships.base_person_object_id.in_(baseMembershipQuery)) | (memberships.base_person_object_id.in_(baseObjectQuery)))

    # UPDATE 'memberships' (reference_count) VALUES (reference_count + 1) WHERE
    #   person_object_id = <objectQuery.id> AND
    #   (base_person_object_id IN (<baseMembershipQuery>) OR
    #     base_person_object_id = <baseObjectQuery.id>) AND
    #   reference_count > 0
    self.database.execute(session, query)

    # Retrieve the field
    query = self.queryMembershipFor(group, member)
    self.database.execute(session, query)

    # Perform the transaction
    self.database.commit(session)

    record = self.database.fetch(session)
    if record:
      from occam.accounts.records.membership import MembershipRecord
      record = MembershipRecord(record)

    return record

  def deleteMembership(self, group, member):
    """ Removes the member from the given group.
    """

    # Gather all membership records pointing from that person to the group in question
    # Decrement their reference counts
    # Delete them if they are now zero

    session  = self.database.session()

    memberships = sql.Table("memberships")

    # Decrement reference counts for the existing records
    # First, the one between member and group
    # And also, all memberships between member and base objects of group
    # This is easier if there exists a membership between person -> themselves

    query = memberships.update(columns = [memberships.reference_count], values = [memberships.reference_count - sql.Literal(1)])

    objectQuery = self.objects.queryObjectRecordFor(member, key = "id")
    baseObjectQuery = self.objects.queryObjectRecordFor(group, key = "id")
    baseMembershipQuery = self.queryMembershipsFor(group).select(memberships.base_person_object_id)

    query.where = (memberships.person_object_id.in_(objectQuery))
    query.where = query.where & ((memberships.base_person_object_id.in_(baseMembershipQuery)) | (memberships.base_person_object_id.in_(baseObjectQuery)))
    query.where = query.where & (memberships.reference_count > 0)

    # UPDATE 'memberships' (reference_count) VALUES (reference_count - 1) WHERE
    #   person_object_id = <objectQuery.id> AND
    #   (base_person_object_id IN (<baseMembershipQuery>) OR
    #     base_person_object_id = <baseObjectQuery.id>) AND
    #   reference_count > 0
    self.database.execute(session, query)

    # Delete all memberships with 0 in the reference_count

    # If another query beats us to this and upserts the reference_count + 1, this is ok
    # This will only trigger when the reference_count is in fact 0
    # We don't necessarily care that this record is destroyed as we only pull out
    # records when the reference_count > 0, but clean-up is good and can obscure
    # membership when people are removed, which may be useful for privacy etc.
    # (Although, the transaction should be atomic, so not a real worry)
    query = memberships.delete()

    query.where = (memberships.person_object_id.in_(objectQuery))
    query.where = query.where & (memberships.reference_count == 0)

    # DELETE FROM 'memberships' WHERE
    #   person_object_id IN <objectQuery> AND
    #   reference_count = 0
    self.database.execute(session, query)

    # Commit the transaction
    self.database.commit(session)
