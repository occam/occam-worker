# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.log import Log

from occam.manager import uses

from occam.accounts.manager    import AccountManager
from occam.permissions.manager import PermissionManager
from occam.objects.manager     import ObjectManager

from occam.commands.manager import command, option, argument

@command('accounts', 'remove',
  category      = 'Account Management',
  documentation = "Removes a person from an existing object.")
@argument("person", type="object")
@argument("object", type="object")
@option("-r", "--role", dest    = "role",
                        action  = "store",
                        default = "author",
                        help    = "the type of role to remove them from.")
@uses(AccountManager)
@uses(ObjectManager)
@uses(PermissionManager)
class RemoveCommand:
  def do(self):
    # Resolve the person
    person = self.objects.resolve(self.options.person, person = self.person)

    if person is None:
      Log.error("Person could not be found.")
      return -1

    personInfo = self.objects.infoFor(person)
    personName = personInfo.get('name', 'unknown')

    obj = self.objects.resolve(self.options.object, person = self.person)
    if obj is None:
      Log.error("The object could not be found.")
      return -1

    # Reject if we cannot write to the given object
    person_id = None
    if self.person:
      person_id = self.person.id

    if not self.permissions.can("write", obj.id, obj.revision, person_id):
      Log.error("The object could not be found.")
      return -1

    objInfo = self.objects.infoFor(obj)
    objName = objInfo.get('name', 'unknown')

    # Delete the authorship
    if self.options.role == "author":
      self.accounts.removeAuthorship(objInfo.get('id'), personInfo.get('id'))
    elif self.options.role == "collaborator":
      self.accounts.removeCollaboratorship(objInfo.get('id'), personInfo.get('id'))
    elif self.options.role == "member":
      self.accounts.removeMember(obj, person)
    else:
      Log.error("Cannot understand the role %s." % (self.options.role))
      return -1

    Log.done("Successfully removed the authorship")
    return 0
