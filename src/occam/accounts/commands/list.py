# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# occam list dd44fcce-5274-11e5-b1d4-dc85debcef4e  # defaults to listing "/"
# occam list dd44fcce-5274-11e5-b1d4-dc85debcef4e/ # same as above
#
# occam list dd44fcce-5274-11e5-b1d4-dc85debcef4e/configs/0
#
# occam list dd44fcce-5274-11e5-b1d4-dc85debcef4e@abc123/configs/0

from occam.commands.manager import command, option, argument

from occam.manager import uses

from occam.accounts.manager   import AccountManager
from occam.objects.manager    import ObjectManager
from occam.resources.manager  import ResourceManager

from occam.object import Object
from occam.log import Log

import os

@command('accounts', 'list',
  category      = 'Account Management',
  documentation = "Lists the members of the given group.")
@argument("object", type = "object", nargs = '?')
@option("-j", "--json", dest   = "to_json",
                        action = "store_true",
                        help   = "returns result as a json document")
@uses(AccountManager)
@uses(ObjectManager)
class List:
  """ The list command will view the contents of a Person, specifically the members of the group.
  """

  def do(self):
    # Retrieve object
    obj = self.objects.resolve(self.options.object)
    if obj is None:
      # Cannot find the object
      Log.error("Cannot find object")
      return -1

    # Pull Memberships
    membershipsOf = self.accounts.retrieveMembershipsOf(obj)

    membershipsOf = [{
      "id": item.uid,
      "revision": item.revision,
      "name": item.name,
      "type": item.object_type,
      "description": item.description
    } for item in membershipsOf]

    membershipsFor = self.accounts.retrieveMembershipsFor(obj)

    membershipsFor = [{
      "id": item.uid,
      "revision": item.revision,
      "name": item.name,
      "type": item.object_type,
      "description": item.description
    } for item in membershipsFor]

    for item in membershipsOf:
      personInfo = self.objects.infoFor(self.objects.retrieve(uuid = item['id']))
      item['images'] = personInfo.get('images', [])

    for item in membershipsFor:
      personInfo = self.objects.infoFor(self.objects.retrieve(uuid = item['id']))
      item['images'] = personInfo.get('images', [])

    memberships = {
      "members": membershipsOf,
      "memberOf": membershipsFor
    }

    if self.options.to_json:
      import json
      Log.output(json.dumps(memberships))
    else:
      Log.output(memberships)

    return 0
