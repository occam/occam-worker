# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import getpass

from occam.log import Log

from occam.manager import uses

from occam.commands.manager import command, option, argument

from occam.accounts.manager import AccountManager

@command('accounts', 'login')
@argument("username_or_token", type = str)
@option("-p", "--password",       dest   = "password",
                                  action = "store",
                                  help   = "the password to use for the account")
@option("-a", "--authenticate",   dest   = "authenticate",
                                  action = "store_true",
                                  help   = "pass along a token instead of a username and authenticate the token.")
@option("-t", "--generate-token", dest   = "generate_token",
                                  action = "store_true",
                                  help   = "when given, outputs the authentication token instead of authenticating a terminal session")
@uses(AccountManager)
class LoginCommand:
  """ Authenticates via password in the terminal creating an authentication token.

  When you are on the command-line, you can use occam login to authenticate the
  current user. It will store the authentication token in the user's home path.
  When Occam runs any command that requires authentication, it will read that
  auth token (usually in .occam/auth.json) and use that token as though it were
  passed as "-T".
  
  You can also simply generate a token which can be used to authenticate an action
  in any terminal session. The stdout of the login command will be the auth token
  you would pass as "-T" to any occam command. You also need to pass the "-A" (or
  --as) argument with the id of the Person you are trying to be, as the secret
  key used to decrypt the token is per-Person, not per-Node.
  """

  def do(self):
    Log.header("Authenticating")

    if self.options.authenticate:
      # Determine if token matches
      person_id, token = self.options.username_or_token.split(':')

      if self.options.generate_token:
        Log.error("Cannot use --generate-token with --authenticate")
        return -1

      person = self.accounts.currentPerson(token, person_id)
      if person is None:
        Log.error("Authentication failed.")
        return -1

      account_db_object = self.accounts.retrieveAccount(person_id = person_id)
      person_db, object_db = self.accounts.retrievePersonObject(account_db_object)
      import json

      output = {
        "token": token,
        "person": person_db._data,
        "roles": person.roles
      }
      Log.output(json.dumps(output))

      Log.done("successfully authenticated as %s" % (person_id))
      return 0

    # Get the password
    password = None
    if self.options.password is not None:
      password = self.options.password
    else:
      Log.write("waiting for password")
      password = getpass.getpass('\npassword for %s (typing will be hidden): ' % (self.options.username_or_token))

    # Determine if credentials match
    if not self.accounts.authenticate(username=self.options.username_or_token, password=password):
      Log.error("username or password is invalid")
      return -1

    # Retrieve the account for this username
    account = self.accounts.retrieveAccount(username=self.options.username_or_token)

    import json

    # Generates a session token for the current terminal for that account
    if self.options.generate_token:
      personObject = self.accounts.retrievePersonObject(account)
      token = self.accounts.generateToken(account, personObject)
      Log.write("auth token: ", end="")
      output = {
        "token": token,
        "person": personObject[1]._data,
        "roles": account.roles.split(';')
      }
      if "uid" in output["person"]:
        del output["person"]["uid"]
      Log.output(json.dumps(output))
    else:
      self.accounts.generateAuthentication(account)

    Log.done("successfully authenticated as %s" % (self.options.username_or_token))
    return 0
