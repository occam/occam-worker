# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.log import Log

from occam.manager import uses

from occam.accounts.manager import AccountManager
from occam.objects.manager  import ObjectManager

from occam.commands.manager import command, option, argument

@command('accounts', 'password',
  category      = 'Account Management',
  documentation = "Resets the password for the given account.")
@argument("update_account_name")
@option("-p", "--password",     dest    = "password",
                                action  = "store",
                                type    = str,
                                help    = "the current password (if not an administrator)")
@option("-n", "--new-password", dest    = "new_password",
                                action  = "store",
                                type    = str,
                                help    = "the new password")
@uses(ObjectManager)
@uses(AccountManager)
class PasswordCommand:
  def do(self):
    account = None

    if self.options.password is None:
      # Only administrators can perform this action
      if self.person is None:
        Log.error("Must be logged in.")
        return -1

      if 'administrator' not in self.person.roles:
        Log.error("Only an administrator can update the password for an account.")
        return -1

    Log.header("Updating password for account %s" % self.options.update_account_name)

    name = self.options.update_account_name

    account = self.accounts.retrieveAccount(name)

    if account is None:
      Log.error("No account exists under that name")
      return -1

    password = self.options.new_password
    if password is None:
      # getpass obscures passwords in the terminal
      import getpass
      password = getpass.getpass('\npassword for %s: ' % (name))
      confirm  = getpass.getpass('confirm password: ')

      if password != confirm:
        Log.error("Passwords do not match.")
        return -1

    self.accounts.updatePassword(account, password)

    Log.done("Successfully updated %s" % (name))
    return 0
