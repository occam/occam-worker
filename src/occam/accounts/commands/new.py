# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from occam.log import Log

from occam.manager import uses

from occam.accounts.manager    import AccountManager
from occam.permissions.manager import PermissionManager
from occam.objects.manager     import ObjectManager
from occam.keys.write_manager  import KeyWriteManager

from occam.commands.manager import command, option, argument

@command('accounts', 'new',
  category      = 'Account Management',
  documentation = "Creates a new account or group on the local system.")
@argument("username")
@option("-p", "--password", dest    = "password",
                            action  = "store",
                            help    = "the password to use for the account (it will ask you, otherwise)")
@option("-g", "--group",   action   = "store_true",
                           dest     = "group",
                           default  = False,
                           help     = "creates an unauthenticated group")
@option("-o", "--role",     dest    = "roles",
                            action  = "append",
                            default = None,
                            type    = str,
                            help    = "adds the given role to the account (requires administrator role)")
@option("-s", "--subtype", action="append",
                           default = None,
                           dest = "subtype",
                           type = str,
                           help="the subtype for the new Person object")
@uses(ObjectManager)
@uses(AccountManager)
@uses(PermissionManager)
@uses(KeyWriteManager)
class NewCommand:
  def do(self):
    account = None

    if self.options.roles is None:
      self.options.roles = []

    if len(self.options.roles) > 0:
      # Only administrators can perform this action
      if self.person is None or 'administrator' not in self.person.roles:
        Log.error("Only an administrator can add roles to an account.")
        return -1

    Log.header("Creating new account for %s" % self.options.username)

    name = self.options.username

    if not self.options.group:
      account = self.accounts.retrieveAccount(name)
      if account is not None:
        Log.error("Username already taken.")
        return -1

    # Get a password

    password = self.options.password
    if password is None and not self.options.group:
      # getpass obscures passwords in the terminal
      import getpass
      password = getpass.getpass('\npassword for %s: ' % (name))
      confirm  = getpass.getpass('confirm password: ')

      if password != confirm:
        Log.error("Passwords do not match.")
        return -1

    if self.options.group and self.options.roles:
      Log.error("Groups (unauthenticated accounts) cannot have roles")
      return -1

    # Create identity
    identity, identityKey = self.keys.write.createIdentity()

    obj = self.accounts.addPerson(name, identity.uri, self.options.subtype)
    objectInfo = self.objects.infoFor(obj)

    person_id = self.objects.idFor(obj, identity.uri)

    if not self.options.group:
      roles = self.options.roles
      self.accounts.addAuthentication(person_id, name, password, identityKey, roles)

    Log.write("new identity:  ", end="")
    Log.output(identity.uri, padding="")
    Log.write("new person id: ", end="")
    Log.output(person_id, padding="")
    #Log.output(objectInfo.get('id'), padding="")

    # Nobody else should be able to edit this Person
    self.permissions.update(id = objectInfo.get('id'), canRead=True, canWrite=False, canClone=False)

    # This person should be able to update themselves
    self.permissions.update(id = objectInfo.get('id'), person_id = objectInfo.get('id'), canRead=True, canWrite=True, canClone=False)

    Log.done("Successfully created %s" % (name))
    return 0
