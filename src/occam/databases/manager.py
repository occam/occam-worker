# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# On the topic of database ORM libraries that could be used instead:

# Much of this would be implemented just as well or better by an ORM type of
# library such as SQLAlchemy. However, that library is so bulky that it slows
# down just about every operation done on the command line because of how long
# it takes to import (around 0.4 to 0.5 seconds) Nothing I tried, no matter how
# clever I thought I was being, worked to reduce this quite noticeable lag.

# Therefore, since the database is used to *speed up* operations, and the ORM
# library makes that prohibitive, we are rolling our own light ORM-type thing
# and creating SQL queries ourselves (which is something you generally end up
# crafting anyway for trickier and heavier queries.)

# The ORM functionality is not really needed by anything the toolchain does.
# Much of the logic would be detached from it, anyway. It just can't justify
# itself enough as a 'clean-code' solution when it gives so much headache.

# Also, this allows for us to create database driver backends for anything
# that is not supported by the ORM much more easily if we ever need to.

# Used to import using a string as a path. This allows us to lazyload the
# database driver to just load the one referred by the configuration file.
# We won't be able to simple import all the drivers because we don't want
# to force the installation of all driver libraries. For instance, postgres
# driver requires psycopg2, which requires the installation of a systemwide
# postgresql installation. That's unacceptable when you have people who will
# simply use sqlite3.

from occam.manager import uses as manager_uses, manager
from occam.config  import Config
from occam.object  import Object

from occam.log import loggable

import sql

class Reader:
  def __init__(self, reader, writer):
    self.write  = writer
    self.reader = reader

  def __getattr__(self, token):
    return self.reader.__getattr__(token)

@loggable
@manager("database")
class DatabaseManager:
  """ This manages database access and management.
  """

  # The database drivers loaded
  drivers    = {}

  # The table schemas we know of
  tables     = {}

  # The datastores we are aware of
  datastores = {}

  def __init__(self):
    """Instantiates the Database Manager.
    """

    self._driver = None

  @staticmethod
  def register(adapter, driverClass):
    """ Adds a new database driver to the possible drivers.
    """
    DatabaseManager.drivers[adapter] = driverClass

  @staticmethod
  def registerTable(tableName, tableClass):
    """ Adds a new database table to the list of known tables.
    """
    DatabaseManager.tables[tableName] = tableClass

  @staticmethod
  def registerDatastore(managerName, datastoreClass):
    """ Adds a new database manager to the list of known datastores.
    """

    DatabaseManager.datastores[managerName] = datastoreClass

  @staticmethod
  def datastoreFor(manager):
    """ Returns the datastore for the given manager name.

    Returns None if the datastore is not known.
    """

    return DatabaseManager.datastores.get(manager)

  def driverFor(self, adapter):
    """ Returns an instance of a driver for the given database adapter.
    """

    if not adapter in self.drivers:
      # TODO: error?
      adapter = 'sqlite3'

    return self.drivers[adapter](self.configuration)

  def disconnect(self):
    """ Uninitializes a database connection.
    """

    self._driver = None

  def connect(self):
    """ Initializes a database connection.
    """

    # Determine the database type
    if self._driver is None:
      databasePath = Config.root()
      adapter      = self.configuration.get('adapter', 'sqlite3')

      # Attempt to find that database driver
      import importlib

      # try:
      importlib.import_module("occam.databases.plugins.%s" % (adapter))
      # except ImportError:
      #   pass

      self._driver = self.driverFor(adapter)

    return self._driver

  def recordFor(self, uuid):
    """ Retrieves the ObjectRecord for the given object uuid.
    """

    rows = self.retrieveObjects(uuid = uuid)
    if len(rows) > 0:
      return rows[0]

    return None

  def insert(self, record):
    """ Inserts the given record into the database.
    """

    session = self.session()

    table = sql.Table(record._table)

    query = table.insert(columns = [table.__getattr__(x) for x in list(record._dirty.keys())],
                         values  = list(record._dirty.values()))

    self._driver.execute(session, query)

  def commit(self, session):
    """ Commits any changes made to the database.
    """

    self._driver.commit(session)

  def retrieveOutputs(self, objectRecord):
    """ Retrieves the ObjectOutputRecord rows for the given ObjectRecord.
    """

    session = self.session()

    outputs = sql.Table('object_outputs')

    query = outputs.select()
    query.where = (outputs.occam_object_id == objectRecord.id)

    from occam.objects.records.object_output import ObjectOutputRecord

    self._driver.execute(session, query)

    records = self._driver.many(session, size=10)
    records = [ObjectOutputRecord(x) for x in records]

    return records

  def retrieveInputs(self, objectRecord):
    """ Retrieves the ObjectOutputRecord rows for the given ObjectRecord.
    """

    session = self.session()

    inputs = sql.Table('object_inputs')

    query = inputs.select()
    query.where = (inputs.occam_object_id == objectRecord.id)

    from occam.objects.records.object_input import ObjectInputRecord

    self._driver.execute(session, query)

    records = self._driver.many(session, size=10)
    records = [ObjectInputRecord(x) for x in records]

    return records

  def execute(self, session, query):
    """ Executes the given query within the given session.
    """

    self._driver.execute(session, query)

    return self._driver.rowCount(session)

  def fetch(self, session):
    """ Fetches the next record from the current session.
    """
    return self._driver.fetch(session)

  def many(self, session, size=10):
    """ Fetches the next set of records from the current session.
    """
    return self._driver.many(session, size)

  def delete(self, session, record):
    """ Deletes the given record or records.
    """

    # Recursively call if this is a list of records
    if isinstance(record, list):
      for item in record:
        self.delete(item)
      return

    table = sql.Table(record._table)

    query = table.delete()
    query.where = (table.id == record.id)

    self._driver.execute(session, query)
    self._driver.commit(session)

    # TODO: return False on error.
    return True

  def session(self):
    """ Creates a session (typically a cursor) to a database.
    
    You can then execute queries and create transactions with this session.
    """

    return self.connect().session()

  def update(self, session, record):
    """ Saves a new version of the given row representing a record.
    """

    # Recursively call if this is a list of records
    if isinstance(record, list):
      for item in record:
        self.update(item)
      return

    table = sql.Table(record._table)

    columns = [table.__getattr__(x) for x in list(record._dirty.keys())]
    values  = [x if x is not True and x is not False else (0 if x is False else 1) for x in record._dirty.values()]

    if record._newRecord:
      # This triggers python-sql's table.attribute style usage for update():

      # INSERT queries can insert multiple rows so values is an array
      query = table.insert(columns = columns, values = [values])
    else:
      # This triggers python-sql's table.attribute style usage for update():

      if columns == []:
        # Nothing to update
        return True

      # UPDATE only takes a single list of values because it is performed per row
      query = table.update(columns = columns, values = values)
      query.where = (table.id == record.id)

    self._driver.execute(session, query)

    if record._newRecord:
      record._newRecord = False
      row_id = self._driver.lastRowID(session)
      if record._data.get('id') is None:
        record._data['id'] = row_id

    return self._driver.rowCount(session)

  def createTable(self, tableName, tableClass):
    """ This will create the given table.
    """

    session = self.session()

    DatabaseManager.Log.noisy("Creating the %s table." % (tableName))
    self._driver.create(session, tableName, tableClass.schema)

  def createDatabase(self):
    """ This will create the database and initialize the tables.
    """

    # Load the driver
    session = self.session()

    # Load all tables in the project
    import os
    import importlib

    # These are located in each component (under 'components' in the configuration)
    configuration = Config.load()
    components = configuration.get('system', {}).get('components', [])

    for component in components:
      component_module = importlib.import_module("%s.manager" % (component))
      component_path = os.path.dirname(component_module.__file__)

      tablePath = os.path.realpath("%s/records" % (os.path.realpath(component_path)))

      # Import every command and list its help contents
      if os.path.exists(tablePath):
        for f in os.listdir(tablePath):
          if f.startswith("_") or not f.endswith(".py"):
            continue

          moduleName = os.path.splitext(f)[0]

          try:
            importlib.import_module("%s.records.%s" % (component, moduleName))
          except ImportError:
            pass

    # Create tables using the driver
    # We have to order them by who depends on who (for foreign key refs)

    # Look for foreign keys
    dependencies = {}
    rootTables   = []

    for tableName, tableClass in DatabaseManager.tables.items():
      dependencies[tableName] = []
      for k,v in tableClass.schema.items():
        if "foreign" in v:
          # Get the dependency
          dependency = v['foreign']['table']

          # Do not add the dependency if it is trying to point to itself
          if dependency != tableName:
            if not dependency in dependencies[tableName]:
              dependencies[tableName].append(dependency)

      if len(dependencies[tableName]) == 0:
        rootTables.append((tableName, tableClass,))

    # For each root table, create that table
    while len(rootTables) > 0:
      tableName, tableClass = rootTables.pop()

      self.createTable(tableName, tableClass)

      for k, v in dependencies.items():
        if tableName in v:
          v.remove(tableName)

          if len(v) == 0:
            rootTables.append((k, DatabaseManager.tables[k],))

def database(name):
  """ This decorator will register a possible database backend.
  """

  def _database(cls):
    DatabaseManager.register(name, cls)
    return cls

  return _database

def table(tableName):
  """ This decorator will register a table into our database. It will keep
      track of changes which can then be used by a database driver to update
      the records.
  """

  def _table(cls):
    DatabaseManager.registerTable(tableName, cls)

    # Go through the schema and create the attributes
    def _readAttribute(self, name):
      if name in cls.schema:
        if not name in self._data:
          self._data[name] = cls.schema[name].get('default')
        if isinstance(self._data[name], str) and cls.schema[name].get('type') == "json":
          import json
          try:
            self._data[name] = json.loads(self._data[name])
          except:
            self._data[name] = None
        return self._data[name]

      return self.__getattribute__(name)

    old_setattr = cls.__setattr__

    def _updateAttribute(self, name, value):
      if name in cls.schema:
        self._data.__setitem__(name, value)
        if 'type' in cls.schema[name] and cls.schema[name]['type'] == 'json':
          import json
          value = json.dumps(value)
        self._dirty.__setitem__(name, value)
        return value

      return old_setattr(self, name, value)

    old_init = cls.__init__

    def _initialize(self, data=None):
      self._newRecord = False
      if data is None:
        data = {}
        self._newRecord = True
      self._data = data
      old_init(self)

      self._dirty = {}

    cls.__getattr__ = _readAttribute
    cls.__setattr__ = _updateAttribute
    cls.__init__    = _initialize

    cls._table = tableName

    return cls

  return _table

def uses(datastoreClass):
  def _uses(cls):
    writerClass = None
    readerClass = datastoreClass

    # Our base case propagation of __getattr__
    def _empty(self, token):
      return self.__getattribute__(token)
    datastoreGetattr = _empty

    if datastoreClass._reader:
      # We append the reader, not ourselves
      # The reader will append us when it sees a 'write' attribute
      writerClass = datastoreClass
      readerClass = datastoreClass._reader

    # Ensure datastore table exists
    if not hasattr(cls, '_datastores'):
      cls._datastores = {}
      cls._datastoreTokens = []
      cls._writers = {}

    if not readerClass._token in cls._datastores:
      # The datastore is new... initialize the lazy loading:

      try:
        # This will fail when __getattr__ isn't already known
        datastoreGetattr = cls.__getattr__
      except:
        pass

      def initManager(self, token):
        if (token in cls._datastoreTokens):
          datastore = cls._datastores[token]

          if datastore._instance is None:
            datastore._instance = datastore()

          instance = datastore._instance

          if (token in cls._writers):
            writer = cls._writers[token]
            if writer._instance is None:
              writer._instance = writer()
            instance = Reader(instance, writer._instance)

          setattr(self, token, instance)
          return instance

        return datastoreGetattr(self, token)

      cls.__getattr__ = initManager

      cls._datastores[readerClass._token] = readerClass

      if writerClass:
        cls._writers[readerClass._token]  = writerClass

    cls._datastoreTokens.append(readerClass._token)

    return cls

  return _uses

def datastore(name, reader=None):
  """ Decorator that marks a class with the capability of accessing the database.
  """

  def _datastore(cls):
    cls._instance = None
    cls._token = name
    cls._reader = reader

    def _empty(self, token):
      return self.__getattribute__(token)

    try:
      old_getattr = cls.__getattr__
    except:
      old_getattr = _empty

    cls = manager_uses(DatabaseManager)(cls)
    DatabaseManager.registerDatastore(name, cls)
    return cls

  return _datastore

class DataError(Exception):
  """ Base class for all database errors.
  """

class DataNotUniqueError(DataError):
  """ When a database record to be inserted already exists.
  """
