# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.databases.manager import database, DataNotUniqueError

from occam.config import Config
from occam.log import loggable

import sql

import os

@loggable
@database('sqlite3')
class SQLite3:
  """ This implements an SQLite3 backend.
  """

  def __init__(self, configuration):
    """ Initializes the SQLite3 driver.
    """

    from threading import local
    self.localThread = local()

    self.connections = {}

  def connect(self):
    import sqlite3

    # TODO: add configuration setting
    databasePath = os.path.join(Config.root(), "occam.db")

    # PARSE_DECLTYPES : parses TIMESTAMP into datetime
    self.localThread.occam_sqlite3_connection = sqlite3.connect(databasePath, detect_types=sqlite3.PARSE_DECLTYPES|sqlite3.PARSE_COLNAMES)
    self.setFlavor()

  def setFlavor(self):
    """ Sets the flavors for this thread
    """

    def dict_factory(cursor, row):
      d = {}
      for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
      return d

    self.localThread.occam_sqlite3_connection.row_factory = dict_factory
    sql.Flavor.set(sql.Flavor(paramstyle='qmark'))

  def session(self):
    """ Returns an SQLite3 cursor.
    """

    connected = getattr(self.localThread, 'occam_sqlite3_connection', None)
    if not connected:
      self.connect()

    cursor = self.localThread.occam_sqlite3_connection.cursor()

    return cursor

  def execute(self, session, query):
    """ Executes the given query upon the given session.
    """

    import sqlite3

    while True:
      try:
        ret = session.execute(*tuple(query))
        break
      except sqlite3.IntegrityError as e:
        if "UNIQUE constraint failed" in str(e):
          raise DataNotUniqueError()
      except sqlite3.OperationalError as e:
        import time
        time.sleep(1)

    return ret

  def fetch(self, session, size=1):
    """ Gets the next row from the query result for this session.
    """

    return session.fetchone()

  def many(self, session, size=1):
    """ Gets the next rows from the query result for this session. Will pull
        the number of rows indicated by 'size'. It may return less than the
        requested number of rows when not enough are available.
    """

    return session.fetchmany(size)

  def commit(self, session):
    """ Commits the given session.
    """

    return self.localThread.occam_sqlite3_connection.commit()

  def create(self, session, tableName, tableSchema):
    """ Creates the table (or alters the table) with the given table schema.
    """

    # We have to form this command ourselves since python-sql doesn't have it
    # and, to be honest, sqlite3's alter table support is less than good.

    prelude  = "CREATE TABLE IF NOT EXISTS %s (" % (tableName)
    epilogue = ");"

    query = prelude
    foreign = ""
    constraints = ""

    for key, value in tableSchema.items():
      line = ""
      if value.get('foreign'):
        line = line + "FOREIGN KEY ("

      if value.get('constraint'):
        line = line + "CONSTRAINT "

      line = line + key

      if value.get('constraint'):
        line = line + " " + value['constraint'].get('type', "unique").upper()
        if 'columns' in value['constraint']:
          line = line + " (" + ",".join(value['constraint'].get('columns', [])) + ")"
        if 'conflict' in value['constraint']:
          line = line + " ON CONFLICT " + value['constraint'].get('conflict', 'replace').upper()

      if value.get('foreign'):
        line = line + ") REFERENCES %s (%s)" % (value['foreign'].get('table'), value['foreign'].get('key'))

      if value.get('type') == "integer":
        line = line + " INTEGER"

      if value.get('type') == "string":
        if isinstance(value.get('length', 128), int):
          line = line + " VARCHAR(%s)" % (value.get('length', 128))
        else:
          raise TypeError("string attribute length is not an integer")

      if value.get('type') == "text" or value.get('type') == "json":
        line = line + " TEXT"

      if value.get('type') == "datetime":
        line = line + " TIMESTAMP"

      if value.get('type') == "boolean":
        line = line + " INTEGER"

      if value.get('primary'):
        line = line + " PRIMARY KEY"
        if value.get('null') != False:
          line = line + " NOT NULL"

      if value.get('null') == False:
        line = line + " NOT NULL"

      if value.get('unique') == True:
        line = line + " UNIQUE"

      if 'default' in value:
        default = value.get('default')
        if value.get('type') == "string" or value.get('type') == "text":
          default = "'%s'" % (default)
        elif value.get('type') == "boolean":
          default = 0 if default == False else 1
        line = line + " DEFAULT %s" % (str(default))

      if value.get('foreign'):
        foreign = foreign + line + ","
        query = query + (" %s INTEGER," % (key))
      elif value.get('constraint'):
        constraints = constraints + line + ","
      else:
        query = query + line + ","

    query = query + foreign
    query = query + constraints

    if query[-1] == ",":
      query = query[:-1]

    query = query + epilogue

    SQLite3.Log.write(query)
    session.execute(query)

  def lastRowID(self, session):
    """ Retrieves the last row id, typically that was just inserted.
    """

    return session.lastrowid

  def rowCount(self, session):
    """ Retrieves the number of rows affected by the last update.
    """

    return session.rowcount
