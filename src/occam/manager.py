# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.config import Config

class Reader:
  def __init__(self, reader, writer):
    self.write  = writer
    self.reader = reader

  def __getattr__(self, token):
    return self.reader.__getattr__(token)

def uses(managerClass):
  def _uses(cls):
    writerClass = None
    readerClass = managerClass

    # Our base case propagation of __getattr__
    def _empty(self, token):
      return self.__getattribute__(token)
    managerGetattr = _empty

    if managerClass._reader:
      # We append the reader, not ourselves
      # The reader will append us when it sees a 'write' attribute
      writerClass = managerClass
      readerClass = managerClass._reader

    # Ensure manager table exists
    if not hasattr(cls, '_managers'):
      cls._managers = {}
      cls._managerTokens = []
      cls._writers = {}

    if not readerClass._token in cls._managers:
      # The manager is new... initialize the lazy loading:

      try:
        # This will fail when __getattr__ isn't already known
        managerGetattr = cls.__getattr__
      except:
        pass

      def initManager(self, token):
        if (token in cls._managerTokens):
          manager = cls._managers[token]

          if manager._instance is None:
            manager._instance = manager()

          instance = manager._instance

          if (token in cls._writers):
            writer = cls._writers[token]
            if writer._instance is None:
              writer._instance = writer()
            instance = Reader(instance, writer._instance)

          setattr(self, token, instance)
          return instance

        return managerGetattr(self, token)

      cls.__getattr__ = initManager

      cls._managers[readerClass._token] = readerClass

      if writerClass:
        cls._writers[readerClass._token]  = writerClass

    cls._managerTokens.append(readerClass._token)

    return cls

  return _uses

def manager(name, reader=None):
  def _manager(cls):
    cls._instance = None
    cls._token = name
    cls._reader = reader

    def _empty(self, token):
      return self.__getattribute__(token)

    try:
      old_getattr = cls.__getattr__
    except:
      old_getattr = _empty

    def configLoad(self, key):
      if (key == "configuration"):
        config = Config.load()
        config = config.get(name, {})

        setattr(cls, "configuration", config)

        return config
      elif (key == "datastore"):
        # Try to import the database of this manager
        baseClassName = name
        if reader:
          baseClassName = reader._token

        import importlib
        if reader:
          try:
            importlib.import_module("occam.%s.write_database" % baseClassName.lower())
          except ImportError:
            pass

        try:
          importlib.import_module("occam.%s.database" % baseClassName.lower())
        except ImportError:
          return None

        # Retrieve the database
        from occam.databases.manager import DatabaseManager

        database = None
        if reader:
          readInstance  = DatabaseManager.datastoreFor(baseClassName)()
          writeInstance = DatabaseManager.datastoreFor(baseClassName + ".write")
          if writeInstance is not None:
            writeInstance = writeInstance()
            writeInstance.read = readInstance
          database = Reader(readInstance, writeInstance)

        if not database:
          database = DatabaseManager.datastoreFor(baseClassName)()

        # Assign it to self.database
        setattr(cls, "datastore", database)

      return old_getattr(self, key)

    cls.__getattr__ = configLoad

    return cls

  return _manager
