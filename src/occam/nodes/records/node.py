# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.databases.manager import table

@table("nodes")
class NodeRecord:
  schema = {

    # Primary Key

    "id": {
      "type": "integer",
      "primary": True
    },

    # Fields

    "external": {
      "type": "integer",
      "default": 1
    },

    "host": {
      "type": "string",
      "length": 128
    },

    "name": {
      "type": "string",
      "length": 128
    },

    "http_port": {
      "type": "integer",
      "default": 80
    },

    "http_ssl": {
      "type": "boolean",
      "default": False
    },

    "capabilities": {
      "type": "text",
      "default": ";http;"
    },

  }
