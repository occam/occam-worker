# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.log import Log

from occam.manager import uses

from occam.commands.manager import command, option, argument
from occam.nodes.manager    import NodeManager

@command('nodes', 'discover',
  documentation = "Discover an Occam node.")
@argument("url", action = "store")
@uses(NodeManager)
class DiscoverCommand:
  """
  This command will discover an OCCAM node that this node may use in the
  future to discover objects.

  Usage:

  occam discover <HOST>
  occam discover <HOST>:<PORT>

  Examples:

  Discovery by hostname:

  $ occam discover occam.cs.pitt.edu

  Discovery by ip address and port:

  $ occam discover 10.0.0.1:9292
  """

  def do(self):
    """
    Perform the command.
    """

    url = self.options.url

    db_node = self.nodes.discover(url)

    if db_node is None:
      Log.error("could not verify the OCCAM server as given")
      return -1
    name = db_node.name

    if db_node is None:
      Log.error("Could not create the Node")
      return -1

    Log.done("Successfully discovered %s" % (name))
    return 0
