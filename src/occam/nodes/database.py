# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sql

from occam.databases.manager import uses, datastore

@datastore("nodes")
class NodeDatabase:
  """ Manages the database interactions for tracking other known nodes.
  """

  def retrieveList(self):
    """ Returns a set of nodes.
    """

    from occam.nodes.records.node import NodeRecord

    session = self.database.session()

    nodes = sql.Table('nodes')
    query = nodes.select()

    self.database.execute(session, query)
    return [NodeRecord(x) for x in self.database.many(session, size = 100)]

  def retrieve(self, url, host, port):
    """ Returns the database entry for the given node url.
    """

    from occam.nodes.records.node import NodeRecord

    session = self.database.session()

    nodes = sql.Table('nodes')
    query = nodes.select(where = (nodes.host == host))

    self.database.execute(session, query)
    node = self.database.fetch(session)

    if not node is None:
      node = NodeRecord(node)

    return node

  def insert(self, node_info, host, port):
    """ Inserts a NodeRecord for the given node information.
    """

    from occam.nodes.records.node import NodeRecord

    db_node = NodeRecord()

    return self.update(db_node, node_info, host, port)

  def update(self, db_node, node_info, host, port):
    """ Updates the given NodeRecord.
    """

    session = self.database.session()

    db_node.name = node_info.get('name', "OCCAM Node")
    db_node.host = host
    db_node.http_port = port

    if node_info['httpSSL']:
      db_node.http_ssl = 1
    else:
      db_node.http_ssl = 0

    self.database.update(session, db_node)
    self.database.commit(session)

    return db_node
