# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import codecs
import os
import json

from occam.log            import loggable
from occam.git_repository import GitRepository
from occam.object         import Object
from occam.config         import Config

from occam.manager import uses, manager

from occam.network.manager  import NetworkManager
from occam.storage.manager  import StorageManager

from urllib.parse import urlparse

@loggable
@uses(NetworkManager)
@uses(StorageManager)
@manager("nodes")
class NodeManager:
  """
  This OCCAM manager handles the Node listing, Node discovery, and Node lookup.
  """

  CERTIFICATE_FILENAME = 'ssl.crt'

  def __init__(self):
    """
    Initialize the node manager.
    """

  def partialFromURL(self, url):
    """
    This method will take a fully realized URL to a resource on an OCCAM node
    and return the canonical name (domain,port) for that node. Port will be
    omitted when it is a normal port (80, 443)
    """
    if url is None:
      url = ""

    # Make sure it *is* a full URL
    url = self.urlFromPartial(url)

    # urlparse works great on real URLs
    urlparts = urlparse(url)

    # we want the host:port (unless port is 80 or 443)
    url = urlparts.hostname

    if urlparts.port and urlparts.port != 80 and urlparts.port != 443:
      url = url + "," + str(urlparts.port)

    return url

  def urlFromPartial(self, url, scheme="https"):
    """
    This method will return a full url from any partial url given as a Node uri.

    You can give the method a 'scheme' which will be applied to the URL if it
    does not have a scheme already.
    """
    if url is None:
      url = ""

    urlparts = urlparse(url)
    if urlparts.scheme == "":
      url = "%s://%s" % (scheme, url)
      urlparts = urlparse(url)

    if urlparts.netloc == "":
      return None

    return url

  def hostAndPortFromURL(self, url):
    """ Returns a pair representing the host and pair respectively.
    """
    urlparts = urlparse(url)
    host = urlparts.netloc.split(':')[0]

    port = "80"
    host = urlparts.netloc.split(':')[0]

    if ':' in urlparts.netloc:
      port = urlparts.netloc.split(':')[1]
    elif urlparts.scheme == "https":
      port = "443"

    return [host, port]

  def search(self, url):
    """ Returns the database entry for the given node url.
    """

    url = self.urlFromPartial(url)
    host, port = self.hostAndPortFromURL(url)

    return self.datastore.retrieve(url, host, port)

  def retrieveInfo(self, url):
    """ Polls and retrieves the node info from the Node.
    """

    url = self.urlFromPartial(url)
    nodeInfoURL = url + "/system"

    # Get certificate
    if not self.retrieveCertificate(url):
      return None

    cert = self.certificatePathFor(url)

    # Query the url and retrieve node information
    try:
      node_info = self.network.getJSON(nodeInfoURL, scheme='https', suppressError=True, cert=cert)
    except:
      node_info = None

    scheme = "https"

    # Negotiate for HTTPS
    # TODO: Flag on the system that never allows non-HTTPS
    if node_info is None:
      scheme = "http"
      try:
        node_info = self.network.getJSON(nodeInfoURL, scheme='http', suppressError=True, cert=None)
      except:
        node_info = None

      if node_info is None:
        return None

    if not isinstance(node_info, dict):
      # We can't query node information, but that's OK
      node_info = {}

    node_info['httpSSL'] = scheme == "https"

    return node_info

  def pathFor(self, url):
    """ Retrieves the system path for the given node.
    """
    nodePath = self.configuration.get('path', os.path.join(Config.root(), "nodes"))
    nodePath = os.path.join(nodePath, self.partialFromURL(url))

    return nodePath

  def createPathFor(self, url):
    nodePath = self.configuration.get('path', os.path.join(Config.root(), "nodes"))
    if not os.path.exists(nodePath):
      os.mkdir(nodePath)

    nodePath = os.path.join(nodePath, self.partialFromURL(url))
    if not os.path.exists(nodePath):
      os.mkdir(nodePath)

    return nodePath

  def retrieveCertificate(self, url):
    """ Retrieves the public key from the given node.
    """

    import ssl, socket
    urlparts = urlparse(url)
    host = urlparts.hostname

    socket.setdefaulttimeout(1)
    verified = False
    try:
      verified = ssl.get_server_certificate((host, 443,))
    except:
      verified = None

    if verified:
      return True

    url = self.urlFromPartial(url)
    url = url + "/public_key"

    # Download that key
    try:
      response, content_type, size = self.network.get(url, scheme="https", doNotVerify=True, suppressError=True)
    except:
      response = None

    if response is None:
      try:
        response, content_type, size = self.network.get(url, scheme="http", doNotVerify=True, suppressError=True)
      except:
        response = None
        return None

    if response is None:
      return None

    reader = codecs.getreader('utf-8')
    data = reader(response).read()

    if not data:
      return False

    certPath = os.path.join(self.createPathFor(url), self.CERTIFICATE_FILENAME)
    with open(certPath, 'w+') as f:
      f.write(data)

    return True

  def certificatePathFor(self, url):
    """ Gets the certificate path for the given node url.
    """

    nodePath = self.pathFor(url)
    nodePath = os.path.join(nodePath, self.CERTIFICATE_FILENAME)

    if not os.path.exists(nodePath):
      return None

    return nodePath

  def discover(self, url, untrusted=True, quiet=False):
    """ This method will lookup or append a new Node record for the given address.

    Returns None when the node cannot be reached.
    """

    url = self.urlFromPartial(url)
    host, port = self.hostAndPortFromURL(url)

    existing_node = self.search(url)

    NodeManager.Log.noisy("attempting to discover node at %s" % (url))

    node_info = self.retrieveInfo(url)
    if node_info is None:
      if not quiet:
        NodeManager.Log.error("No response from the node specified")
      return None

    # Look up any information we have on the Node

    # Create a record for the Node, if it doesn't exist
    if existing_node:
      # Update Node
      if not quiet:
        NodeManager.Log.write("Updating a known node")
      db_node = self.datastore.update(existing_node, node_info, host, port)
    else:
      # Create Node
      NodeManager.Log.write("Found a new node (%s)" % (url))

      db_node = self.datastore.insert(node_info, host, port)

    # TODO: add untrusted boolean to node table

    # Tell subsystems that might care
    if 'storage' in node_info:
      self.storage.discoverNode(node_info['storage'])

    # Return the Node record
    return db_node

  def urlForNode(self, node, path):
    """ Returns a URL for the given path for the given node.
    """

    scheme = "https"
    if node.http_ssl == 0:
      scheme = "http"

    host = node.host
    port = node.http_port

    return "%s://%s:%s/%s" % (scheme, host, port, path)

  def urlForObject(self, node, uuid, revision=None, subPath='objects'):
    """ Returns a URL for the given object id for the given node.
    """

    path = "%s/%s" % (subPath, uuid)

    if not revision is None:
      path = "%s/%s" % (path, revision)

    return self.urlForNode(node, path)

  def findObject(self, uuid):
    """
    When we are in a bind to find an object, we can call this method to go
    and query for which node has this object. Will return either an empty
    array when the search comes up empty, or at least one Node which has the
    requested object.
    """

    # Naive, at the moment. Just go through the node list and query.

    for node in self.datastore.retrieveList():
      NodeManager.Log.write("looking in %s (%s)" % (node.name, node.host))

      url = self.urlForObject(node, uuid)
      cert = self.certificatePathFor(url)
      result = self.network.getJSON(url, cert=cert)

      if result is not None:
        NodeManager.Log.write("found object at %s (%s)" % (node.name, node.host))
        return [node]

    return []

  def pullAllObjects(self, uuid, objectList=None):
    """
    This will replicate the object and all dependencies from any node which has
    them. If the object cannot be found, this returns None. Otherwise, returns the object.
    """

    # TODO: handle error when some objects can't be found

    if objectList is None:
      objectList = []

    if not uuid in objectList:
      obj = self.pullObject(uuid)

      if obj is None:
        return None

      objectList.append(uuid)

      # Get relations, and pull those
      relationList = obj.relationList()
      for uuid in relationList:
        NodeManager.Log.write("pulling related object %s" % (uuid))
        related_object = self.pullAllObjects(uuid, objectList)
    else:
      NodeManager.Log.write("already pulled")
      return None

    return obj

  def pullObject(self, uuid):
    """
    This will replicate the object by retrieving it from any node which has it.
    If the object cannot be found, this returns None. Otherwise, returns the
    Object.
    """

    # Retrieve a list of nodes with this object
    nodes = self.findObject(uuid)

    # If no node has the object, we fail out
    if len(nodes) == 0:
      return None

    # Pull the object from that node
    return self.pullObjectFrom(nodes[0], uuid)

  def pullObjectInfoFrom(self, node, uuid, revision=None):
    """
    This will pull the object info from the server.
    """

    objectPath = self.urlForObject(node, uuid)

    # Retrieve the certificate file path for the node referenced by this url
    cert = self.certificatePathFor(objectPath)

    objectInfoPath = objectPath + "/raw/object.json"

    response, content_type, size = self.network.get(objectInfoPath, 'application/json', cert=cert, suppressError=True)

    # Parse the json from the stream
    reader = codecs.getreader('utf-8')
    try:
      objectInfo = json.load(reader(response))
    except:
      objectInfo = {}

    # Get the latest revision
    objectLatestPath = objectPath + "/latest"
    response, content_type, size = self.network.get(objectLatestPath, 'application/json', cert=cert, suppressError=True)

    # Parse the json from the stream
    reader = codecs.getreader('utf-8')
    try:
      revisionInfo = json.load(reader(response))
    except:
      revisionInfo = {}

    objectInfo['revision'] = revisionInfo.get('revision')

    return objectInfo

  def pullObjectFrom(self, node, uuid, revision=None):
    """
    This will replicate the object by retrieving it from the given node.
    When the replication fails, this returns Node. Otherwise, returns the
    Object. If revision is given, it will pull at least that revision. If
    revision is None, it will replicate all revisions.
    """

    objectPath = self.urlForObject(node, uuid)

    # Retrieve the certificate file path for the node referenced by this url
    cert = self.certificatePathFor(objectPath)

    NodeManager.Log.write("cloning object history")

    # Clone a temporary copy of this object
    git = GitRepository(objectPath, cert=cert)

    # Create an Object based on that clone
    obj = Object(path=git.path, occam=self.occam)

    NodeManager.Log.write("storing %s %s in local object repository" % (obj.objectInfo()['type'], obj.objectInfo()['name']))

    # Copy the object information to the object store
    self.occam.objects.store(obj)

    # Retrieve the stored version of the Object
    objectInfo = obj.objectInfo()
    obj = self.occam.objects.retrieve(objectInfo['id'], objectInfo['type'], 'HEAD')

    # Is it a backed object?
    if not objectInfo.get('backed') is None:
      revision = obj.revision
      self.pullResourceFrom(node, uuid, revision)

    # Is it a built object?
    if not objectInfo.get('build') is None:
      revision = obj.revision
      self.pullBackendFrom(node, uuid, revision)

    # Pull invocation data
    self.pullInvocationDataFrom(node, obj)

    # Return the now replicated object
    return obj

  def pullInvocationDataFrom(self, node, obj, categories=None):
    """
    Pulls invocation data from the given node and object. This will pull
    invocation data from the given set. By default, it will pull "builds",
    "output", and "generated" from the node.
    """

    if categories is None:
      categories = ['builds', 'output', 'generated']

    uuid = obj.uuid

    # Determine the URL for the invocations
    objectPath = self.urlForObject(node, uuid) + "/invocations/"

    # Retrieve the certificate file path for the node referenced by this url
    cert = self.certificatePathFor(objectPath)

    # Pull each category in turn
    for category in categories:
      invocationPath = objectPath + category

      response, content_type, size = self.network.get(invocationPath, 'application/json', cert=cert, suppressError=True)

      # Parse the json from the stream
      reader = codecs.getreader('utf-8')
      try:
        invocation = json.load(reader(response))
      except:
        invocation = {}

      # Write out response to invocations path
      self.occam.objects.mergeInvocation(uuid, category, invocation)

  def pullBackendFrom(self, node, uuid, revision=None):
    """
    This will pull any pre-built backend images for the given object
    from the given node.
    """

    # TODO: pull a backend list from the node to know what exists

    # Right now, assume a docker pre-built image exists
    dockerHandler = self.occam.backends.handlerFor('docker')
    objectPath = self.urlForObject(node, uuid, revision)
    cert = self.certificatePathFor(objectPath)

    return dockerHandler.pull(objectPath, uuid, revision=revision, cert=cert)

  def taskFor(self, fromEnvironment=None, fromArchitecture=None, toEnvironment=None, toArchitecture=None, toBackend=None, obj=None):
    """ Queries for a task from known OCCAM nodes.

    This will ask known nodes how it would construct a VM for the given object
    for the given backend. It returns a VM object with enough metadata to
    discover required resources to replicate the VM on this node.
    """

    # Naive, at the moment. Just go through the node list and query.

    for node in self.datastore.retrieveList():
      NodeManager.Log.write("looking in %s (%s)" % (node.name, node.host))

      return self.taskForFrom(node, fromEnvironment, fromArchitecture, toEnvironment, toArchitecture, toBackend=toBackend, obj=obj)

    return None

  def taskForFrom(self, node, fromEnvironment=None, fromArchitecture=None, toEnvironment=None, toArchitecture=None, toBackend=None, obj=None):
    """
    This will ask the given node how it would construct a VM for the given
    object for the given backend. It returns a VM object with enough metadata
    to discover required resources to replicate the VM on this node.
    """

    if obj is None:
      taskPath = self.urlForNode(node,
        "task?fromEnvironment=%s&fromArchitecture=%s&toEnvironment=%s&toArchitecture=%s" % (
          fromEnvironment, fromArchitecture, toEnvironment, toArchitecture
        )
      )
    else:
      # TODO: pass along revision and env/arch goals?
      taskPath = self.urlForNode(node,
        "task?fromObject=%s" % (
          obj.objectInfo().get('id')
        )
      )

    if not toBackend is None:
      taskPath = "%s&toBackend=%s" % (taskPath, toBackend)

    # Retrieve the certificate file path for the node referenced by this url
    cert = self.certificatePathFor(taskPath)

    return self.network.getJSON(taskPath, "application/json", cert=cert, suppressError=True)

  def pullResourceFrom(self, node, uuid, revision=None):
    """
    This will replicate the resource given by the uuid from the given node.
    When the replication fails, this returns None. Otherwise, returns the
    Object.
    """

    obj = self.occam.objects.retrieve(uuid)
    git = None
    if obj is None:
      # If the object describing the resource does not exist, fail.
      return None

    resourceType = obj.objectInfo()['backed']

    handler = self.occam.resources.handlerFor(resourceType)

    objectPath = self.urlForObject(node, uuid, revision=revision, subPath='resources/%s' % resourceType)
    NodeManager.Log.write("Pulling resource at %s" % (objectPath))

    name = obj.objectInfo()['name']

    # Retrieve the certificate file path for the node referenced by this url
    cert = self.certificatePathFor(objectPath)

    handler.pull(uuid, revision, name, objectPath, cert=cert)

    # Return the now replicated object and the GitRepository object
    return obj, git

  def retrieveDirectoryFrom(self, node, option, person):
    """ Retrieves the given file from the given node.
    """

    token = option.id
    if option.revision:
      token = "%s/%s" % (token, option.revision)

    taskPath = self.urlForNode(node,
      "%s/raw/%s" % (
        token,
        option.path or ""
      )
    )

    # Retrieve the certificate file path for the node referenced by this url
    cert = self.certificatePathFor(taskPath)

    items = self.network.getJSON(taskPath, "application/json", cert=cert, suppressError=True)

    if not items:
      return None

    return {"items": items}

  def retrieveFileStatFrom(self, node, option, person):
    """ Retrieves the given file from the given node.
    """

    token = option.id
    if option.revision:
      token = "%s/%s" % (token, option.revision)

    taskPath = self.urlForNode(node,
      "%s/files/%s" % (
        token,
        option.path or "object.json"
      )
    )

    # Retrieve the certificate file path for the node referenced by this url
    cert = self.certificatePathFor(taskPath)

    return self.network.getJSON(taskPath, "application/json", cert=cert, suppressError=True)

  def retrieveFileFrom(self, node, option, person):
    """ Retrieves the given file from the given node.
    """

    token = option.id
    if option.revision:
      token = "%s/%s" % (token, option.revision)

    taskPath = self.urlForNode(node,
      "%s/raw/%s" % (
        token,
        option.path or "object.json"
      )
    )

    # Retrieve the certificate file path for the node referenced by this url
    cert = self.certificatePathFor(taskPath)

    response, content_type, size = self.network.get(taskPath, cert=cert, suppressError=True)

    return response

  def historyFrom(self, node, option, person):
    """ Returns the object status for the given object at the given node.
    """

    if option.path:
      return self.retrieveFileStatFrom(node, option, person)

    token = option.id
    if option.revision:
      token = "%s/%s" % (token, option.revision)

    taskPath = self.urlForNode(node,
      "%s/history" % (
        token
      )
    )

    # Retrieve the certificate file path for the node referenced by this url
    cert = self.certificatePathFor(taskPath)

    return self.network.getJSON(taskPath, "application/json", cert=cert, suppressError=True)

  def statusFrom(self, node, option, person):
    """ Returns the object status for the given object at the given node.
    """

    if option.path:
      return self.retrieveFileStatFrom(node, option, person)

    token = option.id
    if option.revision:
      token = "%s/%s" % (token, option.revision)

    taskPath = self.urlForNode(node,
      "%s/status" % (
        token
      )
    )

    # Retrieve the certificate file path for the node referenced by this url
    cert = self.certificatePathFor(taskPath)

    return self.network.getJSON(taskPath, "application/json", cert=cert, suppressError=True)

  def identityFrom(self, node, id, person):
    """ Retrieve identity information from the given node.
    """

    taskPath = self.urlForNode(node,
      "identities/%s" % (
        id
      )
    )

    # Retrieve the certificate file path for the node referenced by this url
    cert = self.certificatePathFor(taskPath)

    return self.network.getJSON(taskPath, "application/json", cert=cert, suppressError=True)

  def viewersFor(self, node, type, subtype, person):
    """
    """

    taskPath = self.urlForNode(node,
      "viewers?type=%s&subtype=%s" % (
        type,
        subtype
      )
    )

    # Retrieve the certificate file path for the node referenced by this url
    cert = self.certificatePathFor(taskPath)

    return self.network.getJSON(taskPath, "application/json", cert=cert, suppressError=True)
