# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import sys
import pty

class Daemon:
  """ This lightly wraps the python daemon code to make this process a daemon.

  It adds some support for attaching a pseudo-terminal and the writing
  of a log file.
  """

  def __init__(self, workingDir, logFile, appendProcId=False, pty=False):
    """Sets up the options for our daemon process to be created with the
    create() function
    """

    self.workingDir   = workingDir
    self.logFile      = logFile
    self.appendProcId = appendProcId
    self.pty          = pty

  def create(self):
    import daemon

    logFile = None
    if self.appendProcId:
      logFile = "%s-%s.log" % (self.logFile, os.getpid())
    else:
      logFile = "%s.log" % (self.logFile)

    logFile = os.path.join(self.workingDir, logFile)

    flags = 'w'

    if os.path.exists(logFile):
      flags = 'a'

    log = open(logFile, flags)

    context = daemon.DaemonContext(
      working_directory = self.workingDir,
      umask  = 0o037,
      stdout = log
    )

    context.open()

    if self.pty:
      # Create a pseudo-terminal to mock a tty device
      writer, reader = pty.openpty()

      # Map the read-end of the pty to our own stdin
      # Now things will think we have a controlling terminal
      # (stuff like 'tar' won't run without a tty, it's ridiculous)
      os.dup2(reader, 0)

    # redirect stderr to log file
    os.dup2(1, 2)
