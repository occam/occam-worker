# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.log     import loggable
from occam.manager import manager

import select
import os

class socket_wrap():
  def __init__(self, socket):
    self.socket = socket
    self.buffer = self

  def write(self, data):
    self.socket.send(data)

  def flush(self):
    pass

  def read(self, amount):
    self.socket.read(amount)

  def fileno(self):
    return self.socket.fileno()

class WaitableEvent:
  """Provides an abstract object that can be used to resume select loops with
  indefinite waits from another thread or process. This mimics the standard
  threading.Event interface."""

  def __init__(self):
    self._read_fd, self._write_fd = os.pipe()

  def wait(self, timeout=None):
    rfds, wfds, efds = select.select([self._read_fd], [], [], timeout)
    return self._read_fd in rfds

  def isSet(self):
    return self.wait(0)

  def clear(self):
    if self.isSet():
      os.read(self._read_fd, 1)

  def set(self):
    if not self.isSet():
      os.write(self._write_fd, b'1')

  def fileno(self):
    """Return the FD number of the read side of the pipe, allows this object to
    be used with select.select()."""
    return self._read_fd

  def __del__(self):
    os.close(self._read_fd)
    os.close(self._write_fd)

@loggable
@manager("daemon")
class DaemonManager:
  """ Handles an OCCAM daemon.

  The OCCAM daemon runs as a process and can accept commands from a socket.

  Generally, one daemon would run per server to accept and issue commands for
  that server. For instance, a web server can spawn an occam daemon on a known
  local port and issue occam commands to handle requests.

  The port has a configurable default, but can be overwritten by a command line
  argument to the "occam daemon" command.
  """

  def __init__(self):
    """ Initializes the daemon.
    """

    self.initializeInfoDirectory()

  def defaultPort(self):
    """ Retrieves the default configured port.
    """

    # Get the port from configuration option daemon.port
    return self.configuration.get('port', 32000)

  def defaultHost(self):
    """ Retrieves the default configured hostname.
    """

    # Get the port from configuration option daemon.port
    return self.configuration.get('host', '127.0.0.1')

  def path(self):
    """ Returns the path to the daemon metadata.
    """

    from occam.config import Config
    import os

    basePath = Config.root()

    return os.path.join(basePath, "daemons")

  def initializeInfoDirectory(self):
    """ Initializes the directory for the metadata for running daemons.
    """

    import os

    path = self.path()

    if not os.path.exists(path):
      os.mkdir(path)

  def run(self, host=None, port=None):
    """ Starts the server loop.
    """

    import io
    import os
    import json
    import sys
    import traceback

    from occam.network.manager import NetworkManager
    network = NetworkManager()

    if host is None:
      host = self.defaultHost()

    if port is None:
      port = self.defaultPort()

    try:
      port = int(port)
    except:
      DaemonManager.Log.error("Provided port is not a valid integer.")
      return -1

    # TODO: check port range to fit within 16-bits

    # Get pid for this process
    pid = os.getpid()

    # Write the daemon metadata file
    metadata = {}
    metadata['port'] = port
    metadata['host'] = host
    metadata['pid']  = pid

    cmd_check = None
    try:
      with open(os.path.join("/", "proc", str(pid), "cmdline")) as f:
        cmd_check = f.read()
    except:
      pass

    metadata['cmd'] = cmd_check

    # Open socket
    import socket
    import sys
    import threading
    import codecs
    import select

    writer = codecs.getwriter('utf8')
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

    daemonRunning = True

    def clientThread(conn):
      stopEvent = threading.currentThread().stopEvent

      buffer = b''

      while True:
        readers, _, _ = select.select([stopEvent, conn], [], [])

        if len(readers) > 0 and stopEvent in readers:
          break

        if len(readers) > 0:
          if len(buffer) < 1024:
            buffer += conn.recv(1024)
          buffering = True
          line = ""
          while buffering:
            if b'\n' in buffer:
              (line, buffer) = buffer.split(b'\n', 1)
              try:
                line = codecs.decode(line, 'utf8').strip()
                break
              except:
                pass
            else:
              more = conn.recv(1024)
              if not more:
                buffering = False
              else:
                buffer += more

          if not buffer and buffering == False:
            DaemonManager.Log.write("Connection closed on the client's side.")
            break

          try:
            line = json.loads(line)
          except:
            continue

          if not isinstance(line, dict):
            continue

          # Build out the option (keyword) argument list
          #
          # Here, options is a dict where the key is the argument (i.e. "--set")
          #   and the value is a list of items. If the item itself is a list, it
          #   is the multiple values.
          #
          # options = {
          #   "--set": [['x','y','z'], ['a','b']]
          # }
          # These will be rendered as ("--set x y z --set a b")
          #
          # When the value is explicitly true and not a string, then the option
          # merely exists.
          # options = {
          #   "-j": true
          # }
          # Renders as ("-j")
          options = line.get("options", {})
          optionsArguments = []
          for key, arguments in options.items():
            if arguments is True:
              optionsArguments.append(key)
              continue

            if not isinstance(arguments, list):
              arguments = [arguments]

            for argumentList in arguments:
              optionsArguments.append(key)

              if not isinstance(argumentList, list):
                argumentList = [argumentList]

              optionsArguments.extend(argumentList)

          promoted = ""
          if line.get('promote'):
            promoted = "promoted "

          DaemonManager.Log.write("Received %scommand '%s %s %s -- %s'" % (promoted, line["component"], line["command"], " ".join(optionsArguments), " ".join(line["arguments"])))

          # Get incoming data for use as stdin
          stdin = None
          if "stdin" in line:
            # TODO: wrap the network stream to zero-copy the data
            DaemonManager.Log.write("Looking at stdin for %s bytes" % (line["stdin"]))
            stdin = None
            if len(buffer) >= line["stdin"]:
              stdin = io.BytesIO(buffer[0:line["stdin"]])
              buffer = buffer[line["stdin"]:]
            else:
              stdin = io.BytesIO(buffer)

            buffer = b''

            stdin.seek(0, os.SEEK_END)
            stdinLength = stdin.tell()

            amountNeeded = line["stdin"] - stdinLength

            while stdinLength < line["stdin"]:
              DaemonManager.Log.write("Looking for remaining %s bytes" % (amountNeeded))
              data = conn.recv(amountNeeded)
              stdin.write(data)
              stdinLength = stdin.tell()
              amountNeeded = line["stdin"] - stdinLength

            stdin.seek(0, os.SEEK_SET)

          from occam.commands.manager import CommandManager
          commands = CommandManager()

          import shlex
          error = False
          errorMessage = ""
          code = 0

          if len(line["arguments"]) > 0:
            optionsArguments.append("--")

          invocation = [line["component"]]

          if line["command"]:
            invocation.append(line["command"])

          invocation.extend([*optionsArguments, *line["arguments"]])

          promote = line.get("promote", False)

          stdout = None
          if promote:
            stdout = socket_wrap(conn)
            stdin  = socket_wrap(conn)

            header = {
                'status': 'ok'
            }

            header = json.dumps(header).encode('utf8')
            conn.send((str(len(header)) + "\n").encode('utf8'))
            conn.send(header)
            output = b""
            conn.send((str(len(output)) + "\n").encode('utf8'))
            conn.send(output)

          try:
            code = commands.execute(invocation, storeOutput=not promote, stdin = stdin, stdout = stdout, allowLocalPerson = False)
            if code < 0:
              error = True
          except Exception as e:
            DaemonManager.Log.write("Error handling command (%s)" % (line))
            DaemonManager.Log.write("Error type: %s" % (sys.exc_info()[0]))
            traceback.print_exc()
            # TODO: fix these error codes?
            code = -1
            error = True
          except:
            DaemonManager.Log.write("Error handling command (%s)" % (line))
            DaemonManager.Log.write("Error type: %s" % (sys.exc_info()[0]))
            code = -1
            error = True

          if promote:
            break
          else:
            # Send stdout
            output = DaemonManager.Log.storedOutput()
            output = output.read()

            errorMessage = DaemonManager.Log.errorMessage()
            if error:
              header = {
                  'status': 'error',
                  'message': errorMessage,
                  'code':    code
              }
            else:
              header = {
                  'status': 'ok'
              }

            header = json.dumps(header).encode('utf8')
            conn.send((str(len(header)) + "\n").encode('utf8'))
            conn.send(header)
            conn.send((str(len(output)) + "\n").encode('utf8'))
            conn.send(output)

      conn.close()
      DaemonManager.Log.write("Disconnected from %s: %s" % (threading.currentThread().connectionAddr[0], str(threading.currentThread().connectionAddr[1])))

    try:
      s.bind((host, port))
    except socket.error as msg:
      DaemonManager.Log.error("Could not bind the socket: (host: %s, port: %s) Error %s: %s" % (host, port, str(msg.errno), msg.strerror))
      return -1

    path = self.path()
    metadataFilename = os.path.join(path, "%s.json" % str(port))
    with open(metadataFilename, 'w+') as f:
      json.dump(metadata, f)

    s.listen(10)
    while True:
      try:
        conn, addr = s.accept()
        DaemonManager.Log.write("Connected with %s: %s" % (addr[0], str(addr[1])))

        thread = threading.Thread(target = clientThread, args = (conn,))
        thread.connection = conn
        thread.connectionAddr = addr
        thread.stopEvent = WaitableEvent()
        thread.start()
      except(KeyboardInterrupt):
        DaemonManager.Log.write("Receiving Keyboard Interrupt. Exiting.")

        # Close all threads
        for thread in threading.enumerate():
          if hasattr(thread, "stopEvent"):
            thread.stopEvent.set()

        for thread in threading.enumerate():
          if hasattr(thread, "stopEvent"):
            thread.join()
            DaemonManager.Log.write("Thread exited.")

        break

    DaemonManager.Log.write("Closing socket.")
    s.shutdown(socket.SHUT_RDWR)
    s.close()
    DaemonManager.Log.write("Socket closed.")

    # Clean up the daemon metadata file
    os.remove(metadataFilename)

  def start(self, host=None, port=None):
    """ Starts the daemon.

    This will spawn a separate process to run as a daemon, if it isn't running
    on the given port already. If a daemon is already running on that port, or
    the port is not open for any other reason, the call will fail.

    Returns True upon success and False upon failure.
    """

    if port is None:
      port = self.defaultPort()

    DaemonManager.Log.write("Starting daemon on port %s" % (port))

    path = self.path()

    from occam.daemon.daemon import Daemon

    # Move ourselves to a background process
    server = Daemon(workingDir=path, logFile="daemon")
    server.create()

    # Run the server
    self.run(host = host, port = port)

  def status(self, port=None):
    """ Gives the status of all running daemons on the system.

    If you pass along a port, you can filter the list to just the daemon running
    on that port.

    For each daemon, it will report a dictionary object with keys for the port
    and the time the daemon has been running.
    """

    import os
    import json

    # Walk the daemon path and read and then report the metadata

    path = self.path()
    for filename in os.listdir(path):
      if filename.endswith(".json"):
        # read json data
        try:
          with open(os.path.join(path, filename)) as f:
            data = json.load(f)
        except:
          data = {}

    return []

  def stop(self, port=None):
    """ Stops the daemon.

    Returns True if the daemon is not running.
    """

    import os
    import json

    if port is None:
      port = self.defaultPort()

    # Open metadata
    path = self.path()
    data = {}
    try:
      filename = os.path.join(path, "%s.json" % (str(port)))
      with open(filename) as f:
        data = json.load(f)

      os.remove(filename)
    except:
      pass

    pid = data.get('pid', None)
    cmd = data.get('cmd', None)

    if not pid is None:
      DaemonManager.Log.write("Stopping server running on port %s" % (str(port)))

      cmd_check = None
      try:
        with open(os.path.join("/", "proc", str(pid), "cmdline")) as f:
          cmd_check = f.read()
      except:
        pass

      if cmd and cmd_check and cmd == cmd_check:
        import _signal
        try:
          os.kill(pid, _signal.SIGINT)
          os.kill(pid, _signal.SIGTERM)
        except ProcessLookupError:
          DaemonManager.Log.write("Server not actually running. Cleaning up.")
      elif cmd_check is None:
        DaemonManager.Log.error("Cannot find a running daemon on that port.")
      else:
        DaemonManager.Log.error("Cannot safely kill the daemon process. It may not be a daemon.")
    else:
      DaemonManager.Log.error("Cannot find a running daemon on that port.")
