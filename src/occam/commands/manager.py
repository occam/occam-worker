# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import importlib
import os

from types import SimpleNamespace

from occam.log import Log, loggable

from occam.manager import uses, manager

import sys
import argparse

class Usage(Exception):
  def __init__(self, msg):
    self.msg = msg

from occam.system.manager   import SystemManager
from occam.accounts.manager import AccountManager

@loggable
@uses(SystemManager)
@uses(AccountManager)
@manager("commands")
class CommandManager:
  """
  This handles all of the various Occam commands and registering new commands.
  This also provides the parameter parsing capabilities and is where all
  generic parameters are defined.

  Examples:

    To create a command, write the following in a new file and import it::

      # my_command.py:
      # invoked by: "occam my-component my-command <something_else>+ [-a foo] [-b]"
      from command_manager import command, option, argument

      @command('my-component', 'my-command')
      @argument("something_else", nargs="?")
      @option("-a", "--argument", action = "store",
                                  dest   = "my_argument",
                                  help   = "this is my argument. it is followed by a string.")
      @option("-b", "--boolean",  action = "store_true",
                                  dest   = "my_boolean_argument",
                                  help   = "this turns on something")
      class MyCommand:
        def do(self):
          # Pull out the argument value
          value = self.options.argument

          if self.options.my_boolean_argument:
            # do something
            pass

          for argument in self.options.something_else:
            print(argument)

    And you can run this with: "occam my-component my-command --argument foo --boolean a b c"

    This will print out "a" "b" and "c" since those are normal (positional) arguments.

    Note that all arguments are optional.
  """

  commandList    = {}
  options        = {}
  arguments      = {}
  names          = {}
  argumentObject = {}
  categoryList   = {}

  @staticmethod
  def parser(component, command):
    parser = argparse.ArgumentParser("occam %s %s" % (component, command))

    group = parser.add_argument_group('general arguments')

    group.add_argument("-R", "--rootPath", action = "store",
                                           type   = str,
                                           dest   = "rootPath",
                                           help   = "the root path for occam")
    group.add_argument("-V", "--verbose",  action = "store_true",
                                           dest   = "verbose",
                                           help   = "prints all messages")
    group.add_argument("-L", "--logtype", action = "store",
                                          dest   = "logtype",
                                          help   = "the logging text for commands. Possible options are: 'text' (default), and 'json'.")
    #group.add_argument("-O", "--on",      action = "store",
    #                                      dest   = "remote_machine",
    #                                      help   = "runs the command on the given machine written as: username@host:port")
    group.add_argument("-T", "--token",   action = "store",
                                          dest   = "token",
                                          help   = "The authentication token to run the command as the described Person.")
    return parser

  @staticmethod
  def register(component, name, category, documentation, commandHandler):
    """ Registers the command for the given component with the given documentation.

    Typically, you would use the decorator around a
    handler for that command. See documentation for the CommandManager class
    above.
    """

    # Record the command
    CommandManager.commandList[component] = CommandManager.commandList.get(component, {})
    CommandManager.commandList[component][name] = {
      "component":     component,
      "name":          name,
      "handler":       commandHandler,
      "category":      category,
      "documentation": documentation
    }

    # Cross-reference to the command category
    if not category in CommandManager.categoryList:
      CommandManager.categoryList[category] = {}

    CommandManager.categoryList[category][name] = CommandManager.commandList[component][name]

    CommandManager.names[component] = CommandManager.names.get(component, {})
    CommandManager.names[component][commandHandler] = name

  @staticmethod
  def commands(component=None, category=None):
    if component is None:
      #if category is None:
      return list(CommandManager.commandList.keys())

    return list(CommandManager.categoryList.get(category, {}).keys())

  @staticmethod
  def components():
    return list(CommandManager.commandList.keys())

  @staticmethod
  def categories():
    return list(CommandManager.categoryList.keys())

  @staticmethod
  def commandInfo(component, name):
    return CommandManager.commandList[component][name]

  @staticmethod
  def getObjectIdentifierRegex():
    list_of_separators = r"@[/#"
    optional_expr = r"?"

    # UIDs can be:
    #  the uid itself,
    #  '.' for the local dir,
    #  '+' for the current dir object in the repository
    #  a URL
    uuid_expr =           r"[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}"
    url_expr =            r"[a-z]+://.+"
    extended_uuid_expr =  r"(?P<uuid>"+uuid_expr+"|[.]|[+]|"+url_expr+")"
    # Version (optional)
    #  Ignore the ':' separator for version,
    #  The version is any character except @[/# that indicate the start of the
    #    revision, index, path, and link respectively
    version_expr =        r"(?:[:]" + r"(?P<version>[^@[/#]+)" + r")" + optional_expr

    # Revision (optional)
    #  Ignore the '@' separator for revision,
    #  The revision is any set character except :[/# that indicate the start of the
    #    version, index, path, and link respectively
    """ DEL if it is in fact redundant """
    revision_expr =       r"(?:(?:(?:@)" + r"(?P<revision>[^[:/#]+)" +r"))" + optional_expr
    """     """
    revision_expr =       r"(?:@" + r"(?P<revision>[^[:/#]+)" +r")" + optional_expr

    # Link (optional)
    #  Ignore the '#' separator for revision,
    #  The link is any set numbers
    link_expr =           r"(?:#" + r"(?P<link>\d+)" +r")" + optional_expr

    # Link (optional)
    #  Ignore the '#' separator for revision,
    #  The link is any set numbers
    index_expr =          r"(?P<index>(?:[[].+[]])+)" + optional_expr

    # Path (optional)
    #  Path is any character after an initial trailing '/' that is ignored'
    #  The link is any set numbers
    path_expr =           r"(?P<path>(?:[/].*))" + optional_expr

    location_expr = r"^" +\
                    extended_uuid_expr +\
                    version_expr +\
                    revision_expr +\
                    link_expr +\
                    index_expr +\
                    path_expr +\
                    r"$"
    return location_expr

  @staticmethod
  def parseObjectIdentifier(identifier):
    import re

    if identifier is None:
      return None

    class SubNamespace:
      pass

    isList = False

    if isinstance(identifier, list):
      # More than one object
      isList = True
    else:
      identifier = [identifier]

    ret = []

    parser = re.compile(r"^([a-zA-Z0-9]+|[.]|[+]|[a-z]+://.+$)(?:[:]([^?@[/]+))?(?:(?:(?:@)([^[:/?#]+)))?(?:[?]([^:@[/]+))?(?:#(\d+))?((?:[[].+[]])+)?((?:[/].*))?$")

    for token in identifier:
      item = SubNamespace()

      result = parser.match(token)

      if result is None:
        item = None
      else:
        uuid     = result.group(1) # ex: 3787d734-1bd6-11e7-affa-f23c910a26c8 (or a url)
        version  = result.group(2) # ex: 3.4.5
        revision = result.group(3) # ex: c13ffacf0fe053f707a2b541da9b6f1fcd9e6b56
        identity = result.group(4)
        link     = result.group(5) # ex: 120
        index    = result.group(6) # ex: [2][1]
        path     = result.group(7) # ex: /foo/bar.ext

        if index:
          try:
            index = [int(x) for x in re.findall("[[]([^]]+)[]]", index)]
          except:
            # Invalid index
            raise IndexError("Index for the object identifier is not an integer.")

        setattr(item, 'id',       uuid)
        setattr(item, 'revision', revision)
        setattr(item, 'version',  version)
        setattr(item, 'link',     link)
        setattr(item, 'path',     path)
        setattr(item, 'index',    index)
        setattr(item, 'identity', identity)

        if uuid is None:
          item = None

      ret.append(item)

    if isList:
      return ret
    else:
      return ret[0]

  @staticmethod
  def instantiate(component, name, args):
    if component not in CommandManager.commandList:
      return None, None

    if name not in CommandManager.commandList[component]:
      return None, None

    parser  = CommandManager.parser(component, name)
    handler = CommandManager.commandList[component][name]["handler"]

    parser = handler.initParser(parser)
    opts = parser.parse_args(args)

    # Handle object/revision tag
    for k, v in CommandManager.options[handler].items():
      if 'object' in v:
        arg = getattr(opts, v['args']['dest'])
        newArg = CommandManager.parseObjectIdentifier(arg)
        setattr(opts, v['args']['dest'], newArg)

    for v in CommandManager.arguments[handler]:
      if 'object' in v:
        arg = getattr(opts, v['name'])
        newArg = CommandManager.parseObjectIdentifier(arg)
        setattr(opts, v['name'], newArg)

    return opts, handler

  def execute(self, args, storeOutput=False, stdin=None, stdout=None, allowLocalPerson=True):
    if len(args) == 0:
      CommandManager.Log.error("No component was given")
      return -1

    # The first argument is the component name
    # TODO: Look up the component in the configuration
    try:
      importlib.import_module("occam.%s.manager" % args[0].lower())

      component = args[0]

      if len(args) == 1:
        CommandManager.Log.error("No command was given for component %s" % (args[0]))
        return -1

      command   = args[1]

      args      = args[1:]
    except ImportError:
      CommandManager.Log.error("No such component %s" % args[0])
      return -1

    try:
      importlib.import_module("occam.%s.commands.%s" % (component.lower(), command.lower()))
    except ImportError:
      CommandManager.Log.error("No such command %s in %s" % (command, component))
      return -1

    # If the command is valid, invoke it.
    if component in CommandManager.commandList and command in CommandManager.commandList[component]:
      # Form command line parser
      parser = CommandManager.parser(component, command)

      # Retrieve python class handling this command
      handler = CommandManager.commandList[component][command]["handler"]

      # Add specialized arguments for this command and parse
      opts, handler = CommandManager.instantiate(component, command, args[1:])

      if handler is None:
        CommandManager.Log.error("OCCAM cannot initialize a handler for command %s" % (command))
        return -1

      # Initialize Logger
      Log.initialize(opts, storeOutput=storeOutput, stdout = stdout)

      if not self.system.initialized() and not (component == "system" and command == "initialize"):
        Log.error("OCCAM is not initialized.")
        return -1

      person = None
      if self.system.initialized():
        if opts.token:
          token = self.accounts.decodeToken(opts.token)

          if 'person' in token:
            person = self.accounts.currentPerson(token=opts.token)

            if person is None:
              Log.error("Authentication failed.")
              return -1

            Log.noisy("Authenticated as %s" % (person.id))
          elif 'readOnly' in token:
            person = SimpleNamespace(readonly = token['readOnly'])
          elif 'anonymous' in token:
            person = SimpleNamespace(anonymous = token['anonymous'])
        elif allowLocalPerson:
          person = self.accounts.currentPerson()
          if person:
            Log.noisy("Authenticated as %s" % (person.id))

      if stdin is None:
        stdin = sys.stdin.buffer

      try:
        code = handler(opts, args, person, stdin).do()
      except Exception as e:
        if os.getenv('OCCAM_DEBUG'):
          raise e
        Log.error(e)
        return -1

      return code

    # Otherwise, command was not found
    Log.error("Command not found: %s %s" % (component, command))
    return -1

def command(component, name, documentation="", category="Other"):
  """ This decorator will register a command and give the command a way to log.
  """

  def register_command(cls):
    CommandManager.register(component, name, category, documentation, cls)

    if not cls in CommandManager.options:
      CommandManager.options[cls] = {}

    if not cls in CommandManager.arguments:
      CommandManager.arguments[cls] = []

    def initCommand(self, opts, args, person, stdin):
      self.options = opts
      self.args    = args
      self.person  = person
      self.stdin   = stdin

    def initParser(parser):
      for v in reversed(CommandManager.arguments[cls]):
        parser.add_argument(v['name'], **v['args'])

      for k, v in CommandManager.options[cls].items():
        if v['short']:
          parser.add_argument(v['short'], v['long'], **v['args'])
        else:
          parser.add_argument(v['long'], **v['args'])

      return parser

    cls.initParser = staticmethod(initParser)
    cls.__init__ = initCommand
    return cls

  return register_command

def parseObject():
  class ParseObjectAction(argparse.Action):
    def __call__(self, parser, args, values, option_string=None):
      print(values)
  return ParseObjectAction

def argument(name, action="store", help=None, type=None, default="", nargs=None):
  """ This decorator will add a command line positional argument to the command.
  """

  def addArgument(cls):
    if not cls in CommandManager.arguments:
      CommandManager.arguments[cls] = []

    argInfo = {
      'name':      name,
      'args': {
        'action':  action,
        'default': default,
        'help':    help
      }
    }

    if not nargs is None:
      argInfo['args']['nargs'] = nargs

    if not type is None:
      if type == "object":
        argInfo['object'] = True
        argInfo['args']['type'] = str
      else:
        argInfo['args']['type'] = type

    CommandManager.arguments[cls].append(argInfo)

    return cls

  return addArgument

def option(name, secondName=None, dest=None, action=None, help=None, type=None, default=None, nargs=None):
  """ This decorator will add a command line option to the command.

  Args:
    name (str): The short name for the option. Must be lowercase. i.e. "-a"
    secondName (str): The long name for the option. i.e. "--all"
    dest (str): The destination variable for the option. i.e. "my_option"
        will be seen in self.options.my_option
    action (str): How to store the values. Can be "store", "store_true", "append".
        See the normal argparse documentation.
    help (str): The help documentation string displayed when "--help" is used.
    type: The type that the argument will be interpreted as. Can be `str` or `int`
        or "object" as a string to interpret the value as an Occam object tag.
    default: The default value.
    nargs: How to interpret the number of possible values that can be given.
        See the normal argparse documentation.
  """

  if secondName is None:
    long  = name
    short = None
  else:
    long = secondName
    short = name

  # Do not allow lowercase short names
  if short and short[1] == short[1].upper():
    raise Exception("Cannot use a capitalized argument in a user command")

  def nargs_range(nmin, nmax, action):
    class RequiredLength(argparse.Action):
      def __call__(self, parser, args, values, option_string=None):
        check_min = nmin
        if nmin is None:
          check_nmin = 0

        check_max = nmax
        if nmax is None:
          check_max = len(values)

        if not check_min <= len(values) <= check_max:
          if not nmin is None and not nmax is None:
            msg = 'option "{f} requires {nmin} and {nmax} arguments'.format(f = self.dest, nmin=nmin, nmax=nmax)
          elif nmin is None:
            msg = 'option "{f} requires at most {nmax} arguments'.format(f = self.dest, nmax=nmax)
          else:
            msg = 'option "{f} requires at least {nmin} arguments'.format(f = self.dest, nmin=nmin)
          raise argparse.ArgumentTypeError(msg)
        if action == "append":
          if not hasattr(args, self.dest):
            setattr(args, self.dest, [])
          lst = getattr(args, self.dest)
          lst.append(values)
          setattr(args, self.dest, lst)
        else:
          action(parser, args, self.dest, values)
    return RequiredLength

  def addOption(cls):
    if not cls in CommandManager.options:
      CommandManager.options[cls] = {}

    actual_nargs  = nargs
    actual_action = action

    if not nargs is None and isinstance(nargs, dict):
      # Action is a ranged nargs
      nmin = nargs.get('min')
      nmax = nargs.get('max')

      if nmin and nmin > 0:
        actual_nargs = "+"
      else:
        actual_nargs = "?"

      actual_action = nargs_range(nmin, nmax, action)

    CommandManager.options[cls][dest] = {
      'short':   short,
      'long':    long,
      'args': {
        'dest':    dest,
        'action':  actual_action,
        'default': default,
        'help':    help
      }
    }

    if not actual_nargs is None:
      CommandManager.options[cls][dest]['args']['nargs'] = actual_nargs

    if not type is None:
      if type == "object":
        CommandManager.options[cls][dest]['object'] = True
        CommandManager.options[cls][dest]['args']['type'] = str
      else:
        CommandManager.options[cls][dest]['args']['type'] = type

    return cls

  return addOption
