# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os

from occam.log     import loggable
from occam.manager import manager, uses

from pygments.formatters import HtmlFormatter

class HtmlLineFormatter(HtmlFormatter):
  """ Output as html and wrap each line in a span
  """

  name = 'Html wrap lines'
  aliases = ['htmlline']

  def wrap(self, source, outfile):
    return self._wrap_div(self._wrap_pre(self._wrap_lines(source)))

  def _wrap_lines(self, source):
    for t, line in source:
      if t == 1:
        line = '<span class="line"><span class="line-number"></span><code>%s</code></span>' % (line)
      yield t, line

@loggable
@manager("analysis")
class AnalysisManager:
  """ This OCCAM manager handles analysis on data.

  This manager can look at a particular file stored in the repository and report
  some deep meaning about its contents. For instance, it can determine if an
  arbitrary file is likely text and should be rendered as such. Another example
  can be a process that yields HTML for text that syntax highlights based on the
  type of file it detects.
  """

  def examineText(self, data, name = None, threshold = 0.7):
    """ Returns a report about the text given in data.
    """

    report = {
      "isText": False
    }

    nonText = 0

    if not isinstance(data, bytes) and not isinstance(data, str):
      data = data.read(1000)

    for byte in data:
      if byte == 0:
        report["probability"] = 0.0
        return report

      if chr(byte) in "".join(map(chr, range(0, 31))) and chr(byte) not in "\n\r\t\b":
        nonText += 1

    if len(data) == 0:
      report["probability"] = 1.0
    else:
      report["probability"] = 1.0 - (nonText / len(data))

    if report["probability"] >= threshold:
      report["isText"] = True

    return report

  def highlightText(self, data, name = None, cssClass = "highlight"):
    import pygments
    import pygments.lexers

    try:
      lexer = pygments.lexers.get_lexer_for_filename(os.path.basename(name).lower())
    except pygments.util.ClassNotFound:
      lexer = pygments.lexers.get_lexer_for_filename("foo.txt")

    formatter = HtmlLineFormatter(cssclass=cssClass)

    if not isinstance(data, bytes) and not isinstance(data, str):
      data = data.read()

    ret = pygments.highlight(data, lexer, formatter)

    return ret
