# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.commands.manager import command, option, argument

from occam.manager import uses

from occam.objects.manager  import ObjectManager
from occam.analysis.manager import AnalysisManager
from occam.discover.manager import DiscoverManager

from occam.log import Log

import json

@command('analysis', 'highlight',
  category      = 'Data Analysis',
  documentation = "Renders the given object or file within the object as a syntax highlighted document.")
@argument("object", type = "object")
@option("-a", "--all",  dest   = "list_all",
                        action = "store_true",
                        help   = "includes all files within resources as well and will redirect to linked resources")
@uses(ObjectManager)
@uses(AnalysisManager)
@uses(DiscoverManager)
class HighlightCommand:
  """ Returns a transformed version of the given file.
  """

  def do(self):
    data = None

    if self.options.object.path:
      path = self.options.object.path
    else:
      path = "/object.json"

    obj = self.objects.resolve(self.options.object, person = self.person)
    if obj is None:
      if self.options.object:
        data = self.discover.retrieveFile(self.options.object, person = self.person)
      else:
        Log.error("cannot find object in the current path")
        return -1

      if data is None:
        Log.error("cannot find object with id %s" % (self.options.object.id))
        return -1

    if data is None:
      data = self.objects.retrieveFileFrom(obj, path, includeResources=self.options.list_all, person=self.person)

    ret = self.analysis.highlightText(data, path)

    Log.output(ret)

    return 0
