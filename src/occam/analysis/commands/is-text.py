# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.commands.manager import command, option, argument

from occam.manager import uses

from occam.objects.manager   import ObjectManager
from occam.resources.manager import ResourceManager
from occam.analysis.manager  import AnalysisManager
from occam.discover.manager  import DiscoverManager

from occam.log import Log

import json

@command('analysis', 'is-text',
  category      = 'Data Analysis',
  documentation = "Determines if the given object or file within an object can likely be rendered as text.")
@argument("object", type = "object")
@option("-a", "--all",  dest   = "list_all",
                        action = "store_true",
                        help   = "includes all files within resources as well and will redirect to linked resources")
@uses(ObjectManager)
@uses(ResourceManager) # For pulling resource data
@uses(AnalysisManager)
@uses(DiscoverManager)
class IsTextCommand:
  """ Returns an analysis report about whether or not the given object or file is text.

  It may give some information about the kind of text.
  """

  def do(self):
    if self.options.object.path:
      path = self.options.object.path
    else:
      path = "/object.json"

    data = None

    resource_object = None
    obj = self.objects.resolve(self.options.object, person = self.person)
    if obj is None:
      # See if it is a Resource Object
      if self.options.object and self.options.object.id:
        # Check id
        resource_object = self.resources.infoFor(id=self.options.object.id)

        # Check uid
        if not resource_object:
          resource_object = self.resources.infoFor(uid=self.options.object.id)

    if obj is None and resource_object is None:
      if self.options.object:
        # TODO: start/length ranges on this
        # TODO: handle resource data
        data = self.discover.retrieveFile(self.options.object, person = self.person)
      else:
        Log.error("cannot find object in the current path")
        return -1

      if data is None:
        Log.error("cannot find object with id %s" % (self.options.object.id))
        return -1

    if resource_object:
      # Pull data from resource
      if self.options.object.path:
        data = self.resources.retrieveFileFrom(resource_object["id"],
                                               resource_object["uid"],
                                               revision     = self.options.object.revision,
                                               subpath      = self.options.object.path,
                                               resourceType = resource_object["subtype"],
                                               start        = 0,
                                               length       = 1000)
      else:
        data = self.resources.retrieveFile(resource_object["id"],
                                           resource_object["uid"],
                                           revision     = self.options.object.revision,
                                           resourceType = resource_object["subtype"],
                                           start        = 0,
                                           length       = 1000)

    if data is None:
      data = self.objects.retrieveFileFrom(obj, path, includeResources=self.options.list_all, person=self.person, start = 0, length = 1000)

    report = self.analysis.examineText(data)

    ret = {"result": report["isText"], "report": report}

    Log.output(json.dumps(ret))

    return 0
