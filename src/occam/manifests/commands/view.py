# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.log              import Log
from occam.object           import Object
from occam.manager          import uses

from occam.objects.manager    import ObjectManager
from occam.manifests.manager  import ManifestManager

from occam.commands.manager   import command, option, argument

import json

@command('manifests', 'view',
  category      = 'Running Objects',
  documentation = "Generates and displays a task manifest for viewing the given object.")
@argument("object", type = "object", nargs = '?')
@option("--target-environment",
  action  = "append",
  default = [],
  dest    = "target_environments",
  help    = "the environment the VM would run on")
@option("--target-architecture",
  action  = "append",
  default = [],
  dest    = "target_architectures",
  help    = "the architecture the VM would run on")
@option("-B", "--target-backend",
  action  = "append",
  default = [],
  dest    = "target_backends",
  help    = "the backend the VM would run on")
@option("-c", "--have-capability",
  action  = "append",
  default = [],
  dest    = "have_capabilities",
  help    = "capabilities that are fulfilled already by the backend")
@option("-n", "--need-capability",
  action  = "append",
  default = [],
  dest    = "need_capabilities",
  help    = "capabilities that need to be fulfilled by adding objects to the VM")
@option("-b", "--backends",
  action  = "store_true",
  dest    = "backends",
  help    = "only reports potential backends")
@option("-v", "--viewers",
  action  = "store_true",
  dest    = "viewers",
  help    = "only reports potential viewers")
@option("-z", "--truth",
  action  = "store_true",
  dest    = "truth",
  help    = "only reports whether or not a path exists for each target pair")
@uses(ObjectManager)
@uses(ManifestManager)
class ManifestViewCommand:
  """ This class will handle generating a VM manifest for viewing a given object or for a
  given object type.

  Retrieve the possible viewer objects that can view this object:
  occam manifests view 7a8de00a-16f0-11e6-8bfb-001fd05bb228@95ada4dd719f4de146c66871ede425201821ca50 -v
  """

  def do(self, recursive=False):
    objectId       = self.options.object.id
    objectRevision = self.options.object.revision

    objectInfo = None

    targetEnvironment = None
    if len(self.options.target_environments) > 0:
      targetEnvironment  = self.options.target_environments[0]

    targetArchitecture = None
    if len(self.options.target_architectures) > 0:
      targetArchitecture = self.options.target_architectures[0]

    object = self.objects.resolve(self.options.object, person = self.person)
    if object is None:
      Log.error("Object could not be resolved.")
      return -1

    objectInfo = self.objects.infoFor(object)
    if objectInfo is None:
      Log.error("Object metadata could not be resolved.")
      return -1

    # Generate task manifest
    if self.options.backends:
      # Output json array of backends
      backends = self.manifests.backendsFor(objectInfo, environmentGoal=targetEnvironment, architectureGoal=targetArchitecture, haveCapabilities=self.options.have_capabilities, needCapabilities=self.options.need_capabilities)
      Log.output(json.dumps(backends))
    else:
      # Output the task json
      inputs = None
      #if self.options.input_object:
      #  input = self.objects.retrieve(self.options.input_object.id, revision=self.options.input_object.revision, version=self.options.input_object.version, file=self.options.input_object.path, person=self.person)

      #  if input:
      #    inputs = []
      #    inputs.append(input)
      #    Log.write("Adding input: %s %s" % (self.objects.infoFor(input).get('type', 'object'), self.objects.infoFor(input).get('name', 'unknown')))
      #    inputs = [inputs]

      #runningObject = None
      #if self.options.running_object:
      #  runningObject = self.objects.retrieve(self.options.running_object.id, revision=self.options.running_object.revision, version=self.options.running_object.version, person=self.person)

      #taskInfo = self.manifests.taskFor(objectInfo, revision=objectRevision, environmentGoal=targetEnvironment, architectureGoal=targetArchitecture, haveCapabilities=self.options.have_capabilities, needCapabilities=self.options.need_capabilities, inputs=inputs, toRunObject=runningObject, person=self.person)
      #Log.output(json.dumps(taskInfo), padding="")

    Log.done("Generated task manifest")
    return 0
