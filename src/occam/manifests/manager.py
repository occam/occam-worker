# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from collections import OrderedDict

from occam.log import loggable
from occam.config import Config
from occam.object import Object

from occam.manager import uses, manager

from occam.objects.write_manager import ObjectWriteManager
from occam.storage.manager       import StorageManager
from occam.builds.manager        import BuildManager
from occam.notes.manager         import NoteManager
from occam.backends.manager      import BackendManager
from occam.nodes.manager         import NodeManager
from occam.databases.manager     import DatabaseManager
from occam.permissions.manager   import PermissionManager
from occam.system.manager        import SystemManager

import os
import json
import copy
import shutil
import time
import datetime

from occam.semver import Semver

from occam.key_parser import KeyParser

@loggable
@uses(ObjectWriteManager)
@uses(StorageManager)
@uses(BuildManager)
@uses(BackendManager)
@uses(NoteManager)
@uses(NodeManager)
@uses(DatabaseManager)
@uses(PermissionManager)
@uses(SystemManager)
@manager("manifests")
class ManifestManager:
  """ This manages the creation and querying of tasks to build or run objects.

  The manifest manager can take an Object and, based on its metadata, produce a
  task to build or run that object. This task is a manifest that describes what
  other objects should be present on the virtual machine which ultimately runs
  the artifact.

  Task generation goes through a few phases. The first phase takes the object
  and queries for a path through other artifacts that can be used to run it. For
  instance, a Linux-based application describes itself as such, and then the
  query will find a Linux environment to be used as a base for that object. In
  other examples, the artifact may require a particular software environment.
  Such as a jar file, which may need a Java runtime. In this case, the path is
  found through querying the BackendManager which keeps track of the these
  relationships between objects and "Provider" objects in order to facilitate
  execution diversity.

  The second phase is resolving dependencies for each object in the chain and
  adding those dependencies (and their dependencies and so on) to the task. Many
  times, these are described in terms of tags (or versions) which will use a
  style of semantic versioning to resolve to specific artifact revisions for
  each dependency (for example, a software library at version >= 2.0 will
  resolve to revision "fa63ea96cb258b110d70eb") The revisions are written to the
  task (along with the version) to lock in that particular version of the object
  used.

  The third phase resolves the rules written within object metadata. Most
  objects are simple and do not have such rules, but others may write entries in
  their metadata that are to be filled once the task is generated. These are of
  the form {{ x }} where x is some key within the task, delimited by dots (.).
  When these are encountered, they are parsed and replaced with the value
  indicated by the key written within. For example, {{ input.0.file }} would be
  replaced with the file attached as the first input of that object. In this
  case, you can see why this is powerful. This is only known at the time of
  task generation. Furthermore, it can be filled in differently at any point,
  allowing tasks to be somewhat generic where input can be given later.

  When tasks are generated, they might be stored as an Object themselves and
  assigned an identifier. They can be pushed to federated storage and queried
  from other machines. In some cases, taskFor() can be called to simply return
  a generated task without storing it as an object.
  """

  def __init__(self):
    self.parser = KeyParser()

  def buildAll(self, object, local=False, penalties=None, tasks = None, seen = None, originalPenalties = None, desiredBy = None, person=None):
    """ Build an object and recursively builds any necessary objects.

    This will employ a strategic system to build only what is necessary. It will
    avoid pitfalls such as cyclic dependencies that can happen when version matches
    resolve unexpectedly.
    """

    # This will be the returned list
    if tasks is None:
      tasks = []

    if originalPenalties is None:
      originalPenalties = copy.deepcopy(penalties)

    # Penalties is a hash consisting of arrays of revision strings per uuid keys
    # This is passed around to object resolvers to disallow objects of certain
    #   revisions being chosen as version resolves and such.
    if penalties is None:
      penalties = {}

    # This simply keeps track of the objects we have current seen
    if seen is None:
      seen = {}

    # Issue a build of this object
    while True:
      try:
        info = self.objects.infoFor(object)
        ownerInfo = info
        ManifestManager.Log.write("Building %s %s @ %s" % (info.get('type', 'object'), info.get('name', 'unknown'), object.revision))

        if 'build' not in info:
          ownerInfo = self.objects.ownerInfoFor(object)
          if 'build' in ownerInfo:
            object = self.objects.ownerFor(object, person = self.person)

        # If this is already in penalties, then we are already building it
        # Just fall out as unresolved
        if ownerInfo.get('id') in penalties and object.revision in penalties[ownerInfo.get('id')]:
          return tasks, False

        if not ownerInfo.get('id') in penalties:
          penalties[ownerInfo.get('id')] = []
        if not object.revision in penalties[ownerInfo.get('id')]:
          penalties[ownerInfo.get('id')].append(object.revision)

        if not ownerInfo.get('id') in seen:
          seen[ownerInfo.get('id')] = {}
        if not object.revision in seen[ownerInfo.get('id')]:
          seen[ownerInfo.get('id')][object.revision] = desiredBy

        # Keep a copy of the penalties
        #oldPenalties = copy.deepcopy(penalties)

        task = self.build(object    = object,
                          id        = object.id,
                          revision  = object.revision,
                          local     = local,
                          penalties = penalties,
                          person    = person)
        tasks.append(task)
        break
      except BuildRequiredError as e:
        info = self.objects.infoFor(e.requiredObject)
        ManifestManager.Log.write("Cannot find built copy of %s %s" % (info.get('type', 'object'), info.get('name', 'unknown')))
        tasks, resolved = self.buildAll(e.requiredObject, penalties = penalties, tasks = tasks, seen = seen, originalPenalties = originalPenalties, desiredBy = object, person = person)
        if not isinstance(resolved, bool) or not resolved == True:
          return tasks, resolved
      except DependencyUnresolvedError as e:
        ManifestManager.Log.write("Cyclic dependency detected on %s %s" % (e.objectInfo.get('type', 'object'), e.objectInfo.get('name', 'unknown')))
        for k in list(penalties.keys()):
          if not k in originalPenalties:
            del penalties[k]
        penalties.update(originalPenalties)

        # Add all objects that requested this object to penalties
        owner_id = self.notes.resolveOwner(e.objectInfo.get('id'))
        for revision, requestor in seen[owner_id].items():
          info = self.objects.infoFor(requestor)
          ownerInfo = info

          if 'build' not in info:
            ownerInfo = self.objects.ownerInfoFor(requestor)
            if 'build' in ownerInfo:
              requestor = self.objects.ownerFor(requestor, person = self.person)
          ManifestManager.Log.write("Penalizing %s %s @ %s" % (info.get('type', 'object'), info.get('name', 'unknown'), object.revision))

          # Add the item that added this object originally
          if not ownerInfo.get('id') in penalties:
            penalties[ownerInfo.get('id')] = []
          if not requestor.revision in penalties[ownerInfo.get('id')]:
            penalties[ownerInfo.get('id')].append(requestor.revision)

        return tasks, 1

    return tasks, True

  def build(self, object=None, id=None, revision=None, local=False, penalties=None, person=None):
    """ Builds the given object by any means.
    """

    objectInfo = {}

    if object:
      objectInfo = self.objects.infoFor(object)
      id = object.id

      if not revision:
        revision = object.revision

    cachedTasks = None
    if not local:
      cachedTasks = self.notes.retrieve(id, "tasks/build", key="object", revision=revision, fallback=False)

    task = None

    if cachedTasks:
      for taskObject in cachedTasks:
        taskObject = taskObject.get('value')
        if self.backends.isAvailable(taskObject.get('backend')):
          task = self.objects.retrieve(id=taskObject.get('id'), revision=taskObject.get('revision'), person=person)
          if task:
            task = self.objects.infoFor(task)
            break

    if task is None:
      if object is None:
        object = self.objects.retrieve(id=id, revision=revision, person=person)

      if object is None:
        ManifestManager.Log.error("Object not found.")
        return -1

      objectInfo = self.objects.infoFor(object)
      id = object.id

      if not "build" in objectInfo:
        ManifestManager.Log.write("No build information for this object.")
        return None

      task = self.taskFor(object, section="build", penalties = penalties, person=person)

    if task is None:
      ManifestManager.Log.write("Could not produce a task to build this object.")
      return None

    taskName = "Task to %s %s on %s" % ("build", objectInfo.get('name'), task.get('backend'))
    taskObject = self.objects.write.create(name        = taskName,
                                           object_type = "task",
                                           subtype     = "build",
                                           info        = task)
    if person:
      db_obj = self.objects.write.store(taskObject, identity = person.identity)
      taskObject.id  = db_obj.id
      taskObject.uid = db_obj.uid

    # Set build task permissions to be permissive
    self.permissions.update(id = taskObject.id, canRead=True, canWrite=True, canClone=True)

    return taskObject

  def run(self, object=None, id=None, revision=None, local=False, arguments=None, inputs=None, penalties=None, generator=None, person=None):
    """ Runs the given object.
    """

    ownerInfo = self.objects.ownerInfoFor(object)
    info      = self.objects.infoFor(object)

    if id is None:
      id = self.objects.idFor(object, person.identity)
      object.id = id

    if revision is None:
      revision = object.revision

    # TODO: Pull out the build infos if this object requires a build
    #       and use that task to run this object with the built version.
    buildTask = None
    if local == False and ("build" in info or "build" in ownerInfo):
      buildTasks = self.builds.retrieveAll(object)
      if buildTasks is None:
        buildTasks = []

      if "build" in ownerInfo and "build" not in info:
        buildTasks.extend(self.builds.retrieveAll(self.objects.ownerFor(object, person = person)) or [])

      for buildRecord in buildTasks:
        # Build the object
        # TODO: issue build command
        buildTask = self.objects.retrieve(buildRecord.build_id, buildRecord.build_revision, person=person)

        if buildTask is None:
          continue

        buildTask = self.objects.infoFor(buildTask)
        buildTask['id'] = buildRecord.build_id

      if buildTask is None:
        # TODO: discover the build task perhaps
        ManifestManager.Log.error("Object %s %s must be built first." % (info.get('type'), info.get('name')))
        return None

    if local == True and "build" in self.objects.infoFor(object):
      # Look for the buildtask in the local path
      buildTaskPath = os.path.join(os.getcwd(), ".occam", "local", "object.json")
      import json

      try:
        with open(buildTaskPath, "r") as f:
          buildTask = json.load(f)
      except:
        pass

      if buildTask is None:
        # TODO: discover the build task perhaps
        ManifestManager.Log.error("Object must be built locally first.")
        return None

    cachedTasks = None
    if not local and inputs is None:
      cachedTasks = self.notes.retrieve(id, "tasks/run", key = "object", revision=revision, fallback=False)

    task = None

    cachedTasks = None
    if cachedTasks:
      for taskObject in cachedTasks:
        taskObject = taskObject.get('value')
        if self.backends.isAvailable(taskObject.get('backend')):
          taskObject = self.objects.retrieve(taskObject.get('id'), taskObject.get('revision'), person=person)
          if taskObject:
            task = self.objects.infoFor(taskObject)
            break

    if task is None:
      # Get the object to run
      if object is None:
        ManifestManager.Log.error("Object not found.")
        return None

      task = self.taskFor(object, buildTask=buildTask, penalties = penalties, generator=generator, inputs=inputs, person=person)

      # Store task
      objectInfo = self.objects.infoFor(object)

      environment = task.get('environment')
      architecture = task.get('architecture')

      taskName = "Task to %s %s on %s" % ("run", objectInfo.get('name'), task.get('backend'))
      if environment and architecture:
        taskName = "Task to %s %s on %s/%s" % ("run", objectInfo.get('name'), environment, architecture)

      taskObject = self.objects.write.create(name        = taskName,
                                             object_type = "task",
                                             subtype     = "run",
                                             info        = task)
      if person:
        db_obj = self.objects.write.store(taskObject, identity = person.identity)
        taskObject.id  = db_obj.id
        taskObject.uid = db_obj.uid

      # Cache task
      if environment != "native" and architecture != "native":
        self.notes.store(id, "partialTasks/%s" % ("run"), key = "%s-%s" % (environment, architecture), value= { "revision": taskObject.revision, "id": taskObject.id, "uid": taskObject.uid }, revision=revision)
      else:
        self.notes.store(id, "tasks/%s" % ("run"), key = "object", value={ "backend": task.get('backend'), "revision": taskObject.revision, "id": taskObject.id, "uid": taskObject.uid }, revision=revision)

      # For now the permissions will be permissive:
      # This task should belong to the object, however, and inherit permissions.
      self.permissions.update(id = taskObject.id, canRead=True, canWrite=True, canClone=True)

    if task is None:
      ManifestManager.Log.error("Could not determine how to run this object.")
      return None

    return taskObject

  def resolveDependency(self, forObject, dependency, dependenciesIndex, currentDependencies, filterInject = None, ignoreInject = False, penalties = None, person = None):
    """ Returns a list of resolved dependencies for the given dependencyInfo.
    """

    # Determine the effective id for the dependency (allow it to inherit the
    # proper id/revision from its parent)
    if 'id' not in dependency:
      dependency['id'] = self.objects.idFor(forObject, forObject.identity, subObjectType = dependency.get('type'), subObjectName = dependency.get('name'))

    if 'version' not in dependency:
      dependency['revision'] = dependency.get('revision', forObject.revision)

    # For ease, some of the general info for the dependency:
    id = dependency.get('id')

    # Do not consider if the inject parameter doesn't meet the given value
    if filterInject and dependency.get('inject') not in filterInject:
      return currentDependencies

    ManifestManager.Log.noisy("Raking dependency: %s %s @%s" % (dependency.get('type'), dependency.get('name'), dependency.get('revision', dependency.get('version'))))
    overwriteInfo = None

    # Check for rules for replacement
    if dependency.get('overwrite') == "version" and dependency.get('version'):
      # This dependency is only accepted if there exists no other dependency
      #   of the same id with a higher version
      token = "overwrite-%s" % (dependency.get('id'))
      if token in dependenciesIndex:
        overwriteInfo = dependenciesIndex[token]
        currentHighVersion = overwriteInfo['version']

        # Compare. If this is not higher, then we quit out prematurely
        if Semver.versionMatches(dependency.get('version'), "<=", currentHighVersion):
          return currentDependencies

        # Modify the last dependency in the list
        # TODO: what about dependencies that dependency brought in?
        #       what about cycles?
        #       This is a hard problem.
        dependency = overwriteInfo['dependencyInfo']
      else:
        overwriteInfo = {
          "version": dependency.get('version'),
          "dependencyInfo": None,
          "dependencyIndex": None,
          "dependencies": None
        }

      dependenciesIndex[token] = overwriteInfo

    # Resolve each dependency and pull out subDependencies
    ManifestManager.Log.noisy("looking for dependency %s %s @ %s" % (dependency.get('type'), dependency.get('name'), dependency.get('revision', dependency.get('version'))))
    dependencyObject = self.objects.retrieve(id        = dependency.get('id'),
                                             version   = dependency.get('version'),
                                             revision  = dependency.get('revision'),
                                             penalties = penalties,
                                             person    = person)

    # TODO: report object not found using an Exception (so discovery can happen elsewhere)
    if dependencyObject is None:
      ManifestManager.Log.error("cannot find dependency %s %s @ %s" % (dependency.get('type'), dependency.get('name'), dependency.get('revision', dependency.get('version'))))
      raise DependencyUnresolvedError(dependency, "Cannot find dependency")

    # Check to see if we already satisfied this dependency

    # Our first check is the specific object/revision
    # We don't need to include a dependency if we already added this exact object
    token = "%s@%s" % (id, dependencyObject.revision)
    if token in dependenciesIndex:
      # DO set the inject though
      if 'inject' in dependency:
        for dep in currentDependencies:
          if dep['id'] == id and dep['revision'] == dependencyObject.revision:
            dep['inject'] = dependency['inject']
      return currentDependencies

    # Ensure we don't recurse infinitely while resolving this dependency
    dependenciesIndex[token] = True

    # Our next check is for object-broad considerations
    # For one, for the ordering of this dependency.
    #   We may want to make sure newer versions are placed in the dependency list last
    #   So they are installed last. This is the default behavior. Versioned dependencies
    #   are always considered 'newer' than revisioned dependencies, and versions are
    #   ordered by their Semver relationship
    if not id in dependenciesIndex:
      dependenciesIndex[id] = []

    # Get the actual dependency object metadata
    dependencyInfo = self.objects.infoFor(dependencyObject)
    if dependencyInfo is None or dependencyInfo == {}:
      ManifestManager.Log.error("cannot resolve dependency %s %s @ %s" % (dependency.get('type'), dependency.get('name'), dependency.get('revision', dependency.get('version'))))
      raise "Cannot find dependency"

    # Retain the revision and version
    dependencyInfo['id'] = dependencyObject.id
    dependencyInfo['uid'] = dependencyObject.uid
    dependencyInfo['revision'] = dependencyObject.revision
    if 'version' in dependency:
      dependencyInfo['version'] = dependencyObject.version or dependency.get('version')

    # Retain the inject parameter if this is not an injected dependency
    # (Filtered dependencies have an inject parameter and those shouldn't be viral)
    if filterInject is None and 'inject' in dependency:
      dependencyInfo['inject'] = dependency['inject']

    # Add parameters that are overridden in the dependency query
    for key in ['init', 'build', 'run']:
      if key in dependency:
        dependencyInfo[key] = dependencyInfo.get(key, {})
        for subKey in ['copy', 'link']:
          if subKey in dependency[key]:
            if not isinstance(dependency[key][subKey], list):
              dependency[key][subKey] = [dependency[key][subKey]]
            dependencyInfo[key][subKey] = dependencyInfo[key].get(subKey, [])
            dependencyInfo[key][subKey].extend(dependency[key][subKey])

    # Look at the build metadata for the resolved dependency (unless dependency explicitly ignores injections)
    # We want to pull in any injected dependencies.
    # Injected dependencies are dependencies pulled in when the object was built that are meant to also be
    #   available to the running object. Things such as runtimes (libgcc) and libc are good examples.
    if self.isBuildable(dependencyObject):
      buildTask = self.retrieveBuildTaskFor(dependencyObject, person = person)
      buildId = buildTask.get('id')

      dependencyInfo['buildId'] = buildId

      # Combine built dependencies into runtime dependencies
      if dependency.get('inject') != "ignore" and not ignoreInject:
        self.rakeDependencies(dependencyObject, buildTask['builds'], dependenciesIndex, currentDependencies, filterInject = ['init', 'run'], penalties = penalties, person = person)

    # Recursively pull dependencies of this dependency
    ret = self.rakeDependencies(dependencyObject,
                                dependencyInfo,
                                dependenciesIndex,
                                currentDependencies,
                                ignoreInject = dependency.get('inject') == "ignore" or ignoreInject,
                                penalties = penalties,
                                person = person)

    append = True
    if overwriteInfo:
      if not overwriteInfo.get('dependencyIndex') is None:
        currentDependencies[overwriteInfo['dependencyIndex']] = dependencyInfo
        overwriteInfo['dependencyInfo'] = dependencyInfo
        append = False

    if append:
      if overwriteInfo:
        overwriteInfo['dependencyIndex'] = len(currentDependencies)
      # Determine where the object should go within the list
      # We want dependencies to be ordered by their version
      # We can do that by swapping them so they retain their place in the
      #   list but are ordered without interfering with the position of
      #   other dependencies
      currentOrder = dependenciesIndex[id]
      swapIndex = None
      for index, (priorIndex, priorDependency) in enumerate(currentOrder):
        if priorDependency.get('version') is not None and dependency.get('version') is not None and Semver.versionMatches(priorDependency.get('version'), ">", dependency.get('version')):
          swapIndex = index
          break

      # TODO: can probably clean this up a bit whenever anyone can find the time to breathe
      if swapIndex is None:
        currentOrder.append((len(currentDependencies), dependencyInfo,))
        currentDependencies.append(dependencyInfo)
      else:
        # Push prior dependencies up one place
        # And shift over the dependency list in all spots used by this dependency object
        lastDependency = dependencyInfo
        for subIndex, (priorIndex, priorDependency) in enumerate(currentOrder[swapIndex:]):
          subIndex = subIndex + swapIndex
          currentOrder[subIndex] = (priorIndex, lastDependency,)
          currentDependencies[priorIndex] = lastDependency
          lastDependency = priorDependency
        currentOrder.append((len(currentDependencies), lastDependency,))
        currentDependencies.append(lastDependency)

    return ret

  def rakeDependencies(self, forObject, objectInfo, dependenciesIndex, currentDependencies = None, filterInject = None, ignoreInject = False, penalties = None, person = None):
    """ Returns a list of desired and injected dependencies for the given object.
    """

    if currentDependencies is None:
      currentDependencies = []

    ownerObj = self.objects.ownerFor(forObject, person=person)
    if ownerObj.id != forObject.id:
      objectInfo['owner'] = objectInfo.get('owner', {
        "id": ownerObj.id,
        "uid": ownerObj.uid
      })

    dependencies = objectInfo.get('init', {}).get('dependencies', []) + objectInfo.get('dependencies', [])
    for dependency in dependencies:
      self.resolveDependency(forObject,
                             dependency,
                             dependenciesIndex,
                             currentDependencies,
                             filterInject = filterInject,
                             ignoreInject = ignoreInject,
                             penalties = penalties,
                             person = person)

    return currentDependencies

  def isBuildable(self, object):
    """ Returns True if the given object is buildable.
    """

    dependencyInfo = self.objects.infoFor(object)
    dependencyOwnerInfo = self.objects.ownerInfoFor(object)
    return 'build' in dependencyInfo or 'build' in dependencyOwnerInfo

  def retrieveBuildTaskFor(self, object, person = None):
    """ Retrieve the task object that built the given object.
    """

    buildId = None
    buildTask = None
    if not self.isBuildable(object):
      return None

    # Look up a build or report that we need to build this object
    objectInfo = self.objects.infoFor(object)
    buildingObject = object
    if 'build' not in objectInfo:
      buildingObject = self.objects.ownerFor(object, person = person)

    buildingObjectInfo = self.objects.infoFor(buildingObject)
    buildIds = [{"id": x.build_id, "revision": x.build_revision} for x in self.builds.retrieveAll(buildingObject)]

    # For every build we know, we attempt to find that build's content
    for build in buildIds:
      path = self.objects.buildPathFor(buildingObject, build["id"])

      if path is not None:
        buildTask = self.objects.retrieve(id = build["id"], revision = build["revision"], person = person)

        if buildTask:
          buildId = buildTask.id
          buildTask = self.objects.infoFor(buildTask)
          break

    # TODO: when buildTask is None!
    if buildId is None:
      raise BuildRequiredError(buildingObject, "cannot find a built task for %s %s" % (objectInfo.get('type'), objectInfo.get('name')))

    buildTask['id'] = buildId

    return buildTask

  def completeInitSection(self, currentInit, currentProvider):
    """ Will finalize the given init section based on the given provider object info.

    Modifies currentInit in place.

    Returns:
      currentInit
    """

    providesInfo = currentProvider.get('provides', {})
    if isinstance(providesInfo, list):
      # TODO: handle multiple environments
      providesInfo = providesInfo[0]

    providerEnvVars = providesInfo.get('init', {}).get('env', {})

    # Parse 'env' section
    for key, value in currentInit.get('env', {}).items():
      separator = value.get('separator')
      unique    = value.get('unique', False)

      if separator:
        # TODO: handle escapes
        items = separator.join(value.get('items', []))
      else:
        value = value.get('items', [])
        if len(value) == 0:
          items = ""
        else:
          items = value[-1]

      currentInit['env'] = currentInit.get('env', {})
      currentInit['env'][key] = items

    return currentInit

  def parseInitSection(self, objectInfo, currentInit, currentProvider):
    """ Will modify the given current init section with the contents of the given init section.

    It will use currentProvider as a basis of understanding what certain environment variables mean.
    """

    # Retrieve the 'init' section of the given object
    objectInit = objectInfo.get('init')

    # Bail if the init section is not a dictionary
    if objectInit is None or not isinstance(objectInit, OrderedDict) or not isinstance(objectInit, dict):
      return currentInit

    # Retrieve 'env' section
    objectEnvVars = objectInit.get('env')

    # Bail if env section is invalid
    if objectEnvVars is None or not isinstance(objectEnvVars, OrderedDict) or not isinstance(objectEnvVars, dict):
      return currentInit

    providerEnvVars = currentProvider.get('provides', {}).get('init', {}).get('env', {})

    # Parse 'env' section
    for key, value in objectEnvVars.items():
      separator = None
      unique    = False
      if key in providerEnvVars:
        separator = providerEnvVars[key].get('separator')
        unique    = providerEnvVars[key].get('unique', False)

      if isinstance(value, dict):
        value = value.get('items', [])

      if isinstance(value, list):
        items = value
      elif separator:
        # TODO: handle escapes
        items = value.split(separator)
      else:
        items = [value]

      currentInit['env'] = currentInit.get('env', {})
      newItems = currentInit['env'].get(key, {}).get('items', [])
      newItems.extend(items)

      if unique:
        newItems = list(set(newItems))

      currentInit['env'][key] = {
        "unique":    unique,
        "separator": separator,
        "items":     newItems
      }

    return currentInit

  def resolveDependencyInitSection(self, dependencyInfo):
    """ This will return a revised init section based on the one provided by a dependency.

    Dependencies do not need their entire init sections contained within the task manifest.
    This function will truncate the init section to only what is needed by the deployment
    backend.
    """

    # We will start from an empty dictionary
    ret = {}

    # Keep the 'link' section so the deployment backend knows to mount/copy directories
    # from objects into the root filesystem
    links = dependencyInfo.get('init', {}).get('link', [])
    if links:
      ret["link"] = links

    copies = dependencyInfo.get('init', {}).get('copy', [])
    if copies:
      ret["copy"] = copies

    # Return the minimized init section
    return ret

  def taskFor(self, object, revision=None, indexStart=0, generator=None, environmentGoal=None, architectureGoal=None, inputs=None, haveCapabilities=[], needCapabilities=[], section="run", arguments=None, buildTask=None, toRunObject=None, penalties=None, person=None):
    """ Generates a task manifest for the given object that will run the object
    on this system.

    Arguments:
    """

    reservedIndicies = []

    # Gather the reserved indicies
    for wireIndex, wire in enumerate(inputs or []):
      for input in wire:
        # Gather information about the input
        if isinstance(input, dict):
          if 'index' in input:
            reservedIndicies.append(input['index'])

    if penalties is None:
      penalties = {}

    # Look for any cached successful tasks and use that (even if the revision
    # is blank)

    # TODO: go through each backend we have access to

    if not isinstance(object, dict):
      objInfo = self.objects.infoFor(object)
      ownerInfo = self.objects.ownerInfoFor(object)
      revision = object.revision
    else:
      objInfo = object
      object = self.objects.retrieve(id=objInfo.get('id'), revision=objInfo.get('revision', revision), person=person)
      ownerInfo = {}

    objInfo = copy.deepcopy(objInfo)
    objInfo['id']  = object.id
    objInfo['uid'] = object.uid

    # Put build dependencies in with normal dependencies
    if section == "build":
      objInfo['init'] = {"dependencies": []}
      objInfo['init']['dependencies'].extend(objInfo.get('build', {}).get('dependencies', []))

      # Penalize resolving this object as its own dependency
      penalties[objInfo['id']] = penalties.get(objInfo['id'], []) + [revision]

    # Gather object information
    objName = objInfo.get('name', 'unknown')
    objType = objInfo.get('type', 'object')

    runningObject = None

    sectionInfo = objInfo.get(section, {})

    environment  = sectionInfo.get('environment',  objInfo.get('environment'))
    architecture = sectionInfo.get('architecture', objInfo.get('architecture'))

    # Gather providers and the native backend
    if environment == environmentGoal and architecture == architectureGoal:
      backendPath = []
    else:
      backendPath = self.backends.providerPath(environment, architecture, environmentGoal, architectureGoal, person = person)

    if backendPath is None:
      ManifestManager.Log.error("No suitable way to %s this object has been found" % (section))
      return None

    nativeBackend = None
    if len(backendPath) > 0:
      nativeBackend = backendPath[-1]

    if architectureGoal is None and environmentGoal is None and (nativeBackend is None or isinstance(nativeBackend, Object)):
      # No native backend. We failed.
      ManifestManager.Log.error("No backend found for %s on %s" % (environment, architecture))
      return None

    from occam.objects.records.object import ObjectRecord

    if isinstance(nativeBackend, Object) or nativeBackend is None:
      # The backend is still unknown, we are making a partial VM
      backendName = None
    else:
      # The backend is known. We are making a VM with the intent to run on
      # this backend.
      backendName = nativeBackend.__class__.name()

    # Once we have a native backend, build up the task manifest to run
    # on top of that backend
    from uuid import uuid1

    # Generate the base task object
    task = {
      "type": "task",
      "environment":  environmentGoal  or 'native',
      "architecture": architectureGoal or 'native',
      "backend": backendName,
      "id": str(uuid1())
    }

    if backendName:
      task["name"] = "Virtual Machine to %s %s with %s" % (section, objName, backendName)
    else:
      task["name"] = "Virtual Machine to %s %s on %s/%s" % (section, objName, environmentGoal, architectureGoal)

    # Keep track of the source of the task (workflow, etc)
    if generator:
      task['generator'] = self.objects.infoFor(generator)
      task['generator']['revision'] = generator.revision
      task['generator']['id']  = generator.id
      task['generator']['uid'] = generator.uid

    # We need to run each backend/provider to map the native backend to the environment
    # of the requested object.

    lastInput = None
    currentEnvironment = task
    currentInput = task

    # We need to keep track of any capabilities requested by objects that
    # haven't been fulfilled
    capabilities = set([])

    # The starting index for tagging unique objects in the VM
    objectIndex = indexStart

    objectInfo = copy.deepcopy(self.objects.infoFor(object))
    objectInfo['revision'] = object.revision
    objectInfo['id']       = object.id
    objectInfo['uid']      = object.uid

    ownerObj = self.objects.ownerFor(object, person=person)
    if ownerObj.id != object.id:
      objectInfo['owner'] = objectInfo.get('owner', {
        "id": ownerObj.id,
        "uid": ownerObj.uid
      })

    if section == "build":
      objectInfo['init'] = {"dependencies": []}

    usingSections = []

    # Ensure the object we are running/building is using the section
    # asked for
    if section in objectInfo:
      objectInfo['dependencies'] = objectInfo.get('dependencies', []) + objectInfo.get('init', {}).get('dependencies', []) + objectInfo[section].get('dependencies', [])

      # Capture any other metadata.
      if 'metadata' in objectInfo[section]:
        objectInfo['metadata'] = objectInfo[section]['metadata']

      # Capture the network effects
      if 'network' in objectInfo[section]:
        objectInfo['network'] = objectInfo[section]['network']

      # Capture what file is selected to run
      if 'file' in objectInfo[section]:
        objectInfo['file'] = objectInfo[section]['file']

      # Add 'using' to dependencies
      if 'using' in objectInfo[section]:
        objectInfo['dependencies'] = objectInfo.get('dependencies', []) + [objectInfo[section]['using']]

      if buildTask:
        objectInfo['buildId'] = buildTask.get('id')

      if section != "run":
        objectInfo['run'] = objectInfo[section]
        del objectInfo[section]

    interactive = objectInfo.get('run', {}).get('interactive', False)

    # Ignore the backend class when we go through the objects
    lastIndex = -1
    if backendName is None:
      lastIndex = len(backendPath)

    # Get the provider objects as a list
    [self.objects.infoFor(obj).update({"id": obj.id, "uid": obj.uid}) for obj in backendPath[0:lastIndex]]

    # Enumerate through each object and append the 'revision' key
    for i, obj in enumerate(backendPath[0:lastIndex]):
      info = self.objects.infoFor(obj)
      info['revision'] = backendPath[i].revision

      ownerObj = self.objects.ownerFor(obj, person=person)
      if ownerObj.id != obj.id:
        info['owner'] = info.get('owner', {
          "id": ownerObj.id,
          "uid": ownerObj.uid
        })

    # The current provider
    currentProvider = None

    # For each provider, determine how to run each object
    for backendObject in reversed([object] + backendPath[0:lastIndex]):
      if backendObject is object:
        backend = objectInfo
      else:
        backend = self.objects.infoFor(backendObject)

      ManifestManager.Log.noisy("setting up %s @ %s" % (backend.get('name'), backend.get('revision')))

      # Keep track of all dependencies within this environment
      # We will throw away duplicates
      dependenciesIndex = {}

      # Get the object info for this object and add specific information
      # about the revision and object index
      environmentInfo = backend

      # Add forced inputs
      if backend is objectInfo and inputs is not None and isinstance(inputs, list):
        ManifestManager.Log.write("Adding input to task")

        # Ready the list of inputs
        objectInfo['inputs'] = objectInfo.get('inputs', [])

        # Make sure the inputs are a list
        if not isinstance(objectInfo['inputs'], list):
          objectInfo['inputs'] = [objectInfo['inputs']]

        # For each input requested
        for wireIndex, wire in enumerate(inputs):
          for input in wire:
            # Gather information about the input
            inputInfo = input
            if not isinstance(input, dict):
              inputInfo = self.objects.infoFor(input)
              inputInfo['id']       = input.id
              inputInfo['uid']      = input.uid
              inputInfo['revision'] = input.revision

            # Give the input a relative identifier
            while objectIndex in reservedIndicies:
              objectIndex += 1
            #if 'index' not in inputInfo:
            inputInfo['index'] = objectIndex
            objectIndex += 1
            #else:
            #  print("INDEX of INPUT ALREADY USED", inputInfo.get('index'))

            # TODO: input dependencies??

            objectInfo['inputs'] = objectInfo.get('inputs', [])
            objectInfo['inputs'].extend([{}] * (wireIndex + 1 - len(objectInfo['inputs'])))
            objectInfo['inputs'][wireIndex]['connections'] = objectInfo['inputs'][wireIndex].get('connections', [])
            objectInfo['inputs'][wireIndex]['connections'].append({
              "id":       inputInfo.get('id'),
              "uid":      inputInfo.get('uid'),
              "type":     inputInfo.get('type'),
              "name":     inputInfo.get('name'),
              "revision": inputInfo.get('revision'),
              "index":    inputInfo.get('index'),
            })

            for key in ['version', 'buildId', 'owner', 'file', 'network', 'dependencies', 'run', 'init', 'build', 'metadata']:
              if key in inputInfo:
                objectInfo['inputs'][wireIndex]['connections'][-1][key] = inputInfo[key]

      # inputInfo will be the copy of the object description that gets placed in the task
      # It will have only the minimal information the deployment backend needs to deploy the task
      inputInfo = {
        "id":           backend.get('id'),
        "uid":          backend.get('uid'),
        "name":         backend.get('name'),
        "type":         backend.get('type'),
        "revision":     backend.get('revision'),
        "environment":  backend.get('environment'),
        "architecture": backend.get('architecture'),
        "dependencies": []
      }

      for key in ['run', 'buildId', 'init', 'provides', 'install', 'basepath', 'owner', 'file', 'outputs', 'inputs', 'network', 'metadata']:
        if key in backend:
          inputInfo[key] = backend[key]

      if interactive:
        inputInfo['interactive'] = True

      # Mark the index in this object
      while objectIndex in reservedIndicies:
        objectIndex += 1
      inputInfo['index'] = objectIndex
      objectIndex += 1

      # Make sure the first provider is the base provider
      if currentProvider is None:
        currentProvider = inputInfo

      # For every dependency of this environment, add it to the previous
      # environment

      # Rake all dependencies
      currentDependencies = []

      dependencies = self.rakeDependencies(object, environmentInfo, dependenciesIndex, currentDependencies = currentDependencies, penalties = penalties, person = person)
      initSection = {}

      # Rake dependencies that are to be injected by the build task
      if backend is objectInfo and buildTask is not None:
        # Combine built dependencies into runtime dependencies
        dependencies.extend(self.rakeDependencies(object, buildTask['builds'], dependenciesIndex, currentDependencies = currentDependencies, filterInject = ["init", "run"], penalties = penalties, person = person))

      # Rake input dependencies
      for wire in backend.get('inputs', []):
        for input in wire.get('connections', []):
          # Combine input dependencies into runtime dependencies
          deps = self.rakeDependencies(object, input, dependenciesIndex, currentDependencies = currentDependencies, penalties = penalties, person = person)
          dependencies.extend(deps)

      for dependency in dependencies:
        dependencyInfo = {
          "id":       dependency.get('id'),
          "uid":      dependency.get('uid'),
          "name":     dependency.get('name'),
          "type":     dependency.get('type'),
          "revision": dependency.get('revision'),
        }

        for key in ['inject', 'version', 'buildId', 'owner', 'file', 'network', 'paths']:
          if key in dependency:
            dependencyInfo[key] = dependency[key]

        # Add this object to the context of the running process
        while objectIndex in reservedIndicies:
          objectIndex += 1
        dependencyInfo['index'] = objectIndex
        objectIndex += 1

        # Keep track of the dependencies in the current object
        inputInfo['dependencies'].append(dependencyInfo.copy())

        dependencyInfo["init"] = self.resolveDependencyInitSection(dependency)

        # Combine init section with main section, using the currentProvider
        self.parseInitSection(dependency, initSection, currentProvider)

        currentInput['running'] = currentInput.get('running', [{}])
        currentRunning = currentInput['running'][len(currentInput['running']) - 1]
        currentRunning["process"] = currentRunning.get('process', [])
        currentRunning["process"].append(dependencyInfo)

      # Add our own init section
      self.parseInitSection(inputInfo, initSection, currentProvider)
      self.completeInitSection(initSection, currentProvider)
      if 'run' in inputInfo:
        inputInfo['run'].update(initSection)

      # Follow the environment and then we will loop around and add objects to that
      # context as needed.

      # Environments are objects that will run dependencies. They are emulators or
      # OS environments.
      currentEnvironment['provides'] = {
        'environment': inputInfo.get('environment'),
        'architecture': inputInfo.get('architecture')
      }
      if 'provides' in inputInfo:
        currentEnvironment = inputInfo
        dependencies = {}

      # Update 'using' token. When this token exists, this object gets run in this object's place.
      if 'using' in inputInfo.get('run', {}):
        # Remove the dependency
        inputInfo['dependencies'] = inputInfo['dependencies'][0:-1]

        # Place ourselves as input to the "using" object
        inputInfo['run']['using']['inputs'] = [inputInfo.copy()]

        usingInfo = inputInfo['run']['using']

        # Obviously, this is a cycle in our task graph, so fix that
        if section in usingInfo['inputs'][0]:
          del usingInfo['inputs'][0][section]
        del usingInfo['inputs'][0]['run']

        # TODO: we need to combine the using's 'run' command with the object's run command
        # Make a note that we need to do that and then do it at the end of processing
        usingSections.append(usingInfo)
        inputInfo = usingInfo

      # Add this to our task
      currentInput['inputs'] = currentInput.get('inputs', [])

      ManifestManager.Log.noisy("Adding %s @ %s" % (backend.get('name'), backend.get('revision')))

      if 'provides' in currentInput or lastInput is None:
        currentInput['running'] = currentInput.get('running', [{}])
        currentRunning = currentInput['running'][len(currentInput['running'])-1]
        lastInput = currentInput
        currentRunning["process"] = currentRunning.get('process', [])
        currentRunning["process"].append(inputInfo)
        currentInput = currentRunning["process"][len(currentRunning["process"])-1]
        currentProvider = inputInfo
      else:
        currentInput['inputs'].append(inputInfo)
        # Go to the next element of our task
        lastInput = currentInput
        currentInput = currentInput['inputs'][len(currentInput['inputs'])-1]

      ManifestManager.Log.noisy("seeing capability request for %s" % (backend.get('capabilities', [])))
      capabilities |= set(environmentInfo.get('capabilities', []))

    # The last time this is assigned, it will indicate the index of the intended object
    runningObject = currentInput

    # Ensure 'command' is always an array
    if 'run' in currentInput and 'command' in currentInput['run']:
      if not isinstance(currentInput['run']['command'], list):
        currentInput['run']['command'] = [currentInput['run']['command']]

    # This will be a collection of all capabilities claimed by the objects needed to run
    capabilities |= set(objInfo.get('capabilities', []))

    # Now we need to satisfy the capabilities for the task

    capabilities |= set(needCapabilities)
    capabilities = capabilities - set(haveCapabilities)

    task['capabilities'] = list(capabilities)
    task['client'] = {}
    task['client']['capabilities'] = haveCapabilities
    task['client']['requires'] = needCapabilities
    providers = []
    ManifestManager.Log.noisy("Raking capabilities")
    for capability in task['capabilities']:
      if capability in haveCapabilities:
        continue
      ManifestManager.Log.write("Fulfilling capability '%s'" % (capability))
      objs = self.backends.providersFor(capability=capability)
      if len(objs) > 0:
        provider = self.objects.retrieve(objs[0].uid)
        providerInfo = self.objects.infoFor(provider)
        if not providerInfo.get('id') in providers:
          ManifestManager.Log.write("Found: %s" % (objs[0].name))
          capabilities |= set(providerInfo.get('capabilities', []))
          providers.append(providerInfo.get('id'))

          # Add provider object
          if not 'running' in task['running'][0]['process'][0]:
            task['running'][0]['process'][0]['running'] = [{}]
          if not 'process' in task['running'][0]['process'][0]['running'][0]:
            task['running'][0]['process'][0]['running'][0]['process'] = []
          task['running'][0]['process'][0]['running'][0]['process'].append(providerInfo)

          while objectIndex in reservedIndicies:
            objectIndex += 1

          task['running'][0]['process'][0]['running'][0]['process'][len(task['running'][0]['process'][0]['running'][0]['process'])-1]['index'] = objectIndex
          task['running'][0]['process'][0]['running'][0]['process'][len(task['running'][0]['process'][0]['running'][0]['process'])-1]['revision'] = provider.revision
          objectIndex += 1

          # TODO: the providers need to be ordered by dependency and capability relations
          #       for instance, fluxbox provides windowing but requires x11 first!
      else:
        raise Exception("Cannot Resolve Capability %s for %s" % (capability, task.get('name')))

    task['capabilities'] = list(capabilities)

    # Add Running object
    if toRunObject is not None:
      ManifestManager.Log.write("Adding running object to task")
      currentInput['running'] = objInfo.get('running', [])

      inputInfo = self.objects.infoFor(toRunObject)

      # Assign revision and ids
      inputInfo['id']       = toRunObject.id
      inputInfo['uid']      = toRunObject.uid
      inputInfo['revision'] = toRunObject.revision

      # Give the input a relative identifier
      while objectIndex in reservedIndicies:
        objectIndex += 1
      inputInfo['index'] = objectIndex
      objectIndex += 1

      # TODO: input dependencies??

      currentInput['running'].append({
        "process": [inputInfo]})

    # TODO: move the above capability satisfier somewhere else (object manager??)
    # TODO: ensure that provider object capabilities are also satisfied

    while objectIndex in reservedIndicies:
      objectIndex += 1
    currentInput['index'] = objectIndex
    objectIndex += 1

    # Build using the task manifest (this will build each object that needs
    # to be built to work with this backend)

    # Establish the 'volume' and 'mounted' paths for every object so they
    # know where they, and their inputs, exist in their environments
    basepath = None
    if 'backend' in task and task['backend'] is not None:
      if nativeBackend:
        basepath = {
          "id":           "backend",
          "environment":  task['provides'].get('environment'),
          "architecture": task['provides'].get('architecture'),
          "basepath":     nativeBackend.rootBasepath()
        }
    self.setVolumesInTask(task, basepath = basepath)

    # Parse the {{ tags }} within the task
    self.parseTagsInTask(task)

    usingSections = [section.get('index') for section in usingSections]

    #self.prepareInitForTask(task, usingSections=usingSections)

    #self.combineSectionWithInit(currentInput, currentInput, section='run', root=currentInput)
    self.parseTagsInTask(task, removeUnknown=True)

    if runningObject.get('index') in usingSections:
      runningObject = runningObject['inputs'][0]['connections'][0]

    if section == "build":
      task['builds'] = runningObject
    else:
      task['runs'] = runningObject

    if interactive:
      task['interactive'] = interactive

    return task

  def printTask(self, task):
    """ Prints out the task information to the Log.
    """

    ManifestManager.Log.write("Task: %s" % task.get('name', 'unnamed'))

    def printTaskObject(obj):
      for wire in obj.get('inputs', []):
        for input in wire.get('connections', []):
          ManifestManager.Log.write("Input: %s" % input.get('name', 'unnamed'))
          if 'run' in input:
            ManifestManager.Log.write('Run: %s' % input['run'].get('command', input['run'].get('script')))

          printTaskObject(input)

    printTaskObject(task)

  def backendsFor(self, object, revision=None, indexStart=0, generator=None, environmentGoal=None, architectureGoal=None, inputs=None, haveCapabilities=[], needCapabilities=[], section="run"):
    """ Returns a list of backends that can run the given object.
    """

    info = object
    if isinstance(object, Object):
      info = self.objects.infoFor(object)

    targetEnvironment  = info.get('environment')  or info.get(section, {}).get('environment')
    targetArchitecture = info.get('architecture') or info.get(section, {}).get('architecture')

    return self.backends.backendsFor(environment=targetEnvironment, architecture=targetArchitecture)

  def setVolumesInTask(self, objInfo, rootBasepath = None, basepath=None, rootMountPathIndex=None, mountPathIndex=None):
    """ This method takes a task's metadata and adds 'volume' and 'local' fields
    based on any 'basepath' fields it finds.
    """

    # Start at the root of the virtual machine and go through recursively
    # to any leaves.
    mountId = objInfo.get('id')
    if 'owner' in objInfo:
      mountId = objInfo['owner'].get('id', objInfo.get('id'))

    if rootBasepath is None:
      # TODO: get the rootbase path from the backend?
      rootBasepath = []

    if mountPathIndex is None:
      mountPathIndex = [0]

    if rootMountPathIndex is None:
      rootMountPathIndex = [0]

    # Set the volume and mounted for this entry
    if rootBasepath is not None and basepath is not None:
      rootMountPaths  = {}
      localMountPaths = {}
      for subBasepath in rootBasepath + [basepath]:
        rootMountPath = subBasepath.get('basepath', {}).get('mount', '/')
        if not isinstance(rootMountPath, str) and isinstance(rootMountPath, list):
          index = rootMountPathIndex[0]
          if index < len(rootMountPath):
            rootMountPath = rootMountPath[index]
            rootMountPathIndex[0] += 1
          else:
            # TODO: cannot create this task... place errors somewhere
            rootMountPath = "/"

        baseArchitecture = subBasepath.get('architecture', "unknown")
        baseEnvironment  = subBasepath.get('environment',  "unknown")

        rootMountPaths[baseArchitecture] = rootMountPaths.get(baseArchitecture, {})
        rootMountPaths[baseArchitecture][baseEnvironment] = rootMountPath

        localMountPath = subBasepath.get('basepath', {}).get('local', '/home/occam/local')

        localMountPaths[baseArchitecture] = localMountPaths.get(baseArchitecture, {})
        localMountPaths[baseArchitecture][baseEnvironment] = localMountPath

      mountPath = basepath.get('basepath', {}).get('mount', '/')
      if not isinstance(mountPath, str) and isinstance(mountPath, list):
        index = mountPathIndex[0]
        if index < len(mountPath):
          mountPath = mountPath[index]
          mountPathIndex[0] += 1
        else:
          # TODO: cannot create this task... place errors somewhere
          mountPath = "/"

      objInfo['paths'] = {
        "volume": rootMountPaths,
        "mount":  mountPath,
        "separator": basepath.get('basepath', {}).get('separator', '/'),

        "local":  localMountPaths,
        "localMount": basepath.get('basepath', {}).get('local', '/home/occam/local'),

        "taskLocal":  basepath.get('basepath', {}).get('taskLocal', '/home/occam/task'),

        "cwd": basepath.get('basepath', {}).get('cwd', basepath.get('basepath', {}).get('local', '/home/occam/task'))
      }

      if len(rootBasepath) > 0:
        objInfo["paths"]["mountLocal"] = rootBasepath[-1].get('basepath', {}).get('objectLocal', '/home/occam/local'),

    if 'provides' in objInfo:
      if rootBasepath is None:
        rootBasepath = []
      if basepath is not None:
        rootBasepath = rootBasepath[:]
        rootBasepath.append(basepath)
      basepath = {
        "id": objInfo.get('id'),
        "basepath": objInfo.get('basepath', (basepath or {}).get('basepath', {})),
        "architecture": objInfo.get('provides', {}).get('architecture'),
        "environment":  objInfo.get('provides', {}).get('environment'),
      }

    # Recursively go through the rest of the entries:
    inputs = objInfo.get('inputs', []) + [{'connections': objInfo.get('running', [{}])[0].get('process', [])}]
    if isinstance(inputs, list):
      for wire in inputs:
        for input in wire.get('connections', []):
          self.setVolumesInTask(input, rootBasepath, basepath, rootMountPathIndex, mountPathIndex)

    # Do the outputs, too
    outputs = objInfo.get('outputs', [])
    if isinstance(outputs, list):
      for wire in outputs:
        for output in wire.get('connections', []):
          self.setVolumesInTask(output, rootBasepath, basepath, rootMountPathIndex, mountPathIndex)

    # Dependencies
    for dep in objInfo.get('dependencies', []):
      self.setVolumesInTask(dep, rootBasepath, basepath, rootMountPathIndex, mountPathIndex)

    # Ensure paths referred to within the object are absolute
    if 'file' in objInfo and not objInfo['file'].strip().startswith("/") and 'paths' in objInfo:
      # Ensure relative path is using the required separator
      objInfo['file'] = objInfo['paths']['mount'] + objInfo['paths']['separator'] + objInfo['file'].strip()

    # Update cwd
    if 'run' in objInfo and 'cwd' in objInfo['run']:
      objInfo['paths']['cwd'] = objInfo['run']['cwd']

    if 'run' in objInfo and 'script' in objInfo['run'] and 'command' not in objInfo['run']:
      objInfo['run']['command'] = objInfo['run']['script']

    if 'run' in objInfo and 'command' in objInfo['run']:
      if not isinstance(objInfo['run']['command'], list):
        objInfo['run']['command'] = [objInfo['run']['command']]

      if not objInfo['run']['command'][0].strip().startswith("/"):
        objInfo['run']['command'][0] = objInfo['paths']['mount'] + objInfo['paths']['separator'] + objInfo['run']['command'][0].strip()

  def resolveInitSection(self, task, section="init"):
    """ Turns the init section environment section into pure environment strings.
    """

    if section in task and 'env' in task[section]:
      envSection = task[section]['env']
      for k, v in envSection.items():
        if isinstance(envSection[k], dict):
          envSection[k]['items'] = envSection[k].get('items', [])
          # Divide items by separator
          if envSection[k].get('separator'):
            items = envSection[k].get('items')
            envSection[k]['items'] = []
            for item in items:
              subItems = item.split(envSection[k]['separator'])
              if len(subItems) > 0:
                envSection[k]['items'].extend(subItems)

          if envSection[k].get('unique'):
            envSection[k]['items'] = list(set(envSection[k]['items']))
          envSection[k] = envSection[k].get('separator', ':').join(envSection[k]['items'])

  def combineInitSection(self, a, b, root):
    """ Combines two 'init' sections of a task together and returns the result.
    """

    ret = copy.deepcopy(b)
    self.parseTagsInTask(b, root=root)

    if a and 'env' in a:
      for k,v in a['env'].items():
        if isinstance(v, dict):
          pass
        elif isinstance(v, list):
          v = {
            "items": v
          }
        else:
          if v == "":
            v = {
              "items": []
            }
          else:
            v = {
              "items": [v]
            }

        subItem = v

        if k in ret.get('env', {}):
          subItem = ret['env'][k]
          if isinstance(subItem, dict):
            pass
          elif isinstance(subItem, list):
            subItem = {
              "items": subItem
            }
          else:
            if subItem == "":
              subItem = {"items": []}
            else:
              subItem = {
                "items": [subItem]
              }

          subItem['items'] = subItem.get('items', [])
          values = subItem['items']
          subItem.update(v)
          subItem['items'] = subItem['items'] + values

        ret['env'] = ret.get('env', {})
        ret['env'][k] = subItem

    if a and b and 'env' in b:
      ret['env'] = ret.get('env', {})
      for k,v in b['env'].items():
        if not k in ret['env']:
          if isinstance(v, dict):
            pass
          elif isinstance(v, list):
            v = {
              "items": v
            }
          else:
            if v == "":
              v = {
                "items": []
              }
            else:
              v = {
                "items": [v]
              }

          ret['env'][k] = v

    return ret

  def prepareInitForTask(self, task, provider=None, taskRoot=None, usingSections=None, dependencies=None):
    """ Determines the combined 'init' section for each object based on its context
    within the task.
    """

    if taskRoot is None:
      taskRoot = task

    # Combine with the provider's init section
    if not provider is None:
      task['init'] = self.combineInitSection(provider.get('provides', {}).get('init'), task.get('init', {}), taskRoot)

    # Combine with dependencies and their dependencies
    if dependencies is None:
      dependencies = {}

    def combineDependencies(subTask, dependencies=None):
      subTask['dependencies'] = subTask.get('dependencies', []) + subTask.get('init', {}).get('dependencies', [])

      token = "%s@%s" % (subTask.get('id'), subTask.get('revision'))
      if True or token not in dependencies:
        dependencies[token] = {}
        for dependency in subTask.get('dependencies', []):
          combineDependencies(dependency, dependencies)

          subTask['init'] = self.combineInitSection(dependency.get('init', {}), subTask.get('init', {}), taskRoot)
          #if 'dependencies' in dependencies[token]['init']:
            #del dependencies[token]['init']['dependencies']

      #subTask['init'] = dependencies[token]

    combineDependencies(task, dependencies)

    # Recursively update input tasks
    for wire in task.get('inputs', []):
      for inputObject in wire.get('connections', []):
        self.prepareInitForTask(inputObject, provider=provider, taskRoot=taskRoot, usingSections=usingSections, dependencies = dependencies)

    # Recursively update the running tasks
    for runningObject in task.get('running', [{}])[0].get('process', []):
      self.prepareInitForTask(runningObject, provider=task, taskRoot=taskRoot, usingSections=usingSections, dependencies = dependencies)

    # Combine with input if it was done as a 'using' section
    if task.get('index') in usingSections:
      inputObject = task.get('inputs', [{}])[0].get('connections', [{}])[0]
      task['init'] = self.combineInitSection(inputObject.get('init', {}), task.get('init', {}), taskRoot)

      # Update cwd
      task['paths']['cwd'] = inputObject['paths']['cwd']

    # Combine with 'run' section if exists
    if 'run' in task and 'init' in task:
      task['run'] = self.combineInitSection(task.get('init'), task.get('run'), taskRoot)

    self.resolveInitSection(task, 'init')
    self.resolveInitSection(task, 'run')

  def combineSectionWithInit(self, a, b, section="init", root=None):
    """
    """

    if 'init' in b:
      # This has an init section; parse the 'env'
      dependencyInit = copy.deepcopy(b['init'])
      self.parseTagsInTask(dependencyInit, root=root)
      a[section] = a.get(section, {})
      if 'link' in dependencyInit:
        a[section]['link'] = a[section].get('link', [])
        for item in dependencyInit['link']:
          if not item in a[section]['link']:
            a[section]['link'].append(item)
      if 'copy' in dependencyInit:
        a[section]['copy'] = a[section].get('copy', [])
        for item in dependencyInit['copy']:
          if not item in a[section]['copy']:
            a[section]['copy'].append(item)
      if 'env' in dependencyInit:
        a[section]['env'] = a[section].get('env', {})
        dependencyInit['env'] = dependencyInit.get('env', {})

        for k,v in a[section]['env'].items():
          if isinstance(v, dict):
            pass
          elif isinstance(v, list):
            v = {
              "items": v
            }
          else:
            v = {
              "items": [v]
            }

          if k in dependencyInit['env']:
            b = dependencyInit['env'][k]
            if isinstance(b, dict):
              pass
            elif isinstance(b, list):
              b = {
                "items": b
              }
            else:
              b = {
                "items": [b]
              }

            values = b['items']
            b.update(v)
            b['items'] = b['items'] + values

            a[section]['env'][k] = b

  def updateTag(self, value, originalKey, root):
    # Determine the root of the key
    # Determine the relative key
    # Update the tag with relative keys

    # For each tag, reflect the relative key
    try:
      index = 0
      start = value.index("{{", index)

      while start >= 0:
        end   = value.index("}}", start)
        key   = value[start+2:end].strip()

        if key == "{{":
          # Escaped curly brace section
          replacement = "{{"
        else:
          try:
            path = self.parser.path(originalKey)

            # Determine the root
            newRoot = root
            current = root
            keyPrefix = ""
            currentPrefix = ""
            for subKey in path:
              current = current[subKey]
              if isinstance(subKey, int):
                currentPrefix = currentPrefix + "[" + str(subKey) + "]"
              else:
                currentPrefix = currentPrefix + "." + subKey

              if isinstance(current, dict) and "id" in current:
                newRoot = current
                keyPrefix = currentPrefix

            if keyPrefix.startswith("."):
              keyPrefix = keyPrefix[1:]

            okey = key
            if keyPrefix:
              key = keyPrefix + "." + key

            replacement = "{{ " + key + " }}"
          except:
            replacement = "{{ %s }}" % (key)

        if replacement is None:
          replacement = ""

        replacement = str(replacement)

        try:
          value = value[0:start] + replacement + value[end+2:]
        except:
          value = value

        # Adjust where we look next
        index = start + len(replacement)

        # Pull the index of the next {{ section
        # If there isn't one, it will fire an exception
        start = value.index("{{", index)

    except ValueError as e:
      pass

    if value is None:
      value = ""
    return value

  def parseTagsInTask(self, value, root=None, parent=None, environment=None, removeUnknown=False):
    """ This method goes through every entry in the task info and replaces curly
    brace tags with what they request.

    These replacements are completed to create the task manifest for creating
    a virtual machine. Each tag is based on the root of any object metadata.
    The root of an object is any section with an 'id' tag. The following shows
    how the changes made above are actually the same regardless of where the
    object is inserted into the task manifest as a whole.

    Examples:

      A section such as this::

        {
          "paths": {
            "mount": "/occam/dd44fcce-5274-11e5-b1d4-dc85debcef4e"
          },
          "foo": "./sample {{ paths.mount }}",
          "bar": "{{ paths.mount }}/usr/lib"
        }

      Becomes::

        {
          "paths": {
            "mount": "/occam/dd44fcce-5274-11e5-b1d4-dc85debcef4e"
          },
          "foo": "./sample /occam/dd44fcce-5274-11e5-b1d4-dc85debcef4e",
          "bar": "/occam/dd44fcce-5274-11e5-b1d4-dc85debcef4e/usr/lib"
        }

      And, in a more complex example where some items are self referencing from their parent::

        {
          "paths": {
            "mount": "/occam/1133bb33-5274-11e5-b1d4-dc85debcef4e"
          },
          "input": [
            {
              "paths": {
                "mount": "/occam/dd44fcce-5274-11e5-b1d4-dc85debcef4e"
              },
              "foo": "./sample {{ paths.mount }}",
              "bar": "{{ paths.mount }}/usr/lib"
            }
          ]
        }

      Becomes::

        {
          "paths": {
            "mount": "/occam/1133bb33-5274-11e5-b1d4-dc85debcef4e"
          },
          "input": [
            {
              "paths": {
                "mount": "/occam/dd44fcce-5274-11e5-b1d4-dc85debcef4e"
              },
              "foo": "./sample /occam/dd44fcce-5274-11e5-b1d4-dc85debcef4e",
              "bar": "/occam/dd44fcce-5274-11e5-b1d4-dc85debcef4e/usr/lib"
            }
          ]
        }

      As a special case, ``{{ {{ }}`` will escape double curly brackets,
      although double right curly brackets don't require it::

        {
          "foo": "./sample 'foo bar {{{{}}baz}}'"
        }

      Creates the task manifest with the curly brace intact::

        {
          "foo": "./sample 'foo bar {{baz}}'"
        }
    """

    if root is None:
      root = value

    if isinstance(value, dict):
      # Go through a chunk of metadata

      newRoot = root
      if 'id' in value:
        newRoot = value

      # Handle complex objects first
      for k, v in value.items():
        if isinstance(v, dict):
          self.parseTagsInTask(v, root=newRoot, removeUnknown=removeUnknown)

      # Then lists
      for k, v in value.items():
        if isinstance(v, list):
          self.parseTagsInTask(v, root=newRoot, removeUnknown=removeUnknown)

      # Then strings
      for k, v in value.items():
        if isinstance(v, str):
          value[k] = self.parseTagsInTask(v, root=newRoot, removeUnknown=removeUnknown)

    elif isinstance(value, list):
      # Go through a list of items

      i = 0
      for v in value:
        value[i] = self.parseTagsInTask(v, root=root, removeUnknown=removeUnknown)
        if value[i] == "" and v != "":
          # Delete empty strings in lists
          del value[i]
        else:
          i += 1
    elif isinstance(value, str):
      # This is a string, so we parse it for curly brace sections
      try:
        index = 0
        start = value.index("{{", index)

        while start >= 0:
          end   = value.index("}}", start)
          key   = value[start+2:end].strip()

          commands = []

          if key == "{{":
            # Escaped curly brace section
            replacement = "{{"
          else:
            key, *commands = [x.strip() for x in key.split('|')]
            try:
              replacement = self.parser.get(root, key)
            except:
              if removeUnknown:
                replacement = ""
              else:
                replacement = "{{ %s }}" % (key)

          if replacement is None:
            replacement = ""

          replacement = str(replacement)

          # Update keys *within* the replacement text
          replacement = self.updateTag(replacement, key, root)

          # If there are no more keys, then we're good to execute the commands
          if "{{" not in replacement:
            for command in commands:
              func, *args = [x.strip() for x in command.split(" ")]

              if func == "substring":
                if len(args) == 0:
                  args = [0]
                if len(args) == 1:
                  args.append(len(replacement))
                if len(args) > 2:
                  args = args[0:2]

                args = [int(x) for x in args]

                args[0] = min(args[0], len(replacement))
                args[1] = min(args[1], len(replacement))

                replacement = replacement[args[0]:args[1]]

          try:
            value = value[0:start] + replacement + value[end+2:]
          except:
            value = value

          # Adjust where we look next
          index = start + len(replacement)

          # Pull the index of the next {{ section
          # If there isn't one, it will fire an exception
          start = value.index("{{", index)

      except ValueError as e:
        pass

    if value is None:
      value = ""
    return value

  def rakeTaskObjects(self, objInfo):
    """ Returns an array of every object metadata for the given task metadata.
    """

    ret = []

    for wire in objInfo.get('inputs', []):
      for input in wire.get('connections', []):
        ret.append(input)
        ret += self.rakeTaskObjects(input)

    return ret

class ManifestError(Exception):
  """ Base class for all manifest errors.
  """
  pass

class DependencyUnresolvedError(ManifestError):
  """ Error is generated when a dependency cannot be resolved.
  """

  def __init__(self, objectInfo, message):
    self.objectInfo = objectInfo
    self.message = message

class BuildRequiredError(ManifestError):
  """ Error is generated when a build is required.
  """

  def __init__(self, requiredObject, message):
    self.requiredObject = requiredObject
    self.message = message
