# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import importlib

from occam.config  import Config

from occam.manager import uses

from occam.commands.manager import CommandManager

@uses(CommandManager)
class CLI:
  def componentUsage(self, component):
    import os
    # Pull in the available components.
    config = Config.load()
    components = config.get('system', {}).get('components', [])

    componentModule = None
    for compare in components:
      if compare.split(".")[-1] == component:
        componentModule = compare
        break

    if componentModule is None:
      from occam.log import Log
      Log.error("No such component %s." % (component))
      return

    print("occam {} [COMMAND] [OPTIONS...]".format(component))
    print("")
    print("Commands:")

    # Import the component manager
    try:
      mod = importlib.import_module("%s.manager" % (componentModule))
      mod_path = mod.__file__
    except ImportError:
      return

    commandPath = os.path.realpath("%s/commands" % (os.path.dirname(os.path.realpath(mod_path))))

    # Import every command and list its help contents
    if not os.path.exists(commandPath):
      return

    for f in os.listdir(commandPath):
      if f.startswith("_") or not f.endswith(".py"):
        continue

      moduleName = os.path.splitext(f)[0]

      try:
        importlib.import_module("%s.commands.%s" % (componentModule, moduleName))
      except ImportError:
        pass

    categoryList = CommandManager.categories()
    categoryList.sort()

    # Ensure "Other" is the last category
    try:
      categoryList.remove("Other")
      categoryList.append("Other")
    except:
      pass

    maxCommandLength = 0
    for category in categoryList:
      # Do not show any commands in the blank category
      if category == "":
        continue

      for command in CommandManager.commands(component, category):
        info = CommandManager.commandInfo(component, command)

        if len(info["name"]) > maxCommandLength:
          maxCommandLength = len(info["name"])

    for category in categoryList:
      if category == "":
        continue
      print("")
      print(category + ": ")
      commandList = CommandManager.commands(component, category)
      commandList.sort()
      for command in commandList:
        info = CommandManager.commandInfo(component, command)
        print("  " + info["name"] + (" " * (maxCommandLength - len(info["name"]))) + "   " + info["documentation"])

  def usage(self):
    import os

    print("occam [COMPONENT] [COMMAND] [OPTIONS...]")
    print("")
    print("type 'occam [COMPONENT]' with one of the follow components to see individual usage")
    print("")
    print("Components:")

    # Pull in the available components.
    config = Config.load()
    components = config.get('system', {}).get('components', [])

    for component in components:
      # Import the component manager
      try:
        mod = importlib.import_module("%s.manager" % (component))
        mod_path = mod.__file__
      except ImportError:
        continue

      commandPath = os.path.realpath("%s/commands" % (os.path.dirname(os.path.realpath(mod_path))))

      # Show only if the component has commands
      if not os.path.exists(commandPath):
        continue

      print("  " + component.split(".")[-1])

    categoryList = CommandManager.categories()
    categoryList.sort()

    # Ensure "Other" is the last category
    try:
      categoryList.remove("Other")
      categoryList.append("Other")
    except:
      pass

    for category in categoryList:
      # Do not show any commands in the blank category
      if category == "":
        continue

      print("")
      print(category + ": ")

      maxCommandLength = 0
      for command in CommandManager.commands(category):
        info = CommandManager.commandInfo(command)

        if len(info["name"]) > maxCommandLength:
          maxCommandLength = len(info["name"])

      commandList = CommandManager.commands(category)
      commandList.sort()
      for command in commandList:
        info = CommandManager.commandInfo(command)
        print("  " + info["name"] + (" " * (maxCommandLength - len(info["name"]))) + " - " + info["documentation"])

  def execute(self, args):
    return self.commands.execute(args)
