# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os, json, codecs, io

from occam.config     import Config
from occam.log        import loggable

from occam.manager import manager, uses

@loggable
@manager("git")
class GitManager:
  """ This OCCAM manager handles git access and retrieval.
  """

  @staticmethod
  def popen(command, stdout=None, stdin=None, stderr=None, cwd=None, env=None):
    import subprocess
    if stdout is None:
      stdout = subprocess.DEVNULL

    if stderr is None:
      stderr = subprocess.DEVNULL

    return subprocess.Popen(command, stdout=stdout, stdin=stdin, stderr=stderr, cwd=cwd, env=env)

  def issueService(self, stdin, path, service):
    """ Issues the requested service and returns a bytestream.
    """

    import subprocess
    p = GitManager.popen(['git', service, '--stateless-rpc', path], stdin = subprocess.PIPE, stdout = subprocess.PIPE, cwd=path)
    p.stdin.write(stdin.read())
    p.stdin.close()

    return p.stdout

  def publishGitHead(self, stdin, path):
    """ Returns the HEAD data as a bytestream.
    """

    return open(os.path.join(path, "HEAD"), "rb")

  def packetWrite(self, line):
    """ Forms a git protocol packet from the given bytestring.
    """

    return "{:04x}".format(len(line) + 4).encode('utf-8') + line

  def publishInfoRefs(self, stdin, path):
    """ Returns the info refs as a bytestream.
    """

    return open(os.path.join(path, "info", "refs"), "rb")

  def publishUploadPack(self, stdin, path, service):
    """ Returns a git info ref upload-pack object as a bytestream.
    """

    # Write header
    ret = io.BytesIO(b'')
    ret.write(self.packetWrite(("# service=%s\n" % (service)).encode('utf-8')))
    ret.write(b"0000")

    # Write the pack contents

    import subprocess
    command = ['git', 'upload-pack', '--stateless-rpc', '--advertise-refs', path]
    print(" ".join(command))
    p = GitManager.popen(['git', 'upload-pack', '--stateless-rpc', '--advertise-refs', path], stdout = subprocess.PIPE, cwd=path)
    ret.write(p.stdout.read())
    p.communicate()

    ret.seek(0, io.SEEK_SET)

    return ret

  def publishObjectsInfoFile(self, stdin, path, file):
    """ Returns the particular object info pack as a bytestream.
    """

    return open(os.path.join(path, "objects", "info", file), "rb")

  def publishObjectsPack(self, stdin, path, prefix):
    """ Returns the pack object file.
    """

    return open(os.path.join(path, "objects", "pack", prefix))

  def publishObjectsFile(self, stdin, path, prefix, suffix):
    """ Returns the loose object file for the given prefix and suffix as a bytestream.
    """

    return open(os.path.join(path, "objects", prefix, suffix))
