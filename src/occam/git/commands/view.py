# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json, os

from occam.commands.manager import command, option, argument

from occam.object import Object
from occam.log    import Log

from occam.manager import uses

from occam.storage.manager import StorageManager
from occam.objects.manager import ObjectManager
from occam.git.manager     import GitManager

@command('git', 'view',
  category      = 'Git Interface',
  documentation = "Returns git objects as a bytestream")
@argument("object",    type = "object",
                       help = "The object to interface with")
@argument("action",    type = str,
                       help = "The action to perform")
@argument("arguments", type = str,
                       help = "The argument to this action",
                       nargs = "*")
@uses(ObjectManager)
@uses(StorageManager)
@uses(GitManager)
class GitViewCommand:
  def do(self):
    object = self.objects.resolve(self.options.object, person = self.person)

    if object is None:
      Log.error("cannot find object with id %s" % (self.options.object.id))
      return -1

    path = self.storage.repositoryPathFor(uuid     = object.uid,
                                          revision = object.revision,
                                          identity = object.identity)

    if path is None:
      Log.error("cannot find git repository for object with id %s" % (self.options.object.id))
      return -1

    path = os.path.join(path, ".git")

    try:
      action = getattr(self.git, self.options.action)
    except:
      Log.error("no such git action %s" % (self.options.action))
      return -1

    byteStream = action(self.stdin, path, *self.options.arguments)

    Log.pipe(byteStream)
    return 0
