# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.databases.manager import table

@table("local_links")
class LocalLinkRecord():
  """ Relates a local path to a workset object.
  """

  schema = {

    # Primary Key

    "id": {
      "type": "integer",
      "primary": True
    },

    # Foreign Key to Object

    "target_object_id": {
      "foreign": {
        "table": "objects",
        "key":   "id"
      }
    },

    # Attributes

    # The local system location for the tracked object
    "path": {
      "type": "string",
      "length": 128
    },

    # The current revision of the tracked object
    "revision": {
      "type": "string",
      "length": 128
    },

    # Whether or not the LocalLink path is tracked by the system
    # As opposed to a LocalLink that tracks somebody's local copy
    "internal": {
      "type": "boolean",
      "default": False
    }
  }
