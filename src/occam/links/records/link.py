# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.databases.manager import table

@table("links")
class LinkRecord():
  """ Relates an object to another object.
  """

  schema = {

    # Primary Key

    "id": {
      "type": "integer",
      "primary": True
    },

    # Foreign Key to Object

    "source_object_id": {
      "foreign": {
        "table": "objects",
        "key":   "id"
      }
    },

    "target_object_id": {
      "foreign": {
        "table": "objects",
        "key":   "id"
      }
    },

    "target_revision": {
      "type": "string",
      "length": 128
    },

    # Local Link (creates a tracked link, that follows changes)

    "local_link_id": {
      "foreign": {
        "table": "local_links",
        "key":   "id"
      }
    },

    # Attributes

    "relationship": {
      "type": "string",
      "length": 128
    },

    "published": {
      "type": "datetime",
    },

    # Constraints
    "ensure_dated_order": {
      "constraint": {
        "type": "unique",
        "columns": ["source_object_id", "target_object_id", "target_revision", "relationship"],
        "conflict": "replace"
      }
    }
  }
