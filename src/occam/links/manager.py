# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os

from occam.config  import Config

from occam.manager import uses, manager
from occam.log     import loggable

from occam.databases.manager   import DatabaseManager
from occam.permissions.manager import PermissionManager
from occam.caches.manager      import CacheManager

@loggable
@uses(DatabaseManager)
@uses(PermissionManager)
@uses(CacheManager)
@manager("links")
class LinkManager:
  """ Manages relationships among objects and synchronization of objects and local clones.

  Essentially, some objects can be created in a directory and then modified
  outside of that context. Perhaps, through a web-based client. This manager
  establishes a link between objects and their local content ensuring that if
  they are updated, they change locally as well.

  This is useful for caching and ensuring that, for instance, a workset that
  is being modified isn't cloned repeatedly. It also gives a single point for
  all collaborators to modify an object.

  Other relationships tie objects together. One such type of relationship is
  one between a Person and an Object. Such as a recently used items list,
  bookmarks, and any active work. A client can make links and use them to
  organize objects appropriately.

  Links are not propagated across the federation.
  """

  def _cacheKey(self, source_object_id, relationship, order = "ascending"):
    """ Internal method to produce the cache key for the given object and relationship.
    """

    return "links-%s-%s-%s" % (source_object_id, relationship, order)

  def create(self, source_object_db, relationship, target_object_db, target_revision, limit = None, local_link_id = None, person=None):
    """ Creates a link between two objects.

    This is used primarily to connect a Person to an Object. For instance, a
    "bookmark" link will let people save objects at a particular revision to
    a list. This way, they can recall them at some point.

    The main list of active work for a Person is the "collected" relation. When
    one removes an item from their dashboard, that item then becomes "archived"
    for one particular example of this might be used by an Occam client.

    If the link already exists, the relationship will be updated.

    You cannot add a link to an object you cannot see.
    You cannot add a link from an object you cannot edit.

    Args:
      source_object_db (ObjectRecord): The object record to link from.
      relationship (str): The relationship between the objects.
      target_object_db (ObjectRecord): The object record to link to.
      target_revision (str): The revision of the target object.
      limit (int): The maximum number of links for this relation.
        Defaults to None which implies an infinite number. When the limit is
        set, it will add this link, and if the limit is exceeded, truncate the
        oldest stored link.
      local_link_id (str): The identifier of the local link.
      person (Person): The person adding the link, for permission checking.

    Returns:
      LinkRecord: The resulting link record.
    """

    if not self.permissions.can("write", source_object_db.id, person=person):
      return None

    if not self.permissions.can("read", target_object_db.id, person=person):
      return None

    # Create a new LinkRecord

    import datetime
    published = datetime.datetime.now()
    link = self.datastore.createLink(sourceObjectId       = source_object_db.id,
                                     targetObjectId       = target_object_db.id,
                                     targetObjectRevision = target_revision,
                                     relationship         = relationship,
                                     published            = published,
                                     trackingId           = local_link_id)

    if limit and limit > 0:
      self.datastore.truncateLinks(sourceObjectId       = source_object_db.id,
                                   relationship         = relationship,
                                   limit                = limit)

    # Invalidate the cache key
    self.cache.delete(self._cacheKey(source_object_db.id, relationship, order = "ascending"))
    self.cache.delete(self._cacheKey(source_object_db.id, relationship, order = "descending"))

    return link

  def retrieve(self, source_object_db, relationship, target_object_db=None, target_revision = None, person = None):
    """ Retrieves a list of LinkRecords for the given source and relationship.

    You may optionally provide the target to filter to just the single link
    (if it exists) between source and target. It will return a list of one
    link (or an empty list.)
    """

    targetObjectId = None
    if target_object_db:
      targetObjectId = target_object_db.id

    return self.datastore.retrieveLinks(sourceObjectId       = source_object_db.id,
                                        targetObjectId       = targetObjectId,
                                        targetObjectRevision = target_revision,
                                        relationship         = relationship)


  def retrieveObjects(self, source_object_db, relationship=None, target_object_db=None, target_revision = None, person = None, order = "ascending"):
    """ Retrieves an ObjectRecord list for the links for the given source and relationship.
    """

    import json
    from occam.objects.records.object import ObjectRecord

    if relationship:
      cacheKey = self._cacheKey(source_object_db.id, relationship, order)

      data = self.cache.get(cacheKey)

      if not data is None:
        import codecs
        return [ObjectRecord(x) for x in json.loads(codecs.decode(data, 'utf8'))]

    session = self.database.session()

    import sql

    links   = sql.Table('links')
    objects = sql.Table('objects')

    join = links.join(objects)
    join.condition = (links.target_object_id == join.right.id)

    query = join.select(links.id.as_('link_id'),
                        links.target_revision,
                        links.relationship,
                        links.local_link_id,
                        join.right.id,
                        join.right.object_type,
                        join.right.subtype,
                        join.right.name,
                        join.right.organization,
                        join.right.description,
                        join.right.updated,
                        join.right.published,
                        join.right.tags,
                        join.right.environment,
                        join.right.architecture,
                        join.right.capabilities)

    query.where = (links.source_object_id == source_object_db.id)
    if relationship:
      query.where = query.where & (links.relationship == relationship)

    if order == "ascending":
      query.order_by = sql.Asc(links.id)
    else:
      query.order_by = sql.Desc(links.id)

    self.database.execute(session, query)

    ret = self.database.many(session)

    if relationship:
      self.cache.set(cacheKey, json.dumps(ret))

    return [ObjectRecord(x) for x in ret]

  def destroy(self, link_db, person=None):
    """ Destroys the given link.
    """

    ret = self.datastore.delete(id = link_db.id)

    # Invalidate the cache key
    self.cache.delete(self._cacheKey(link_db.source_object_id, link_db.relationship, order = "ascending"))
    self.cache.delete(self._cacheKey(link_db.source_object_id, link_db.relationship, order = "descending"))

    return ret

  def destroyLocalLink(self, link_id):
    """ Destroys any local link (if it exists) for the given link.
    """

    import sql

    session = self.database.session()

    localLinks = sql.Table("local_links")

    query = localLinks.delete(where = (localLinks.id == link_id))

    self.database.execute(session, query)
    self.database.commit(session)

    return True

  def path(self):
    """ Returns the path to the local tracked-object store.
    """

    return self.configuration.get('path', os.path.join(Config.root(), 'links'))

  def pathFor(self, link_id):
    """ Returns a suitable path for the tracked object.
    """

    # Find the full path to the stored invocation metadata
    # Ex: .occam/links/2-code/2-code/full-code

    from uuid import uuid4

    uuid = str(uuid4())

    code = uuid
    code1 = code[0:2]
    code2 = code[2:4]

    path = self.path()

    if not os.path.exists(path):
      os.mkdir(path)
    path = os.path.join(path, code1)
    if not os.path.exists(path):
      os.mkdir(path)
    path = os.path.join(path, code2)
    if not os.path.exists(path):
      os.mkdir(path)
    path = os.path.join(path, uuid)
    if not os.path.exists(path):
      os.mkdir(path)

    return path

  def createLocalLink(self, db_object, revision, path):
    """ Creates a link between a local data object and a local path.
    
    This record will allow changes that happen within the system
    or seen by the outside to be reflected at the current path automatically
    in some situations.

    When path is None, the tracked object is placed in a tracked object pool
    somewhere within the occam system's file storage. These are system-tracked
    objects and have some special semantics. For instance, they collapse
    whenever revisions eventually match other tracked objects. They also
    regenerate when they are missing, as opposed to non-system local links
    which are destroyed when the path they track goes away.

    Returns the LocalLinkRecord created.
    """

    from occam.links.records.local_link import LocalLinkRecord

    session = self.database.session()

    local_link = LocalLinkRecord()
    local_link.internal = False

    if path is None:
      # Create a suitable path outside of the object store
      path = self.pathFor(0)
      local_link.internal = True

    # Assign keys to local link
    local_link.path = path
    local_link.target_object_id = db_object.id
    local_link.revision = revision

    LinkManager.Log.noisy("creating local link between object (%s) at %s" % (db_object.id, path))

    self.database.update(session, local_link)
    self.database.commit(session)

    return local_link

  def updateLocalLink(self, link, newRevision):
    """ Updates the status of the local version based on a stored revision.

    It will collapse internal tracked objects when the newRevision is one that
    is already known and tracked elsewhere. This can happen when objects are
    reverted. Essentially, it just deletes it and lets the current LocalLink
    handle tracking the object from now on.
    """

    import sql

    session = self.database.session()

    link.revision = newRevision

    self.database.update(session, link)
    self.database.commit(session)

    # Update any minor links that are attached to this local link

    links = sql.Table('links')

    query = links.update(where = (links.local_link_id == link.id), columns = [links.target_revision], values = [link.revision])

    self.database.execute(session, query)

    query = links.select(where = (links.local_link_id == link.id))

    self.database.execute(session, query)
    links = self.database.many(session)

    from occam.links.records.link import LinkRecord
    for link in links:
      # TODO: simplify this as a query instead
      link = LinkRecord(link)
      self.cache.delete(self._cacheKey(link.source_object_id, link.relationship, order = "ascending"))
      self.cache.delete(self._cacheKey(link.source_object_id, link.relationship, order = "descending"))

    return link

  def retrieveLocalLinks(self, obj, path = None, link_id = None):
    """ Retrieves the LocalLink, if it exists, for the given object.

    This should be done for every read/write of an Object that might be done.
    If there is a local link, it serves as a cached repository for that Object.

    That is, and in particular collaborations or large project worksets, this
    is used not only for command line usage that goes along with any web or GUI
    client... it is also used internally or by clients to create a quicker way
    of modifying an object repeatedly. That way, the object does not need to be
    cloned and then modified and then stored each time.
    """

    import sql

    session = self.database.session()

    from occam.links.records.local_link import LocalLinkRecord

    links = sql.Table("links")
    subQuery = links.select(links.local_link_id, where = (links.id == link_id))

    localLinks = sql.Table("local_links")
    objects    = sql.Table("objects")

    query = localLinks.select(where = (localLinks.target_object_id == obj.id) and (localLinks.id == link_id))

    if path:
      query.where = query.where & (localLinks.path == path)

    self.database.execute(session, query)

    return [LocalLinkRecord(x) for x in self.database.many(session)]
