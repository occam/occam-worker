# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.log import Log

from occam.manager import uses

from occam.links.manager import LinkManager
from occam.objects.manager  import ObjectManager

from occam.commands.manager import command, option, argument

@command('links', 'delete',
  category      = 'Link Management',
  documentation = "Deletes an existing link between two objects on the local system.")
@argument("source", type="object")
@argument("relationship", type=str)
@argument("target", type="object")
@uses(LinkManager)
@uses(ObjectManager)
class DeleteCommand:
  def do(self):
    Log.header("Deleting an existing %s link" % self.options.relationship)

    sources = self.objects.search(id=self.options.source.id)

    if len(sources) == 0:
      Log.error("Cannot find source object (%s)" % (self.options.source.id))
      return -1

    source_db = sources[0]

    targets = self.objects.search(id=self.options.target.id)

    if len(targets) == 0:
      Log.error("Cannot find target object (%s)" % (self.options.target.id))
      return -1

    target_db = targets[0]

    links = self.links.retrieve(source_db, self.options.relationship, target_db, self.options.target.revision)
    if len(links) > 0:
      for link in links:
        Log.write("Removing link id %s" % (link.id))
        self.links.destroy(link)

    Log.done("Successfully deleted relationship %s between %s and %s" % (self.options.relationship,
                                                                         source_db.name,
                                                                         target_db.name))
    return 0
