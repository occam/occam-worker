# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.log import Log

from occam.manager import uses

from occam.links.manager import LinkManager
from occam.objects.manager  import ObjectManager

from occam.commands.manager import command, option, argument

@command('links', 'new',
  category      = 'Link Management',
  documentation = "Creates a new link between two objects on the local system.")
@argument("source", type="object")
@argument("relationship", type=str)
@argument("target", type="object")
@option("-l", "--limit", dest    = "limit",
                         type    = int,
                         default = -1,
                         action  = "store",
                         help    = "The maximum number of links to keep (removes oldest).")
@option("-t", "--track", dest    = "track",
                         action  = "store",
                         help    = "Will be attached to the given tracked link.")
@uses(LinkManager)
@uses(ObjectManager)
class NewCommand:
  def do(self):
    Log.header("Creating a new %s link" % self.options.relationship)

    source = self.objects.resolve(self.options.source, person = self.person)

    if source is None:
      Log.error("Cannot find source object (%s)" % (self.options.source.id))
      return -1

    rows = self.objects.search(id=source.id)
    source_db = rows[0]

    target = self.objects.resolve(self.options.target, person = self.person)

    if target is None:
      Log.error("Cannot find target object (%s)" % (self.options.target.id))
      return -1

    rows = self.objects.search(id=target.id)
    target_db = rows[0]

    link_db = self.links.create(source_object_db = source_db,
                                relationship     = self.options.relationship,
                                target_object_db = target_db,
                                target_revision  = target.revision,
                                person           = self.person,
                                local_link_id    = self.options.track,
                                limit            = self.options.limit)

    Log.write("new link id: ", end ="")
    Log.output(str(link_db.id), padding="")

    Log.done("Successfully created relationship %s between %s and %s" % (self.options.relationship,
                                                                         source_db.name,
                                                                         target_db.name))
    return 0
