# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os

from occam.log import Log

from occam.manager import uses

from occam.links.manager import LinkManager
from occam.objects.manager  import ObjectManager

from occam.commands.manager import command, option, argument

@command('links', 'track',
  category      = 'Link Management',
  documentation = "Creates a tracked link for a particular object")
@argument("source", type="object", nargs="?")
@argument("path",   type=str, nargs="?")
@uses(LinkManager)
@uses(ObjectManager)
class TrackCommand:
  def do(self):
    path = self.options.path

    if path == "":
      path = None

    if self.options.source is None:
      from occam.object import Object
      obj = Object(path = ".")

      if self.options.path is None or self.options.path == "":
        path = os.path.abspath(".")
    else:
      obj = self.objects.retrieve(id = self.options.source.id, revision = self.options.source.revision, version = self.options.source.version, person = self.person)

    Log.header("Tracking object %s" % obj.id)

    if obj is None:
      Log.error("Cannot find source object (%s)" % (self.options.source.id))
      return -1

    db_objects = self.objects.search(id = obj.id)
    if len(db_objects) == 0:
      Log.error("No record of this object found.")
      return -1

    db_object = db_objects[0]

    lookupPath = path
    localLinks = self.links.retrieveLocalLinks(obj, path = lookupPath);
    if len(localLinks) > 0 and path is not None:
      Log.warning("This path is already tracking this object.")
      link = localLinks[0]
    else:
      # Create the tracked link
      link = self.links.createLocalLink(db_object, obj.revision, path)

      if link.internal:
        # Clone object into this path
        obj.revision = link.revision
        self.objects.clone(obj, link.path, increment=False)

    Log.write("link id: ", end = "")
    Log.output(str(link.id), padding = "")

    Log.done("Successfully tracking %s in %s" % (obj.id, link.path))
    return 0
