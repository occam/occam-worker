# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sql

from occam.databases.manager import uses, datastore

from occam.accounts.database import AccountDatabase

@uses(AccountDatabase)
@datastore("permissions")
class PermissionDatabase:
  """ Manages the database interactions for access control.
  """

  def retrieveAllRecords(self, id=None, db_obj=None, person_id=None, person_obj=None, allPeople=False, addChildren=False):
    session  = self.database.session()

    objects  = sql.Table('objects')
    controls = sql.Table('access_controls')

    if person_obj is None:
      if person_id is None:
        personQuery = []
      else:
        personQuery = objects.select(objects.id)
        personQuery.where = (objects.id == person_id)
    else:
      personQuery = [person_obj.id]

    if db_obj is None:
      objectQuery = objects.select(objects.id, objects.parent_id)
      objectQuery.where = (objects.id == id)

      objQuery = sql.With(query=objectQuery)

      query = controls.select(with_ = [objQuery])

      # Pull out the id/parent_id from a sub-query
      object_id = objQuery.select(objQuery.id)
      parent_id = objQuery.select(objQuery.parent_id)
    else:
      query = controls.select()

      # Just use the id/parent_id from the given record
      object_id = [db_obj.id]
      parent_id = [db_obj.parent_id]

    if allPeople:
      query.where = ((controls.internal_object_id.in_(object_id)) & (controls.for_child_access == 0))
      if addChildren:
        query.where = query.where \
                    | ((controls.internal_object_id.in_(object_id)) & (controls.for_child_access == 1))
      else:
        query.where = query.where \
                    | ((controls.internal_object_id.in_(parent_id)) & (controls.for_child_access == 1))
    else:
      query.where = ((controls.internal_object_id.in_(object_id)) & (controls.for_child_access == 0) & ((controls.person_object_id.in_(personQuery)) | (controls.person_object_id == sql.Null)))
      if addChildren:
        query.where = query.where \
                    | ((controls.internal_object_id.in_(object_id)) & (controls.for_child_access == 1) & ((controls.person_object_id.in_(personQuery)) | (controls.person_object_id == sql.Null)))
      else:
        query.where = query.where \
                    | ((controls.internal_object_id.in_(parent_id)) & (controls.for_child_access == 1) & ((controls.person_object_id.in_(personQuery)) | (controls.person_object_id == sql.Null)))

    join = query.join(objects, type_ = "LEFT")
    join.condition = (join.right.id == query.person_object_id)
    query = join.select()

    self.database.execute(session, query)

    from occam.permissions.records.access_control import AccessControlRecord
    from occam.objects.records.object             import ObjectRecord
    records = self.database.many(session, size=10)
    records = [(AccessControlRecord(x), ObjectRecord(x),) for x in records]

    return records

  def retrieveAccessControl(self, id=None, db_obj=None, person_id=None, person_obj=None):
    """ Retrieves the access control records for the given object.
    """

    session  = self.database.session()

    objects  = sql.Table('objects')
    controls = sql.Table('access_controls')

    # We want AccessControl records for:
    #   internal_object_id == db_obj.id, for_child=0, people=person
    #   internal_object_id == db_obj.parent_id, for_child=1, people=person
    #   internal_object_id == db_obj.id, for_child=0, people=None
    #   internal_object_id == db_obj.parent_id, for_child=1, people=None
    # (and in that order)
    # We should return up to four records for the given object.
    # The query should hopefully pull out all 4

    # Looking at this, it is a small miracle that it could be written, and a
    # larger miracle still that people go through the trouble of creating DSLs
    # for their DSLs and yet I still want to use it instead of writing the
    # query by hand. Weird weird world.

    if person_obj is None:
      if person_id is None:
        personQuery = []
      else:
        personQuery = objects.select(objects.id)
        personQuery.where = (objects.id == person_id)
    else:
      personQuery = [person_obj.id]

    if db_obj is None:
      objectQuery = objects.select(objects.id, objects.parent_id)
      objectQuery.where = (objects.id == id)

      objQuery = sql.With(query=objectQuery)

      query = controls.select(with_ = [objQuery])

      # Pull out the id/parent_id from a sub-query
      object_id = objQuery.select(objQuery.id)
      parent_id = objQuery.select(objQuery.parent_id)
    else:
      query = controls.select()

      # Just use the id/parent_id from the given record
      object_id = [db_obj.id]
      parent_id = [db_obj.parent_id]

    membershipsQuery = self.accounts.queryMembershipsFor(personQuery, key = "base_person_object_id")

    query.where = ((controls.internal_object_id.in_(object_id)) & (controls.for_child_access == 0) & ((controls.person_object_id.in_(personQuery)) | (controls.person_object_id == sql.Null) | (controls.person_object_id.in_(membershipsQuery))))
    query.where = query.where \
                | ((controls.internal_object_id.in_(parent_id)) & (controls.for_child_access == 1) & ((controls.person_object_id.in_(personQuery)) | (controls.person_object_id == sql.Null) | (controls.person_object_id.in_(membershipsQuery))))

    self.database.execute(session, query)

    from occam.permissions.records.access_control import AccessControlRecord
    records = self.database.many(session, size=10)
    records = [AccessControlRecord(x) for x in records]

    # Sort by for_child_access.
    ret = []

    # Don't be clever, here. Just look at them. It's just 4 records (and change). Calm down.
    #   The database engine doesn't have to do ALL the work.
    for record in records:
      if (not record.person_object_id is None) and record.for_child_access == 0:
        # Append the person's individual record
        ret.append(record)

    for record in records:
      if not record.person_object_id is None and record.for_child_access == 1:
        # Append the person's individual record for the parent object
        ret.append(record)

    for record in records:
      if record.person_object_id is None and record.for_child_access == 0:
        # Append the universe's individual record for object
        ret.append(record)

    for record in records:
      if record.person_object_id is None and record.for_child_access == 1:
        # Append the universe's individual record for parent object
        ret.append(record)

    return ret

  def updateAccessControl(self, id=None, db_obj=None,
                                person_id=None, person_obj=None,
                                children=False,
                                canRead=0, canWrite=0, canClone=0, canRun=0):
    """ Updates the access control record for the given object.
    """

    session = self.database.session()

    if db_obj is None and id is not None:
      objects = sql.Table('objects')
      subquery = objects.select(objects.id)
      subquery.where = (objects.id == id)
    elif db_obj:
      subquery = [db_obj.id]
    else:
      return None

    if person_obj is None and person_id is not None:
      objects = sql.Table('objects')
      personSubquery = objects.select(objects.id)
      personSubquery.where = (objects.id == person_id)
    elif person_obj:
      personSubquery = [person_obj.id]
    else:
      personSubquery = None

    controls = sql.Table('access_controls')

    query = controls.select()
    query.where = (controls.internal_object_id.in_(subquery))
    if personSubquery is None:
      query.where = query.where & (controls.person_object_id == None)
    else:
      query.where = query.where & (controls.person_object_id.in_(personSubquery))
    query.where = query.where & (controls.for_child_access == (0 if children == False else 1)) 

    self.database.execute(session, query)

    from occam.permissions.records.access_control import AccessControlRecord
    record = self.database.fetch(session)

    if record is None:
      # Create a new access control record instead
      record = AccessControlRecord()

      record.can_read  = None
      record.can_write = None
      record.can_clone = None
      record.can_run   = None

      if db_obj is None:
        objects = sql.Table('objects')
        subquery = objects.select()
        subquery.where = (objects.id == id)

        self.database.execute(session, subquery)
        db_obj_row = self.database.fetch(session)

        if db_obj_row is None:
          return None

        from occam.objects.records.object import ObjectRecord
        db_obj = ObjectRecord(db_obj_row)

      record.internal_object_id = db_obj.id

      if person_obj is None and person_id is not None:
        objects = sql.Table('objects')
        subquery = objects.select()
        subquery.where = (objects.id == person_id)

        self.database.execute(session, subquery)
        db_obj_row = self.database.fetch(session)

        if db_obj_row is None:
          return None

        from occam.objects.records.object import ObjectRecord
        db_obj = ObjectRecord(db_obj_row)

        record.person_object_id = db_obj.id

      if children:
        record.for_child_access = True
    else:
      record = AccessControlRecord(record)

    if isinstance(canRead, bool) or canRead is None:
      record.can_read = canRead

    if isinstance(canWrite, bool) or canWrite is None:
      record.can_write = canWrite

    if isinstance(canClone, bool) or canClone is None:
      record.can_clone = canClone

    if isinstance(canRun, bool) or canRun is None:
      record.can_run = canRun

    self.database.update(session, record)
    self.database.commit(session)

    return record

  def migrateAccessControls(self, oldId, newId):
    """ Updates all records for the given oldId to use the given newId.
    """

    session = self.database.session()

    controls = sql.Table('access_controls')
    query = controls.update(where = (controls.internal_object_id == oldId), columns = [controls.internal_object_id], values = [newId])

    self.database.execute(session, query)
    self.database.commit(session)

  def retrieveReviewLinks(self, obj, revision = None):
    """ Retrieves the list of RecordCapabilityRecord entries for the given object.
    """

    from occam.permissions.records.review_capability import ReviewCapabilityRecord

    session = self.database.session()

    objects = sql.Table('objects')
    query = objects.select(objects.id)
    query.where = (objects.id == obj.id)

    self.database.execute(session, query)
    db_object = self.database.fetch(session)

    records = sql.Table('review_capabilities')

    query = records.select()
    query.where = (records.internal_object_id == db_object['id'])

    if revision:
      query.where = query.where & (records.revision == revision)

    self.database.execute(session, query)
    db_records = [ReviewCapabilityRecord(x) for x in self.database.many(session)]

    return db_records

  def retrieveReviewLink(self, obj):
    """ Retrieves the RecordCapabilityRecord or None for the given object.
    """

    records = self.retrieveReviewLinks(obj, revision = obj.revision)

    ret = None
    if len(records) > 0:
      ret = records[0]

    return ret

  def createReviewLink(self, obj):
    """ Creates a review link for the given object at its given revision.

    A review link will allow an anonymous access to that object by any
    Person on the system.

    Returns:
      The ReviewCapability record that already exists or was created.
    """

    link = self.retrieveReviewLink(obj)

    if link is None:
      # Create the link
      from occam.permissions.records.review_capability import ReviewCapabilityRecord

      link = ReviewCapabilityRecord()

      session = self.database.session()

      import datetime

      objects = sql.Table('objects')
      query = objects.select(objects.id)
      query.where = (objects.id == obj.id)

      self.database.execute(session, query)
      db_object = self.database.fetch(session)

      link.internal_object_id = db_object['id']
      link.revision = obj.revision
      link.published = datetime.datetime.utcnow()

      self.database.update(session, link)
      self.database.commit(session)

    return link

  def removeReviewLink(self, obj):
    """ Deletes the given review link for this object.

    Returns:
      The ReviewCapabilityRecord that was deleted.
    """

    link = self.retrieveReviewLink(obj)

    if link:
      session = self.database.session()
      self.database.delete(session, link)
      self.database.commit(session)

    return link

  def retrieveTrustAssociation(self, obj, person, all=False):
    """
    """

    # Retrieve any trust relationship
    session  = self.database.session()

    objects = sql.Table('objects')
    subquery = objects.select(objects.id)
    subquery.where = (objects.id == obj.id)

    trustAssociations = sql.Table('trust_associations')
    query = trustAssociations.select()
    query.where = (trustAssociations.internal_object_id.in_(subquery))

    subsubquery = (trustAssociations.person_object_id == sql.Null)

    if person:
      subsubsubquery = objects.select(objects.id)
      subsubsubquery.where = (objects.id == person.id)

      if all:
        subsubquery = subsubquery | (trustAssociations.person_object_id.in_(subsubsubquery))
      else:
        subsubquery = trustAssociations.person_object_id.in_(subsubsubquery)

    query.where = query.where & subsubquery

    self.database.execute(session, query)
    ret = self.database.fetch(session)

    if not ret:
      return None

    from occam.permissions.records.trust_association import TrustAssociationRecord
    return TrustAssociationRecord(ret)

  def createTrustAssociation(self, obj, person):
    """
    """

    # Retrieve any trust relationship
    session  = self.database.session()

    from occam.permissions.records.trust_association import TrustAssociationRecord
    record = self.retrieveTrustAssociation(obj, person)

    if record:
      return record

    record = record or TrustAssociationRecord()

    objects = sql.Table('objects')
    query = objects.select(objects.id)
    query.where = (objects.id == obj.id)

    self.database.execute(session, query)
    db_object = self.database.fetch(session)

    if db_object:
      record.internal_object_id = db_object['id']

    if person:
      objects = sql.Table('objects')
      query = objects.select(objects.id)
      query.where = (objects.id == person.id)

      self.database.execute(session, query)
      db_object = self.database.fetch(session)

      if db_object:
        record.person_object_id = db_object['id']

    self.database.update(session, record)
    self.database.commit(session)

    return record

  def removeTrustAssociation(self, obj, person):
    """
    """

    record = self.retrieveTrustAssociation(obj, person)

    if record:
      session = self.database.session()
      self.database.delete(session, record)
      self.database.commit(session)

    return record

  def isTrusted(self, obj, person):
    """
    """

    record = self.retrieveTrustAssociation(obj, person, all=True)

    return record is not None
