# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.databases.manager import table

@table("access_controls")
class AccessControlRecord:
  """ Stores information about how an object can be access by the given person.

  The person id may be None, in which case this tells you the default action for
  any non-collaborator who happens across the object on a client.

  The ObjectManager will use these records to know if the given person has
  access to the record, and the UpdateManager looks to see if the person has
  write access.
  """

  schema = {

    # Primary Key

    "id": {
      "type": "integer",
      "primary": True
    },

    # Foreign Key to Object

    "internal_object_id": {
      "foreign": {
        "table": "objects",
        "key":   "id"
      }
    },

    # Foreign Key to Person Object

    "person_object_id": {
      "foreign": {
        "table": "objects",
        "key":   "id"
      }
    },

    # Whether or not this describes actions on children

    "for_child_access": {
      "type": "boolean",
      "default": False
    },

    # Attributes

    "can_read": {
      "type": "boolean"
    },

    "can_write": {
      "type": "boolean"
    },

    "can_clone": {
      "type": "boolean"
    },

    "can_run": {
      "type": "boolean"
    }
  }
