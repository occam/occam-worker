# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.databases.manager import table

@table("replication_controls")
class ReplicationControlRecord:
  """ Stores information about how an object can be cloned and replicated off-site.

  In our archive worldview, objects are constantly shared and replicated across
  different systems. This controls how such objects are replicated. Some objects
  don't want to be shared at all, and we can respect that. Some objects want to
  be cloned, but not replicated automatically. Others, still, want to be copied
  indefinitely.

  There are many modes and use-cases for stiffling replication. Some unfortunate
  modes of thinking, as well, but otherwise some issues related to the whims
  of object owners.
  """

  schema = {

    # Primary Key

    "id": {
      "type": "integer",
      "primary": True
    },

    # Foreign Key to Object

    "internal_object_id": {
      "foreign": {
        "table": "objects",
        "key":   "id"
      }
    },

    # Foreign Key to Replication Control for children

    "child_replication_control_id": {
      "foreign": {
        "table": "replication_controls",
        "key":   "id"
      }
    },

    # Attributes

    "can_push": {
      "type": "boolean",
      "default": False
    }
  }
