# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.databases.manager import table

@table("trust_associations")
class TrustAssociationRecord:
  """ A record that reflects that the given person (or system) trusts an object.

  Some objects may have malicious or destructive effects on a system. By default
  objects are executed in a sandbox which may limit their behavior. A system
  administrator or individual user (when the objects run on the client-side) may
  elect to remove such protections. This record keeps track of that decision.
  """

  schema = {

    # Primary Key

    "id": {
      "type": "integer",
      "primary": True
    },

    # Foreign Key to Object

    "internal_object_id": {
      "foreign": {
        "table": "objects",
        "key":   "id"
      }
    },

    # Foreign Key to Person Object (can be NULL to apply system-wide)

    "person_object_id": {
      "foreign": {
        "table": "objects",
        "key":   "id"
      }
    },

    # Fields

    "capabilities": {
      "type": "text"
    }
  }
