# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.manager import uses, manager

from occam.log import loggable

@loggable
@manager("permissions")
class PermissionManager:
  """ Manages access control and replication control for objects.
  """

  def can(self, key, id, revision=None, person=None):
    """ Determines if the given person has the given access for the given object.
    """

    return self.retrieve(id, revision=revision, person=person).get(key, False)

  def retrieve(self, id, revision=None, person=None):
    """ Retrieves the aggregated access control record for this object.

    This will compute the permissions for the given object. The precedence
    follows with the record associated with the given person being considered
    over the default record for the object and then, finally, the record
    for child permissions of the owning object.
    """

    from types import SimpleNamespace

    records = []
    if person:
      if hasattr(person, 'readonly') and person.readonly:
        if id == person.readonly:
          records = [SimpleNamespace(can_read = True, can_write = None, can_clone = None, can_run = None)]
      elif hasattr(person, 'anonymous') and person.anonymous:
        if id == person.anonymous:
          records = [SimpleNamespace(can_read = True, can_write = None, can_clone = None, can_run = None)]
      else:
        records = self.retrieveAccessControl(id=id, person_id=person.id)
    else:
      records = self.retrieveAccessControl(id=id)

    ret = {}

    for record in records:
      if not 'read' in ret and record.can_read is not None:
        ret['read']  = record.can_read  == 1
      if not 'write' in ret and record.can_write is not None:
        ret['write'] = record.can_write == 1
      if not 'clone' in ret and record.can_clone is not None:
        ret['clone'] = record.can_clone == 1
      if not 'run' in ret and record.can_run is not None:
        ret['run'] = record.can_run == 1

    # The default permissions are to make the object inaccessible
    if not 'read' in ret:
      ret['read']  = True
    if not 'write' in ret:
      ret['write'] = False
    if not 'clone' in ret:
      ret['clone'] = True
    if not 'run' in ret:
      ret['run'] = True

    return ret

  def update(self, id, person_id=None, canRead=0, canWrite=0, canClone=0, canRun=0):
    """ Updates the access control record for this object.

    canRead, canWrite, etc can be set to True or False to apply those to the
    access control record. They can also be set to None to clear them from the
    record. When options are cleared, they inherit their value in terms of
    precedence (see retrieve())
    """

    self.updateAccessControl(id = id, person_id = person_id, children = False,
                             canRead = canRead, canWrite = canWrite, canClone = canClone, canRun = canRun)

  def updateChildren(self, id, person_id=None, canRead=0, canWrite=0, canClone=0, canRun=0):
    """ Updates the access control for children of the object.

    See update() for a description of the arguments and possible values.
    """

    self.updateAccessControl(id = id, person_id = person_id, children = True,
                             canRead = canRead, canWrite = canWrite, canClone = canClone, canRun = canRun)

  def retrieveAllRecords(self, id=None, db_obj=None, person=None, person_obj=None, allPeople=False, addChildren=False):
    """ Retrieves the universe and children access control records for the given object and person.
    """

    person_id = None
    if person and hasattr(person, 'id'):
      person_id = person.id

    return self.datastore.retrieveAllRecords(id, db_obj, person_id, person_obj, allPeople, addChildren)

  def retrieveAccessControl(self, id=None, db_obj=None, person_id=None, person_obj=None):
    """ Retrieves the access control records for the given object.
    """

    return self.datastore.retrieveAccessControl(id, db_obj, person_id, person_obj)

  def updateAccessControl(self, id=None, db_obj=None,
                                person_id=None, person_obj=None,
                                children=False,
                                canRead=0, canWrite=0, canClone=0, canRun=0):
    """ Updates the access control record for the given object.
    """

    return self.datastore.updateAccessControl(id, db_obj, person_id, person_obj, children, canRead, canWrite, canClone, canRun)

  def migrate(self, oldId, newId):
    """ Moves permission records that were under a particular id to a new id.
    """

    self.datastore.migrateAccessControls(oldId, newId)

  def retrieveReviewLinks(self, obj, revision = None):
    """ Retrieves the list of RecordCapabilityRecord entries for the given object.
    """

    return self.datastore.retrieveReviewLinks(obj, revision)

  def retrieveReviewLink(self, obj):
    """ Retrieves the RecordCapabilityRecord or None for the given object.
    """

    return self.datastore.retrieveReviewLink(obj)

  def createReviewLink(self, obj):
    """ Creates a review link for the given object at its given revision.

    A review link will allow an anonymous access to that object by any
    Person on the system.

    Returns:
      The ReviewCapability record that already exists or was created.
    """

    return self.datastore.createReviewLink(obj)

  def removeReviewLink(self, obj):
    """ Deletes the given review link for this object.

    Returns:
      The ReviewCapabilityRecord that was deleted.
    """

    return self.datastore.removeReviewLink(obj)

  def trust(self, obj, person):
    """ Adds a trust association between the given object and person.

    Args:
      obj (Object): The object to apply the trust association.
      person (Person): The person to apply the trust association with (or None, for system-wide trust)

    Returns:
      TrustAssociationRecord: The new (or existing) trust association record.
    """

    return self.datastore.createTrustAssociation(obj, person)

  def untrust(self, obj, person):
    """ Removes an existing trust association between the given object and person.

    Args:
      obj (Object): The object to apply the trust association.
      person (Person): The person to apply the trust association with (or None, for system-wide trust)
    """

    return self.datastore.removeTrustAssociation(obj, person)

  def isTrusted(self, obj, person):
    """ Returns whether or not the object is currently trusted.
    """

    if not hasattr(person, 'id'):
      person = None

    return self.datastore.isTrusted(obj, person)
