# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.log import Log

from occam.manager import uses

from occam.accounts.manager    import AccountManager
from occam.permissions.manager import PermissionManager
from occam.objects.manager     import ObjectManager

from occam.commands.manager import command, option, argument

@command('permissions', 'set',
  category      = 'Access Management',
  documentation = "Updates or creates an access record to the given permissions")
@argument("object", type="object")
@argument("person", type="object", nargs="?")
@option("-i", "--item",        action  = "append",
                               dest    = "items",
                               nargs   = "+",
                               default = [],
                               help    = "the access type (read, write, clone, or run) and the value (true, false, and empty removes the key altogether)")
@option("-c", "--children",    action  = "store_true",
                               dest    = "children",
                               default = False,
                               help    = "when given, updates access for children of this object.")
@uses(ObjectManager)
@uses(AccountManager)
@uses(PermissionManager)
class AddCommand:
  def do(self):
    # Resolve the person
    person = None
    if self.options.person:
      person = self.objects.resolve(self.options.person, person = self.person)

      if person is None:
        Log.error("Person could not be found.")
        return -1

      personInfo = self.objects.infoFor(person)
      personName = personInfo.get('name', 'unknown')

    # Resolve the object
    obj = self.objects.resolve(self.options.object, person = self.person)
    if obj is None:
      Log.error("The object could not be found.")
      return -1

    objInfo = self.objects.infoFor(obj)
    objName = objInfo.get('name', 'unknown')

    # Update permissions
    perms = {}
    for item in self.options.items:
      key = item[0]
      value = None
      if len(item) > 1:
        if item[1] != "true" and item[1] != "false":
          Log.error("The given value '%s' is not 'true' or 'false'" % (item[1]))
          return -1

        value = item[1] == "true"

      if not key in ["read", "write", "clone", "run"]:
        Log.error("The given key '%s' is not found" % (key))
        return -1

      perms["can" + key.title()] = value

    person_id = None
    if person:
      person_id = person.id

    if self.person and ('administrator' in self.person.roles or self.permissions.can('write', obj.id, person = self.person)):
      record = self.permissions.updateAccessControl(id        = obj.id,
                                                    person_id = person_id,
                                                    children  = self.options.children, **perms)

      # Output the record
      item = {}
      for key, value in record._data.items():
        if key.startswith("can_"):
          key = key[4:]
          if value is None:
            item[key] = None
          else:
            item[key] = value == 1

      if record.person_object_id is not None:
        item['person'] = {
          'id': person.id,
          'name': personInfo.get('name', 'unnamed'),
          'revision': person.revision,
          'type': personInfo.get('type', 'person')
        }

      import json
      Log.output(json.dumps(item))
    else:
      Log.error("Access denied.")
      return -1

    return 0
