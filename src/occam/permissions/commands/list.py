# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.log import Log

from occam.manager import uses

from occam.permissions.manager import PermissionManager
from occam.objects.manager     import ObjectManager
from occam.resources.manager   import ResourceManager

from occam.commands.manager import command, option, argument

@command('permissions', 'list',
  category      = 'Access Management',
  documentation = "Lists the permissions available for the given object.")
@argument("object", type="object")
@uses(ObjectManager)
@uses(ResourceManager)   # For pulling resource objects
@uses(PermissionManager)
class AddCommand:
  def do(self):
    resource = False

    # Resolve the object
    obj = self.objects.resolve(self.options.object, person = self.person)
    if obj is None:
      # See if it is a Resource Object
      if self.options.object and self.options.object.id:
        # Check id
        obj = self.resources.infoFor(id=self.options.object.id)

        # Check uid
        if not obj:
          obj = self.resources.infoFor(uid=self.options.object.id)

        if obj:
          from types import SimpleNamespace
          obj = SimpleNamespace(id = obj["id"], uid = obj["uid"], revision = "", info = obj, name = obj["name"])
          resource = True

    if obj is None:
      Log.error("The object could not be found.")
      return -1

    if not resource:
      objInfo = self.objects.infoFor(obj)
      objName = objInfo.get('name', 'unknown')
    else:
      objInfo = obj.info
      objName = obj.name

    # Default output
    ret = {'object': [], 'children': [], 'reviewLinks': []}

    records = []

    # Only get the rows when the object is not anonymous
    if not hasattr(obj, 'anonymous') or not obj.anonymous:
      # Get permissions
      records = self.permissions.retrieveAllRecords(id = obj.id, allPeople=True, addChildren=True)

    for record, person in records:
      item = {}
      for key, value in record._data.items():
        if key.startswith("can_"):
          key = key[4:]
          if value is None:
            item[key] = None
          else:
            item[key] = value == 1

      if record.person_object_id is not None:
        item['person'] = {
          'id': person._data.get('id'),
          'name': person._data.get('name'),
          'revision': person._data.get('revision'),
          'type': person._data.get('object_type')
        }

        personInfo = self.objects.infoFor(self.objects.retrieve(id = item['person']['id']))
        item['person']['images'] = personInfo.get('images', [])

      if record.for_child_access == 1:
        ret['children'].append(item)
      else:
        ret['object'].append(item)

    # Get review links
    reviewLinks = []
    if not resource:
      reviewLinks = self.permissions.retrieveReviewLinks(obj)

    for reviewLink in reviewLinks:
      ret['reviewLinks'].append({
        'id': reviewLink.id,
        'published': reviewLink.published.isoformat(),
        'object': {
          'id': obj.uuid,
          'revision': reviewLink.revision
        }
      })

    import json
    Log.output(json.dumps(ret))

    return 0
