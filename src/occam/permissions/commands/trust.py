# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.log import Log

from occam.manager import uses

from occam.accounts.manager    import AccountManager
from occam.permissions.manager import PermissionManager
from occam.objects.manager     import ObjectManager

from occam.commands.manager import command, option, argument

@command('permissions', 'trust',
  category      = 'Access Management',
  documentation = "Creates a trust association")
@argument("object", type="object")
@option("-s", "--system-wide", action = "store_true",
                               help   = "will trust the object on the entire system (requires admin role)")
@uses(ObjectManager)
@uses(AccountManager)
@uses(PermissionManager)
class PermissionsTrustCommand:
  def do(self):
    person = self.person

    if self.options.system_wide:
      if not self.person:
        Log.error("Must be logged in")
        return -1

      if not 'administrator' in self.person.roles:
        Log.error("Must be an administrator")
        return -1

      person = None

    obj = self.objects.resolve(self.options.object, person = self.person)

    if obj is None:
      Log.error("Object could not be resolved")
      return -1

    self.permissions.trust(obj, person = person)

    return 0
