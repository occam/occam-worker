# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.log import Log

from occam.manager import uses

from occam.accounts.manager    import AccountManager
from occam.permissions.manager import PermissionManager
from occam.objects.manager     import ObjectManager

from occam.commands.manager import command, option, argument

@command('permissions', 'remove',
  category      = 'Access Management',
  documentation = "Removes an existing review link")
@argument("object", type="object")
@uses(ObjectManager)
@uses(AccountManager)
@uses(PermissionManager)
class AddCommand:
  def do(self):
    # Resolve the object
    obj = self.objects.resolve(self.options.object, person = self.person)
    if obj is None:
      Log.error("The object could not be found.")
      return -1

    objInfo = self.objects.infoFor(obj)
    objName = objInfo.get('name', 'unknown')

    # Remove a review link
    reviewLink = self.permissions.removeReviewLink(obj)

    # Return the review link information
    if reviewLink:
      import json
      ret = {
        'id': reviewLink.id,
        'published': reviewLink.published.isoformat(),
        'object': {
          'id': obj.uuid,
          'revision': obj.revision
        }
      }
      Log.output(json.dumps(ret))

    return 0
