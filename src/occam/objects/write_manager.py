# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.log     import loggable
from occam.manager import manager, uses
from occam.object  import Object

import os

from occam.objects.manager         import ObjectManager, ObjectError
from occam.nodes.manager           import NodeManager
from occam.notes.manager           import NoteManager
from occam.storage.manager         import StorageManager
from occam.resources.write_manager import ResourceWriteManager
from occam.backends.manager        import BackendManager
from occam.permissions.manager     import PermissionManager

@loggable
@manager("objects.write", reader=ObjectManager)
@uses(ObjectManager)
@uses(NodeManager)
@uses(NoteManager)
@uses(StorageManager)
@uses(BackendManager)
@uses(ResourceWriteManager)
@uses(PermissionManager)
class ObjectWriteManager:
  """ This manages the storing and updating of objects in the object store.
  """

  def store(self, obj, identity, public=True):
    """ Retains a copy of the object in occam object pool.
    
    Local objects are on disk and can be altered at will by anybody. These
    objects are the actual persisted artifacts.
    """

    info = self.objects.infoFor(obj)
    revision = obj.head()
    obj.revision = revision

    # Place it in: .occam/objects/{id[0:2]}/{id[2:4]}/{id[4:6]}/{id[6:8]}/full-code
    # The UUID is generated such that objects with the same type, name, and
    # root commit (clones of existing objects) are stored in the same commit
    # pool
    uid = self.objects.uidFor(obj)
    uids = [uid]
    self.storage.push(uid, revision, identity, obj.path)

    id = self.objects.idFor(obj, identity)
    ids = [id]

    obj.id  = id
    obj.uid = uid
    obj.identity = identity

    for subObject in info.get('includes', []):
      ids.append(self.objects.idFor(obj, identity, subObjectType = subObject.get('type', ''), subObjectName = subObject.get('name', '')))
      uids.append(self.objects.uidFor(obj, subObjectType = subObject.get('type', ''), subObjectName = subObject.get('name', '')))

    # Add Database Entry
    db_obj = self.updateDatabase(identity = identity, uids = uids, ids = ids, revision=revision, objectInfo=info)

    return db_obj

  def commit(self, obj, identity, message="OCCAM automated commit", filepath="-a"):
    """ Commits the current state of the Object on disk with the given commit
    message.
    """

    from occam.git_repository import GitRepository

    info = self.objects.infoFor(obj)
    revision = None
    if obj.path:
      revision = GitRepository(obj.path).commit(message, filepath)

    # Update this object's revision
    obj.revision = revision
    ret = [revision]

    oldId = obj.id
    obj.id = self.objects.idFor(obj, identity)

    if oldId is not None and oldId != obj.id:
      # We need to migrate permission entries
      # Possibly, this is the place to vacate object records
      # That is, to not leave a bunch of records to the same object when
      # the name/type changes
      self.permissions.migrate(oldId, obj.id)

    # Update the folks
    refs = self.updateParents(obj, identity)
    for ref in refs:
      ret.append(ref)

    return ret

  def clone(self, obj, identity, to=None, info=None, person=None):
    """ Creates an updated clone of the object.

    If no "path" is given, it will be performed in a temporary path. Otherwise,
    the object will be placed in that path.

    When "to" is specified, the object will be linked within the object given
    by "to" in the 'contains' category. The "belongsTo" will be set in the
    cloned object.

    The "info" parameter allows further modifications to be made to the object
    metadata of the new object.

    The "person" parameter gives the Person that is authorizing the clone. If
    this person does not have the current permission, the clone will fail.

    Returns: If the clone fails, this function will return None. Otherwise it
    returns an Object pointing to the newly created clone.
    """

    temporary = False

    person_id = None
    if person:
      person_id = person.id

    canClone = False

    if person and ('administrator' in person.roles):
      canClone = True

    if not canClone and not self.permissions.can("clone", id=self.objects.infoFor(obj).get('id'), person = person):
      # Not allowed to clone this object
      return None

    old_info = self.objects.infoFor(obj)

    tempToObject = None
    tempPath = None
    if to:
      # Create a temporary path for the "to" object
      root, tempToObject, tempPath = self.objects.temporaryClone(to, person = person)
      tempPath = os.path.join(tempPath, "%s-%s" % (Object.slugFor(old_info.get('type', 'object')), Object.slugFor(old_info.get('name', 'unknown'))))

      # Since this is temporary clone, the object will clean up itself
      # (although a linked copy may linger and be recloned under a different directory)
    else:
      # Create temporary path
      import tempfile
      tempPath = os.path.realpath(tempfile.mkdtemp(prefix="occam-"))
      temporary = True

      # Ensure it is an absolute path
      tempPath = os.path.realpath(tempPath)

    # Clone to this path
    new_obj = self.objects.clone(obj, path = tempPath)

    # Gather and update metadata
    new_info = self.objects.infoFor(new_obj)

    import datetime

    new_info['clonedFrom'] = {
      "type":     old_info['type'],
      "name":     old_info['name'],
      "id":       obj.id,
      "uid":      obj.uid,
      "revision": obj.revision,
      "published": datetime.datetime.now().isoformat()
    }

    # If this is being added to an object, update that metadata
    if to:
      to_info = self.objects.infoFor(to)

      new_info["belongsTo"] = {
        "id":   to.id,
        "uid":  to.uid,
        "name": to_info.get('name', 'unknown'),
        "type": to_info.get('type', 'object')
      }

    # Add other properties requested
    if info:
      new_info.update(info)

    # Update its identity
    new_obj.identity = identity

    # Store object
    self.updateObject(new_obj, identity, new_info, 'cloned from %s to %s' % (obj.id, new_obj.id))

    # Add this object to the requested parent
    if to:
      revisions, position = self.addDependency(tempToObject, object_type = new_info.get('type'),
                                                             name        = new_info.get('name'),
                                                             id          = new_obj.id,
                                                             revision    = new_obj.revision,
                                                             identity    = identity,
                                                             category    = 'contains')

      roots = (tempToObject.roots or [])[:]
      roots.append(tempToObject)
      new_obj = Object(id=new_obj.id, uid=new_obj.uid, path=new_obj.path, root=tempToObject.root, roots=roots, position=position)
    else:
      new_obj = Object(id=new_obj.id, uid=new_obj.uid, path=new_obj.path, root=new_obj.root, roots=new_obj.roots, position=new_obj.position)

    return new_obj

  def create(self, name, object_type, subtype = None, id = None, uid = None, info = None, belongsTo = None, root = None, path = None, source = None, createPath = True, identity = None):
    """ Creates a new object with the given properties.
    """

    import json

    # Make sure the incoming metadata is at least a dict
    if info is None:
      info = {}

    # Craft the object metadata
    object_json = info.copy()
    object_json.update({
      "name": name,
      "type": object_type,
    })

    # Add root field. The 'workset', for instance, is the root object for knowing
    # the access privilege of the object.
    if root:
      object_json['root'] = {
        "type": root.get('type'),
        "id":   root.get('id'),
        "uid":  root.get('uid'),
        "name": root.get('name')
      }

    # Add source
    if source:
      object_json['source'] = source

    # Add subtype
    if subtype:
      object_json['subtype'] = subtype

    # Add belongsTo field. This determines the object that contains this one.
    if belongsTo:
      object_json['belongsTo'] = {
        "type": belongsTo.get('type'),
        "id":   belongsTo.get('id'),
        "uid":  belongsTo.get('uid'),
        "name": belongsTo.get('name')
      }

    # Create directory, if it doesn't exist, warn if it does
    temporary = False
    if path:
      if os.path.exists(path):
        if createPath:
          ObjectWriteManager.Log.warning("directory %s already exists" % (path))
      else:
        ObjectWriteManager.Log.noisy("creating directory %s" % (path))
        os.mkdir(path)
    else:
      # Create in a temporary path
      import tempfile
      path = os.path.realpath(tempfile.mkdtemp(prefix="occam-"))
      temporary = True

    # Create object metadata
    object_json_path = os.path.join(path, "object.json")

    with open(object_json_path, "w") as f:
      f.write(json.dumps(object_json, indent=2, separators=(',', ': ')))

    # Create .gitignore
    with open("%s/.gitignore" % (path), 'w+') as f:
      pass

    # Create git repository in group path
    from occam.git_repository import GitRepository
    git = GitRepository.create(path)

    # Initial commit to get a revision
    git.add('.gitignore')
    git.add('*')
    git.commit("Created %s %s" % (object_type, name))

    # Pull revision
    revision = git.head()
    ObjectWriteManager.Log.noisy("Committed to repository as %s" % (revision))

    obj = Object(path=path, root=root, belongsTo=belongsTo, identity = identity, revision = revision)

    if uid is None:
      uid = self.objects.uidFor(obj)

    ret = Object(id = id, uid = uid, path = path, root = root, belongsTo = belongsTo, identity = identity, revision = revision)

    if temporary:
      ret.temporary = True

    return ret

  def renameFile(self, obj, identity, path, name, message = None):
    """ Renames the given file in the given object.
    """

    info = self.objects.infoFor(obj)

    # Rename this file in the git index
    if path.startswith("/"):
      path = path[1:]

    if obj.path:
      from occam.git_repository import GitRepository
      GitRepository(obj.path).move(path, os.path.join(os.path.dirname(path), name))

    # Commit this file
    if message is None:
      message = "Renaming file."
    new_revisions = self.commit(obj, identity, message)

    # Update normalized view
    obj.revision = new_revisions[0]

    uid = self.objects.uidFor(obj)
    uids = [uid]

    id = self.objects.idFor(obj, identity)
    ids = [id]

    for subObject in info.get('includes', []):
      ids.append(self.objects.idFor(obj, identity, subObjectType = subObject.get('type', ''), subObjectName = subObject.get('name', '')))
      uids.append(self.objects.uidFor(obj, subObjectType = subObject.get('type', ''), subObjectName = subObject.get('name', '')))

    # Add Database Entry
    self.updateDatabase(identity   = identity,
                        revision   = obj.revision,
                        objectInfo = info,
                        uids = uids,
                        ids = ids)

    return new_revisions

  def deleteFile(self, obj, identity, path, message = None):
    """ Deletes the given file.
    """

    info = self.objects.infoFor(obj)

    # Delete this file from the git index
    if path.startswith("/"):
      path = path[1:]

    if obj.path:
      from occam.git_repository import GitRepository
      GitRepository(obj.path).delete(path)

    # Commit this file
    if message is None:
      message = "Deleting file."
    new_revisions = self.commit(obj, identity, message)

    # Update normalized view
    obj.revision = new_revisions[0]

    uid = self.objects.uidFor(obj)
    uids = [uid]

    id = self.objects.idFor(obj, identity)
    ids = [id]

    for subObject in info.get('includes', []):
      ids.append(self.objects.idFor(obj, identity, subObjectType = subObject.get('type', ''), subObjectName = subObject.get('name', '')))
      uids.append(self.objects.uidFor(obj, subObjectType = subObject.get('type', ''), subObjectName = subObject.get('name', '')))

    # Add Database Entry
    self.updateDatabase(identity   = identity,
                        revision   = obj.revision,
                        objectInfo = info,
                        uids = uids,
                        ids = ids)

    return new_revisions

  def updateData(self, obj, identity, path, data, message):
    """ Rewrites the given file in the given object with the given data.
    """

    info = self.objects.infoFor(obj)

    if path.startswith('/'):
        path = path[1:]

    directory = os.path.join(obj.path, os.path.dirname(path))
    if not os.path.exists(directory):
      os.makedirs(directory)

    if path == "object.json":
      # Ensure that there is no invalid json
      import json
      json.loads(data.decode('utf-8'))

    with open(os.path.join(obj.path, path), 'wb+') as f:
      f.write(data)

    # Add this file to the git index
    if obj.path:
      from occam.git_repository import GitRepository
      GitRepository(obj.path).add(path)

    # Commit this file
    if message is None:
      message = "Updating data file."
    new_revisions = self.commit(obj, identity, message)

    # Update normalized view
    obj.revision = new_revisions[0]

    uid = self.objects.uidFor(obj)
    uids = [uid]

    id = self.objects.idFor(obj, identity)
    ids = [id]

    for subObject in info.get('includes', []):
      ids.append(self.objects.idFor(obj, identity, subObjectType = subObject.get('type', ''), subObjectName = subObject.get('name', '')))
      uids.append(self.objects.uidFor(obj, subObjectType = subObject.get('type', ''), subObjectName = subObject.get('name', '')))

    # Add Database Entry
    self.updateDatabase(identity   = identity,
                        revision   = obj.revision,
                        objectInfo = info,
                        uids = uids,
                        ids = ids)

    return new_revisions

  def updateObjectJSON(self, obj, identity, path, data, message):
    """ Rewrites the given JSON file in the given object with the given data.
    """

    # Write out the new object metadata
    import json

    info = self.objects.infoFor(obj)

    if path.startswith('/'):
        path = path[1:]
    with open(os.path.join(obj.path, path), 'w+') as f:
      f.write(json.dumps(data, indent=2, separators=(',', ': ')))

    # Add this file to the git index
    if obj.path:
      from occam.git_repository import GitRepository
      GitRepository(obj.path).add(path)

    # Commit this file
    if message is None:
      message = "Updating json file."
    new_revisions = self.commit(obj, identity, message)

    # Update normalized view
    obj.revision = new_revisions[0]

    uid = self.objects.uidFor(obj)
    uids = [uid]

    id = self.objects.idFor(obj, identity)
    ids = [id]

    for subObject in info.get('includes', []):
      ids.append(self.objects.idFor(obj, identity, subObjectType = subObject.get('type', ''), subObjectName = subObject.get('name', '')))
      uids.append(self.objects.uidFor(obj, subObjectType = subObject.get('type', ''), subObjectName = subObject.get('name', '')))

    # Add Database Entry
    self.updateDatabase(identity   = identity,
                        revision   = obj.revision,
                        objectInfo = info,
                        uids = uids,
                        ids = ids)

    return new_revisions

  def updateObject(self, obj, identity, info, message):
    """ Rewrites the object.json for the given object with the given data.

    This method will replace the object metadata with the given metadata. It
    will commit that change in the log with the given message. It will update
    the database normalization as well.
    """

    # Write out the new object metadata
    import json

    with open(os.path.join(obj.path, "object.json"), 'w+') as f:
      f.write(json.dumps(info, indent=2, separators=(',', ': ')))

    # Update our view of ourselves
    obj.info = info
    obj.ownerInfo = info

    obj.uid = self.objects.uidFor(obj, objectInfo=info)
    obj.id  = self.objects.idFor(obj, identity, objectInfo=info)

    new_revisions = self.commit(obj, identity, message, 'object.json')

    links = self.objects.links.retrieveLocalLinks(obj, path = obj.path, link_id = obj.link)
    ObjectWriteManager.Log.write("updating link? %s %s %s" % (obj.id, obj.path, obj.link))
    for link in links:
      # Update local link!
      ObjectWriteManager.Log.write("updating link!")
      self.objects.links.updateLocalLink(link, newRevision = new_revisions[0])

    # Update normalized view
    obj.revision = new_revisions[0]
    obj.infoRevision = new_revisions[0]
    obj.ownerInfoRevision = new_revisions[0]

    uid = self.objects.uidFor(obj)
    uids = [uid]

    id = self.objects.idFor(obj, identity)
    ids = [id]

    for subObject in info.get('includes', []):
      ids.append(self.objects.idFor(obj, identity, subObjectType = subObject.get('type', ''), subObjectName = subObject.get('name', '')))
      uids.append(self.objects.uidFor(obj, subObjectType = subObject.get('type', ''), subObjectName = subObject.get('name', '')))

    # Add Database Entry
    self.updateDatabase(identity   = identity,
                        revision   = obj.revision,
                        objectInfo = info,
                        uids = uids,
                        ids = ids)
    obj.info = info

    # Store
    self.store(obj, identity)

    return new_revisions

  def updateParents(self, obj, identity):
    info = self.objects.infoFor(obj)

    ret = []

    # Update parent
    parent = self.objects.parentFor(obj)
    if parent:
      parent_revision, _ = self.updateDependency(parent, identity, obj.id, info['type'], info['name'], obj.head(), position = obj.position, category="contains")
      self.store(parent, identity)
      for ref in parent_revision:
        ret.append(ref)

    return ret

  def updateDependency(self, obj, identity, id, object_type=None, name=None, revision=None, path=None, position=None, category='dependencies'):
    info = self.objects.infoFor(obj)
    info[category] = info.get(category, [])

    ret = None
    if position:
      ret = info[category][position]
    else:
      i = 0
      for dependency in info[category]:
        if dependency['id'] == id:
          position = i
          ret = dependency
          break
        i = i + 1

    if ret:
      ret['id'] = id
      if name:
        ret['name'] = name
      if revision:
        ret['revision'] = revision
      if object_type:
        ret['type'] = object_type
      if path:
        ret['local'] = True
        ret['path'] = path
      else:
        if 'local' in ret:
          del ret['local']

    return self.updateObject(obj, identity, info, 'updates dependency %s %s' % (object_type, name)), position

  def addDependency(self, obj, identity, id, object_type, name, revision, path=None, category='dependencies'):
    info = self.objects.infoFor(obj)
    info[category] = info.get(category, [])

    ret = {
      'name':     name,
      'type':     object_type,
      'id':       id,
      'revision': revision
    }

    if not path is None:
      ret['local'] = True
      ret['path'] = path

    position = len(info[category])
    info[category].append(ret)

    return self.updateObject(obj, identity, info, 'adds %s %s as a dependency' % (object_type, name)), position

  def purgeGenerates(self, obj, identity):
    info = self.objects.infoFor(obj)

    info['generates'] = []
    self.updateObject(obj, identity, info, 'Clearing any generated objects.')

    return True

  def addFileTo(self, obj, filename, data):
    """ Adds the given file data to the requested filename.
    """

    filepath = os.path.realpath(os.path.join(obj.path, filename))

    if isinstance(data, str):
      with open(filepath, "w+") as f:
        f.write(data)
    else:
      with open(filepath, "wb+") as f:
        f.write(data)

    from occam.git_repository import GitRepository
    git = GitRepository(obj.path)
    git.add(filename)

  def removeObjectFrom(self, obj, identity, subObject, revision = None):
    """ Removes the given subObject from the given object's "contains" section.

    Returns:
      (Object) The revised object.
    """

    info = self.objects.infoFor(obj)

    subIndex = self.objects.objectPositionWithin(obj, subObject, revision = revision)

    if subIndex is not None:
      del info['contains'][subIndex]
      self.updateObject(obj, identity, info, "Removed object.")

    return obj

  def addObjectTo(self, obj, identity, name=None, object_type=None, path=None, info=None, create=True, subObject = None):
    """ Creates a new object on disk.

    Returns:
      (Object) The created object.
    """

    if subObject:
      if not name:
        name = self.objects.infoFor(subObject).get('name')

      if not object_type:
        object_type = self.objects.infoFor(subObject).get('type')

    if name == "" or name is None:
      # Do not allow an empty name
      raise ObjectCreateError("name cannot be blank")

    if object_type == "" or object_type is None:
      # Do not allow an empty type
      raise ObjectCreateError("object type cannot be blank")

    # Create directory, if it doesn't exist, warn if it does
    from occam.object import Object
    slug = "%s-%s" % (Object.slugFor(object_type), Object.slugFor(name))

    if path is None:
      path = os.path.realpath(os.path.join(obj.path, slug))

    rootInfo = None
    if obj.root:
      import copy
      rootInfo = copy.copy(self.objects.infoFor(obj.root))
      rootInfo['id'] = obj.root.id
      rootInfo['uid'] = obj.root.uid
      rootInfo['revision'] = obj.root.revision

    if create:
      import copy
      belongsTo = copy.copy(self.objects.infoFor(obj))
      belongsTo['id']  = obj.id
      belongsTo['uid'] = obj.uid
      belongsTo['revision'] = obj.revision
      subObject = self.create(path=path, name=name, object_type=object_type, belongsTo=belongsTo, root=rootInfo, info=info)
      subObject.link = obj.link

      self.store(subObject, identity)

      # Update base object to ignore object in repository
      f = open('%s/.gitignore' % (obj.path), 'a+')
      f.write('%s\n' % (slug))
      f.close()

    revision = subObject.head()
    id = subObject.id
    position = None

    revisions, position = self.addDependency(obj,
                                             identity,
                                             object_type = object_type,
                                             name        = name,
                                             revision    = revision,
                                             id          = id,
                                             category    = 'contains')

    roots = (obj.roots or [])[:]
    roots.append(obj)
    return Object(id=subObject.id, uid=subObject.uid, identity=identity, path=subObject.path, root=self.objects.infoFor(obj), roots=roots, position = position, link=obj.link)

  def pullResources(self, obj, identity):
    """ Installs the listed linked resources for the given object to the given path.

    Installs the listed resources in the Object's "install" section to the
    given path or the local path of the Object if no path is given. Will
    install or discover resources as needed.
    """

    object_info = self.objects.infoFor(obj)

    save_object_info = False
    for section in [None, "build", "run"]:
      section_info = object_info
      if section is not None:
        section_info = object_info.get(section)

      if section_info is None:
        continue

      section_info['install'] = section_info.get('install', [])

      if not isinstance(section_info['install'], list):
        section_info['install'] = [section_info['install']]

      resources = self.resources.write.pullAll(section_info, identity, rootPath=obj.path)

      repositories = section_info['install']

      for resourceInfo, info in zip(resources, repositories):
        if resourceInfo.get('info') is None:
          continue

        id       = resourceInfo.get('info').get('id')
        uid      = resourceInfo.get('info').get('uid')
        revision = resourceInfo.get('info').get('revision')
        install  = resourceInfo.get('info').get('install')

        # Apply id/revision
        if info.get('id') != id or info.get('revision') != revision or info.get('install') != install:
          info['id']  = id
          info['uid'] = uid
          info['revision'] = revision
          if install:
            info['install'] = install
          save_object_info = True

      # New resources
      if len(resources) > len(repositories):
        ObjectManager.Log.write("Discovered new resources")
        for resourceInfo in resources[len(repositories):]:
          ObjectManager.Log.write("Found new resource: %s" % (resourceInfo.get('name')))
          repositories.append(resourceInfo)
          save_object_info = True

    if save_object_info:
      message = "Updated object's install repositories id/revisions."
      if len(object_info['install']) == 0:
        del object_info['install']
      self.updateObject(obj, identity, object_info, message)

    return resources

  def addAuthorTo(self, obj, identity, person):
    """ Add author to an object's metadata.

    This will not add an Authorship record. This is done by the AccountManager.
    """

    info = self.objects.infoFor(obj)

    id = person.id
    name = self.objects.infoFor(person).get('name', 'unknown')

    info['authors'] = (info.get('authors') or [])
    if not id in info['authors']:
      info['authors'].append(id)
    else:
      Log.warning("%s is already an author in %s" % (name, info.get('name')))
      return False

    # TODO: handle revisions?

    self.updateObject(obj, identity, info, "adds %s as author" % (name))

    return True

  def addCollaboratorTo(self, obj, identity, person):
    """ Add collaborator to an object.

    This will not add a Collaboratorship record. This is done by the AccountManager.
    """

    info = self.objectInfo()

    id = person.objectInfo()['id']
    name = person.objectInfo()['name']

    info['collaborators'] = (info.get('collaborators') or [])
    if not id in info['collaborators']:
      info['collaborators'].append(id)
    else:
      Log.warning("%s is already a collaborator in workset %s" % (name, info['name']))
      return False

    # TODO: handle revisions?

    self.updateObject(info, identity, "adds %s as collaborator" % (name))

    return True

  def delete(self, obj):
    """ Deletes the given object.
    """

    uid = self.objects.uidFor(obj)

    # Remove local content
    path = self.storage.repositoryPathFor(uid, revision=None, create=True) 
    ObjectManager.Log.write("Deleting content at %s" % (path))

    if os.path.exists(path):
      import shutil
      shutil.rmtree(path)

    return self.datastore.delete(obj)

  def updateDatabase(self, identity, revision, objectInfo, owner_db_obj=None, uids = None, ids = None):
    """ Updates or creates the ObjectRecord for the given object.

    Returns the ObjectRecord created or updated.
    """

    ret = self.datastore.updateDatabase(identity, revision, objectInfo, owner_db_obj, uids = uids, ids = ids)

    # Rake 'provides' and add Provider records:
    self.backends.store(ids[0], revision, objectInfo)

    return ret

  def rebuild(self, person = None):
    """ Rebuilds information about objects stored in the object store.

    This can be called when the database is damaged or lost to rebuild the
    local knowledge of the objects on the system.
    """

    # TODO: it needs to retain the identities used
    identity = None
    if person:
      identity = person.identity

    for uuid in self.notes.retrieveObjectList():
      # Determine if we know this object already
      obj = self.objects.retrieve(uuid = uuid, person = person)

      if obj is None:
        # Look for the object in storage
        repositoryPath = self.storage.repositoryPathFor(uuid, None)
        resourcePath   = self.storage.resourcePathFor(uuid, None)

        if resourcePath:
          ObjectWriteManager.Log.write("Storing resource %s" % (uuid))

          try:
            obj = Object(uuid = uuid, path = repositoryPath)
            info = self.objects.infoFor(obj)
          except:
            continue

          # Look for object record
          db_rows = self.objects.search(uuid=uuid)
          db_obj  = None
          if db_rows:
            db_obj = db_rows[0]

          # If no object record for this resource, create it
          if db_obj is None:
            sub_obj = self.createResourceObject(resourceType = info['type'],
                                                uuid         = uuid,
                                                name         = info.get('name', 'resource'),
                                                source       = info.get('source'))
            self.store(sub_obj, identity=identity)

            db_rows = self.objects.search(uuid=uuid)
            if len(db_rows) > 0:
              db_obj = db_rows[0]

          # Look for the resource record
          db_resource = self.resources.retrieveResource(resourceType = info.get('type'),
                                                        uuid         = uuid,
                                                        revision     = info.get('revision'),
                                                        source       = info.get('source'))

          if db_resource is None:
            self.resources.write.createResource(resourceType = info['type'],
                                                objectRecord = db_obj,
                                                revision     = info['revision'],
                                                source       = info.get('source'))


        elif repositoryPath:
          ObjectWriteManager.Log.write("Storing object %s" % (uuid))

          obj = Object(uuid = uuid, path = repositoryPath)
          info = self.objects.infoFor(obj)

          self.updateDatabase(uuid=uuid, revision=obj.revision, objectInfo=info)

class ObjectCreateError(ObjectError):
  """ Object creation error.
  """

  def __init__(self, message):
    """ Creates a new ObjectCreateError.
    """
    self.message = message

  def __str__(self):
    return self.message
