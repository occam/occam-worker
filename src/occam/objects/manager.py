# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import json

from occam.git_repository import GitRepository
from occam.semver         import Semver
from occam.log            import loggable

from occam.object     import Object
from occam.group      import Group
from occam.experiment import Experiment
from occam.workset    import Workset
from occam.person     import Person

from occam.manager import manager, uses

from occam.object_info import ObjectInfo

from occam.storage.manager     import StorageManager
from occam.notes.manager       import NoteManager
from occam.resources.manager   import ResourceManager
from occam.versions.manager    import VersionManager
from occam.caches.manager      import CacheManager
from occam.links.manager       import LinkManager
from occam.permissions.manager import PermissionManager

from occam.config import Config

@loggable
@manager("objects")
@uses(StorageManager)
@uses(NoteManager)
@uses(VersionManager)
@uses(ResourceManager)
@uses(CacheManager)
@uses(PermissionManager)
@uses(LinkManager)
class ObjectManager:
  """ This OCCAM manager handles the Object listing, and Object lookup.
  """

  def __init__(self):
    """ Initialize the object manager.
    """

    self.licenses = None

  def retrieveLocal(self, path, revision = None, subPath = None, roots=None, position=None, link=None, person=None):
    """ Retrieves the object and its workset from the given path.
    """

    if not os.path.exists(os.path.join(path, "object.json")):
      return None, None

    # Check for a reference to the intended identity
    identityPath = os.path.join(path, ".occam", "identity")
    identity = None
    if os.path.exists(identityPath):
      try:
        with open(identityPath, "r") as f:
          identity = f.read()
      except:
        pass

    if identity is None and person is not None:
      identity = person.identity

    obj = Object(path = path, roots = roots, file = subPath, revision = revision, identity=identity)
    obj.uid = self.uidFor(obj)
    obj.id = self.idFor(obj, identity=identity)
    obj.link = link
    obj.position = position

    workset = None

    objectType = self.infoFor(obj).get('type')

    if objectType == "person":
      obj = Person(path=path, identity = identity)

    if workset is None:
      parentPath = os.path.realpath(os.path.join(path, ".."))
      workset = self.localSearch(parentPath, "workset", person = person)
      obj.root = workset
      if obj.root:
        obj.root.link = link

    return obj, workset

  def localSearch(self, startingPath, searchType=None, maxDepth=3, person = None):
    """
    Retrieves the object by looking for it in the given path and failing to
    find it, look in the parent directory. If will recurse by the given
    maxDepth. If it cannot find an Object, it will return None.
    """

    if maxDepth < 1:
      return None

    # Look for an Object in the current path
    obj, workset = self.retrieveLocal(startingPath, person = person)
    if obj:
      if (searchType is None) or (self.infoFor(obj).get('type') == searchType):
        return obj

    parentPath = os.path.realpath(os.path.join(startingPath, ".."))
    return self.localSearch(parentPath, searchType, maxDepth=(maxDepth-1), person = person)

  def resolve(self, option, person = None, allowNone = False):
    """ Returns an Object based on a CLI object argument.
    """

    if option is None and not allowNone:
      return None

    if option is None or option.id == ".":
      # Look in the current directory
      revision = None
      if option:
        revision = option.revision
      subPath = None
      if option:
        subPath = option.path
      ret = self.retrieveLocal(".", subPath = subPath, revision = revision, person = person)[0]
      return ret

    id = option.id
    if option.id == "+":
      # Look at the current directory, but resolve globally
      tmpObject = self.retrieveLocal(".")[0]
      if person:
        id = self.idFor(tmpObject, identity = person.identity)
      else:
        id = self.uidFor(tmpObject)

    return self.retrieve(id             = id,
                         revision       = option.revision,
                         version        = option.version,
                         index          = option.index,
                         link           = option.link,
                         file           = option.path,
                         identity       = option.identity,
                         person         = person)

  def retrieve(self, id, object_type="object", revision=None, version=None, owner_id=None, index=[], file=None, person=None, roots=None, link = None, position=None, penalties=None, identity=None, object_options = None):
    """ Returns an Object representing the given stored OCCAM object.

    Returns an object instance representing the given stored OCCAM object. Will
    return a Group, Experiment, Workset, or generic Object respective of the
    object type.

    If a person_id is given, it will only retrieve the object if the object
    is accessible (readable) by that Person. Otherwise, it will return None
    even if the object is stored.
    """

    if object_options:
      revision = object_options.revision
      version  = object_options.version
      index    = object_options.index
      link     = object_options.link

    rows = self.datastore.retrieveObjects(id=id)

    db_obj = None
    if len(rows) > 0:
      db_obj = rows[0]

    if db_obj is None:
      return None

    if owner_id is None:
      owner_id = db_obj.owner_id or id

    # Determine if the given person has administrator (aka full) access
    administrator = False
    person_id   = None
    if person and hasattr(person, 'id'):
      administrator = 'administrator' in person.roles
      person_id     = person.id

    # Get the access control
    if not administrator:
      accessRecord = self.permissions.retrieve(id=owner_id, revision=revision, person=person)

      # Return None if the object is inaccessible
      if not accessRecord['read']:
        return None

    identity = db_obj.identity_uri

    if revision is None:
      revision = db_obj.revision

    uid = db_obj.owner_uid or db_obj.uid

    path = self.storage.repositoryPathFor(uid, revision=revision)
    localPath = path

    resourcePath = self.storage.resourcePathFor(owner_id)

    resource = None

    if resourcePath:
      # This object stores a resource
      resource = self.resources.retrievePath(owner_id, resourcePath, revision)

    if not path:
    #  path = self.pathFor(owner_id, subPath="notes")
    #  localPath = None
      return None

    ret = None
    if os.path.exists(path):
      if resource:
        ObjectManager.Log.noisy("Found resource object %s at %s" % (id, revision))
      else:
        ObjectManager.Log.noisy("Found object %s at %s as %s" % (id, revision, identity))
      ret = Object(path=localPath, id=id, uid = uid, revision=revision, file=file, resource = resource, roots = roots, link = link, position = position, identity=identity, owner_id=owner_id)

    # Object not found
    if ret is None:
      return None

    # Find a particular version
    resolvedVersion = None
    if not version is None:
      ObjectManager.Log.noisy("Looking for version %s of %s" % (version, owner_id))
      owner = self.ownerFor(ret, person = person)
      revision, resolvedVersion = self.versions.resolve(owner, version, penalties = penalties)

      if revision is None:
        # Cannot find the version requested
        return None

      ObjectManager.Log.noisy("Found revision %s" % (revision))

      ret = None
      if os.path.exists(path):
        if resource:
          ObjectManager.Log.noisy("Found resource object %s at %s" % (id, revision))
        else:
          ObjectManager.Log.noisy("Found object %s at %s as %s" % (id, revision, identity))
        ret = Object(path=localPath, id=id, uid = uid, revision=revision, file=file, resource = resource, roots = roots, link = link, position = position, identity=identity, version=resolvedVersion, owner_id=owner_id)

    if ret and index:
      # Traverse index
      info = self.infoFor(ret)
      if info is None:
        return None
      if index[0] >= 0 and index[0] < len(info.get('contains', [])):
        subInfo = info['contains'][index[0]]
        if roots is None:
          roots = []
        roots = roots[:]
        roots.append(ret)
        ret = self.retrieve(id = subInfo.get('id'), revision = subInfo.get('revision'), index = index[1:], person=person, identity=identity, roots = roots, link = link, position = index[0])
      else:
        ret = None

    if person and hasattr(person, 'anonymous') and person.anonymous == ret.id:
      ret.anonymous = True
      ret.info = None
      ret.ownerInfo = None
      ownerInfo = self.ownerInfoFor(ret)

    return ret

  def buildPathFor(self, obj, buildId):
    """
    """

    return self.storage.pathFor(obj.uid, revision = obj.revision, buildId = buildId)

  def instantiate(self, obj):
    """ This will create a repository in the local store that reflects the given
    revision.
    
    Obviously, this is just a cache of the information in the git
    repository path (<uuid>/repository). It places this in a directory based
    on the revision (<uuid>/<revision>).

    Returns the path to the instantiated object.
    """

    cachePath = self.storage.pathFor(obj.uid, obj.revision, create=False)

    if cachePath is None:
      cachePath = self.storage.pathFor(obj.uid, obj.revision, create=True)

      repositoryPath = self.storage.repositoryPathFor(obj.uid, obj.revision)

      # Clone the object at the requested revision
      git = GitRepository(repositoryPath)

      git = git.clone(cachePath, shared=True)
      git.reset(obj.revision, hard = True)

      # Destroy the git metadata path
      import shutil
      metadataPath = os.path.join(cachePath, ".git")
      shutil.rmtree(metadataPath)

      # Unpack dependencies
      self.install(obj, cachePath)

    return cachePath

  def pathFor(self, uuid, subPath='objects'):
    """ Returns the path the object of the given uuid would be stored.
    """

    code = uuid
    code1 = code[0:2]
    code2 = code[2:4]

    # Objects are in: .occam/objects/<type>/<code[0:2]>/<code[2:4]>/full-code
    object_path = self.configuration.get('paths', {}).get(subPath, os.path.join(Config.root(), subPath)) #self.occam.configuration()['paths'].get(subPath)
    if object_path is None:
      return None
    return os.path.realpath(os.path.join(object_path, code1, code2, uuid))

  def temporaryClone(self, obj, person=None):
    """ Clones the object to a temporary location where it can be updated.
    """

    # We are adding this to an object that already exists
    # We need to create a temporary object to house it

    if obj is None:
      return None, None, None

    if obj.root:
      root = obj.root
    else:
      root = obj

    rootObjectInfo = self.infoFor(root)
    destObjectInfo = self.infoFor(obj)

    dest_obj_name = destObjectInfo.get('name', 'unknown')
    dest_obj_type = destObjectInfo.get('type', 'object')

    # Look for a LocalLink, because this will already be the clone path
    link = obj.link

    if link:
      # The root is already at this path... confirm it
      linkRecords = self.links.retrieveLocalLinks(root, link_id = int(link))
      if len(linkRecords) == 0:
        link = None
      else:
        link = linkRecords[0]
        ObjectManager.Log.write("found link at %s" % (link.path))

    roots = []
    root.roots = roots

    clonedRoot = None

    # Pull workset and all objects at this revision
    if link:
      # Of course, this may be a link to a different revision
      # But that is OK when queuing updates
      localRoot, _ = self.retrieveLocal(link.path, roots=roots, link = obj.link, person=person)
      clonedRoot = localRoot

    if clonedRoot is None:
      # Clone root to a temporary path
      ObjectManager.Log.write("cloning a temporary copy")
      localRoot = self.clone(root, path=None)
      clonedRoot = localRoot

    root = clonedRoot
    baseRoots = obj.roots or []
    baseRoots = baseRoots[1:]
    if len(obj.roots or []) > 0:
      baseRoots.append(obj)
    obj = root

    # Clone all intermediate objects, when needed
    current = root
    clonePath = root.path
    for baseObject in baseRoots:
      info = self.infoFor(current)
      position = baseObject.position

      clonePath = os.path.join(clonePath, "dependency-%s" % (position))

      roots = roots[:]
      roots.append(current)

      if os.path.exists(clonePath):
        current, _ = self.retrieveLocal(clonePath, roots=roots, position = position, person = person)

        # Update current to the requested revision
        self.reset(current, baseObject.revision)
        continue

      if len(info.get('contains', [])) > position:
        dependency = info["contains"][position]
        current = self.retrieve(dependency.get('id'), revision = dependency.get('revision'), roots=roots, person = person, position=position)

        if current is None:
          break

        ObjectManager.Log.write("cloning %s to %s" % (dependency.get('id'), clonePath))
        current = self.clone(current, clonePath)

        if not current:
          break

    if not current:
      return None, None, None

    obj = current

    return root, obj, root.path

  def reset(self, obj, revision=None):
    """ Updates the given object that is at a local path to the given revision.

    This will update the object as it is seen in the filesystem to the given revision.
    """

    GitRepository(obj.path).reset(revision=revision, hard=True)
    obj.revision = revision

    return obj

  def clone(self, obj, path=None, increment=True):
    """ Clones the given object to the given path.

    Explicitly clones a copy of this object to the given path. If create
    is True, we assign a new UUID to this object and mark it as cloned.
    Otherwise, we just pull down a copy of the object. Changes will be
    merged into the object itself.

    If path is not given, it will be placed in a temporary path. You should
    delete that path when you are finished with the object.

    If person is given, then the clone will only happen when the given
    Person is allowed. Otherwise, the clone does not happen and None is
    returned.
    """

    temporary = False

    if path is None:
      # Create temporary path
      import tempfile
      path = os.path.realpath(tempfile.mkdtemp(prefix="occam-"))
      temporary = True
    else:
      # Ensure it is an absolute path
      path = os.path.realpath(path)

    revision = obj.revision

    if increment and os.path.exists(path):
      path = path + '-1'
      index = 1
      while os.path.exists(path):
        index += 1
        path = path[0:path.rindex('-')] + '-' + str(index)

    if os.path.exists(os.path.join(path, '.git')):
      return False

    ObjectManager.Log.noisy("cloning object to %s" % (path))

    if obj.path:
      GitRepository(obj.path).clone(path)
      git = GitRepository(path)
      if not revision is None:
        git.reset(revision)
    else:
      self.storage.clone(obj.id, revision, path=path)

    obj = Object(path=path, id=obj.id, uid=obj.uid, identity=obj.identity, revision=revision, roots=obj.roots, position = obj.position, link = obj.link)
    obj.temporary = temporary

    return obj

  def parentFor(self, obj, revision=None, person=None):
    # "roots" override the hierarchy
    if obj.roots is not None:
      if len(obj.roots) > 0:
        return obj.roots[-1]
      else:
        return None

    # Otherwise, we determine the parent by either the directory structure
    # or the "belongsTo" field
    objectInfo = self.infoFor(obj)
    if 'belongsTo' in objectInfo:
      belongsToUUID = objectInfo['belongsTo']['id']
      if os.path.exists(os.path.join(obj.path, '..', 'object.json')):
        parentObject = Object(path=os.path.join(obj.path, '..'), id=objectInfo['belongsTo']['id'], uid=objectInfo['belongsTo']['uid'], revision=revision, link = obj.link)
        if parentObject and person and self.idFor(parentObject, person.identity) == belongsToUUID:
          return parentObject

      db_rows = self.datastore.retrieveObjects(id=belongsToUUID)
      if db_rows:
        db_obj = db_rows[0]
      else:
        return None

      return self.retrieve(db_obj.uid, db_obj.object_type, revision=revision, link = obj.link, identity = obj.identity)
    else:
      if os.path.exists(os.path.join(obj.path, '..', 'object.json')):
        identity = obj.identity
        if person:
          identity = person.identity
        ret = Object(path=os.path.join(obj.path, '..'), link = obj.link, identity = identity)
        ret.uid = self.uidFor(obj)
        ret.id  = self.idFor(obj, identity=obj.identity)
        return ret
      return None

  def retrieveHistoryFor(self, obj):
    """ Returns a list of entries for each revision from the current revision.

    Generally, this gives you a timeline for the object.
    """

    if obj.anonymous:
      return []

    revision = obj.revision

    if obj.path:
      try:
        return GitRepository(obj.path, revision=revision).history()
      except IOError:
        pass

    return self.storage.retrieveHistory(obj.id, revision)

  def retrieveDirectoryWithResourcesFrom(self, resources, path, person):
    """ Yields a list of tuples regarding files or directories within resources of the given set of resource descriptions and subpath.

    The result is an array of tuples containing:
    (subObj: the resource Object, data: directory listing, internalPath: the path within the resource).

    When internalPath is None, this is not a file within the resource. Rather,
    it is the resource itself where it would be installed in the given Object.
    """

    ret = []

    for resource in resources:
      if 'id' not in resource and 'uid' not in resource:
        continue

      resourceInfo = self.resources.infoFor(uid=resource.get('uid'), id=resource.get('id'))

      if resourceInfo is None:
        ObjectManager.Log.error("Cannot find resource with id %s" % (resource.get('id')))
        return []

      resourceInfo['revision'] = resource.get('revision')

      # Read 'to' field to understand where the resource would be installed
      # And use that to find the relative path within the resource
      installPath = os.path.normpath(resource.get('to', 'package'))
      items = self.resources.retrieveRelativeDirectory(id           = resourceInfo.get('id'),
                                                       uid          = resourceInfo.get('uid'),
                                                       revision     = resourceInfo.get('revision'),
                                                       resourceType = resourceInfo.get('subtype'),
                                                       actions      = resource.get('actions', {}),
                                                       installPath  = resource.get('to', 'package'),
                                                       path         = path)

      ret.extend(items)

      if 'install' in resource:
        # Recursively handle sub-resources
        ret.extend(self.retrieveDirectoryWithResourcesFrom(resource['install'], path, person))

    return ret

  def retrieveDirectoryFrom(self, obj, path, includeResources=False, person=None, buildId = None):
    """ Pulls out the directory structure for the given path.
    """

    pathNotFound = False

    items = {"items": []}

    try:
      if buildId is None and obj.path:
        items = GitRepository(obj.path, revision=obj.revision).retrieveDirectory(path)
      else:
        items = self.storage.retrieveDirectory(obj.uid, obj.revision, path=path, buildId = buildId)
    except IOError:
      pathNotFound = True

    items['items'] = items.get('items', [])

    if includeResources:
      info = self.infoFor(obj)
      resources = info.get('install', []) + info.get("run", {}).get('install', [])
      subItems = self.retrieveDirectoryWithResourcesFrom(resources, path, person = person)
      for item in subItems:
        # TODO: Merge names that already exist?
        items['items'].extend(item[1].get('items', []))
        pathNotFound = False

    if pathNotFound:
      raise IOError("File not found")

    if 'items' in items:
      for item in items['items']:
        if not 'mime' in item:
          item['mime'] = self.storage.mimeTypeFor(item['name'])

    return items

  def retrieveFileStatFrom(self, obj, path, includeResources=False, person=None, fromObject=False, buildId = None):
    """ Pulls out a stream for retrieving the given file for this object.
    """

    owner_id = self.ownerFor(obj, person = person).id

    ret = None

    revision = obj.revision

    # We look for resources first because they override the contents of the object (usually?)
    if includeResources:
      info = self.infoFor(obj)
      resources = info.get('install', []) + info.get("run", {}).get('install', [])
      subItems = self.retrieveDirectoryWithResourcesFrom(resources, os.path.dirname(path), person = person)
      for item in subItems:
        resourceInfo = item[0]
        listing      = item[1]
        internalPath = item[2]

        for entry in listing.get('items', []):
          if entry["name"] == os.path.basename(path):
            if internalPath is not None:
              relativePath = os.path.join(internalPath, entry["name"])
              ret = self.resources.retrieveFileStatFrom(resourceInfo['id'], resourceInfo['uid'], resourceInfo['revision'], resourceInfo['subtype'], relativePath)
            else:
              # This is a resource itself
              ret = self.resources.retrieveFileStat(resourceInfo['id'], resourceInfo['uid'], resourceInfo['revision'], resourceInfo['subtype'])

    if ret is None and obj.path:
      try:
        ret = GitRepository(obj.path, revision=revision).retrieveFileStat(path)
      except IOError:
        pass

    if ret is None:
      if path == "/":
        ret = {"type": "tree", "name": "/", "size": 0}
      else:
        ret = self.storage.retrieveFileStat(owner_id, revision, path=path)

    ret["mime"] = self.storage.mimeTypeFor(path)

    return ret

  def retrieveFileFrom(self, obj, path, includeResources=False, person=None, fromObject=False, buildId = None, start=0, length=None):
    """ Pulls out a stream for retrieving the given file for this object.
    """

    if obj.resource and path and not fromObject:
      return self.resources.retrieveFileFrom(obj.id, obj.uid, obj.revision, obj.resource, path, start=start, length=length)

    revision = obj.revision

    if obj.resource and fromObject:
      revision = "HEAD"

    # We look for resources first because they override the contents of the object (usually?)
    # (unless we are looking for object.json... because that can't be inside a resource)
    # (also, it would recurse a lot, which means WE curse a lot... if you know what I mean)
    if path != "/object.json" and path != "object.json" and includeResources:
      info = self.infoFor(obj)
      resources = info.get('install', []) + info.get("run", {}).get('install', [])
      subItems = self.retrieveDirectoryWithResourcesFrom(resources, os.path.dirname(path), person = person)
      for item in subItems:
        resourceInfo = item[0]
        listing      = item[1]
        internalPath = item[2]

        for entry in listing.get('items', []):
          if entry["name"] == os.path.basename(path):
            if internalPath is not None:
              # This is within a resource
              relativePath = os.path.join(internalPath, entry["name"])
              return self.resources.retrieveFileFrom(resourceInfo['id'], resourceInfo['uid'], resourceInfo['revision'], resourceInfo['subtype'], relativePath, start=start, length=length)
            else:
              # This is a resource itself
              return self.resources.retrieveFile(resourceInfo['id'], resourceInfo['uid'], resourceInfo['revision'], resourceInfo['subtype'], start=start, length=length)

    if obj.path:
      try:
        return GitRepository(obj.path, revision=revision).retrieveFile(path, start=start, length=length)
      except IOError:
        pass

    return self.storage.retrieveFile(obj.uid, revision, path=path)

  def retrieveJSONFrom(self, obj, path, includeResources=False, person=None, fromObject=False):
    """ Pulls out a JSON document at the given path for this object.
    """

    import codecs
    import hashlib
    from collections import OrderedDict

    shouldCache = True

    if obj.anonymous and (path == "/object.json" or path == "object.json"):
      # Do not cache or pull out an object json for anonymized objects
      shouldCache = False

    if shouldCache:
      m = hashlib.sha1()
      m.update(os.path.join(obj.path, path).encode('utf8'));

      indexes = "%s-%s" % (len(obj.roots or []), obj.position or 0)

      cacheTag = "%s-%s-%s-%s-%s-%s" % (obj.id, obj.uid, obj.revision, indexes, m.hexdigest(), includeResources)
      data = self.cache.get(cacheTag)

      if not data is None:
        return json.loads(codecs.decode(data, 'utf8'), object_pairs_hook=OrderedDict)

    data = self.retrieveFileFrom(obj, path, includeResources, person, fromObject)
    data = json.loads(codecs.decode(data, 'utf8'), object_pairs_hook=OrderedDict)

    if obj.anonymous and (path == "/object.json" or path == "object.json"):
      # Anonymize object metadata
      for key in ['authors', 'collaborators', 'publisher', 'published', 'organization', 'website']:
        if key in data:
          del data[key]

    # TODO: understand when information should be cached...
    #       we must always do access control
    if shouldCache:
      if not data is None:
        self.cache.set(cacheTag, json.dumps(data))
    return data

  def ownerFor(self, obj, person):
    """ Retrieves the Object that is the owner of the given object.
    """

    # Check for owner id
    owner_id = obj.owner_id
    if owner_id is None:
      return obj

    return self.retrieve(owner_id, revision = obj.revision, person = person)

  def ownerInfoFor(self, obj):
    """ Pull object info from the owner of the given object.

    Pull object info from the owner of the given object.
    If the object is unowned, then it will pull its own object info.

    Args:
      obj (Object): The resolved object to poll for its metadata.

    Returns:
      ObjectInfo: The ordered dictionary containing the metadata.
    """

    if obj.revision != obj.ownerInfoRevision:
      obj.ownerInfo = None

    if obj.ownerInfo is None:
      obj.ownerInfoRevision = obj.revision
      obj.info = None
      try:
        obj.ownerInfo = self.retrieveJSONFrom(obj, "object.json", fromObject=True, includeResources=False)
      except json.decoder.JSONDecodeError as e:
        raise ObjectJSONError(e)

      obj.ownerInfo = ObjectInfo(obj.ownerInfo)

      # Update included ids
      for included in obj.ownerInfo.get('includes', []):
        included['id']  = self.idFor(obj, obj.identity, subObjectType = included.get('type'), subObjectName = included.get('name'))
        included['uid'] = self.uidFor(obj, subObjectType = included.get('type'), subObjectName = included.get('name'))

    return obj.ownerInfo

  def objectPositionWithin(self, obj, subObject, revision = None):
    """ Returns the position of the given subObject within the given object.

    When revision is not given, this will return the first index for any
    revision of the object that exists within the list. Otherwise, it will only
    return for the exact revision.
    """

    info = self.infoFor(obj)
    for i,dependency in enumerate(info.get('contains', [])):
      if dependency.get('id') == subObject.id and (revision is None or dependency.get('revision') == revision):
        return i

    return None

  def licenseFor(self, obj, person = None):
    """ Pull out the license metadata.
    """

    # Our goal is to fill this with the license information
    ret = []

    # Determine the path, or explore the object
    info = self.infoFor(obj)
    if 'license' in info:
      if not isinstance(info['license'], list):
        info['license'] = [info['license']]

      for license in info['license']:
        if not isinstance(license, dict):
          license = {
            'name': license
          }

        if not 'text' in license:
          # Determine the license text
          if 'file' in license:
            try:
              data = self.retrieveFileFrom(obj, license['file'], includeResources = True, person = person)
              if hasattr(data, 'read'):
                data = data.read()
              license['text'] = data.decode('utf-8')
            except:
              pass
          else:
            # Determine default license text

            # Look for a LICENSE or COPYING file
            try:
              f = self.retrieveFileFrom(obj, "LICENSE", includeResources = True, person = person)
            except:
              f = None

            try:
              f = self.retrieveFileFrom(obj, "COPYING", includeResources = True, person = person)
            except:
              f = None

            if not f:
              # Look inside resources
              install = info.get('install', [])

              if 'build' in info:
                if 'install' in info['build']:
                  install.extend(info['build']['install'])

              for resourceInfo in install:
                subPath = "/"
                items = self.resources.retrieveDirectoryFrom(resourceInfo.get('id'), resourceInfo.get('uid'), resourceInfo.get('revision'), resourceInfo['subtype'], subPath)['items']

                unpackPath = resourceInfo.get('actions', {}).get('unpack')
                if unpackPath:
                  unpackPath = os.path.normpath(os.path.join("/", unpackPath))
                else:
                  unpackPath = "/"

                for item in items:
                  if item['name'] == "LICENSE" or item['name'] == "COPYING":
                    try:
                      f = self.resources.retrieveFileFrom(resourceInfo.get('id'), resourceInfo.get('uid'), resourceInfo.get('revision'), resourceInfo['subtype'], os.path.join(subPath, item['name']))
                      license['file'] = os.path.join(unpackPath, subPath, item['name'])
                      if license['file'].startswith("/"):
                        license['file'] = license['file'][1:]
                      break
                    except:
                      f = None
                  elif item['type'] == "tree":
                    subPath = "/" + item['name']
                    subitems = self.resources.retrieveDirectoryFrom(resourceInfo.get('id'), resourceInfo.get('uid'), resourceInfo.get('revision'), resourceInfo['subtype'], subPath)['items']
                    for subitem in subitems:
                      if subitem['name'] == "LICENSE" or subitem['name'] == "COPYING":
                        try:
                          f = self.resources.retrieveFileFrom(resourceInfo.get('id'), resourceInfo.get('uid'), resourceInfo.get('revision'), resourceInfo['subtype'], os.path.join(subPath, subitem['name']))
                          license['file'] = os.path.join(unpackPath, subPath, subitem['name'])
                          if license['file'].startswith("/"):
                            license['file'] = license['file'][1:]
                          break
                        except:
                          f = None

                    if f:
                      break

                if f:
                  break

            # If we found a license file, good... read it
            if f:
              # Resource handlers can return a bytestring OR a stream, unfortunately
              if hasattr(f, "read"):
                f = f.read()
              license['text'] = f.decode('utf-8')
            else:
              # Now, exhaust a name search
              if self.licenses is None:
                # Initialize license database
                import yaml
                try:
                  self.licenses = yaml.safe_load(open(os.path.join(os.path.dirname(__file__), "licenses", "index.yml")))
                except:
                  self.licenses = {}

              licensePath = os.path.join(os.path.dirname(__file__), "licenses")
              for filename, tags in self.licenses.items():
                for tag in tags:
                  if license.get('name', '').lower() == tag.lower():
                    with open(os.path.join(licensePath, filename), "r") as f:
                      license['text'] = f.read()

        if 'name' not in license and 'text' in license:
          # TODO: Determine the license name from analysing text
          license['name'] = "unknown"

        ret.append(license)

    return ret

  def infoFor(self, obj):
    """ Pull object info from revision
    """

    if obj is None:
      return None

    if obj.revision == obj.infoRevision and obj.info is not None:
      return obj.info

    obj.infoRevision = obj.revision

    obj.info = obj.info or self.ownerInfoFor(obj)

    if obj.info is None:
      # We don't know how to get the info
      return None

    checkId = self.idFor(obj, obj.identity, objectInfo = obj.ownerInfo)
    if obj.id and checkId != obj.id:
      info = obj.info
      obj.info = {}
      for derivative in info.get('includes', []):
        includedId = self.idFor(obj, obj.identity, subObjectType = derivative.get('type', ''), subObjectName = derivative.get('name', ''))
        if includedId == obj.id:
          new_info = info.copy()

          new_info['install'] = new_info.get('install', []).copy()
          new_info['install'].extend(new_info.get('build', {}).get('install', []))
          new_info['install'].extend(new_info.get('run', {}).get('install', []))

          if new_info['install'] == []:
            del new_info['install']

          if 'includes' in new_info:
            del new_info['includes']
          if 'subtype' in new_info:
            del new_info['subtype']
          if 'inputs' in new_info:
            del new_info['inputs']
          if 'outputs' in new_info:
            del new_info['outputs']
          if 'build' in new_info:
            del new_info['build']
          if 'run' in new_info:
            del new_info['run']
          if 'configurations' in new_info:
            del new_info['configurations']
          if 'include' in new_info:
            del new_info['include']

          new_info.update(derivative)
          new_info['owner'] = {
            'id':  self.idFor(obj, obj.identity, objectInfo = obj.ownerInfo),
            'uid': self.uidFor(obj, objectInfo = obj.ownerInfo),
          }
          obj.info = new_info
          break

    # Inject the selected 'file'
    if obj.file:
      path = []

      fn = os.path.basename(obj.file)
      curPath = obj.file

      if obj.file.startswith("/"):
        curPath = obj.file[1:]

      while fn and fn != "":
        path.append(fn)
        curPath = os.path.dirname(curPath)
        fn = os.path.basename(curPath)

      path.reverse()

      obj.info['file'] = "{{ paths.separator }}".join(path)

    return obj.info

  def install(self, obj, basePath, build = False):
    """ Installs resources from within the object to the given path.

    The final path for the resource is derived from the given base path and the
    ``to`` attribute in the resource info.
    """

    objectInfo = self.infoFor(obj)

    if objectInfo is None:
      return

    resources = objectInfo.get('install', [])

    if build and 'build' in objectInfo:
      resources.extend(objectInfo['build'].get('install', []))
    else:
      resources.extend(objectInfo.get('run', {}).get('install', []))

    resourceInfo = self.resources.installAll({"install": resources}, basePath)

    return resourceInfo

  def searchEnvironments(self, query):
    """ Returns a list with a string for each environment that matches the given query.
    """

    return self.datastore.retrieveEnvironments(search = query)

  def searchArchitectures(self, query):
    """ Returns a list with a string for each architectures that matches the given query.
    """

    return self.datastore.retrieveArchitectures(search = query)

  def searchTypes(self, query):
    """ Returns a list with a string for each type that matches the given query.
    """

    return self.datastore.retrieveObjectTypes(search = query)

  def fullSearch(self, id = None, uid = None, name = None, object_type=None, environment=None, architecture=None, provides=None, viewsType=None, viewsSubtype=None, excludeTypes = []):
    """ Returns a dict containing a search and aggregate information.
    """

    return self.datastore.fullSearch(id           = id,
                                     uid          = uid,
                                     name         = name,
                                     objectType   = object_type,
                                     environment  = environment,
                                     architecture = architecture,
                                     viewsType    = viewsType,
                                     viewsSubtype = viewsSubtype,
                                     excludeTypes = excludeTypes)

  def search(self, id = None, uid = None, name = None, object_type=None, environment=None, architecture=None, provides=None, viewsType=None, viewsSubtype=None, excludeTypes = []):
    """ Returns a list with an ObjectRecord for each object found for the given terms.
    """

    return self.datastore.retrieveObjects(id           = id,
                                          uid          = uid,
                                          name         = name,
                                          objectType   = object_type,
                                          environment  = environment,
                                          architecture = architecture,
                                          viewsType    = viewsType,
                                          viewsSubtype = viewsSubtype,
                                          excludeTypes = excludeTypes)

  def viewersFor(self, viewsType = None, viewsSubtype = None):
    """ Returns rows of objects that can view the given type and subtype.
    """

    return self.datastore.viewersFor(viewsType, viewsSubtype)

  def editorsFor(self, editsType = None, editsSubtype = None):
    """ Returns rows of objects that can view the given type and subtype.
    """

    return self.datastore.editorsFor(editsType, editsSubtype)

  def configuratorsFor(self, configures_id):
    """ Returns rows of objects that can configure the given object.
    """

    return self.datastore.configuratorsFor(configures_id)

  def originFor(self, obj):
    """ Returns a reference to the given object at its initial commit.
    """

    if obj.origin is None:
      revision = GitRepository(obj.path).initialCommit(revision = obj.revision)

      obj.origin = Object(path     = obj.path,
                          id       = obj.id,
                          uid      = obj.uid,
                          revision = revision,
                          roots    = obj.roots,
                          position = obj.position,
                          identity = obj.identity,
                          link     = obj.link)

    return obj.origin

  def uidTokenFor(self, obj, subObjectType = None, subObjectName = None, objectInfo=None):
    """ Returns the reference UID token for this Versioned Object.
    """

    # [subobjecttype, subobjectname], type, name, source (when source exists)
    # [subobjecttype, subobjectname], type, name, root, origin info (when source does not exist)

    if subObjectName and not subObjectType or \
       subObjectType and not subObjectName:
      raise ValueError("Need both subObjectType and subObjectName")

    if objectInfo is None:
      objectInfo = self.ownerInfoFor(obj)

    if objectInfo.get('source'):
      token = ("type:%s\nname:%s\nsource:%s" % (objectInfo.get('type', ''), objectInfo.get('name', ''), objectInfo.get('source'))).encode('utf-8')
    else:
      origin = self.originFor(obj)

      # Place data in the object's fileCache to speed up recursive token
      # generation
      if not "object.json" in origin.fileCache:
        try:
          origin.fileCache["object.json"] = self.retrieveFileFrom(origin, "object.json")
        except:
          # Origin commit does not have an object metadata file
          origin.fileCache["object.json"] = b""

      info = origin.fileCache["object.json"]

      token = ("type:%s\nname:%s\nroot:%s\ninfo:" % (objectInfo.get('type', ''), objectInfo.get('name', ''), origin.revision)).encode('utf-8') + info

    if subObjectName and subObjectType:
      token = ("subobjecttype:%s\nsubobjectname:%s\n" % (subObjectType, subObjectName)).encode('utf-8') + token

    # Prepend the token with the length of the token which may aid in preventing
    # any future append based collisions. Although, the ultimate strategy here
    # is to rehash and require two valid hashes (one 'vulnerable' hash and the
    # other a signed 'updated' hash)
    token = str(len(token)).encode('utf-8') + token

    return token

  def uidFor(self, obj, subObjectType = None, subObjectName = None, objectInfo=None):
    """ Returns the reference UID for this Versioned Object.
    """

    # Base58(SHA256([source], [subobjecttype, subobjectname], type, name, root, origin info))

    token = self.uidTokenFor(obj, subObjectType, subObjectName, objectInfo)

    # Create a URI by hashing the public key with a multihash
    from occam.storage.plugins.ipfs_vendor.multihash import multihash
    hashedBytes = multihash.encode(token, multihash.SHA2_256)

    from occam.storage.plugins.ipfs_vendor.base58 import b58encode
    uri = b58encode(bytes(hashedBytes))
    
    return uri

  def idTokenFor(self, obj, identity, subObjectType=None, subObjectName=None, objectInfo=None):
    """ Returns the identifier for the given object and identity.
    """

    # [cloned date], [cloned revision], uid, identity uri, [source], [subobjecttype, subobjectname], type, name, root, origin info

    if objectInfo is None:
      objectInfo = self.ownerInfoFor(obj)

    uidToken = self.uidTokenFor(obj, objectInfo=objectInfo, subObjectType=subObjectType, subObjectName=subObjectName)
    uid = self.uidFor(obj, objectInfo=objectInfo, subObjectType=subObjectType, subObjectName=subObjectName)

    token = ("uid:%s\nidentity:%s\n" % (uid, identity)).encode('utf-8') + uidToken

    if objectInfo.get('clonedFrom'):
      token = ("clonedfrompublished:%s\nclonedfromrevision:%s\n" % (objectInfo['clonedFrom'].get('published'), objectInfo['clonedFrom'].get('revision'))).encode('utf-8') + token

    return token

  def idFor(self, obj, identity, subObjectType = None, subObjectName = None, objectInfo=None):
    """ Returns the identifier for the given object and identity.
    """

    # Base58(SHA256(uid, identity uri, [source], [subobjecttype, subobjectname], type, name, root, origin info))

    token = self.idTokenFor(obj, identity, subObjectType, subObjectName, objectInfo)

    # Create a URI by hashing the public key with a multihash
    from occam.storage.plugins.ipfs_vendor.multihash import multihash
    hashedBytes = multihash.encode(token, multihash.SHA2_256)

    from occam.storage.plugins.ipfs_vendor.base58 import b58encode
    uri = b58encode(bytes(hashedBytes))
    
    return uri

class ObjectError(Exception):
  """ Base class for all object errors.
  """

class ObjectJSONError(ObjectError):
  """ JSON encoding error.
  """

  def __init__(self, jsonDecodeError):
    """ Creates a new ObjectJSONError.
    """
    self.message      = jsonDecodeError.msg
    self.lineNumber   = jsonDecodeError.lineno
    self.columnNumber = jsonDecodeError.colno
    self.position     = jsonDecodeError.pos
    self.filename     = jsonDecodeError.doc

    self.endOfFile = False
    if self.position == len(self.filename):
      self.endOfFile = True

    self.context = self.explanation()

    endOfFileMessage = ""
    if self.endOfFile:
      endOfFileMessage = " (end of file)"

    self.report = "line {}, col {}{}: {}".format(self.lineNumber, self.columnNumber, endOfFileMessage, self.message)

  def explanation(self):
    """ Returns a string pointing to the problem.
    """

    import re

    # Get 2 context lines
    self.filename = self.filename + " "
    lines = self.filename.split("\n")

    # Friendlier Error Detection(tm)
    redefineErrorAtCharacter = None
    if self.message == "Expecting ',' delimiter":
      if self.endOfFile:
        self.message = "Expecting a terminating brace '}' at the end of the document"
      else:
        # Look for the last non-whitespace
        redefineErrorAtCharacter = r'\S\s*$'
    elif self.message == "Expecting property name enclosed in double quotes" or self.message == "Expecting value":
      # Generally this is the presence of a terminating comma
      # Check for this by seeing { at position
      if self.filename[self.position] == "}":
        self.message = "Redundant comma found before a terminating brace"
        redefineErrorAtCharacter = ","
      elif self.filename[self.position] == "]":
        self.message = "Redundant comma found before the end of a list"
        redefineErrorAtCharacter = ","

    if redefineErrorAtCharacter:
      # Find the comma
      location = None
      newLineNumber = self.lineNumber
      newColNumber  = self.columnNumber
      try:
        location = re.search(redefineErrorAtCharacter, lines[self.lineNumber-1][:self.columnNumber-1]).end()
      except:
        newLineNumber -= 1

      if location is None:
        for line in reversed(lines[:self.lineNumber-1]):
          try:
            location = re.search(redefineErrorAtCharacter, line).end()
            break
          except:
            newLineNumber -= 1

      if location is not None:
        self.columnNumber = location + 1
        self.lineNumber = newLineNumber

    first = self.lineNumber - 3
    if first < 0:
      first = 0

    beginLines = lines[first:self.lineNumber-1]
    endLines   = lines[self.lineNumber:self.lineNumber+3]
    arrow      = "-" * (self.columnNumber) + "~^\n"
    if len(endLines) == 0 or (len(endLines) == 1 and endLines[0] == ""):
      arrow = arrow.rstrip()

    currentLine = lines[self.lineNumber-1]
    start  = currentLine[:self.columnNumber-1]
    if self.columnNumber == len(currentLine) + 1:
      middle = ""
      end = ""
    else:
      middle = currentLine[self.columnNumber-1]
      end    = currentLine[self.columnNumber:]

    from termcolor import colored

    return "  {}\n  {}{}{}\n{}  {}".format("\n  ".join(beginLines).rstrip(), start, colored(middle, 'red', attrs=['bold']), end, colored(arrow, 'white', attrs=['bold']), "\n  ".join(endLines).rstrip())

  def __str__(self):
    return self.report + "\n" + self.context
