# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sql
import sql.aggregate

from occam.log import loggable
from occam.databases.manager import uses, datastore

@loggable
@datastore("objects")
class ObjectDatabase:
  """ Manages the database interactions for objects and the main Object component.
  """

  def queryObjectRecordFor(self, obj, key=None):
    """ Returns the database query to lookup the given object.
    """

    objects = sql.Table("objects")
    if key:
      query = objects.select(objects.__getattr__(key))
    else:
      query = objects.select()
    query.where = (objects.id == obj.id)

    return query

  def retrieveObjectsForIds(self, subQuery):
    from occam.objects.records.object import ObjectRecord

    session = self.database.session()

    objects = sql.Table("objects")
    query = objects.select()
    query.where = (objects.id.in_(subQuery))

    self.database.execute(session, query)
    return [ObjectRecord(x) for x in self.database.many(session, size=100)]

  def queryObjects(self, id = None, identity=None, uid=None, name=None, objectType=None, environment=None, architecture=None, viewsType=None, viewsSubtype=None, excludeTypes=[]):
    objects = sql.Table('objects')
    query = objects.select()

    if not id is None:
      query.where = (objects.id == id)

    if not identity is None:
      if query.where:
        query.where = query.where & (objects.identity_uri == identity)
      else:
        query.where = (objects.identity_uri == identity)

    if not uid is None:
      if query.where:
        query.where = query.where & (objects.uid == uid)
      else:
        query.where = (objects.uid == uid)

    if not name is None:
      if query.where:
        query.where = query.where & (objects.name.like("%" + name + "%"))
      else:
        query.where = (objects.name.like("%" + name + "%"))
    if not objectType is None:
      if query.where:
        query.where = query.where & (objects.object_type == objectType)
      else:
        query.where = (objects.object_type == objectType)

    if environment:
      if query.where:
        query.where = query.where & (objects.environment == environment)
      else:
        query.where = (objects.environment == environment)

    if architecture:
      if query.where:
        query.where = query.where & (objects.architecture == architecture)
      else:
        query.where = (objects.architecture == architecture)

    for excludeType in excludeTypes:
      query.where = query.where & (objects.object_type != excludeType)

    return query

  def fullSearch(self, id=None, identity=None, uid=None, name=None, objectType=None, environment=None, architecture=None, viewsType=None, viewsSubtype=None, excludeTypes=[]):
    """ Finds objects and returns aggregate information about the result.
    """

    from occam.objects.records.object import ObjectRecord

    session = self.database.session()

    query = self.queryObjects(id, identity, uid, name, objectType, environment, architecture, viewsType, viewsSubtype, excludeTypes)

    # Gather the object and join on an aggregate
    # The 'with' query will hold the main query
    withQuery = sql.With()
    withQuery.query = query

    # We will get the rows and also the row count as another column

    # Get representative rows for each distinct type
    typesQuery = withQuery.select(getattr(withQuery, "*"),
                                  sql.As(withQuery.select(sql.aggregate.Count(withQuery.uid)),                         "count"),
                                  sql.As(withQuery.select(sql.aggregate.Count(withQuery.object_type,  distinct=True)), "num_types"),
                                  sql.As(withQuery.select(sql.aggregate.Count(withQuery.environment,  distinct=True)), "num_environments"),
                                  sql.As(withQuery.select(sql.aggregate.Count(withQuery.architecture, distinct=True)), "num_architectures"))
    typesQuery.group_by = withQuery.object_type

    # Get representative rows for each distinct environment
    envsQuery = withQuery.select(getattr(withQuery, "*"),
                                 sql.As(sql.Literal(0), "count"),
                                 sql.As(sql.Literal(0), "num_types"),
                                 sql.As(sql.Literal(0), "num_environments"),
                                 sql.As(sql.Literal(0), "num_architectures"))
    envsQuery.where = withQuery.environment != sql.Null
    envsQuery.group_by = withQuery.environment

    # Get representative rows for each distinct architecture
    archQuery = withQuery.select(getattr(withQuery, "*"),
                                 sql.As(sql.Literal(0), "count"),
                                 sql.As(sql.Literal(0), "num_types"),
                                 sql.As(sql.Literal(0), "num_environments"),
                                 sql.As(sql.Literal(0), "num_architectures"))
    archQuery.where = withQuery.environment != sql.Null
    archQuery.group_by = withQuery.architecture

    # The true search
    allQuery = withQuery.select(getattr(withQuery, "*"),
                                sql.As(sql.Literal(0), "count"),
                                sql.As(sql.Literal(0), "num_types"),
                                sql.As(sql.Literal(0), "num_environments"),
                                sql.As(sql.Literal(0), "num_architectures"))

    # We will UNION ALL each table in the order:
    #  types, environments, architecture, and then all
    unionQuery = typesQuery | envsQuery
    unionQuery.all_ = True

    unionQuery = unionQuery | archQuery
    unionQuery.all_ = True

    unionQuery = unionQuery | allQuery
    unionQuery.all_ = True

    mainQuery = unionQuery.select()
    mainQuery.with_ = [withQuery]

    # We gather the rows
    self.database.execute(session, mainQuery)
    rows = self.database.many(session, size=1000)

    # And the first row will have our aggregate values
    # (presuming all returned objects have a stored type, which
    #  they should)
    count = 0
    if len(rows) > 0:
      count = rows[0]['count']

    typesCount = 0
    if len(rows) > 0:
      typesCount = rows[0]['num_types']

    environmentsCount = 0
    if len(rows) > 0:
      environmentsCount = rows[0]['num_environments']

    architecturesCount = 0
    if len(rows) > 0:
      architecturesCount = rows[0]['num_architectures']

    # gather types (the first set of rows)
    types = [row['object_type'] for row in rows[0:typesCount]]

    # gather environments (the next set)
    environments = [row['environment'] for row in rows[typesCount:typesCount+environmentsCount]]

    # gather architectures (and so on...)
    architectures = [row['architecture'] for row in rows[typesCount+environmentsCount:typesCount+environmentsCount+architecturesCount]]

    # throw away the types, etc rows to get our final search rows
    if typesCount > 0:
      rows = rows[typesCount+environmentsCount+architecturesCount:]

    # Represent the aggregate search and return
    return {
      "count":     count,
      "num_types": typesCount,
      "num_environments": environmentsCount,
      "num_architectures": architecturesCount,

      "types": types,
      "environments": environments,
      "architectures": architectures,

      "objects": [ObjectRecord(x) for x in rows],
    }

  def retrieveObjects(self, id=None, identity=None, uid=None, name=None, objectType=None, environment=None, architecture=None, viewsType=None, viewsSubtype=None, excludeTypes=[]):
    """ Finds a set of ObjectRecords pointing to any Objects for the given terms.
    """

    from occam.objects.records.object import ObjectRecord

    session = self.database.session()

    query = self.queryObjects(id           = id,
                              identity     = identity,
                              uid          = uid,
                              name         = name,
                              objectType   = objectType,
                              environment  = environment,
                              architecture = architecture,
                              viewsType    = viewsType,
                              viewsSubtype = viewsSubtype,
                              excludeTypes = excludeTypes)

    self.database.execute(session, query)
    return [ObjectRecord(x) for x in self.database.many(session, size=1000)]

  def retrieveObjectTypes(self, search=None):
    """ Returns a list of object types.
    """

    from occam.objects.records.object import ObjectRecord

    session = self.database.session()

    query = ("SELECT DISTINCT objects.object_type FROM objects", ())

    if search:
      search = "%" + search + "%"
      query = (query[0] + " WHERE objects.object_type LIKE ?", (search,))

    self.database.execute(session, query)

    ret = [x["object_type"] for x in self.database.many(session, size=500)]

    return ret

  def retrieveEnvironments(self, search=None):
    """ Returns a list of known environments.
    """

    from occam.objects.records.object import ObjectRecord

    session = self.database.session()

    query = ("SELECT DISTINCT objects.environment FROM objects", ())

    if search:
      search = "%" + search + "%"
      query = (query[0] + " WHERE objects.environment LIKE ?", (search,))

    self.database.execute(session, query)

    ret = [x["environment"] for x in self.database.many(session, size=500)]

    return ret

  def retrieveArchitectures(self, search=None):
    """ Returns a list of known architectures.
    """

    from occam.objects.records.object import ObjectRecord

    session = self.database.session()

    query = ("SELECT DISTINCT objects.architecture FROM objects", ())

    if search:
      search = "%" + search + "%"
      query = (query[0] + " WHERE objects.architecture LIKE ?", (search,))

    self.database.execute(session, query)

    ret = [x["architecture"] for x in self.database.many(session, size=500)]

    return ret

  def retrieveEditor(self, editsType = None, editsSubtype = None, db_obj = None):
    """ Returns the EditorRecord for the given type and subtype or the given object.

    If db_obj is given, then it will retrieve the EditorRecord that reflects the
    given object.
    """

    session = self.database.session()

    editors = sql.Table('editors')

    query = editors.select()
    if db_obj:
      query.where = (editors.internal_object_id == db_obj.id)
    else:
      query.where = True

    if editsType or editsSubtype:
      query.where = query.where & (editors.edits_type    == editsType)    \
                                & (editors.edits_subtype == editsSubtype) 

    self.database.execute(session, query)

    from occam.objects.records.editor import EditorRecord

    row = self.database.fetch(session)

    if row is not None:
      row = EditorRecord(row)

    return row

  def retrieveEditorsFor(self, editsType, editsSubtype = None):
    """ Returns rows of ObjectRecord that can edit the given type and subtype.
    """

    session = self.database.session()

    objects = sql.Table('objects')
    editors = sql.Table('editors')

    subquery = editors.select(editors.internal_object_id)
    subquery.where = (editors.edits_type    == editsType)    \
                   & (editors.edits_subtype == editsSubtype) 

    query = objects.select(where = objects.id.in_(subquery))

    self.database.execute(session, query)

    from occam.objects.records.object import ObjectRecord

    return [ObjectRecord(x) for x in self.database.many(session, size=10)]

  def retrieveViewer(self, viewsType = None, viewsSubtype = None, db_obj = None):
    """ Returns the ViewerRecord for the given type and subtype or the given object.

    If db_obj is given, then it will retrieve the ViewerRecord that reflects the
    given object.
    """

    session = self.database.session()

    viewers = sql.Table('viewers')

    query = viewers.select()
    if db_obj:
      query.where = (viewers.internal_object_id == db_obj.id)
    else:
      query.where = True

    if viewsType or viewsSubtype:
      query.where = query.where & (viewers.views_type    == viewsType)    \
                                & (viewers.views_subtype == viewsSubtype) 

    self.database.execute(session, query)

    from occam.objects.records.viewer import ViewerRecord

    row = self.database.fetch(session)

    if row is not None:
      row = ViewerRecord(row)

    return row

  def retrieveViewersFor(self, viewsType, viewsSubtype = None):
    """ Returns rows of ObjectRecord that can view the given type and subtype.
    """

    session = self.database.session()

    objects = sql.Table('objects')
    viewers = sql.Table('viewers')

    subquery = viewers.select(viewers.internal_object_id)
    subquery.where = (viewers.views_type    == viewsType)    \
                   & (viewers.views_subtype == viewsSubtype) 

    query = objects.select(where = objects.id.in_(subquery))

    self.database.execute(session, query)

    from occam.objects.records.object import ObjectRecord

    return [ObjectRecord(x) for x in self.database.many(session, size=10)]

  def configuratorsFor(self, configures_uid):
    """ Returns rows of objects that can configure the given object.
    """

    session = self.database.session()

    objects       = sql.Table('objects')
    configurators = sql.Table('configurators')

    subquery = configurators.select(configurators.internal_object_id)
    subquery.where = (configurators.configures_uid == configures_uid)

    query = objects.select(where = objects.id.in_(subquery))

    self.database.execute(session, query)

    from occam.objects.records.object import ObjectRecord

    return [ObjectRecord(x) for x in self.database.many(session, size=10)]

  def editorsFor(self, editsType = None, editsSubtype = None):
    """ Returns rows of objects that can view the given type and subtype.
    """

    session = self.database.session()

    objects = sql.Table('objects')
    editors = sql.Table('editors')

    subquery = editors.select(editors.internal_object_id)
    if editsSubtype is not None and editsType is None:
      subquery.where = (editors.edits_subtype == editsSubtype) 
    else:
      subquery.where = (editors.edits_type    == editsType)    \
                     & (editors.edits_subtype == editsSubtype) 

    query = objects.select(where = objects.id.in_(subquery))

    self.database.execute(session, query)

    from occam.objects.records.object import ObjectRecord

    return [ObjectRecord(x) for x in self.database.many(session, size=10)]

  def viewersFor(self, viewsType = None, viewsSubtype = None):
    """ Returns rows of objects that can view the given type and subtype.
    """

    session = self.database.session()

    objects = sql.Table('objects')
    viewers = sql.Table('viewers')

    subquery = viewers.select(viewers.internal_object_id)
    if viewsSubtype is not None and viewsType is None:
      subquery.where = (viewers.views_subtype == viewsSubtype) 
    else:
      subquery.where = (viewers.views_type    == viewsType)    \
                     & (viewers.views_subtype == viewsSubtype) 

    query = objects.select(where = objects.id.in_(subquery))

    self.database.execute(session, query)

    from occam.objects.records.object import ObjectRecord

    return [ObjectRecord(x) for x in self.database.many(session, size=10)]

  def configuratorsFor(self, configures_uid):
    """ Returns rows of objects that can configure the given object.
    """

    session = self.database.session()

    objects       = sql.Table('objects')
    configurators = sql.Table('configurators')

    subquery = configurators.select(configurators.internal_object_id)
    subquery.where = (configurators.configures_uid == configures_uid)

    query = objects.select(where = objects.id.in_(subquery))

    self.database.execute(session, query)

    from occam.objects.records.object import ObjectRecord

    return [ObjectRecord(x) for x in self.database.many(session, size=10)]

  def retrieveConfigurator(self, configures_uid, db_obj = None):
    """ Returns the ViewerRecord for the given type and subtype or the given object.

    If db_obj is given, then it will retrieve the ViewerRecord that reflects the
    given object.
    """

    import sql

    session = self.database.session()

    configurators = sql.Table('configurators')

    query = configurators.select()
    if db_obj:
      query.where = (configurators.internal_object_id == db_obj.id)
    else:
      query.where = (configurators.configures_uid == configures_uid)

    self.database.execute(session, query)

    from occam.objects.records.configurator import ConfiguratorRecord

    row = self.database.fetch(session)

    if row is not None:
      row = ConfiguratorRecord(row)

    return row

  def updateDatabase(self, identity, revision, objectInfo, owner_db_obj=None, uids = None, ids = None):
    """ Updates or creates the ObjectRecord for the given object.

    Returns the ObjectRecord created or updated.
    """

    session = self.database.session()

    rows = self.retrieveObjects(identity=identity, id=ids[0])

    if len(rows) > 0:
      ObjectDatabase.Log.noisy('Updating Object record')
      db_obj = rows[0]
    else:
      from occam.objects.records.object import ObjectRecord
      ObjectDatabase.Log.noisy('Creating Object record')

      if ids is None:
        raise Exception("ID required to create new object record")

      if uids is None:
        raise Exception("UID required to create a new object record")

      db_obj = ObjectRecord()
      db_obj.id = ids[0]
      db_obj.uid = uids[0]
      db_obj.identity_uri = identity

    # Update the normalization
    self._updateObjectNormalization(identity, objectInfo=objectInfo, db_obj=db_obj, owner_db_obj=owner_db_obj, revision=revision, uids=uids, ids=ids)

    self.database.update(session, db_obj)
    self.database.commit(session)

    # Rake 'views' and add Viewer records
    self._updateViewers(db_obj, objectInfo)

    # Rake 'edits' and add Editor records
    self._updateEditors(db_obj, objectInfo)

    # Rake 'configures' and add Configurator records
    self._updateConfigurators(db_obj, objectInfo)

    # Rake 'outputs' and add ObjectOutput records
    #self._updateObjectOutputs(obj, db_obj)

    # Rake 'inputs' and add ObjectInput records
    #self.updateObjectInputs(obj, db_obj)

    if objectInfo.get('type') == "person":
      self._updatePerson(db_obj, objectInfo)

    self.database.update(session, db_obj)
    self.database.commit(session)

    return db_obj

  def _updateObjectNormalization(self, identity, db_obj, owner_db_obj = None, objectInfo=None, revision=None, uids=None, ids=None):
    """ Updates the ObjectRecord to reflect the given object
    This method will look at the given object info and update the database
    record for this object to reflect that information. This includes the
    revision of the object.
    """

    ObjectDatabase.Log.write("Updating database record.")

    # Retrieve the object's database record (unless given)
    if db_obj is None:
      ObjectDatabase.Log.error("Cannot find the database record for this object.")
      return -1

    # Get object info (unless given)
    if objectInfo is None:
      ObjectDatabase.Log.error("Cannot find the object information for this object.")
      return -1

    # Handle object ownership
    if not owner_db_obj is None:
      db_obj.owner_id  = owner_db_obj.id
      db_obj.owner_uid = owner_db_obj.uid

    # Set the fields
    db_obj.name             = objectInfo.get('name', 'unknown')
    db_obj.object_type      = objectInfo.get('type', 'object')
    db_obj.organization     = objectInfo.get('organization')
    db_obj.revision         = revision or db_obj.revision;
    db_obj.source           = objectInfo.get('source')
    db_obj.resource         = objectInfo.get('resource')

    # Try to set the description, if it is text
    description = objectInfo.get('description', '')
    if not isinstance(description, str):
      description = ''
    db_obj.description = description

    subtype = objectInfo.get('subtype', [])
    if not isinstance(subtype, list):
      subtype = [subtype]
    db_obj.subtype = ";" + ";".join(subtype) + ";"

    # Pull out tags
    db_obj.tags = ";" + ";".join(objectInfo.get('tags', [])) + ";"

    # Pull out capabilities
    db_obj.tags = ";" + ";".join(objectInfo.get('capabilities', [])) + ";"

    # Pull out env/arch
    db_obj.environment  = objectInfo.get('environment')
    db_obj.architecture = objectInfo.get('architecture')

    # Detect a 'runnable' object
    db_obj.runnable = 'run' in objectInfo

    for i, derivative in enumerate(objectInfo.get('includes', [])):
      ObjectDatabase.Log.write("Storing included object %s %s" % (derivative.get('type', 'unknown'),
                                                   derivative.get('name', 'unknown')))

      # TODO: move object info inheritance logic to ObjectInfo
      new_info = objectInfo.copy()

      if 'includes' in new_info:
        del new_info['includes']
      if 'subtype' in new_info:
        del new_info['subtype']

      if 'inputs' in new_info:
        del new_info['inputs']
      if 'outputs' in new_info:
        del new_info['outputs']
      if 'install' in new_info:
        del new_info['install']
      if 'build' in new_info:
        del new_info['build']
      if 'run' in new_info:
        del new_info['run']
      if 'configurations' in new_info:
        del new_info['configurations']
      if 'contains' in new_info:
        del new_info['contains']

      new_info.update(derivative)
      derivative = new_info

      db_obj_sub = self.updateDatabase(identity, revision=revision, owner_db_obj = owner_db_obj or db_obj, objectInfo = derivative, uids=[uids[i+1]], ids=[ids[i+1]])

    return 0

  def _updateEditors(self, db_obj, objectInfo):
    """ This method will update or add Editor records for the given object.
    
    It will create one record for every object type listed in the given object's
    'edits' field in its metadata.
    """

    session = self.database.session()

    ObjectDatabase.Log.noisy("Checking for any updated Editors")

    # Store a token for any Viewer object
    if not 'edits' in objectInfo:
      return False

    edits = objectInfo.get('edits', [])
    if not isinstance(edits, list):
      edits = [edits]

    # For every object type this object can edit, make a new record as long as
    # a record doesn't already exist.
    for objectType in edits:
      if not isinstance(objectType, dict):
        objectType = {
          "type": objectType
        }

      # Query for an existing editor record
      db_editor = self.retrieveEditor(db_obj = db_obj, editsType = objectType.get('type'), editsSubtype = objectType.get('subtype'))

      if db_editor is None:
        from occam.objects.records.editor import EditorRecord

        # Creates a new Viewer record referring to this object
        ObjectDatabase.Log.write("Creating a Viewer record for %s" % objectType.get('type', objectType.get('subtype')))
        db_editor = EditorRecord()

      db_editor.edits_type      = objectType.get('type')
      db_editor.edits_subtype   = objectType.get('subtype')
      db_editor.internal_object_id = db_obj.id
      db_editor.revision        = db_obj.revision

      self.database.update(session, db_editor)
      self.database.commit(session)

  def _updateViewers(self, db_obj, objectInfo):
    """ This method will update or add Viewer records for the given object.
    
    It will create one record for every object type listed in the given object's
    'views' field in its metadata.
    """

    session = self.database.session()

    ObjectDatabase.Log.noisy("Checking for any updated Viewers")

    # Store a token for any Viewer object
    if not 'views' in objectInfo:
      return False

    views = objectInfo.get('views', [])
    if not isinstance(views, list):
      views = [views]

    # For every object type this object can view, make a new record as long as
    # a record doesn't already exist.
    for objectType in views:
      if not isinstance(objectType, dict):
        objectType = {
          "type": objectType
        }

      # Query for an existing viewer record
      db_viewer = self.retrieveViewer(db_obj = db_obj, viewsType = objectType.get('type'), viewsSubtype = objectType.get('subtype'))

      if db_viewer is None:
        from occam.objects.records.viewer import ViewerRecord

        # Creates a new Viewer record referring to this object
        ObjectDatabase.Log.write("Creating a Viewer record for %s" % objectType.get('type', objectType.get('subtype')))
        db_viewer = ViewerRecord()

      db_viewer.views_type      = objectType.get('type')
      db_viewer.views_subtype   = objectType.get('subtype')
      db_viewer.internal_object_id = db_obj.id
      db_viewer.revision        = db_obj.revision

      self.database.update(session, db_viewer)
      self.database.commit(session)

  def _updateConfigurators(self, db_obj, objectInfo):
    """ Adds or updates a Configurator object when the given object is one.

    Looks for the 'configures' key in the object info and based on that updates
    or creates a ConfiguratorRecord to signal this object as a widget that can
    be used to configure the given object.
    """

    ObjectDatabase.Log.noisy("Checking for any updated Configurators")

    # Store a token for any Viewer object
    if not 'configures' in objectInfo:
      return False

    configures = objectInfo.get('configures', [])
    if not isinstance(configures, list):
      configures = [configures]

    # For every object type this object can view, make a new record as long as
    # a record doesn't already exist.
    for configureInfo in configures:
      if not isinstance(configureInfo, dict):
        configureInfo = {
          "id": configureInfo
        }

      # Query for an existing viewer record
      db_configurator = self.retrieveConfigurator(db_obj = db_obj)

      if db_configurator is None:
        from occam.objects.records.configurator import ConfiguratorRecord

        # Creates a new Viewer record referring to this object
        ObjectDatabase.Log.write("Creating a Configurator record for %s" % objectType)
        db_configurator = ConfiguratorRecord()

      db_configurator.configures_uid = configureInfo.get('id')
      db_configurator.internal_object_id = db_obj.id
      db_configurator.revision        = db_obj.revision

      self.update(db_configurator)
      self.commit(session)

  def _updatePerson(self, db_obj, objectInfo):
    """ Creates the Person record with the given information for the given object record.
    """

    from occam.objects.records.person import PersonRecord

    import sql

    session = self.database.session()

    people = sql.Table('people')
    query = people.select()

    query.where = (people.id == db_obj.id)

    self.database.execute(session, query)
    record = self.database.fetch(session)

    if record is None:
      # Create new Person record
      db_person_object = PersonRecord()

      db_person_object.id = db_obj.id
      db_person_object.username = objectInfo.get('name')

      self.database.update(session, db_person_object)
      self.database.commit(session)

  def delete(self, obj):
    """ Deletes the given object.
    """
    uid = obj.uid

    # TODO: unmap in backends (add a delete method there and call it)

    # Remove object and all associated records in DB
    session = self.database.session()

    rows = self.retrieveObjects(uid=uid)

    if len(rows) > 0:
      ObjectDatabase.Log.write('Deleting Object record')
      db_obj = rows[0]
      self.database.delete(session, db_obj)
      self.database.commit(session)

    return True
