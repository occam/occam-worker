# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.databases.manager import table

@table("objects")
class ObjectRecord:
  schema = {

    # Primary Key

    "id": {
      "type": "string",
      "length": 256,
      "primary": True
    },

    # The originating identity
    "identity_uri": {
      "type": "string",
      "length": 256,
    },

    # Parent foreign key
    "parent_id": {
      "foreign": {
        "key":   "parent_id",
        "table": "objects"
      }
    },

    # Normalized metadata

    "name": {
      "type": "string",
      "length": 128
    },

    "uid": {
      "type": "string",
      "length": 256
    },

    "object_type": {
      "type": "string",
      "length": 128
    },

    "subtype": {
      "type": "string",
      "length": 128
    },

    "revision": {
      "type": "string",
      "length": 128
    },

    "organization": {
      "type": "string",
      "length": 128
    },

    "resource": {
      "type": "string",
      "length": 128
    },

    "source": {
      "type": "string",
      "length": 128
    },

    "description": {
      "type": "text"
    },

    "tags": {
      "type": "text",
      "default": ";;"
    },

    # Whether or not this is a runnable object
    "runnable": {
      "type": "boolean",
      "default": False
    },

    "owner_id": {
      "type": "string",
      "length": 256
    },

    "owner_uid": {
      "type": "string",
      "length": 256
    },

    "published": {
      "type": "datetime",
    },

    "updated": {
      "type": "datetime",
    },

    # Whether or not the object's "latest" revision has been verifed

    "verified": {
      "type": "boolean",
    },

    # Various Normalized Env/Arch Runtime Requirements

    "environment": {
      "type": "string",
      "length": 128
    },

    "architecture": {
      "type": "string",
      "length": 128
    },

    "capabilities": {
      "type": "text",
      "default": ";;"
    }
  }
