# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.commands.manager import command, option, argument

from occam.object import Object
from occam.log    import Log

from occam.manager import uses

from occam.objects.manager  import ObjectManager
from occam.discover.manager import DiscoverManager

@command('objects', 'history',
  category      = 'Object Inspection',
  documentation = "Displays the revision history for the object.")
@argument("object", type = "object", help = "The object ID to view the history of.")
@option("-j", "--json", dest   = "to_json",
                        action = "store_true",
                        help   = "returns result as a json document")
@uses(ObjectManager)
@uses(DiscoverManager)
class ObjectsHistoryCommand:
  def do(self):
    # Get the object to list
    obj = None
    data = None

    # Query for object by id
    obj = self.objects.resolve(self.options.object, person = self.person, allowNone = True)
    if obj is None:
      if self.options.object:
        data = self.discover.history(self.options.object, person = self.person)
      else:
        Log.error("cannot find object in the current path")
        return -1

      if data is None:
        Log.error("cannot find object with id %s" % (self.options.object.id))
        return -1

    if data is None:
      try:
        data = self.objects.retrieveHistoryFor(obj)
      except IOError:
        data = []

    import json
    Log.output(json.dumps(data))

    return 0
