# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os

from occam.object         import Object
from occam.object_info    import ObjectInfo

from occam.log import Log

from occam.manager import uses

from occam.network.manager       import NetworkManager
from occam.notes.manager         import NoteManager
from occam.nodes.manager         import NodeManager
from occam.backends.manager      import BackendManager
from occam.discover.manager      import DiscoverManager
from occam.objects.write_manager import ObjectWriteManager
from occam.objects.manager       import ObjectJSONError
from occam.permissions.manager   import PermissionManager
from occam.keys.write_manager    import KeyWriteManager, KeySignatureExistsError

from occam.commands.manager import command, option, argument

@command('objects', 'pull',
  category      = "Object Discovery",
  documentation = "Pulls an object from a specific URL or path.")
@argument("object", action = "store", type="object", default=".", nargs="?")
@option("-a", "--all",       action = "store_true",
                             dest   = "pull_all",
                             help   = "will pull each known tag")
@option("-t", "--task",      action = "store_true",
                             dest   = "pull_task",
                             help   = "will pull any necessary objects needed to run this object")
@uses(NetworkManager)
@uses(NoteManager)
@uses(NodeManager)
@uses(DiscoverManager)
@uses(BackendManager)
@uses(PermissionManager)
@uses(ObjectWriteManager)
@uses(KeyWriteManager)
class PullCommand:
  def pull(self, obj):
    # Pull any objects determined by other nodes to be necessary to build/run
    # this one
    revision = obj.revision

    if self.options.pull_task:
      # TODO: update for more than one backend
      docker_backend = self.backends.handlerFor('docker')
      backend = docker_backend.provides()[0]
      objInfo = obj.objectInfo();
      hypotheticalTask = self.nodes.taskFor(backend[0], backend[1],
                           objInfo.get('environment'),
                           objInfo.get('architecture'))
      objs = self.occam.taskObjectsFor(hypotheticalTask)
      for discoveredObj in objs:
        Log.write("Discovered object %s %s" % (discoveredObj.get('type'),
                                               discoveredObj.get('name')))

        # Pull this object
        self.nodes.pullAllObjects(discoveredObj.get('id'))

    # Store the object
    self.objects.write.store(obj, self.person.identity)

    # Discover the resources
    resources = self.objects.write.pullResources(obj, self.person.identity)

    # Sign the stored object
    obj.revision = revision
    try:
      id, signature, published = self.keys.write.signObject(obj, self.person.identity)
      self.keys.write.store(obj, signature, self.person.identity, id, published)
      Log.write("Signed as %s" % (self.person.identity))
    except KeySignatureExistsError as e:
      pass

    objectInfo = self.objects.infoFor(obj)
    self.permissions.update(id = obj.id, canRead=True, canWrite=False, canClone=True)

    self.discover.announce(obj.id, self.objects.idTokenFor(obj, self.person.identity))

    # Announce each viewer
    objectInfo['views'] = objectInfo.get('views', [])

    if not isinstance(objectInfo['views'], list):
      objectInfo['views'] = [objectInfo['views']]

    for viewer in objectInfo.get('views', []):
      self.discover.announceViewer(viewer.get('type'), viewer.get('subtype'))

    for editor in objectInfo.get('edits', []):
      self.discover.announceEditor(editor.get('type'), editor.get('subtype'))

    Log.done("discovered %s %s (%s)" % (objectInfo.get('type'), objectInfo.get('name'), obj.id))
    return 0

  def do(self):
    if (self.person is None or not hasattr(self.person, 'id')) and (self.options.object is None or self.options.object.id == "."):
      Log.error("Must be authenticated to store new objects")
      return -1

    Log.header("Pulling object")

    git_path = None

    # Grab the object from the command line argument
    try:
      obj = self.objects.resolve(self.options.object, person = self.person, allowNone = True)
    except ObjectJSONError as e:
      Log.error("Invalid object.json")
      Log.write(e.report)
      Log.output(e.context, padding="")
      return -1

    if obj is None:
      Log.error("Object not found")
      return -1

    if self.options.pull_all:
      # Pull each revision along with the normal one
      tagsPath = os.path.join(".occam", "tags")
      tags = {}
      import json
      try:
        with open(tagsPath, "r") as f:
          tags = json.load(f)
      except:
        pass

      for tag,revision in tags.items():
        Log.write("Pulling tag %s" % (tag))
        self.options.object.revision = revision
        try:
          taggedObj = self.objects.resolve(self.options.object, person = self.person, allowNone = True)
        except ObjectJSONError as e:
          Log.error("Invalid object.json")
          Log.write(e.report)
          Log.output(e.context, padding="")
          return -1

        self.pull(taggedObj)

        # Look at whether or not the tag is already stored
        revisions = self.notes.retrieveValues(id       = taggedObj.id,
                                              category = "versions",
                                              key      = tag,
                                              revision = None)

        if revisions:
          if taggedObj.revision not in revisions:
            Log.warning("Tag %s already exists." % (tag))
          else:
            Log.warning("The tag %s is already set to this revision." % (tag))
        else:
          # Tag the version in the note store
          self.notes.store(id       = taggedObj.id,
                           category = "versions",
                           key      = tag,
                           value    = revision,
                           revision = None)

    return self.pull(obj)
