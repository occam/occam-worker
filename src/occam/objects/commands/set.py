# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os

from occam.log        import Log
from occam.object     import Object
from occam.key_parser import KeyParser

from occam.manager import uses

from occam.commands.manager import command, option, argument

from occam.objects.write_manager import ObjectWriteManager

@command('objects', 'set',
  category      = 'Object Management',
  documentation = "Updates a metadata value or a file within a given object")
@argument("object", type = "object", nargs = '?', help = "the object to update. defaults to the object in the current path")
@argument("infile", nargs = '?')
@option("-t", "--input-type",  action  = "store",
                               dest    = "input_type",
                               default = "text",
                               help    = "determines what encoding the new value is. defaults to 'text'. 'json' for JSON encoded values.")
@option("-i", "--item",        action  = "append",
                               dest    = "items",
                               nargs   = "+",
                               help    = "the key/value pair to set. When there is no value, the key is deleted.")
@option("-r", "--rename",      action = "append",
                               dest   = "renames",
                               nargs  = 2,
                               help   = "the original key followed by the new key.")
@option("-m", "--message",     action = "store",
                               dest   = "message",
                               help   = "the commit message for this change")
@option("-j", "--json",        dest    = "to_json",
                               action  = "store_true",
                               help    = "returns result as a json document")

@uses(ObjectWriteManager)
class Set:
  def do(self):
    # Determine the object we are updating
    obj = None
    path = "object.json"
    data = {}
    stat = {}
    if self.options.object is None:
      # Use the object in the current directory if <object> argument is omitted
      obj = Object(path='.')
      objInfo = self.objects.infoFor(obj)
      data = objInfo
    else:
      obj = self.objects.resolve(self.options.object, person = self.person)

      # Handle when the object cannot be retrieved
      if obj is None:
        Log.error("Cannot find the given object.")
        return -1

      if not self.options.object.id == ".":
        root, obj, objpath = self.objects.temporaryClone(obj, person = self.person)

      if self.options.object.path:
        path = self.options.object.path

        # Stat the file
        try:
          stat = self.objects.retrieveFileStatFrom(obj, path, includeResources=True, person=self.person)
        except:
          pass

      data = {}
      if stat.get('type') != "tree":
        if self.options.infile is None or self.options.infile == "":
          try:
            data = self.objects.retrieveJSONFrom(obj, path)
          except IOError as e:
            data={}

    # Directory management
    if stat.get('type') == "tree":
      for name, *value in self.options.items or []:
        if not value:
          print("deleting file", name)
          self.objects.write.deleteFile(obj, os.path.join(path, name))
        else:
          # Create a symlink ( unsupported at the moment )
          value = value[0]
          print("attempt to set the key is invalid", name, value)
          raise ValueError("Cannot update the value of a key in a directory")

      for old, new in self.options.renames or []:
        print("renaming file", old, new)
        self.objects.write.renameFile(obj, os.path.join(path, old), new)

      # No need to continue to file management
      return 0

    dirty = False
    keys  = []

    for item in self.options.items or []:
      if self.options.input_type == "json":
        if len(item) == 1:
          Log.noisy("Clearing key %s" % item[0])
        else:
          Log.noisy("Setting key %s to json value" % item[0])

          import json
          try:
            item[1] = json.loads(item[1])
          except:
            Log.error("Could not decode the given json value.")
            return -1

      else:
        if len(item) == 1:
          Log.noisy("Clearing key %s" % item[0])
        else:
          Log.noisy("Setting key %s" % item[0])

      # Parse the key ("foo.1.bar" -> foo[1]['bar'], essentially)
      parser = KeyParser()
      try:
        oldValue = parser.get(data, item[0])
        if len(item) == 1:
          if oldValue:
            parser.delete(data, item[0])
            dirty = True
        else:
          if oldValue != item[1]:
            parser.set(data, item[0], item[1])

            # Keep track of the keys we actually set
            keys.append(item[0])

            dirty = True
      except:
        Log.error("Could not set the value because the key was not found.")
        return -1

    if self.options.infile:
      Log.write("Writing data from stdin")
      message = self.options.message
      self.objects.write.updateData(obj, self.person.identity, path, self.stdin.read(), message)
      self.objects.write.store(obj, self.person.identity)

    # Go through and rename keys
    if self.options.renames:
      for old, new in self.options.renames:
        parser = KeyParser()
        parent, indexKey = parser.getParent(data, old)
        if parent:
          parent[new] = parser.get(data, old)
          del parent[parser.path(old)[-1]]
          dirty = True

    if dirty:
      # Commit the new object info
      Log.noisy("Updating object.")
      message = self.options.message
      if message is None:
        message = "Sets key(s) %s" % (", ".join(keys))
      self.objects.write.updateObjectJSON(obj, self.person.identity, path, data, message)
      self.objects.write.store(obj, self.person.identity)

    ret = {}
    ret["updated"] = []

    for x in (obj.roots or []):
      ret["updated"].append({
        "id": x.id,
        "uid": x.uid,
        "revision": x.revision,
        "position": x.position,
      })

    ret["updated"].append({
      "id": obj.id,
      "uid": obj.uid,
      "revision": obj.revision,
      "position": obj.position,
    })

    if self.options.to_json:
      import json
      Log.output(json.dumps(ret))
    else:
      Log.output(str(ret))

    Log.done("Set keys successfully.")
    return 0
