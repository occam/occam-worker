# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# occam view dd44fcce-5274-11e5-b1d4-dc85debcef4e (defaults to object.json)
# occam view dd44fcce-5274-11e5-b1d4-dc85debcef4e/object.json (same as default)
# occam view dd44fcce-5274-11e5-b1d4-dc85debcef4e/configs/0/input.json
# occam view dd44fcce-5274-11e5-b1d4-dc85debcef4e@abc124/configs/0/input.json

import json
import os

from occam.commands.manager import command, option, argument

from occam.object import Object
from occam.log    import Log

from occam.key_parser import KeyParser

from occam.manager import uses

from occam.objects.manager  import ObjectManager
from occam.discover.manager import DiscoverManager

@command('objects', 'view',
  category      = 'Object Inspection',
  documentation = "Displays the metadata for the object.")
@argument("objects", type = "object", nargs = '*', help = "A list of object identifiers of the form <uuid>@<revision>/<path>. The default path is '/object.json'")
@option("-a", "--all",    dest    = "list_all",
                          action  = "store_true",
                          help    = "includes all files within resources as well and will redirect to linked resources")
@option("-k", "--key",    action  = "store",
                          dest    = "key",
                          help    = "the key whose value we'll pull from the object metadata or JSON file")
@option("-i", "--info",   action  = "store_true",
                          dest    = "retrieve_info",
                          help    = "when specified, will return the realized object info for the object, ignoring path and owner.")
@option("-s", "--start",  action  = "store",
                          dest    = "start",
                          type    = int,
                          default = 0,
                          help    = "the byte position to start reading from")
@option("-l", "--length", action  = "store",
                          dest    = "length",
                          type    = int,
                          help    = "the number of bytes to read")
@option("-c", "--compress", action = "store",
                            dest   = "compress",
                            help   = "compress the directory or file with the given type ('tgz', 'zip')")
@uses(ObjectManager)
@uses(DiscoverManager)
class View:
  def printObject(self, obj, path):
    fromObject = False
    if path == "":
      path = "/object.json"
      fromObject = True

    data = None
    if isinstance(obj, str) or hasattr(obj, "read"):
      data = obj

    if self.options.key:
      try:
        if data is None:
          if self.options.retrieve_info:
            data = self.objects.infoFor(obj)
          else:
            data = self.objects.retrieveJSONFrom(obj, path)
        else:
          import codecs
          reader = codecs.getreader('utf-8')
          data = json.load(reader(data))
      except ValueError:
        Log.error("Could not parse JSON data.")
        return -1
      except IOError:
        raise Exception("Could not locate data.")

      parser = KeyParser()
      try:
        value = parser.get(data, self.options.key)
      except:
        Log.error("Key could not be found in document")
        return -1

      value = json.dumps(value)
      Log.output(value, padding="")
    else:
      if data is None:
        try:
          if self.options.retrieve_info:
            data = json.dumps(self.objects.infoFor(obj))
            Log.output(data, padding="")
            return
          else:
            data = self.objects.retrieveFileFrom(obj, path, includeResources=self.options.list_all, person=self.person, fromObject=fromObject, start = self.options.start, length = self.options.length)
        except:
          raise Exception("Could not locate data.")

      if data is None or object is None:
        Log.error("No object found in the current directory.")
        return -1

      if self.options.compress == "zip":
        import zipstream
        z = zipstream.ZipFile()

        # This iterator can chunk the file contents
        # But we lose the file stat because the zipstream cannot
        # handle adding that information
        class ChunkIterator:
          def __init__(self, f):
            self.__f = f

          def __iter__(self):
            return self

          def __next__(self):
            if self.__f is None:
              raise StopIteration

            if isinstance(self.__f, bytes):
              ret = self.__f
              self.__f = None
            else:
              ret = self.__f.read(1024)

            if len(ret) == 0:
              raise StopIteration

            return ret

        class OccamFileIterator:
          def __init__(self, objects, obj, curPath, includeResources, person, fromObject):
            self.__obj = obj
            self.__curPath = curPath
            self.__includeResources = includeResources
            self.__person = person
            self.__fromObject = fromObject

            self.objects = objects

          def __iter__(self):
            return ChunkIterator(self.objects.retrieveFileFrom(self.__obj, self.__curPath, includeResources = self.__includeResources, person = self.__person, fromObject = self.__fromObject))

        stat = self.objects.retrieveFileStatFrom(obj, path, includeResources = self.options.list_all, person = self.person, fromObject = fromObject)
        if stat and stat["type"] == "tree":
          # Go through the directory and pull each file
          def packPath(z, path):
            # Get directory listing
            listing = self.objects.retrieveDirectoryFrom(obj, path, includeResources = self.options.list_all, person = self.person)
            for item in listing.get('items'):
              curPath = os.path.join(path, item.get('name'))
              if item.get('type') == 'tree':
                packPath(z, curPath)
              else:
                z.write_iter(curPath, OccamFileIterator(self.objects, obj, curPath, includeResources = self.options.list_all, person = self.person, fromObject = fromObject))

          packPath(z, path)
        else:
          z.write_iter(path, ChunkIterator(data))

        for zipdata in z:
          Log.pipe(zipdata)
      else:
        Log.pipe(data, length=self.options.length)

  def do(self):
    obj = None
    path = ""

    if self.options.objects is None:
      # Use the object in the current directory if <object> argument is omitted
      obj, _ = self.objects.retrieveLocal(".")

      self.printObject(obj, path)
      return 0
    else:
      for object in self.options.objects:
        # Query for object by id
        if object is None:
          Log.error("the object's given id is invalid")
          return -1

        obj = self.objects.resolve(object, person=self.person)
        if obj is None:
          # Discover
          obj = self.discover.retrieveFile(object, person = self.person)
          #Log.error("cannot find object with id %s" % (object.id))
          #return -1

        if object.path:
          path = object.path

        self.printObject(obj, path)
        return 0
