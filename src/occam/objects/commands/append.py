# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.log import Log
from occam.object import Object
from occam.key_parser import KeyParser

from occam.commands.manager import command, option, argument

from occam.objects.write_manager import ObjectWriteManager

from occam.manager import uses

@command('objects', 'append',
  category      = 'Object Management',
  documentation = "Appends a value to a list in a given object's metadata.")
@argument("object", type = "object", nargs = '?', help = "the object whose value should be set. defaults to the object in the current path")
@option("-i", "--item",        action  = "append",
                               dest    = "items",
                               nargs   = 2,
                               default = [],
                               help    = "the value to append to the list at the given key")
@option("-u", "--unique",     action = "store_true",
                              dest   = "forceUnique",
                              help   = "When given, it will only add the item if it doesn't already exist.")
@option("-a", "--at",         action = "store",
                              dest   = "at_index",
                              help   = "Specifies the index to add the new item to the array.")
@option("-t", "--input-type",  action  = "store",
                               dest    = "input_type",
                               default = "text",
                               help    = "determines what encoding the new value is. defaults to 'text'. 'json' for JSON encoded values.")
@option("-w", "--within",   action = "store",
                            type   = "object",
                            dest   = "within_object",
                            help   = "the id of the object to clone this object within")
@option("-m", "--message",  action = "store",
                            dest   = "message",
                            help   = "the commit message for this change")
@uses(ObjectWriteManager)
class Append:
  def do(self):
    # Determine the object we are updating
    obj = None
    path = "/object.json"

    obj = self.objects.resolve(self.options.object, person = self.person)

    objInfo = self.objects.infoFor(obj)
    dirty = False
    keys  = []

    for item in self.options.items:
      if self.options.input_type == "json":
        Log.noisy("Appending to key %s with json value" % item[0])

        import json
        try:
          item[1] = json.loads(item[1])
        except:
          Log.error("Could not decode the given json value.")
          return -1

      else:
        Log.noisy("Setting key %s" % item[0])

      # Parse the key ("foo.1.bar" -> foo[1]['bar'], essentially)
      parser = KeyParser()
      try:
        oldValue = parser.get(objInfo, item[0])
        if isinstance(oldValue, list):
          appendValue = False

          if self.options.forceUnique:
            if not item[1] in oldValue:
              appendValue = True
          else:
            appendValue = True

          if appendValue:
            oldValue.append(item[1])
            parser.set(objInfo, item[0], oldValue)

            # Keep track of the keys we actually set
            keys.append(item[0])

            dirty = True
        else:
          Log.error("Could not set the value because the item at the key was not a list.")

      except:
        Log.error("Could not set the value because the key was not found.")
        return -1

    if dirty:
      # Commit the new object info
      Log.noisy("Updating object.")
      message = self.options.message
      if message is None:
        message = "Appends at key(s) %s" % (", ".join(keys))
      self.objects.write.updateObject(obj, objInfo, message)

    return 0
