# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# occam objects license QmSNFXddXT6rxQGRsKdruPHg9R7zYpn2QYRjT7osyQZNPF

import json
import os

from occam.commands.manager import command, option, argument

from occam.object import Object
from occam.log    import Log

from occam.key_parser import KeyParser

from occam.manager import uses

from occam.objects.manager  import ObjectManager
from occam.discover.manager import DiscoverManager

@command('objects', 'license',
  category      = 'Object Inspection',
  documentation = "Displays the license information for the object.")
@argument("object", type = "object", help = "The requested object.")
@uses(ObjectManager)
@uses(DiscoverManager)
class ObjectsLicenseCommand:
  def do(self):
    obj = None

    if self.options.object is None:
      # Use the object in the current directory if <object> argument is omitted
      obj, _ = self.objects.retrieveLocal(".")
    else:
      object = self.options.object

      # Query for object by id
      if object is None:
        Log.error("the object's given id is invalid")
        return -1

      obj = self.objects.resolve(object, person=self.person)
      if obj is None:
        # TODO: Discover
        obj = self.discover.retrieveFile(object, person = self.person)

      if obj is None:
        Log.error("cannot find object with id %s" % (object.id))
        return -1

    ret = self.objects.licenseFor(obj)
    Log.output(json.dumps(ret))
    return 0
