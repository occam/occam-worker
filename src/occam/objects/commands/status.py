# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.log import Log

from types import SimpleNamespace

from occam.commands.manager import command, option, argument

from occam.objects.manager     import ObjectManager
from occam.versions.manager    import VersionManager
from occam.resources.manager   import ResourceManager
from occam.databases.manager   import DatabaseManager
from occam.permissions.manager import PermissionManager
from occam.accounts.manager    import AccountManager
from occam.discover.manager    import DiscoverManager

from occam.manager import uses

@command('objects', 'status',
  category      = 'Object Inspection',
  documentation = "Lists high-level information about the given object or current path.")
@argument("object", type = "object", nargs = '?')
@option("-a", "--all",  dest   = "list_all",
                        action = "store_true",
                        help   = "includes all files within resources as well and will redirect to linked resources")
@option("-j", "--json", dest   = "to_json",
                        action = "store_true",
                        help   = "returns result as a json document")
@uses(ObjectManager)     # For pulling the object
@uses(VersionManager)    # For pulling version information
@uses(ResourceManager)   # For pulling resource objects
@uses(DatabaseManager)   # For pulling out the normalized data
@uses(PermissionManager) # For pulling access control information
@uses(AccountManager)    # For determining authorship
@uses(DiscoverManager)
class StatusCommand:
  """ This class handles listing the current knowledge about an object or a file within an object.

  For local paths when developing an object, this will show you the differences
  between the stored object and the current state of the object.
  """

  def do(self):
    # Get the object to list
    obj = None
    resource_object = None
    path = "/"
    ret = None

    id = None

    # Query for object by id
    obj = self.objects.resolve(self.options.object, person = self.person, allowNone = True)
    if obj is None:
      # See if it is a Resource Object
      if self.options.object and self.options.object.id:
        # Check id
        resource_object = self.resources.infoFor(id=self.options.object.id)

        # Check uid
        if not resource_object:
          resource_object = self.resources.infoFor(uid=self.options.object.id)

      # Last ditch effort: ask the federation
      if resource_object is None and self.options.object:
        ret = self.discover.status(self.options.object, person = self.person)
      elif resource_object is None:
        Log.error("cannot find object in the current path")
        return -1

      if resource_object is None and ret is None:
        Log.error("cannot find object with id %s" % (self.options.object.id))
        return -1

    if ret is None and self.options.object and self.options.object.path:
      # Return file stat
      ret = self.objects.retrieveFileStatFrom(obj, path             = self.options.object.path,
                                                   includeResources = self.options.list_all,
                                                   person           = self.person)
    elif ret is None and resource_object:
      # This is a Resource Object, so ret is the basic description
      ret = resource_object

      id = resource_object["id"]
    elif obj and ret is None:
      # A Version Object

      id = obj.id

      # Get the database knowledge of the object
      db_rows = self.objects.search(id=obj.id)
      storedRevision = None
      identity = None

      db_obj = None
      if db_rows:
        db_obj = db_rows[0]
        storedRevision = db_obj.revision
        identity = db_obj.identity_uri

      if db_obj is None:
        raise Exception("Cannot find the object in the database for id %s" % (obj.id))

      # Get authors/collaborators
      authors = self.accounts.authorsFor(obj.id)
      collaborators = self.accounts.collaboratorsFor(obj.id)

      # Get included objects
      included = []

      # Generate the result
      ret = {
        "id": obj.id,
        "uid": obj.uid,
        "identity": identity,
        "revision": obj.revision,
        "currentRevision": storedRevision,
        "includes": included,
        "trusted": self.permissions.isTrusted(obj, self.person),
        "authors": [x._data for x in authors],
        "collaborators": [x._data for x in collaborators]
      }

      owner_obj = obj

      if obj.owner_id != obj.id:
        ret["owner"] = {
          "id": obj.owner_id
        }
        owner_obj = self.objects.ownerFor(obj, self.person)

      versions = self.versions.retrieve(owner_obj)
      ret["versions"] = dict([(version.tag, version.revision,) for version in versions])

      # If this is a Person, determine authorization credentials
      if db_obj.object_type == "person":
        db_account = self.accounts.retrieveAccount(person_id = db_obj.id)
        if db_account:
          ret["account"] = {
            "username": db_account.username,
            "roles":    (db_account.roles or ";;").split(';')[1:-1]
          }

          if db_account.email and db_account.email_public:
            ret["account"]["email"] = db_account.email

      # Handle index navigation (aka uuid@revision[1][2][0]
      #   will report "roots" with 3 objects, the root, the item at root[1]
      #   and the object at root[1][2])
      if obj.roots:
        ret["roots"] = []

        for subObject in obj.roots or []:
          subInfo = self.objects.infoFor(subObject)
          ret["roots"].append({
            "id": subObject.id,
            "uid": subObject.uid,
            "type": subInfo["type"],
            "name": subInfo["name"],
            "revision": subObject.revision})

    # Gather access permissions
    person_id = None
    if self.person and hasattr(self.person, 'id'):
      person_id = self.person.id

    mainAccess = self.permissions.retrieve(
        id     = id,
        person = self.person)

    records = self.permissions.retrieveAllRecords(
        id     = id,
        person = self.person)

    # Determine each type of access
    universeAccess = {}
    personAccess   = {}

    universeChildAccess = {}
    personChildAccess   = {}

    # Put into a more standard form:
    for record, person in records:
      item = {}
      if record.can_read is not None:
        item['read']  = record.can_read  == 1
      if record.can_write is not None:
        item['write'] = record.can_write == 1
      if record.can_clone is not None:
        item['clone'] = record.can_clone == 1
      if record.can_run is not None:
        item['run'] = record.can_run == 1

      if record.person_object_id is None and record.for_child_access == 1:
        universeChildAccess = item
      if record.person_object_id is None and record.for_child_access == 0:
        universeAccess = item
      if record.person_object_id is not None and record.for_child_access == 1:
        personChildAccess = item
      if record.person_object_id is not None and record.for_child_access == 0:
        personAccess = item

    ret.update({
      "access": {
        "current": mainAccess,
        "object": {
          "person":   personAccess,
          "universe": universeAccess
        },
        "children": {
          "person":   personChildAccess,
          "universe": universeChildAccess
        }
      }
    })

    # Gather read-only and review tokens
    if self.person and hasattr(self.person, 'id'):
      if obj is None and resource_object:
        obj = SimpleNamespace(id = resource_object['id'])
      ret.update({
        "tokens": {
          "readOnly": self.accounts.generateToken(self.person.account, [obj], "readOnly"),
          "anonymous": self.accounts.generateToken(self.person.account, [obj], "anonymous"),
        }
      })
    elif self.person and hasattr(self.person, 'anonymous') and self.person.anonymous:
      ret["anonymous"] = True
    elif self.person and hasattr(self.person, 'readonly') and self.person.readonly:
      ret["readOnly"] = True

    if self.options.to_json:
      import json
      Log.output(json.dumps(ret))
    else:
      Log.output(str(ret))

    return 0
