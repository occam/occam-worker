# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# occam run dd44fcce-5274-11e5-b1d4-dc85debcef4e
# occam run dd44fcce-5274-11e5-b1d4-dc85debcef4e@abc124
# occam run dd44fcce-5274-11e5-b1d4-dc85debcef4e --within dd44fcce-5274-11e5-b1d4-0123456789ab
# occam run dd44fcce-5274-11e5-b1d4-dc85debcef4e --within dd44fcce-5274-11e5-b1d4-0123456789ab@abc124
# occam run dd44fcce-5274-11e5-b1d4-dc85debcef4e@ffdabc --within dd44fcce-5274-11e5-b1d4-0123456789ab@abc124
# occam run dd44fcce-5274-11e5-b1d4-dc85debcef4e@ffdabc --within dd44fcce-5274-11e5-b1d4-0123456789ab@abc124

import json
import os

from occam.log               import Log
from occam.object            import Object
from occam.manager           import uses
from occam.object_info       import ObjectInfo

from occam.commands.manager import command, option, argument

from occam.objects.manager   import ObjectManager
from occam.notes.manager     import NoteManager
from occam.manifests.manager import ManifestManager, BuildRequiredError
from occam.jobs.manager      import JobManager

# TODO: add --stdout option to put the program's own stdout as our stdout
#       we default to the structured output as standard out.

@command('objects', 'run',
  category      = 'Running Objects',
  documentation = "Runs the given object.")
@argument("object", type = "object", nargs = "?")
@argument("arguments", nargs = '*')
@option("-b", "--build",       action = "store_true",
                               dest   = "build",
                               help   = "whether or not to build any object that requires it")
@option("-d", "--dispatch",    action = "store_true",
                               dest   = "dispatch",
                               help   = "whether or not to dispatch the jobs on the worker queue")
@option("-w", "--within",      action = "store",
                               type   = "object",
                               dest   = "within",
                               help   = "the workset that contains the experiment")
@option("-i", "--interactive", action = "store_true",
                               help   = "runs in interactive mode. no stdout or stderr are generated.",
                               dest   = "interactive")
@option("-n", "--input",       action = "store",
                               type   = "object",
                               dest   = "input",
                               help   = "the id and revision of an object to run as input")
@option("-a", "--as",          action = "store",
                               dest   = "author_uuid",
                               help   = "the id of the person that creates this object")
@option("-p", "--pull",        action = "store_true",
                               dest   = "pull_objects",
                               help   = "will pull any necessary objects needed to run this object")
@option("-e", "--environment", action = "store",
                               dest   = "environment",
                               help   = "the environment to target the VM")
@option("-u", "--architecture", action = "store",
                                dest   = "architecture",
                                help   = "the architecture to target the VM")
@option(      "--have-capability", action  = "append",
                                   dest    = "have_capabilities",
                                   default = [],
                                   help    = "capabilities that are fulfilled already by the backend")
@option("-y", "--need-capability", action  = "append",
                                   dest    = "need_capabilities",
                                   default = [],
                                   help    = "capabilities that need to be fulfilled by adding objects to the VM")
@option("-t", "--task-only",       action  = "store_true",
                                   dest    = "task_only",
                                   help    = "when used, a task will be generated and stored. The uuid and revision will be output.")
@option("-c", "--command",         action  = "store",
                                   help    = "overrides the given run command.")
@option("-r", "--recursive", action  = "store_true",
                             dest    = "recursive",
                             help    = "when specified, builds all necessary objects when needed.")
@uses(ManifestManager)
@uses(ObjectManager)
@uses(NoteManager)
@uses(JobManager)
class Run:
  """
  This class will handle running experiments and spawning experiment processes.

  The exit codes that Occam will generate on behalf of system errors are
  based on those described here: http://tldp.org/LDP/abs/html/exitcodes.html

  Namely, this returns an error code of 125 when the system cannot generate a
  task or run the task. Otherwise, it returns the exit code for the process
  requested (or the backend's own exit code upon error.)
  """

  def do(self, recursive=False, pull=True):
    cachedTask = None
    local = True
    path = "."
    obj = None

    if self.options.object:
      obj = self.objects.resolve(self.options.object, person=self.person)
      path = obj.path
      if obj is None:
        Log.error("Cannot determine what this object is.")
        return 125

      id = obj.id

    if obj is None:
      obj = Object(path = path)
      id = self.objects.tokenFor(obj)

    if path != os.path.realpath(os.curdir):
      local = False

    revision = obj.revision

    inputs = self.options.input
    if inputs:
      inputs = [[self.objects.resolve(inputs, person = self.person)]]

    while True:
      try:
        task = self.manifests.run(object      = obj,
                                  id          = id,
                                  revision    = revision,
                                  local       = local,
                                  inputs      = inputs,
                                  arguments   = self.options.arguments,
                                  person      = self.person)
        break
      except BuildRequiredError as e:
        if self.options.recursive:
          def build(object, local):
            while True:
              try:
                info = self.objects.infoFor(object)
                Log.write("Building %s %s" % (info.get('type', 'object'), info.get('name', 'unknown')))

                if 'build' not in info:
                  ownerInfo = self.objects.ownerInfoFor(object)
                  if 'build' in ownerInfo:
                    object = self.objects.ownerFor(object, person = self.person)

                task = self.manifests.build(object    = object,
                                            uuid      = object.uuid,
                                            revision  = object.revision,
                                            local     = local,
                                            #arguments = self.options.arguments,
                                            person    = self.person)
                if task is None:
                  Log.error("Build task could not be generated.")
                  return -1

                # Deploy the task as a job
                job = self.jobs.deploy(self.objects.infoFor(task), revision=task.revision, local=local, person = self.person)

                if job is None or job != 0:
                  Log.error("Build failed.")
                  return -1

              except BuildRequiredError as e:
                job = build(e.requiredObject, False)

          job = build(e.requiredObject, False)
        else:
          Log.error("Build required for dependency.")
          Log.write("You can try to build each required object as needed by invoking with --recursive.")

    if task is None:
      Log.error("Could not generate a task for %s" % (obj.id))
      return 125

    if self.options.task_only:
      import json

      task.id = self.objects.idFor(task, self.person.identity)

      ret = {
        "id":       task.id,
        "uid":      task.uid,
        "revision": task.revision
      }

      Log.output(json.dumps(ret))
      return 0

    opts = self.jobs.deploy(self.objects.infoFor(task),
                            revision  = task.revision,
                            interactive = self.options.interactive,
                            arguments = self.options.arguments,
                            command   = self.options.command,
                            person    = self.person,
                            local     = local)
    ret = self.jobs.execute(*opts, ignoreStdin = True)
    for runSection in ret['runReport']:
      opts = self.jobs.deploy(self.objects.infoFor(task),
                             revision   = task.revision,
                             interactive = self.options.interactive,
                             runSection = runSection,
                             arguments  = self.options.arguments,
                             uuid       = ret['id'],
                             person     = self.person,
                             local      = local)
      ret = self.jobs.execute(*opts, ignoreStdin = True)

    # Rake output
    Log.noisy("Raking Output")
    self.jobs.rakeOutput(task     = self.objects.infoFor(task),
                         taskPath = opts[1]['home'],
                         identity = self.person.identity)

    return ret['code']
