# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.log              import Log
from occam.manager          import uses

from occam.commands.manager import command, option

from occam.objects.manager  import ObjectManager

@command('objects', 'search',
  category      = "Object Discovery",
  documentation = "Look up object details only from our local knowledge.")
@option("-t", "--type",         action  = "store",
                                dest    = "object_type",
                                help    = "search for objects with the specified type")
@option("-s", "--subtype",      action  = "store",
                                dest    = "subtype",
                                help    = "search for objects with the specified subtype")
@option("-n", "--name",         action  = "store",
                                dest    = "name",
                                help    = "search for objects with the specified name")
@option("-i", "--id",           action  = "store",
                                dest    = "id",
                                help    = "search for the object with the specified id")
@option("-u", "--uid",          action  = "store",
                                dest    = "uid",
                                help    = "search for the object with the specified uid")
@option("-e", "--environment",  action  = "store",
                                dest    = "environment",
                                help    = "search for objects with the specified environment")
@option("-a", "--architecture", action  = "store",
                                dest    = "architecture",
                                help    = "search for objects with the specified architecture")
@option("--environments",       action  = "store",
                                dest    = "environments",
                                help    = "search for known environments")
@option("--architectures",      action  = "store",
                                dest    = "architectures",
                                help    = "search for known architectures")
@option("--types",              dest    = "return_types",
                                action  = "store",
                                type    = str,
                                default = None,
                                nargs   = "?",
                                help    = "search types matching the given string")
@option("-x", "--excludeType",  dest    = "exclude_types",
                                action  = "append",
                                type    = str,
                                help    = "filter objects by their type")
@option("-v", "--views",        action  = "store",
                                dest    = "views",
                                nargs   = "+",
                                help    = "search for objects that can view the specified type. a subtype may follow.")
@option("-p", "--provides",     action  = "store",
                                dest    = "provides",
                                nargs   = 2,
                                help    = "search of objects that provide the given environment and architecture")
@option("-f", "--full",         dest    = "full",
                                action  = "store_true",
                                help    = "returns full aggregate information about the search")
@option("-j", "--json",         dest    = "to_json",
                                action  = "store_true",
                                help    = "returns result as a json document")
@uses(ObjectManager)
class SearchCommand:
  def do(self):
    viewsType    = None
    viewsSubtype = None

    if self.options.views:
      viewsType = self.options.views[0]
      if len(self.options.views) == 2:
        viewsSubtype = self.options.views[1]

    if not self.options.return_types is None:
      Log.header("Listing Found Object Types")

      types = self.objects.searchTypes(query = self.options.return_types)

      if self.options.to_json:
        import json
        Log.output(json.dumps(types))
      else:
        for object_type in types:
          Log.output(object_type)
    elif not self.options.environments is None:
      Log.header("Listing Found Environments")

      rows = self.objects.searchEnvironments(query = self.options.environments)

      if self.options.to_json:
        import json
        Log.output(json.dumps(rows))
      else:
        for result in rows:
          Log.output(result)
    elif not self.options.architectures is None:
      Log.header("Listing Found Architectures")

      rows = self.objects.searchArchitectures(query = self.options.architectures)

      if self.options.to_json:
        import json
        Log.output(json.dumps(rows))
      else:
        for result in rows:
          Log.output(result)
    else:
      if self.options.name is None:
        self.options.name = ""

      data = self.objects.fullSearch(object_type  = self.options.object_type,
                                     name         = self.options.name,
                                     id           = self.options.id,
                                     uid          = self.options.uid,
                                     viewsType    = viewsType,
                                     viewsSubtype = viewsSubtype,
                                     provides     = self.options.provides,
                                     environment  = self.options.environment,
                                     architecture = self.options.architecture,
                                     excludeTypes = self.options.exclude_types or [])

      Log.header("Listing Found Objects")

      ret = data

      if self.options.to_json:
        ret['objects'] = [{"uid":          obj.uid,
                           "id":           obj.id,
                           "revision":     obj.revision,
                           "name":         obj.name,
                           "owner": {
                             "id": obj.owner_id or obj.id
                           },
                           "description":  obj.description,
                           "type":         obj.object_type,
                           "subtype":      obj.subtype,
                           "architecture": obj.architecture,
                           "organization": obj.organization,
                           "environment":  obj.environment,
                          } for obj in data['objects']
                         ]

        ret['numTypes'] = ret['num_types']
        del ret['num_types']
        ret['numEnvironments'] = ret['num_environments']
        del ret['num_environments']
        ret['numArchitectures'] = ret['num_architectures']
        del ret['num_architectures']

        import json
        if self.options.full:
          Log.output(json.dumps(ret))
        else:
          Log.output(json.dumps(ret['objects']))
      else:
        for obj in data['objects']:
          Log.output(obj.name)
          Log.output(obj.uid, padding="     uid: ")
          Log.output(obj.id,  padding="      id: ")

      Log.write("found %s object(s)" % (data['count']))

    return 0
