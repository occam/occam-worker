# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os

from occam.log          import Log
from occam.object       import Object
from occam.object_info  import ObjectInfo

from occam.manager import uses

from occam.manifests.manager import ManifestManager, BuildRequiredError
from occam.objects.manager   import ObjectManager
from occam.notes.manager     import NoteManager
from occam.jobs.manager      import JobManager
from occam.storage.manager      import StorageManager
from occam.system.manager      import SystemManager

from occam.commands.manager  import command, option, argument

@command('objects', 'build',
  category      = 'Running Objects',
  documentation = "Builds the given object, if it can.")
@argument("object", type = "object", nargs = "?")
@argument("arguments", nargs = '*')
@option("-t", "--task-only", action  = "store_true",
                             dest    = "task_only",
                             help    = "when used, a task will be generated and stored. The uuid and revision will be output.")
@option("-g", "--global",    action  = "store_true",
                             dest    = "force_global",
                             help    = "when used, ensures that a global build of any local object is done.")
@option("-r", "--recursive", action  = "store_true",
                             dest    = "recursive",
                             help    = "when specified, builds all necessary objects when needed.")
@option("-i", "--interactive", action = "store_true",
                               help   = "runs in interactive mode. no stdout or stderr are generated.",
                               dest   = "interactive")
@uses(ManifestManager)
@uses(ObjectManager)
@uses(NoteManager)
@uses(JobManager)
@uses(StorageManager)
@uses(SystemManager)
class Build:
  def build(self, object, local, penalties, seen = None, original=True):
    """ Builds the given object.
    """

    if seen is None:
      seen = {}

    task = None
    lastRequiredObject = None

    while True:
      try:
        info = self.objects.infoFor(object)
        ownerInfo = info
        Log.write("Building %s %s" % (info.get('type', 'object'), info.get('name', 'unknown')))

        if 'build' not in info:
          ownerInfo = self.objects.ownerInfoFor(object)
          if 'build' in ownerInfo:
            object = self.objects.ownerFor(object, person = self.person)

        if ownerInfo.get('id') in seen and object.revision in seen[ownerInfo.get('id')]:
          if not ownerInfo.get('id') in penalties:
            penalties[ownerInfo.get('id')] = []
          if not object.revision in penalties[ownerInfo.get('id')]:
            penalties[ownerInfo.get('id')].append(object.revision)

          Log.warning("Recursive build. Trying again.")
          return -2

        task = self.manifests.build(object    = object,
                                    uuid      = object.uuid,
                                    revision  = object.revision,
                                    local     = local,
                                    penalties = penalties,
                                    person    = self.person)
        break
      except BuildRequiredError as e:
        if lastRequiredObject is e.requiredObject or not self.options.recursive:
          Log.error(e.message) 
          Log.write("You can try to build each required object as needed by invoking with --recursive.")
          return -1

        # Add this object to the penalties so it doesn't get used cyclicly
        if not ownerInfo.get('id') in seen:
          seen[ownerInfo.get('id')] = []
        if not object.revision in seen[ownerInfo.get('id')]:
          seen[ownerInfo.get('id')].append(object.revision)

        job = self.build(e.requiredObject, False, penalties = penalties, seen = seen, original = False)
        if job == -1:
          return -1

    if task is None:
      Log.error("Build task could not be generated.")
      return -1

    if self.options.task_only:
      import json

      ret = {
        "id":       task.uuid,
        "revision": task.revision
      }

      Log.output(json.dumps(ret))
      return 0

    # Deploy the task as a job
    job = self.jobs.deploy(self.objects.infoFor(task), revision=task.revision, local=local, person = self.person, interactive = self.options.interactive)

    if job is None or job != 0:
      Log.error("Build failed.")
      return -1

    return job

  def do(self):
    Log.header("Building Object")

    cachedTask = None
    local = False
    path = "."
    object = None

    object = self.objects.resolve(self.options.object, person=self.person)

    if self.options.object is None or self.options.object.id == ".":
      # TODO: allow non authenticated accounts to perform local builds?
      local = True
      object = Object(path = path, identity = self.person.identity)

      object.id  = self.objects.idFor(object, identity = self.person.identity)
      object.uid = self.objects.uidFor(object)

    if object is None:
      Log.error("Cannot find object")
      return -1

    if self.options.force_global:
      local = False

    resolved = False
    penalties = {}

    while not resolved:
      if self.options.recursive:
        tasks, resolved = self.manifests.buildAll(object, local = local, penalties = penalties, person = self.person)
      else:
        task = self.manifests.build(object, id = object.id, revision = object.revision, local = local, penalties = penalties, person = self.person)

        tasks = [task]
        resolved = True

      if isinstance(resolved, bool) and resolved == False:
        penalties = {}
      if not isinstance(resolved, bool) and resolved == 1:
        resolved = False

      for task in reversed(tasks):
        # TODO: local
        if self.options.task_only:
          import json

          ret = {
            "id":       task.id,
            "uid":      task.uid,
            "revision": task.revision
          }

          Log.output(json.dumps(ret))
          return 0

        taskInfo = self.objects.infoFor(task)
        originalTaskId = task.id
        taskInfo['id'] = task.id
        opts = self.jobs.deploy(taskInfo, revision = task.revision, local=local, person = self.person, interactive=self.options.interactive)
        report = self.jobs.execute(*opts)

        if not local:
          elapsed = report['time']
          buildPath = report.get('paths').get(taskInfo.get('builds').get('index')).get('buildPath')

          self.builds.write.store(self.person.identity, object, task, buildPath, elapsed)

    Log.done("Build Complete")
