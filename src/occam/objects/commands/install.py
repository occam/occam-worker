# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.log    import Log
from occam.object import Object

from occam.manager import uses
from occam.objects.write_manager import ObjectWriteManager

from occam.commands.manager import command, option, argument

@command('objects', 'install')
@argument("object", type = "object", nargs='?')
@option("-t", "--to", action = "store",
                      dest   = "path",
                      help   = "The path to install the resources for the given object. [default = current directory]",
                      default = ".")
@uses(ObjectWriteManager)
class Install:
  def do(self):
    Log.header("Installing Object")

    if self.options.object:
      # if it is a occam/task, we should pull out and clone the generator (experiment)
      obj = self.objects.resolve(self.options.object, person = self.person)
    else:
      obj = Object(path=".")

    self.objects.install(obj, self.options.path, build = True)
