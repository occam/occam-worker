# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.object         import Object
from occam.object_info    import ObjectInfo

from occam.log import Log

from occam.manager import uses

from occam.network.manager       import NetworkManager
from occam.nodes.manager         import NodeManager
from occam.backends.manager      import BackendManager
from occam.objects.write_manager import ObjectWriteManager
from occam.permissions.manager   import PermissionManager

from occam.commands.manager import command, option, argument

@command('objects', 'DELETE',
  category      = '',
  documentation = "Deletes the object from our local knowledge.")
@argument("object", action = "store", type="object", default=".", nargs="?")
@uses(NetworkManager)
@uses(NodeManager)
@uses(BackendManager)
@uses(PermissionManager)
@uses(ObjectWriteManager)
class Delete:
  def do(self):
    Log.header("Deleting object")

    obj = self.objects.resolve(self.options.object, person = self.person, allowNone = True)
    if obj is None:
      Log.write("Cannot find object.")
    else:
      self.objects.write.delete(obj)

    Log.done("Deleted object")
    return 0
