# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.log import Log
from occam.manager import uses

from occam.commands.manager import command, option

from occam.objects.write_manager   import ObjectWriteManager
from occam.resources.write_manager import ResourceWriteManager

@command('objects', 'commit',
  category      = 'Object Management',
  documentation = "Stores any new versions of a local object to the archive.")
@uses(ObjectWriteManager)
@uses(ResourceWriteManager)
class Commit:
  """
  This commits the object's revision to the tree.
  """
  def do(self):
    path = '.'
    Log.header('Committing object')

    # pull from path
    obj = self.objects.localSearch(startingPath=".")

    if obj is None:
      Log.error('No object in the current path.')
      return -1

    # Update resources
    objInfo = self.objects.infoFor(obj)

    Log.write("Committing resources")
    resources, dirty = self.resources.write.commitAll(obj, objInfo)

    if dirty:
      objInfo['install'] = resources
      obj.updateObject(objInfo, "Committing resources.")

    self.objects.write.updateParents(obj)
    self.objects.write.store(obj)

    Log.done("Updated object %s" % objInfo.get('id'))
