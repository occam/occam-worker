# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os          # path functions

from occam.log    import Log
from occam.object import Object

from occam.commands.manager import command, option, argument

from occam.manager import uses

from occam.objects.write_manager import ObjectWriteManager
from occam.permissions.manager   import PermissionManager

@command('objects', 'clone',
  category      = 'Object Management',
  documentation = "Creates a copy of the given object.")
@argument("object", type = "object", help = "the object to clone")
@argument("path",   type = str,      help = "path to clone the object", nargs = "?")
@option("-t", "--to",       action = "store",
                            dest   = "to_object",
                            type   = "object",
                            help   = "the object id to clone into")
@option("-n", "--name",     action = "store",
                            dest   = "name",
                            help   = "the new name for the cloned object")
@option("-p", "--temporary",action = "store_true",
                            dest   = "temporary",
                            help   = "create a temporary path (you are responsible for deleting)")
@option("-i", "--internal", action = "store_true",
                            dest   = "store",
                            help   = "build the clone in the object store instead of making a local path")
@option("-l", "--linked",   action = "store_true",
                            dest   = "linked",
                            help   = "whether or not to link to the original object instead of creating a new object with a new id")
@option("-j", "--json",     dest    = "to_json",
                            action  = "store_true",
                            help    = "returns result as a json document")
@uses(ObjectWriteManager)
@uses(PermissionManager)
class CloneCommand:
  def do(self):
    if self.person is None and (self.options.object is None or self.options.object.id == "."):
      Log.error("Must be authenticated to store new objects")
      return -1

    Log.header("Cloning object")

    obj = self.objects.resolve(self.options.object, person = self.person)

    if obj is None:
      Log.error("Object could not be located")
      return -1

    to_object = None
    if self.options.to_object:
      to_object = self.objects.resolve(self.options.to_object, person = self.person)

      if to_object is None:
        Log.error('Object specified by `--to` could not be located')
        return -1

    obj_type = self.objects.infoFor(obj).get('type', 'object')
    obj_name = self.options.name or self.objects.infoFor(obj).get('name', 'object')

    path = self.options.path
    if path is None or path == "":
      path = os.path.join('.', Object.slugFor(obj_type) + '-' + Object.slugFor(obj_name))

    path = os.path.realpath(path)

    # Clone
    new_obj  = None
    if self.options.store or self.options.name:
      # Clone the object within the provided object
      # We need the space to do this, so make a temp clone of to_object
      new_obj = self.objects.write.clone(obj, to = to_object, identity = self.person.identity, person = self.person, info = {
        "name": self.options.name or obj_name
      })

      # Set initial permissions
      self.permissions.update(new_obj.id, canRead=False, canWrite=False, canClone=False, canRun=False)

      if self.person:
        self.permissions.update(new_obj.id, person_id=self.person.id, canRead=True, canWrite=True, canClone=True, canRun = True)

      # Clone into a directory by default
      if not self.options.store:
        new_obj = self.objects.retrieve(new_obj.id, new_obj.revision, person = self.person)
        new_obj = self.objects.clone(new_obj, path = path)
    else:
      Log.write("Cloning to %s" % (path))
      new_obj = self.objects.clone(obj, path = path)

    if new_obj is None:
      Log.error("could not clone")
      return -1

    ret = {}
    ret["updated"] = []

    for x in (new_obj.roots or []):
      ret["updated"].append({
        "id": x.id,
        "uid": x.uid,
        "revision": x.revision,
        "position": x.position,
      })

    ret["updated"].append({
      "id": new_obj.id,
      "uid": new_obj.uid,
      "revision": new_obj.revision,
      "position": new_obj.position,
    })

    if self.options.to_json:
      import json

      Log.output(json.dumps(ret))
    else:
      Log.write("new object id: ", end="")
      Log.output("%s" % (self.objects.idFor(new_obj, identity=new_obj.identity)), padding="")
      Log.write("new object revision: ", end="")
      Log.output("%s" % (new_obj.revision), padding="")

    Log.done("Cloned %s %s" % (obj_type, obj_name))
    return 0
