# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.log              import Log
from occam.object           import Object
from occam.manager          import uses

from occam.objects.manager   import ObjectManager
from occam.manifests.manager import ManifestManager
from occam.discover.manager  import DiscoverManager

from occam.commands.manager   import command, option, argument

import json

@command('objects', 'editors',
  category      = 'Running Objects',
  documentation = "Returns a list of objects that may be used for editing the given object.")
@argument("object", type = "object", nargs = '?')
@option("-j", "--json",    dest    = "to_json",
                           action  = "store_true",
                           help    = "returns result as a json document")
@option("-s", "--subtype", dest    = "subtype",
                           action  = "append",
                           help    = "the subtype to query")
@option("-t", "--type",    dest    = "type",
                           action  = "append",
                           help    = "the type to query")
@uses(ObjectManager)
@uses(DiscoverManager)
class ObjectEditorsCommand:
  """ This class will handle gathering a list of editor objects for the given object.

  Retrieve the possible editor objects that can view this object:
  occam objects editors QmaDnmWbTyibktXANJ3LJZzd8TPir9vruxbtberyCrpbkq@95ada4dd719f4de146c66871ede425201821ca50
  """

  def do(self, recursive=False):
    # Query for object by id (if necessary)
    info = None
    if not (self.options.subtype and self.options.type):
      obj = self.objects.resolve(self.options.object, person = self.person, allowNone = True)
      if obj is None:
        if self.options.object:
          info = self.discover.retrieveJSON(self.options.object, person = self.person)
        else:
          Log.error("cannot find object in the current path")
          return -1

        if info is None:
          Log.error("cannot find object with id %s" % (self.options.object.id))
          return -1

    subtypes = None

    if self.options.subtype:
      subtypes = self.options.subtype
    else:
      if info is None:
        info = self.objects.infoFor(obj)
      subtypes = info.get('subtype')

    if subtypes is None:
      subtypes = []

    if not isinstance(subtypes, list):
      subtypes = [subtypes]

    type = None
    if self.options.type:
      type = self.options.type[0]
    else:
      if info is None:
        info = self.objects.infoFor(obj)
      type = info.get('type')

    # We will return this array of objects
    editors = []

    # First get the editors that are very specific
    for subtype in subtypes:
      editors.extend(self.objects.editorsFor(editsType    = type,
                                             editsSubtype = subtype))

    # Get the generic editors
    editors.extend(self.objects.editorsFor(editsType = type))

    for subtype in subtypes:
      editors.extend(self.objects.editorsFor(editsSubtype = subtype))

    # Do some discovery of editors within the federation, perhaps
    if not editors:
      for subtype in subtypes:
        editors.extend(self.discover.editorsFor(type = type, subtype = subtype))
      editors.extend(self.discover.editorsFor(type = type))

    if self.options.to_json:
      ret = [{
        "id":           obj.id,
        "uid":          obj.uid,
        "identity":     obj.identity_uri,
        "revision":     obj.revision,
        "name":         obj.name,
        "description":  obj.description,
        "type":         obj.object_type,
        "subtype":      obj.subtype,
        "architecture": obj.architecture,
        "organization": obj.organization,
        "environment":  obj.environment,
      } for obj in editors]
      Log.output(json.dumps(ret))
    else:
      for editor in editors:
        Log.output(editor.name + " - " + editor.uid + "@" + editor.revision)

    return 0
