# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import stat

from occam.log    import Log
from occam.config import Config

from occam.commands.manager import command, option, argument

@command('objects', 'alias',
  category      = 'Running Objects',
  documentation = "Creates a name that can be used to refer and run this object locally.")
@argument("object", type = "object")
@argument("name",   type = str)
# TODO: add arguments flag to add arguments to the command
# TODO: allow all 'run' command arguments and add those
class AliasCommand:
  """ This command will add a command to the local account's Occam bin path so it
  can be executed on the command line.
  """

  def do(self):
    # Determine the tag
    tag = self.options.object.id

    if self.options.object.revision:
      tag = "%s@%s" % (tag, self.options.object.revision)

    if self.options.object.version:
      # Determine the revision for this version
      obj = self.objects.retrieve(self.options.object.id, version=self.options.object.version)
      if obj is None:
        Log.error("Cannot determine the object's revision")
        return -1

      tag = "%s@%s" % (tag, obj.revision)

    # Add the script
    script = """#!/bin/sh 

# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

occam objects run %s -- "$@"
    """ % (tag)

    rootPath = Config.root()
    binPath = os.path.join(rootPath, "binaries")

    if not os.path.exists(binPath):
      os.mkdir(binPath)

    filename = os.path.join(binPath, self.options.name)

    f = open(filename, "w+")
    f.write(script)
    f.close()

    st = os.stat(filename)
    os.chmod(filename, st.st_mode | stat.S_IEXEC)

    Log.done("Alias created.")
