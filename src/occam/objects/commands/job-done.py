# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json
import datetime

from occam.commands.manager import command, option, argument

from occam.object import Object
from occam.log    import Log

from occam.manager import uses

from occam.objects.write_manager  import ObjectWriteManager
from occam.configurations.manager import ConfigurationManager
from occam.workflows.manager      import WorkflowManager
from occam.jobs.manager           import JobManager
from occam.databases.manager      import DatabaseManager
from occam.notes.manager          import NoteManager

@command('objects', 'job-done',
  category      = 'Object Build Management',
  documentation = "Respond to a finished build job.")
@argument("job_id", type=str, help = "The identifier for the job.")
@uses(ObjectWriteManager)
@uses(DatabaseManager)
@uses(ConfigurationManager)
@uses(WorkflowManager)
@uses(JobManager)
@uses(NoteManager)
class JobDoneCommand:
  def do(self):
    job_id = self.options.job_id
    job = self.jobs.retrieve(job_id = self.options.job_id)[0]
    kind = job.kind
    if kind == "build":
      task = self.jobs.taskForJob(job_id)
      taskInfo = self.objects.infoFor(task)
      # taskInfo['id'] = task.id
      if job.status == "failed":
        # Can we do better?
        exitCode = -1
      else:
        exitCode = 0

      objectInfo = task.info.get('builds')
      owner_uid = self.notes.resolveOwner(objectInfo.get('uid'))
      buildPath  = self.jobs.storage.buildPathFor(owner_uid, revision=objectInfo.get('revision'), buildId=task.info.get("id"), create=True)
      paths=self.jobs.pathsForTask(
                          task.info,
                          buildPath = buildPath,
                          person    = self.person)

      start_time = job.start_time or datetime.datetime.utcnow()
      finish_time = job.finish_time or datetime.datetime.utcnow()

      report = {
        'time':      (job.finish_time-job.start_time).total_seconds(),
        'date':      datetime.datetime.utcnow().isoformat(),
        'code':      exitCode,
        #'runReport': runReport,
        'paths':     paths
      }
      self.jobs.finishBuild(report, taskInfo, task.revision, task.id)

    else:
      pass
