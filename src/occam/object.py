# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.log            import Log
from occam.git_repository import GitRepository

import os
import json

class Object():
  """ This class represents a basic OCCAM object.
  """

  info = None
  id = None
  uid = None
  path = None
  revision = None
  infoRevision = None
  ownerInfo = None
  owner_id = None
  origin = None
  ownerInfoRevision = None
  file = None
  parent = None
  root = None
  roots = None
  resource = None
  link = None
  position = None
  version = None
  identity = None
  fileCache = None
  anonymous = False

  def __del__(self):
    if self.temporary:
      # Delete temporary object from filesystem
      Log.write("Deleting temporary path %s" % self.path)
      import shutil
      shutil.rmtree(self.path)

  @staticmethod
  def slugFor(name):
    import re
    return re.sub(r'[^a-zA-Z0-9]', '-', name)

  @staticmethod
  def slugUUID(object_type, uid):
    return "%s-%s" % (Object.slugFor(object_type), Object.slugFor(uid))

  def __init__(self, id = None, uid = None, revision=None, root=None, empty=False, path=None, info=None, file=None, belongsTo=None, resource=None, roots=None, link=None, position=None, identity=None, version=None, owner_id=None):
    """ Creates an instance of an object wrapping an existing object at the given path.
    """

    self.revision  = revision
    self.path      = path
    if not path is None:
      self.path    = os.path.realpath(path)
    self.root      = root
    self.roots     = roots

    if self.root is None and self.roots:
      self.root = self.roots[0]

    self.info              = info
    self.infoRevision      = None
    self.ownerInfoRevision = None
    self.ownerInfo         = info
    self.owner_id          = owner_id
    self.origin            = None
    self.id                = id
    self.uid               = uid
    self.temporary         = False
    self.file              = file
    self.parent            = belongsTo
    self.resource          = resource
    self.link              = link
    self.position          = position
    self.version           = version
    self.identity          = identity
    self.fileCache         = {}
    self.anonymous         = False

    if revision is None and not path is None:
      self.revision = self.head()

    if empty:
      self.info = {}

  def head(self):
    """ Returns the revision of HEAD for this Object.
    """

    if self.path:
      return GitRepository(self.path).head()
    else:
      return self.revision

  def parentRevision(self):
    if self.path:
      return GitRepository(self.path).parent()
    else:
      return None

  def fullRevision(self, revision):
    return GitRepository.fullRevision(self.git.path, revision)
