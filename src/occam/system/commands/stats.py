# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.log          import Log

from occam.manager import uses

from occam.system.manager    import SystemManager

from occam.commands.manager  import command, option, argument

@command('system', 'stats',
  category      = 'System Commands',
  documentation = "Computes and displays statistics about the store.")
@uses(SystemManager)
class StatsCommand:
  def do(self):
    Log.header("Determining the storage taken by the local object store")
    print(self.system.computeStoreSize())

    Log.header("Determining the storage taken by the run cache")
    print(self.system.computeRunCacheSize())
    return 0
