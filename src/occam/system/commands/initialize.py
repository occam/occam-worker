# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.log     import Log
from occam.manager import uses

from occam.commands.manager import command, option

from occam.system.manager        import SystemManager
from occam.storage.manager       import StorageManager
from occam.backends.manager      import BackendManager
from occam.nodes.manager         import NodeManager
from occam.objects.write_manager import ObjectWriteManager

@command('system', 'initialize',
  category      = "System Commands",
  documentation = "Sets up an Occam instance.")
@option('-t', '--test', action = "store_true",
                        dest   = "test",
                        help   = "initialize a testing database")
@uses(SystemManager)
@uses(StorageManager)
@uses(BackendManager)
@uses(NodeManager)
@uses(ObjectWriteManager)
class Initialize:
  """ This command will initialize the Occam node.
  """

  def do(self):
    Log.header("Initializing OCCAM Node")

    Log.write("Initializing the database")
    self.system.initialize()

    # Initialize Subsystems
    Log.write("Initializing storage backends")
    storageInfo = self.storage.initialize()

    Log.write("Initializing VM backends")
    backendInfo = self.backends.initialize()

    # Store information about ourselves
    Log.write("Recording information about self")
    nodeInfo = {
      "storage":  storageInfo,
      "backends": backendInfo,
    }
    self.system.storeInfo(nodeInfo)

    # Store objects already in our object store
    Log.write("Rebuilding object index")
    self.objects.write.rebuild(person = self.person)

    Log.done("OCCAM node initialized and ready")

    return 0
