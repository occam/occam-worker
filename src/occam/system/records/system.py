# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.databases.manager import table

@table("systems")
class SystemRecord:
  """ Stores administrative information about this node.
  """

  schema = {

    # Primary Key

    "id": {
      "type": "integer",
      "primary": True
    },

    # Attributes

    "host": {
      "type": "string",
      "length": 128
    },

    "port": {
      "type": "integer"
    },

    "default_roles": {
      "type": "string",
      "length": 128
    },

    "moderate_objects": {
      "type": "boolean",
      "default": False
    },

    "moderate_accounts": {
      "type": "boolean",
      "default": False
    }
  }
