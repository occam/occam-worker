# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# This is based on code by Aaron Hall:
#   http://stackoverflow.com/questions/3387691/python-how-to-perfectly-override-a-dict

from itertools import chain
try:              # Python 2
  str_base = basestring
  items = 'iteritems'
except NameError: # Python 3
  str_base = str, bytes, bytearray
  items = 'items'

from collections import OrderedDict

class ObjectInfo(OrderedDict):
  """ This class represents the object.json metadata.
  """

  def dependency(self, uuid, category='dependencies'):
    """ Returns the dependency info given a uuid.
    
    Returns the dependency info for an object in the given category.
    If there is no such dependency then it returns None.
    """

    self[category] = self.get(category, [])

    for dependency in self[category]:
      if dependency['id'] == uuid:
        return dependency

    return None

  def relationList(self):
    """ Returns a list of uuids that relate to this object.
    """

    ret = []

    parent = self.get('belongsTo', {})

    if isinstance(parent, dict):
      parent = parent.get('id')

    if parent is not None:
      ret.append(parent)

    # TODO: gather parents of parents??
    #       this might be something that object resolvers need to solve

    # Dependencies
    dependencies = self.get('dependencies', [])
    for dependency in dependencies:
      if isinstance(dependency, dict):
        dependency = dependency.get('id')

      if dependency is not None and self.isUUID(dependency):
        ret.append(dependency)

    # Authors
    people = self.get('authors', [])
    for person in people:
      if isinstance(person, dict):
        person = person.get('id')

      if person is not None and self.isUUID(person):
        ret.append(person)

    # Collaborators
    people = self.get('collaborators', [])
    for person in people:
      if isinstance(person, dict):
        person = person.get('id')

      if person is not None and self.isUUID(person):
        ret.append(person)

    # Generated Objects
    generates = self.get('generates', [])
    for generated in generates:
      if isinstance(generated, dict):
        generated = generated.get('id')

      if generated is not None and self.isUUID(generated):
        ret.append(generated)

    # Install section (repos)
    installs = self.get('install', [])
    for install_info in installs:
      if isinstance(install_info, dict):
        repo_id = install_info.get('id')
        if repo_id is not None and self.isUUID(repo_id):
          ret.append(repo_id)

    # Workflow objects
    workflow = self.get('workflow', {})
    connections = workflow.get('connections', [])
    for connection in connections:
      obj = connection.get('object', {})
      id = obj.get('id')
      if id is not None and self.isUUID(id):
        ret.append(id)

    # Build section (using, etc)
    # TODO

    return ret

  @staticmethod
  def isUUID(uuid):
    """ Helper method to determine if the given value is a UUID.
    """
    return isinstance(uuid, str) and len(uuid) == 36 and uuid[8]  == '-' \
                                                     and uuid[13] == '-' \
                                                     and uuid[18] == '-' \
                                                     and uuid[23] == '-'
