# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.manager import manager

from occam.log import loggable

@loggable
@manager("cache")
class CacheManager:
  """ This manages cache (key/value store/retrieve) management.

  Difference caching backends can be added to be used to manage the pulling of
  documents or object metadata. This is to subvert the speed of pulling data
  from the network (at worst) or from the local filesystem (most common) or
  from the database (at best)

  Pulling from the cache layer could result in never loading the database layer
  at all. Hopefully, (and caching is always a hope, isn't it?) this results in
  the drastic improvement of any repeated tasks.
  """

  drivers = {}

  @staticmethod
  def register(adapter, driverClass):
    """ Adds a new cache driver to the possible drivers.
    """
    CacheManager.drivers[adapter] = driverClass

  def driverFor(self, adapter):
    """ Returns an instance of a driver for the given cache adapter.
    """

    if not adapter in self.drivers:
      # TODO: error?
      adapter = 'lru'

    return self.drivers[adapter](self.configuration)

  def __init__(self):
    """ Instantiates the Cache Manager.
    """

    self._driver = None

  def disconnect(self):
    """ Uninitializes the cache connection.
    """

    self._driver = None

  def connect(self):
    """ Initializes a cache connection.
    """

    # Determine the database type
    if self._driver is None:
      adapter = self.configuration.get('adapter', 'lru')

      # Attempt to find that database driver
      import importlib
      
      try:
        importlib.import_module("occam.caches.plugins.%s" % (adapter))
      except ImportError:
        pass

      self._driver = self.driverFor(adapter)

    return self._driver

  def delete(self, key):
    """ Deletes the given key.
    """

    CacheManager.Log.noisy("deleting key %s" % (key))

    return self.connect().delete(key)

  def get(self, key):
    """ Returns the value stored at the given key. Returns None if not found.
    """

    CacheManager.Log.noisy("retrieving cache item at %s" % (key))

    return self.connect().get(key)

  def set(self, key, value):
    """ Stores the given value at the given key. It MAY be retrievable later.

    This will store the value in the current cache. It may push something else
    out, and it may be pushed out before a corresponding get() is called. The
    cache backend makes any and all decisions about what stays or goes.
    """

    # The value passed to this must always be a byte array
    if isinstance(value, str):
      value = value.encode('utf8')

    return self.connect().set(key, value)

def cache(name):
  """ This decorator will register a possible cache backend.
  """

  def _cache(cls):
    CacheManager.register(name, cls)
    return cls

  return _cache
