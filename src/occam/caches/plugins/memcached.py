# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.caches.manager import cache

@cache("memcached")
class Memcached:
  """ The memcached cache driver.
  """

  def __init__(self, configuration):
    """ Initializes the driver.
    """

    from pymemcache.client import Client

    host = configuration.get('host', 'localhost')
    port = configuration.get('port', 11211)

    self.client = Client((host, port))

  def get(self, key):
    """ Retrieves the value at the given key. Returns None if not found.
    """

    key = "occam-" + key

    return self.client.get(key)

  def delete(self, key):
    """ Deletes the given key.
    """

    key = "occam-" + key

    self.client.delete(key)

    return True

  def set(self, key, value):
    """ Sets the value at the given key.
    """

    key = "occam-" + key

    return self.client.set(key, value)
