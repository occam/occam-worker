# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.caches.manager import cache

@cache("lru")
class LRU:
  """ The naive cache driver which implements a simple queue based cache.

  This cache is not preserved between runs. It simply caches results during
  a run or while the daemon is running. For command-line usage, this won't help
  very much.
  """

  def __init__(self, configuration):
    """ Initializes the driver.
    """

    from collections import deque

    self.keys = deque()
    self.data = {}

  def delete(self, key):
    """ Deletes the given key.
    """

    if key in self.data:
      oldIndex = self.keys.index(key)
      del self.keys[oldIndex]
      del self.data[key]

    return True

  def get(self, key):
    """ Retrieves the value at the given key. Returns None if not found.
    """

    if key in self.data:
      # Put accessed key at top of priority so it gets removes last
      oldIndex = self.keys.index(key)
      del self.keys[oldIndex]
      self.keys.append(key)
      return self.data[key]

    return None

  def set(self, key, value):
    """ Sets the value at the given key.
    """

    # Pop least-recently used option
    if len(self.keys) > 9:
      removalKey = self.keys.popleft()
      del self.data[removalKey]

    # Append/set data to queue
    self.data[key] = value

    # Ensure this key is most recently used (if it exists)
    if key in self.keys:
      oldIndex = self.keys.index(key)
      del self.keys[oldIndex]

    # Append key
    self.keys.append(key)

    return self.data[key]
