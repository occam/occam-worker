
# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sql
import sql.aggregate

from occam.log import loggable
from occam.databases.manager import uses, datastore

@loggable
@datastore("versions")
class VersionDatabase:
  """ Manages the database interactions for version tagging.
  """

  def queryVersions(self, id, uid):
    versions = sql.Table("versions")

    query = versions.select()
    query.where = (versions.id == id) & (versions.uid == uid)

    return query

  def retrieveVersions(self, id, uid):
    from occam.versions.records.version import VersionRecord

    session = self.database.session()

    query = self.queryVersions(id, uid)

    self.database.execute(session, query)
    return [VersionRecord(x) for x in self.database.many(session, size=100)]
