# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.log     import loggable
from occam.manager import manager, uses

import os

from occam.versions.manager import VersionManager, VersionError

@loggable
@manager("versions.write", reader=VersionManager)
class VersionWriteManager:
  """ This manages the storing and updating of version tags in the object store.
  """

  def update(self, obj, tag, identity, published, signature):
    """ Updates or creates the given tag for the given object.

    Args:
      obj (Object): The object to tag.
      tag (str): The tag to apply.
      identity (str): The Identity URI of the actor applying the tag.
      published (datetime): The date and time when the tag was created.
      signature (bytes): The signature that validates the tag.

    Returns:
      VersionRecord: The existing or created tag record.
    """

    # TODO: Verify the signature

    ret = self.datastore.write.updateVersion(obj.id, obj.uid, obj.revision, tag, identity, published, signature)

    return ret
