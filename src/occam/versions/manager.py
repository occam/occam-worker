# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os

from occam.log     import loggable
from occam.manager import manager, uses

from occam.semver  import Semver

@loggable
@manager("versions")
class VersionManager:
  """ 
  """

  def retrieve(self, obj):
    """ Will return a list of known versions for the given object.
    """

    # TODO: ask the federation to resolve when a tag is not known.
    #       There is definitely room for DHT innovation for version ranges.
    return self.datastore.retrieveVersions(obj.id, obj.uid)

  def resolve(self, obj, version, versions = None, penalties = None):
    """ This function will return the revision of the given version string.
    
    The version string can be a parsable identifier or a full semver string. It
    works the same as npm's semver.

    Returns None when a version within the requested version range cannot be
    found.
    """

    if version is None:
      return None, None

    ranges = Semver.parseVersion(version)

    # Look up versions

    tags = self.retrieve(obj)

    if versions is None:
      versions = [tag.tag for tag in tags]

    currentVersion = Semver.resolveVersion(version, versions)

    # Go and pick the best (most-likely) revision
    revision = None
    if currentVersion:
      for item in tags:
        if item.tag == currentVersion:
          revision = item.revision

    if revision is None and currentVersion in versions:
      # Negate this version and try again
      versions.remove(currentVersion)
      return self.resolve(obj, version, versions=versions, penalties=penalties)

    return revision, currentVersion

class VersionError(Exception):
  """ Base class for all versioning errors.
  """
