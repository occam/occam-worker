# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os

from occam.log    import Log
from occam.object import Object

from occam.commands.manager import command, option, argument

from occam.objects.manager        import ObjectManager
from occam.versions.write_manager import VersionWriteManager
from occam.keys.write_manager     import KeyWriteManager, KeySignatureExistsError

from occam.manager import uses

@command('versions', 'new',
  category      = 'Version Management',
  documentation = "Tags the revision of the given object as the given version.")
@argument("object", type="object", nargs="?")
@argument("tag")
@option("--overwrite", dest   = "overwrite",
                       action = "store_true",
                       help   = "forces the adding of the version even if it already exists")
@option("-j", "--json", dest   = "to_json",
                        action = "store_true",
                        help   = "returns result as a json document")
@uses(ObjectManager)
@uses(VersionWriteManager)
@uses(KeyWriteManager)
class VersionsNewCommand:
  def do(self):
    if self.person is None:
      Log.error("Must be authenticated to adds tags")
      return -1

    if self.options.object is None:
      obj = Object(path=".", identity=self.person.identity)
      obj.id = self.objects.idFor(obj, obj.identity)
    else:
      obj = self.objects.resolve(self.options.object,
                                 person = self.person)

    Log.header("Applying tag")

    if obj is None:
      Log.error("Could not find the object to tag.")
      return -1

    # Look at whether or not the tag is already stored
    owner_obj = obj

    if obj.owner_id != obj.id:
      ret["owner"] = {
        "id": obj.owner_id
      }
      owner_obj = self.objects.ownerFor(obj, self.person)

    versions = self.versions.retrieve(owner_obj)
    versions = [version.revision for version in versions]

    if versions:
      if obj.revision not in versions:
        if not self.options.overwrite:
          Log.error("The requested tag is already being used. Use --overwrite to add this tag.")
          return -1
        else:
          Log.warning("Tag already exists and is being appended.")
      else:
        Log.warning("The tag is already set to this revision.")

    # Tag the version in the note store
    try:
      signature, published = self.keys.write.signTag(obj, self.person.identity, self.options.tag)
      Log.write("Signed as %s" % (self.person.identity))
    except KeySignatureExistsError as e:
      pass

    self.versions.write.update(obj, self.options.tag, self.person.identity, published, signature)

    # Store the version locally as well, if needed
    if self.options.object is None or self.options.object.id == ".":
      if not os.path.exists(".occam"):
        os.mkdir(".occam")

      tagsPath = os.path.join(".occam", "tags")
      tags = {}

      import json
      try:
        with open(tagsPath, "r") as f:
          tags = json.load(f)
      except:
        pass

      tags[self.options.tag] = obj.revision

      with open(tagsPath, "w+") as f:
        f.write(json.dumps(tags))

    # Success
    Log.write("Version %s applied to revision %s" % (self.options.tag, obj.revision))
    return 0
