# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.log    import Log
from occam.object import Object

from occam.commands.manager import command, option, argument

from occam.objects.manager  import ObjectManager
from occam.versions.manager import VersionManager

from occam.manager import uses

@command('versions', 'list',
  category      = 'Version Management',
  documentation = "Lists the known versions of the given object.")
@argument("object", type="object", nargs="?")
@option("-j", "--json", dest   = "to_json",
                        action = "store_true",
                        help   = "returns result as a json document")
@uses(ObjectManager)
@uses(VersionManager)
class VersionsListCommand:
  def do(self):
    if self.options.object is None:
      obj = Object(path=".")
    else:
      obj = self.objects.resolve(self.options.object,
                                 person = self.person)

    Log.header("Listing tags")

    # List existing tags
    tags = self.versions.retrieve(obj)

    if not tags and not self.options.to_json:
      Log.write("No tags")

    if self.options.to_json:
      import json
      json_tags={"tags":[]}
      for tag in tags:
        json_tags["tags"].append({
          "tag": tag.tag,
          "revision": tag.revision
        })
      Log.output(json.dumps(json_tags))
    else:
      for tag in tags:
        Log.write("%s: %s" % (tag.tag, tag.revision))

    return 0
