# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.databases.manager import table

@table("versions")
class VersionRecord:
  schema = {

    # Foreign keys

    "id": {
      "foreign": {
        "table": "objects",
        "key": "id"
      }
    },

    "uid": {
      "foreign": {
        "table": "objects",
        "key": "uid"
      }
    },

    # Normalized metadata

    # The name (version string) that tags this revision
    "tag": {
      "type": "string",
      "length": 128
    },

    # The revision of the object tied to that tag
    "revision": {
      "type": "string",
      "length": 128
    },

    # A timestamp for the request (can be whatever, honestly)
    "published": {
      "type": "datetime",
    },
    
    # The signing Person
    "identity_uri": {
      "type": "string",
      "length": 256
    },

    "signature": {
      "type": "string",
      "length": 512,
    },

    # Constraints
    "relationship": {
      "constraint": {
        "type": "unique",
        "columns": ["identity_uri", "uid", "id", "tag", "revision"],
        "conflict": "replace"
      }
    }
  }
