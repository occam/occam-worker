# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sql

from occam.databases.manager import uses, datastore

from occam.versions.database import VersionDatabase

@datastore("versions.write", reader=VersionDatabase)
class VersionWriteDatabase:
  """ Manages the database interactions for writing new version tags.
  """

  def updateVersion(self, id, uid, revision, tag, identity, published, signature):
    from occam.versions.records.version import VersionRecord

    session = self.database.session()

    r = VersionRecord()
    r.id = id
    r.uid = uid
    r.revision = revision
    r.tag = tag
    r.identity_uri = identity
    r.published = published
    r.signature = signature

    self.database.update(session, r)
    self.database.commit(session)

    return r
