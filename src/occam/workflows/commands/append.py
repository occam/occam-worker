# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json

from occam.commands.manager import command, option, argument

from occam.object import Object
from occam.log    import Log

from occam.manager import uses

from occam.objects.write_manager  import ObjectWriteManager
from occam.configurations.manager import ConfigurationManager

@command('workflows', 'append',
  category      = 'Workflow Management',
  documentation = "Appends an object to a workflow")
@argument("workflow",            type = "object",
                                 help = "The workflow object")
@argument("object",              type = "object",
                                 help = "The object to add to the workflow")
@option("-j", "--json",          dest   = "to_json",
                                 action = "store_true",
                                 help   = "returns result as a json document")
@option("-a", "--at",            dest   = "at",
                                 action = "store",
                                 help   = "The node index to insert the new connection or to refer to an existing connection if adding an input/output.")
@option("-c", "--configuration", dest   = "configuration",
                                 action = "store",
                                 help   = "specifies a configuration index that will instead append a new configuration (based on the defaults) for the given object")
@uses(ObjectWriteManager)
@uses(ConfigurationManager)
class AppendCommand:
  def do(self):
    # Get the object to update
    workflow = self.objects.resolve(self.options.workflow, person = self.person)

    if workflow is None:
      Log.error("cannot find workflow with id %s" % (self.options.workflow.id))
      return -1

    obj = self.objects.resolve(self.options.object, person = self.person)

    if obj is None:
      Log.error("cannot find object with id %s" % (self.options.object.id))
      return -1

    # Create/use a mutable copy of workflow
    root, workflow, path = self.objects.temporaryClone(workflow, person = self.person)

    info = self.objects.infoFor(obj)
    workflowInfo = self.objects.infoFor(workflow)
    objName = info.get('name', 'unknown')
    workflowName = workflowInfo.get('name', 'unknown')
    Log.write("Adding %s to workflow %s" % (objName, workflowName))

    # Create configuration objects for defaults for this node
    # and add them as contents of the workflow itself

    # Update workflow's metadata object to include the new object
    workflowData = self.objects.dataFor(workflow, person = self.person)
    workflowData['connections'] = workflowData.get('connections', [])
    connectionIndex = len(workflowData['connections'])
    if self.options.at:
      connectionIndex = self.options.at
    if self.options.configuration:
      # Create the configuration object
      pass
    else:
      # Add the node to the workflow and all of the possible wires
      workflowData['connections'].insert(connectionIndex, {
        "id":       info.get('id'),
        "name":     info.get('name'),
        "type":     info.get('type'),
        "revision": obj.revision
      })

      workflowData['connections'][-1]['inputs'] = workflowData['connections'][-1].get('inputs', [])
      for inputInfo in info.get('inputs', []):
        if not isinstance(inputInfo.get('subtype'), list):
          inputInfo['subtype'] = [inputInfo['subtype']]
        workflowData['connections'][-1]['inputs'].append({
          "type": inputInfo.get('type'),
          "name": inputInfo.get('name'),
          "subtype": inputInfo.get('subtype')
        })

      workflowData['connections'][-1]['outputs'] = workflowData['connections'][-1].get('outputs', [])
      for outputInfo in info.get('outputs', []):
        if not isinstance(outputInfo.get('subtype'), list):
          outputInfo['subtype'] = [outputInfo['subtype']]
        workflowData['connections'][-1]['outputs'].append({
          "type": outputInfo.get('type'),
          "name": outputInfo.get('name'),
          "subtype": outputInfo.get('subtype')
        })

    ret = {}
    ret["updated"] = []

    for x in (workflow.roots or []):
      ret["updated"].append({
        "id": x.uuid,
        "revision": x.revision,
        "position": x.position,
      })

    ret["updated"].append({
      "id": workflow.uuid,
      "revision": workflow.revision,
      "position": workflow.position,
    })

    if self.options.to_json:
      Log.output(json.dumps(ret))
    else:
      Log.write("new object id: ", end ="")
      Log.output("%s" % (self.objects.infoFor(workflow)['id']), padding="")
      Log.write("new object revision: ", end ="")
      Log.output("%s" % (workflow.revision), padding="")

    Log.done("Successfully updated workflow %s" % (workflowName))
