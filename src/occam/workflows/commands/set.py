# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json

from occam.commands.manager import command, option, argument

from occam.object import Object
from occam.log    import Log

from occam.manager import uses

from occam.objects.write_manager  import ObjectWriteManager
from occam.configurations.manager import ConfigurationManager

@command('workflows', 'set',
  category      = 'Workflow Management',
  documentation = "Appends an object to a workflow")
@argument("workflow",   type = "object",
                        help = "The workflow object")
@argument("data",       type = str,
                        help = "The data to store")
@option("-t", "--input-type",  action  = "store",
                               dest    = "input_type",
                               default = "text",
                               help    = "determines what encoding the new value is. defaults to 'text'. 'json' for JSON encoded values.")
@option("-i", "--item", action  = "append",
                        dest    = "items",
                        nargs   = "+",
                        default = [],
                        help    = "the key/value pair to set. If no value is given, the key is deleted or written to its default.")
@option("-j", "--json", dest   = "to_json",
                        action = "store_true",
                        help   = "returns result as a json document")
@uses(ObjectWriteManager)
@uses(ConfigurationManager)
class SetCommand:
  def do(self):
    # Get the object to update
    workflow = None
    if self.options.workflow:
      workflow = self.objects.resolve(self.options.workflow, person = self.person)

    if workflow is None:
      Log.error("cannot find workflow with id %s" % (self.options.workflow.id))
      return -1

    # Create/use a mutable copy of workflow
    root, workflow, path = self.objects.temporaryClone(workflow, person = self.person)

    workflowInfo = self.objects.infoFor(workflow)
    workflowName = workflowInfo.get('name', 'unknown')
    
    if workflowInfo.get('type') != "workflow":
      Log.error("Given object is not a workflow.")
      return -1

    Log.write("Updating workflow %s" % (workflowName))

    # Set the workflow data object.
    if self.options.data:
      try:
        data = json.loads(self.options.data)
        self.objects.write.addFileTo(workflow, workflowInfo["file"], json.dumps(data))
      except:
        Log.error("Could not interpret input data.")
        return -1

    # Store the updated configuration object
    self.objects.write.commit(workflow, message="Updates workflow.")
    self.objects.write.store(workflow)

    ret = {}
    ret["updated"] = []

    for x in (workflow.roots or []):
      ret["updated"].append({
        "id": x.uuid,
        "revision": x.revision,
        "position": x.position,
      })

    ret["updated"].append({
      "id": workflow.uuid,
      "revision": workflow.revision,
      "position": workflow.position,
    })

    if self.options.to_json:
      Log.output(json.dumps(ret))
    else:
      Log.write("new object id: ", end ="")
      Log.output("%s" % (self.objects.infoFor(workflow)['id']), padding="")
      Log.write("new object revision: ", end ="")
      Log.output("%s" % (workflow.revision), padding="")

    Log.done("Successfully updated workflow %s" % (workflowName))
