# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json

from occam.commands.manager import command, option, argument

from occam.object import Object
from occam.log    import Log

from occam.manager import uses

from occam.objects.write_manager  import ObjectWriteManager
from occam.configurations.manager import ConfigurationManager

@command('workflows', 'connect',
  category      = 'Workflow Management',
  documentation = "Connects two objects to a workflow")
@argument("workflow",            type = "object",
                                 help = "The workflow object")
@argument("object",              type = "object",
                                 help = "The object to add to the workflow")
@option("-j", "--json",          dest   = "to_json",
                                 action = "store_true",
                                 help   = "returns result as a json document")
@option("-a", "--at",            dest   = "at",
                                 action = "store",
                                 help   = "The node index to insert the new connection or to refer to an existing connection if adding an input/output.")
@option("-i", "--input",         dest   = "input_at",
                                 action = "store",
                                 help   = "the input index to insert the new connection")
@option("-o", "--output",        dest   = "output_at",
                                 action = "store",
                                 help   = "the output index to insert the new connection")
@option("-c", "--configuration", dest   = "configuration",
                                 action = "store",
                                 help   = "specifies a configuration index that will instead append a new configuration (based on the defaults) for the given object")
@uses(ObjectWriteManager)
@uses(ConfigurationManager)
class ConnectCommand:
  def do(self):
    return 0
