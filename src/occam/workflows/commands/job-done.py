# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json

from occam.commands.manager import command, option, argument

from occam.object import Object
from occam.log    import Log

from occam.manager import uses

from occam.objects.write_manager  import ObjectWriteManager
from occam.configurations.manager import ConfigurationManager
from occam.workflows.manager      import WorkflowManager
from occam.jobs.manager           import JobManager
from occam.databases.manager      import DatabaseManager

@command('workflows', 'job-done',
  category      = 'Workflow Management',
  documentation = "Respond to a finished job.")
@argument("job_id", type=str, help = "The identifier for the job.")
@argument("run_id", type=str, help = "The identifier for the run.")
@uses(ObjectWriteManager)
@uses(DatabaseManager)
@uses(ConfigurationManager)
@uses(WorkflowManager)
@uses(JobManager)
class JobDoneCommand:
  def do(self):
    run = self.workflows.retrieveRun(self.options.run_id)

    experiment = self.objects.retrieve(id       = run.object_uid,
                                       revision = run.object_revision,
                                       person   = self.person)

    job = self.jobs.retrieve(job_id = self.options.job_id)
    if len(job) > 0:
      job = job[0]
    else:
      return -1

    if job.kind != "workflow":
      task = self.objects.retrieve(id       = job.task_uid,
                                   revision = job.task_revision,
                                   person   = self.person)

      taskInfo = self.objects.infoFor(task)

      # experimentInfo = self.objects.infoFor(self.getToplevelWorkflow(run))
      experimentInfo = self.objects.infoFor(experiment)

      # Rake the outputs of the job
      outputs = self.jobs.rakeOutput(self.objects.infoFor(task), job.path, generators=[{
        "name":     taskInfo.get('name'),
        "type":     taskInfo.get('type'),
        "id":       task.id,
        "revision": task.revision
      },{
        "name":     experimentInfo.get('name'),
        "type":     experimentInfo.get('type'),
        "id":       experimentInfo.get('id'),
        "revision": experiment.revision
      }])

      run_job = self.workflows.runJobFor(job.id)
      for output in outputs:
        from occam.workflows.records.run_output import RunOutputRecord as RunOutput
        if len(output.get("objects", []) ) == 0:
          continue
        new_output = RunOutput()
        new_output.output_index = output["output_index"]
        new_output.output_count = output["output_count"]
        new_output.run_id = run_job.run_id
        new_output.job_id = run_job.job_id
        new_output.node_index = run_job.node_index
        session = self.database.session()
        self.database.update(session, new_output)
        self.database.commit(session)
        for output_object in output.get("objects", []):
          from occam.workflows.records.run_output_object import RunOutputObjectRecord
          out_object_record = RunOutputObjectRecord()
          out_object_record.output_index = output_object["output_index"]
          out_object_record.object_uid = output_object["object_uid"]
          out_object_record.object_revision = output_object["object_revision"]
          out_object_record.run_id = new_output.run_id
          out_object_record.job_id = new_output.job_id
          session = self.database.session()
          self.database.update(session, out_object_record)
          self.database.commit(session)

    # Determine if the run is complete
    # and finalize the Run
    self.workflows.completeJob(job, person = self.person)

    # Or, queue further jobs based on the last ones
    jobs = self.workflows.jobsFor(run.id, statusNot = ["failed", "finished"])
    if len(jobs['nodes'].keys()) == 0:
      print("finishing workflow")
      self.workflows.finish(run)

    return 0
