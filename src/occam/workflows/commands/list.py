# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json

from occam.commands.manager import command, option, argument

from occam.object import Object
from occam.log    import Log

from occam.manager import uses

from occam.objects.write_manager  import ObjectWriteManager
from occam.configurations.manager import ConfigurationManager
from occam.workflows.manager      import WorkflowManager
from occam.jobs.manager           import JobManager

@command('workflows', 'list',
  category      = 'Workflow Management',
  documentation = "Returns the list of queued runs of the given workflow or containing object.")
@argument("workflow",   type = "object",
                        help = "The workflow or a containing object")
@uses(ObjectWriteManager)
@uses(ConfigurationManager)
@uses(WorkflowManager)
@uses(JobManager)
class StatusCommand:
  def do(self):
    workflow = self.objects.resolve(self.options.workflow, person = self.person)

    if workflow is None:
      Log.error("cannot find workflow with id %s" % (self.options.workflow.id))
      return -1

    runs = self.workflows.runsForObject(workflow, workflow.revision)

    Log.output(json.dumps({
      "runs": [{
        "id": x.id,
        "queueTime":   x.queue_time   and x.queue_time.isoformat(),
        "failureTime": x.failure_time and x.failure_time.isoformat(),
        "finishTime":  x.finish_time  and x.finish_time.isoformat()
      } for x in runs]}))

    return 0
