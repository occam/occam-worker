# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json

from occam.commands.manager import command, option, argument

from occam.object import Object
from occam.log    import Log

from occam.manager import uses

from occam.objects.write_manager  import ObjectWriteManager
from occam.configurations.manager import ConfigurationManager
from occam.workflows.manager      import WorkflowManager
from occam.jobs.manager           import JobManager

@command('workflows', 'status',
  category      = 'Workflow Management',
  documentation = "Returns the status of a particular queued workflow run.")
@argument("workflow",   type = "object",
                        help = "The workflow or a containing object")
@argument("run_id", type=str, help="The run id given by a previous queue command.")
@uses(ObjectWriteManager)
@uses(ConfigurationManager)
@uses(WorkflowManager)
@uses(JobManager)
class StatusCommand:
  def do(self):
    workflow = self.objects.resolve(self.options.workflow, person = self.person)

    if workflow is None:
      Log.error("cannot find workflow with id %s" % (self.options.workflow.id))
      return -1

    runId = self.options.run_id

    run = self.workflows.retrieveRun(runId, workflow)

    if run is None:
      Log.error("Cannot find the requested run.")
      return -1

    # Get the jobs for this run
    jobs = self.workflows.jobsFor(runId)
    super_job = self.jobs.retrieve(job_id=run.job_id)[0]
    jobs.update({"job":{
        "id": run.job_id,
        "status": super_job.status
      },
      "run": {
        "id": run.id,
        "queueTime":   run.queue_time   and run.queue_time.isoformat(),
        "failureTime": run.failure_time and run.failure_time.isoformat(),
        "finishTime":  run.finish_time  and run.finish_time.isoformat()
      }
    })

    Log.output(json.dumps(jobs))

    return 0
