# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json
import os          # path functions

from occam.object     import Object

from occam.log import Log

from occam.commands.manager import command, option, argument

from occam.manager import uses

from occam.objects.write_manager import ObjectWriteManager
from occam.permissions.manager   import PermissionManager

# Returns an object with the following keys:

# "updated": an array for each object that was updated ordered by root
#            first and then every object that goes through it.
#            The elements have the following keys:
#
#            "id": the uuid of the object
#            "revision": the new revision
#            "position": the position of this object in the previous object's
#                        contains list

@command('workflows', 'new',
  category      = 'Workflow Management',
  documentation = "Creates a new workflow and stores it in the archive.")
@argument('name')
@option("-i", "--internal", dest    = "internal",
                            action  = "store_true",
                            help    = "whether or not to just create this in the repository without also creating a directory on disk")
@option("-t", "--to",       dest    = "to_object",
                            action  = "store",
                            type    = "object",
                            help    = "the object to add the new object as a dependency")
@option("-j", "--json",     dest    = "to_json",
                            action  = "store_true",
                            help    = "returns result as a json document")
@uses(ObjectWriteManager)
@uses(PermissionManager)
class NewCommand:
  """ Creates a new object.

  This class adds a new object to the current container object, generally a workset.
  It will first have to find out the context (which workset are we in) and then
  do the appropriate set-up to create the new type of object requested.
  """

  def do(self):
    # Look for context. Which workset are we in?
    # Base it off the current directory and go up the path until we find a base
    # object we understand

    type = "workflow"
    name = self.options.name

    obj = None
    new_obj = None
    root = None

    info = {
      "file": "data.json"
    }

    if self.options.to_object:
      # Find the root object that we will add an object to
      obj = self.objects.resolve(self.options.to_object, person = self.person)

      # Handle when the object cannot be retrieved
      if obj is None:
        Log.error("Cannot find the root object to add this object to.")
        return -1

      # Add a new object to the given object
      root, obj, path = self.objects.temporaryClone(obj, person = self.person)
      # If the object is to be created within a link, create it with a different name
      create_in_path = None
      if root.link is not None:
        folder = "dependency-%d" % (len(self.objects.infoFor(obj).get("contains", [])))
        create_in_path = os.path.realpath(os.path.join(obj.path, folder))
      new_obj = self.objects.write.addObjectTo(obj, self.person.identity, name=name, object_type=type, path=create_in_path, info=info)
    elif self.options.internal:
      new_obj = self.objects.write.create(name=name, object_type=type, root=None, path=None, info=info)
    else:
      # We are adding to the cwd
      path = "."
      base_path = path
      path = os.path.realpath(path)

      slug = "%s-%s" % (Object.slugFor(type), Object.slugFor(name))

      Log.header("Creating new %s" % (type))

      # Get the current root
      if root is None:
        obj, root = self.objects.retrieveLocal(path)

      # Get the current object
      if not obj is None:
        obj.root = root

      container_object = None
      if not root is None:
        container_object = self.objects.infoFor(root)

      object_path = os.path.join(base_path, slug)
      if obj:
        object_path = os.path.join(obj.path, slug)

      # Create directory, if it doesn't exist, warn if it does
      if os.path.exists(object_path):
        Log.error("directory %s already exists" % (object_path))
        return -1

      if obj:
        new_obj = self.objects.write.addObjectTo(obj, name, type, info=info)
      else:
        new_obj = self.objects.write.create(name=name, object_type=type, root=root, path=object_path, info=info)

    # Add empty workflow file
    data = {}
    self.objects.write.addFileTo(new_obj, "data.json", json.dumps(data))
    self.objects.write.commit(new_obj, self.person.identity, message="Initializes workflow.")

    # Store new object
    self.objects.write.store(new_obj, self.person.identity)

    # Set initial permissions
    self.permissions.update(new_obj.id, canRead=False, canWrite=False, canClone=False)
    self.permissions.update(new_obj.id, person_id=self.person.id, canRead=True, canWrite=True, canClone=True)

    ret = {}
    ret["updated"] = []

    for x in (new_obj.roots or []):
      ret["updated"].append({
        "id": x.id,
        "revision": x.revision,
        "position": x.position,
      })

    ret["updated"].append({
      "id": new_obj.id,
      "revision": new_obj.revision,
      "position": new_obj.position,
    })

    if self.options.to_json:
      Log.output(json.dumps(ret))
    else:
      Log.write("new object id: ", end ="")
      Log.output("%s" % (self.objects.infoFor(new_obj)['id']), padding="")
      Log.write("new object revision: ", end ="")
      Log.output("%s" % (new_obj.revision), padding="")

    Log.done("Successfully created %s %s" % (type, name))

    return 0
