# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json

from occam.commands.manager import command, option, argument

from occam.object import Object
from occam.log    import Log

from occam.manager import uses

from occam.objects.write_manager  import ObjectWriteManager
from occam.configurations.manager import ConfigurationManager
from occam.workflows.manager      import WorkflowManager
from occam.jobs.manager           import JobManager

@command('workflows', 'queue',
  category      = 'Workflow Management',
  documentation = "Queues a workflow or experiment to run in the background.")
@argument("workflow",   type = "object",
                        help = "The workflow or a containing object")
@uses(ObjectWriteManager)
@uses(ConfigurationManager)
@uses(WorkflowManager)
@uses(JobManager)
class QueueCommand:
  def do(self):
    # Get the workflow to run
    experiment = self.objects.resolve(self.options.workflow, person = self.person)

    if experiment is None:
      Log.error("cannot find workflow with id %s" % (self.options.workflow.id))
      return -1

    info = self.objects.infoFor(experiment)
    workflow = None
    if info.get('type') == "workflow":
      workflow   = experiment
      experiment = None
    elif info.get('type') == "experiment":
      for item in info.get('contains', []):
        if item.get('type') == "workflow":
          workflow = self.objects.retrieve(item.get('id'), revision = item.get('revision'), person = self.person)

    if workflow is None:
      Log.error("given object %s is not a workflow" % (self.options.workflow.id))
      return -1

    run = self.workflows.queue(workflow, experiment=experiment, person = self.person)
    self.jobs.launchDaemon()

    Log.output(json.dumps({
      "job": {
        "id": run.job_id
      },
      "run": {
        "id": run.id,
        "queueTime": run.queue_time and run.queue_time.isoformat()
      }
    }))

    Log.done("Successfully queued this workflow.")

    return 0
