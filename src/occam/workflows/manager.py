# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os, json

from types import SimpleNamespace

from occam.config     import Config
from occam.log        import loggable

from occam.manager import manager, uses

from occam.objects.write_manager  import ObjectWriteManager
from occam.manifests.manager      import ManifestManager
from occam.configurations.manager import ConfigurationManager
from occam.databases.manager      import DatabaseManager
from occam.jobs.manager           import JobManager

from occam.workflows.datatypes.workflow import Workflow

@loggable
@uses(ObjectWriteManager)
@uses(ManifestManager)
@uses(ConfigurationManager)
@uses(DatabaseManager)
@uses(JobManager)
@manager("workflows")
class WorkflowManager:
  """ This OCCAM manager handles workflows, configurations, and job generation.

  This generates the next set of tasks by permuting the current inputs.

  This doesn't take into account cycle detection and any possible optimizations.

  generateNextTasks(finishedJob):
    let run <- finishedJob.run
    let workflow <- run.workflow
    let finishedNode <- workflow.nodes[finishedJob.nodeIndex]

    let taskSet = []

    for each outputWire in finishedNode:
      let nextNode <- workflow.nodes[outputWire.connectedNode]

      let inputSet = []

      for each inputWire in nextNode:
        let relatedNode <- workflow.nodes[inputWire.connectedNode]

        if relatedNode is finishedNode:
          continue

        let inputs = []
        if inputWire.type == "configuration":
          if inputs.empty?:
            inputs <- [relatedNode.defaultConfiguration(inputWire)]
          else:
            inputs <- (Configuration)relatedNode.permute
        else:
          inputs <- relatedNode.outputs(inputWire).filter(published < finishedJob.finishTime)

        inputSet.permute(inputs)

      taskSet.append((nextNode, inputSet,))

    return taskSet
  """

  def create_workflow(self, object, person=None):
    info = self.objects.infoFor(object)
    try:
      data = self.objects.retrieveJSONFrom(object, info['file'])
    except:
      data = {}
    return Workflow(object, info, data)

  # We will turn a workflow into a set of workflows for each possible
  # configuration (taking into account ranges and the like)

  # We will then turn each of these into a set of tasks which can be queued
  # together

  def dataFor(self, workflowObject, person = None):
    """ Returns the data for the workflow.
    """

    workflow = self.create_workflow(workflowObject, person=person)
    return workflow.data

  def isNodeWorkflow(self, node, person = None):
    """ Returns True if the given node describes a workflow object.
    """
    return node.isType('workflow')

  def isNodeRunnable(self, node, person = None):
    """ Returns True if the given node describes a running object.
    """

    object = self.objects.retrieve(id       = node.id,
                                   revision = node.revision,
                                   version  = node.version,
                                   person   = person)
    info = self.objects.infoFor(object)

    # If this node is attached as input, then it does not run
    # It is input instead.

    # However, it may need to run its initial phase to determine
    # its command if it has a 'run.script' key and the wire
    # consuming it has an 'occam/runnable' tag.
    if len(node.self.get('connections', [])) > 0:
      if 'run' in info and 'script' in info['run']:
        return True
      return False

    info = self.objects.infoFor(object)

    return 'run' in info

  def jobsForNode(self, runId, nodeIndex):
    """ Returns a list of unique job ids pertaining to the given node.
    """

    import sql

    # Look up all run job records
    runJobs = sql.Table('run_jobs')

    session = self.database.session()

    query = runJobs.select(runJobs.job_id)
    query.where = (runJobs.run_id == runId) & \
                  (runJobs.node_index == nodeIndex)

    self.database.execute(session, query)
    rows = self.database.many(session, size=1000)

    return list(set([x["job_id"] for x in rows]))

  def generateRunForNode(self, workflow, info, run, person = None):
    """ Returns a list of objects and inputs to run for the given node.

    When withInput is specified, it indicates new data into an output wire.
    It will then use that data on the corresponding input wire(s) and permute
    the remaining wires.
    """

    # REMEMBER: if the same wire is connected to two or more inputs, it shares
    #           the data. It does not permute the possible data.

    ret = []

    node         = info.get('node')
    existingTask = info.get('task')
    taskPath     = info.get('path')

    # Get the node object
    object = self.objects.retrieve(id       = node.id,
                                   revision = node.revision,
                                   version  = node.version,
                                   person   = person)

    objectInfo = self.objects.infoFor(object)

    # Returns empty array when node is not runnable
    if not (self.isNodeRunnable(node, person=person) or self.isNodeWorkflow(node, person=person)):
      return ret

    nodeCanRun = True
    inputs = []
    for wireIndex, wire in enumerate(node.inputs):
      inputs.append([])

      # Look at configuration inputs and permute them when necessary to generate the
      # actual inputs for the run.
      if wire.get('type') == "configuration" and len(wire.get('connections', [])) == 0:
        # Permute default configurations

        # Add an imaginary source attached to this node
        inputs[-1].append([])

        schema = self.configurations.schemaFor(object, wireIndex, person = person)
        baseConfiguration = self.configurations.defaultsFor(schema)

        inputMetadata = objectInfo.get('inputs', [])[wireIndex]

        for configuration in self.configurations.permute({}, baseConfiguration):
          WorkflowManager.Log.write("Generated default configuration for %s for %s" % (wire.get('name'), node.name))
          # Generate the configuration object and store that as a possible input
          # We will permute all the configuration inputs when we build tasks

          # This would generate a uuid that hashes the configuration
          #import uuid
          #print(uuid.uuid5(uuid.NAMESPACE_URL, json.dumps(configuration, sort_keys=True)))

          # Create the configuration object
          configObject = self.objects.write.create(name="configuration", object_type="configuration", info = {
            "file": inputMetadata.get('file', 'config.json'),
            "schema": {
              "id": objectInfo.get('id'),
              "revision": object.revision,
              "file": inputMetadata.get('schema'),
              "type": "application/json",
              "name": objectInfo.get('name')
            }
          }, identity = person.identity)

          # Store the generated configuration
          self.objects.write.addFileTo(configObject, inputMetadata.get('file', 'config.json'), json.dumps(configuration))
          self.objects.write.commit(configObject, person.identity, message="Adds configuration")
          self.objects.write.store(configObject, person.identity)

          # TODO: Make sure to tie the subset of updated parameters to the output

          # Add it is an input set for this wire
          inputs[-1][-1].append([configObject])

      # Otherwise, look at any other type of input
      else:
        objects = self.resolveInputs(run, workflow, node, wireIndex, person)

        if len(objects) == 0 and len(wire.get('connections', [])) != 0:
          nodeCanRun = False

        for i, input_object in enumerate(objects):
          # Make room for the source
          inputs[-1].append([])

          if wire.get('type') == "configuration":
            # We need to append all permutations of this object

            schema = self.configurations.schemaFor(input_object, person = person)
            data = self.configurations.dataFor(input_object)
            baseConfiguration = self.configurations.defaultsFor(schema)
            subObjectInfo = self.objects.infoFor(input_object)

            inputMetadata = objectInfo.get('inputs', [])[wireIndex]
            ranges = self.configurations.rangedValuesFor(data, schema)
            configurationSpace = self.configurations.expandRangedValue(ranges)
            for configuration in self.configurations.permute(configurationSpace, baseConfiguration):
              WorkflowManager.Log.write("Generated configuration for %s for %s" % (wire.get('name'), node.name))
              # Generate the configuration object and store that as a possible input
              # We will permute all the configuration inputs when we build tasks

              # This would generate a uuid that hashes the configuration
              # We should ensure that common configurations are not constantly recreated
              #import uuid
              #print(uuid.uuid5(uuid.NAMESPACE_URL, json.dumps(configuration, sort_keys=True)))

              # Create the configuration object
              configObject = self.objects.write.create(name="configuration", object_type="configuration", info = {
                "file": inputMetadata.get('file', 'config.json'),
                "schema": {
                  "id": objectInfo.get('id'),
                  "revision": input_object.revision,
                  "file": inputMetadata.get('schema'),
                  "type": "application/json",
                  "name": objectInfo.get('name')
                },
                "generator": [{
                  "id": input_object.id,
                  "revision": input_object.revision,
                  "file": subObjectInfo.get('file'),
                  "name": subObjectInfo.get('name'),
                  "type": subObjectInfo.get('type'),
                }]
              }, identity = person.identity)

              # Store the generated configuration
              self.objects.write.addFileTo(configObject, inputMetadata.get('file', 'config.json'), json.dumps(configuration))
              self.objects.write.commit(configObject, person.identity, message="Adds configuration")
              self.objects.write.store(configObject, person.identity)

              # TODO: Make sure to tie the subset of updated parameters to the output

              # Add it is an input set for this wire
              inputs[-1][-1].append([configObject])
          else: # Not configuration
            input_info = self.objects.infoFor(input_object)
            input_info['revision'] = input_object.revision
            input_info['id']  = input_object.id
            input_info['uid'] = input_object.uid

            # If this is a runnable self object, we need to include the run
            # report as the command.
            source = self.resolveInputSource(workflow, node, wireIndex, i)
            if input_info.get('run', {}).get('script') and self.isInputSourceSelf(workflow, node, wireIndex, i):
              # Get the job that ran this and determine its report
              jobs = self.jobsFor(run.id, nodeIndex = source.index).get('nodes', {}).get(source.index, {}).get('jobs', [])
              for job in jobs:
                # TODO: handle these permutations
                job_rows = self.jobs.retrieve(job_id = job['id'])
                job_db = job_rows[0]
                task = self.jobs.taskForJob(job_db, person=person)
                task = self.objects.infoFor(task)
                rootPath = os.path.join(job_db.path, "..")
                runReport = self.jobs.pullRunReport(task, rootPath)

                # We want to lock the input's index to what it was before
                objectIndex = task['runs'].get('index')
                if objectIndex:
                  input_info['index'] = objectIndex

                if isinstance(runReport, list) and len(runReport) > 0:
                  runReport = runReport[0]

                if runReport:
                  input_info['run'] = input_info.get('run', {})
                  input_info['run'].update(runReport)

                # We then, also, want to add the input's runtime
                # dependencies as normal dependencies.
                input_info['dependencies'] = input_info.get('dependencies', [])
                input_info['dependencies'].extend(input_info.get('run', {}).get('dependencies', []))

            inputs[-1][-1].append([input_info])

    # Permute the input sets and generate partial tasks
    # TODO: permute
    # for now we will take the first item in each input
    runs = self.permuteRun(inputs)
    ret = []

    # TODO: ignore nodes that already executed until loops are fully supported
    import sql
    runJobsTable = sql.Table("run_jobs")
    session = self.database.session()
    query = runJobsTable.select(where = (runJobsTable.run_id == run.id))
    self.database.execute(session, query)

    rows = self.database.many(session, size=1000)
    for row in rows:
      if row.get("node_index") == node.index:
        return [ret]

    for run in runs:
      newNode = node.raw.copy()
      newNode['input'] = run
      if existingTask:
        newNode['task'] = existingTask
        newNode['path'] = taskPath
      ret.append(newNode)

    # If there are no inputs connected, then it still needs to run
    if (nodeCanRun and len(runs)==0):
      newNode = node.raw.copy()
      newNode['input'] = []
      if existingTask:
        newNode['task'] = existingTask
        newNode['path'] = taskPath
      ret.append(newNode)

    return [ret]

  def permuteRun(self, runSpace):
    """
    """

    if len(runSpace) == 0:
      return []

    # For each permutation of the remaining inputs,
    # We create a run by adding this input
    runs = self.permuteRun(runSpace[:-1])
    currentCount = len(runs)

    currentWire = runSpace[-1]

    for index, wire in enumerate(currentWire):
      for sourceIndex, item in enumerate(wire):
        # If we have no permutations yet, just add the input list as the
        # first run. We will permute this set with other inputs below.
        if currentCount == 0:
          runs.append([item])

        # Add the run to the current list (if there exists one)
        for input in runs[0:currentCount]:
          if sourceIndex == 0:
            # First, go through each run and add the given item
            # This updates the basic runs, but for the next permutation
            # of this particular input, we go to the bottom else case.
            input.append(item)
          else:
            # Duplicate the existing run input set.
            runs.append(input[:])

            # Recall above that we added the item to the existing set,
            # So now we simply replace that added item with this new item
            runs[-1][-1] = item

    if len(currentWire) == 0:
      if currentCount == 0:
        runs.append([[]])

      for input in runs[0:currentCount]:
        input.append([])

    return runs

  def tailNodes(self, workflowObject, person = None):
    """ Returns a list of nodes that have a dangling output.
    """

    workflow = self.create_workflow(workflowObject, person=person)
    return workflow.getTailNodes()

  def generateTasks(self, experiment, run, person = None):
    """ Returns a set of task objects for the given workflow run.
    """

    ret = []

    for node in run:
      # Look at the node and its inputs/outputs
      # If it is runnable, generate a task

      # Get the object
      uuid     = node['id']
      revision = node['revision']

      obj = self.objects.retrieve(id       = uuid,
                                  revision = revision,
                                  person   = person)

      if obj is None:
        return None

      inputs = node.get('input', [])

      existingTask = node.get('task')
      taskPath = node.get('path')

      task = self.manifests.run(object    = obj,
                                id        = uuid,
                                revision  = revision,
                                inputs    = inputs,
                                generator = experiment,
                                person    = person)

      ret.append(task)

    return ret

  def runTask(self, task):
    """
    """

  def lockRun(self, runId):
    """ Locks the run record to give exclusive access to modifying the run metadata.

    Will return None if the run cannot be locked, such as if it does not exist.
    """

    # Get the initial run
    run_db = self.retrieveRun(runId)

    # This run does not exist
    if run_db is None:
      return None

    import sql
    import time

    runs = sql.Table('runs')

    session = self.database.session()

    while True:
      # Attempt to lock the record
      query = runs.update(where   = (runs.id == runId) & (runs.lock == 0),
                          columns = [runs.lock],
                          values  = [1])

      count = self.database.execute(session, query)
      self.database.commit(session)

      # If the record was updated, then we succeeded
      if count == 1:
        return run_db

      # Sleep to reduce the query load and so we don't DoS the database
      time.sleep(1)

    # Could not lock
    return None

  def unlockRun(self, runRecord):
    """ Unlocks the run record so that it may be amended.
    """

    # We don't check at all since we should already be the only owner
    runRecord.lock = 0

    # Just save it and get on with our lives
    session = self.database.session()
    self.database.update(session, runRecord)
    self.database.commit(session)

    # Return the run record
    return runRecord

  def queue(self, object, experiment=None, person = None):
    """ Creates a queued workflow and spawns initial jobs.
    """

    # Discover the workflow
    objInfo = self.objects.infoFor(object)
    #try:

    workflow = self.resolve(object, person=person)
    #except ValueError:
    #  return None
    #except:
    #  WorkflowManager.Log.error("Unknown exception.")
    #  WorkflowManager.Log.error(
    #    "Object is of type %s and does not contain a workflow." %
    #      (objInfo.get('type', 'object'))
    #  )
    #  return None

    job = self.createJob(workflow,
                         initialize="workflows",
                         person=person)
    run = self.createRun(experiment or object, workflow, job, person)

    self.mapJobToRun(run.id, job.id, None)

    # Return the run record
    return run

  def rootJobFor(self, jobId):
    """ Returns the root job for the given job id.
    """

    run = self.runFor(jobId)

  def runJobFor(self, jobId):
    """ Returns the RunJobRecord for the given job.
    """

    return self.datastore.retrieveRunJobFor(jobId)

  def nodesIn(self, workflow, person = None):
    """ Returns the nodes within the given workflow.
    """
    return workflow.nodes

  def nodeAt(self, workflow, nodeIndex, person = None):
    """ Returns information about the node at the given index in the given workflow.
    """
    return self.nodesIn(workflow, person = person)[nodeIndex]

  def launch(self, object, run, person=None, inputs=None):
    """ This function takes a workflow and runs it.

    Arguments:
      object(Object): The OCCAM workflow object
      person(Object): The person running the workflow
      inputs(list): A list of inputs to override the workflow connections
    """

    # Store the object info: it can be an experiment
    # that contains a workflow
    objInfo = self.objects.infoFor(object)

    # Get the actual workflow (either the object itself
    # or the first workflow in it)

    workflow = self.resolve(object, person=person)

    # Get a list of nodes that can execute, i.e. nodes
    # that do not depend on other results
    nodes = self.getInitialNodes(workflow, person)

    # Generate tasks for this node with new inputs
    tasks = self.generateNextTasks(nodes       = nodes,
                                   finishedJob = None,
                                   run         = run,
                                   person      = person)

    for node, task in tasks:
      taskInfo = self.objects.infoFor(task)
      job = self.createJob(task,
                           run_owner=run,
                           existingPath=None,
                           person=person)
      self.mapJobToRun(run.id, job.id, node.index)
      WorkflowManager.Log.write("Created job %s." % (job.id))

  def completeJob(self, job, person = None):
    """
    """

    nodes = self.nextNodes(finishedJob = job,
                           person      = person)

    run = self.runFor(job.id)

    # Generate tasks for this node with new inputs
    tasks = self.generateNextTasks(nodes       = nodes,
                                   finishedJob = job,
                                   run         = run,
                                   person      = person)

    for node, task in tasks:
      taskInfo = self.objects.infoFor(task)
      job = self.createJob(task,
                           run_owner=run,
                           existingPath=None,
                           person=person)
      self.mapJobToRun(run.id, job.id, node.index)
      WorkflowManager.Log.write("Created job %s." % (job.id))

  def finish(self, run):
    """ Completes a run as a success.
    """

    import datetime
    run.finish_time = datetime.datetime.now()

    session = self.database.session()

    self.database.update(session, run)
    self.database.commit(session)

  def fail(self, run):
    """ Completes a run as a failure.
    """

    import datetime
    run.failure_time = datetime.datetime.now()

    session = self.database.session()

    self.database.update(session, run)
    self.database.commit(session)

  def objectForRun(self, runId, person = None):
    """ Returns the containing object that has spawned the workflow run.
    """

    run_db = self.retrieveRun(runId)

    ret = self.objects.retrieve(id       = run_db.object_uid,
                                revision = run_db.object_revision,
                                person   = person)

    return ret

  def workflowForRun(self, runId, person = None):
    """ Returns the Object representing the workflow that is reflected in the run.
    """

    run_db = self.retrieveRun(runId)

    ret = self.objects.retrieve(id       = run_db.workflow_uid,
                                revision = run_db.workflow_revision,
                                person   = person)

    return ret

  def runsForObject(self, object, revision = None, person=None):
    """ Returns a RunRecord list for the given object.

    When the given revision is None, then this will return all runs. Otherwise,
    it will limit the search to the given revision.
    """

    from occam.workflows.records.run import RunRecord

    import sql

    session = self.database.session()

    runs = sql.Table('runs')

    query = runs.select()
    query.where = (runs.object_uid == object.id)

    if revision:
      query.where = (runs.object_revision == revision)

    self.database.execute(session, query)
    rows = self.database.many(session, size=1000)

    return [RunRecord(x) for x in rows]

  def mapJobToRun(self, run_id, job_id, nodeIndex):
    """ Attaches the given job to the given run.
    """

    from occam.workflows.records.run_job import RunJobRecord

    session = self.database.session()

    runJob_db  = RunJobRecord()
    runJob_db.run_id = run_id
    runJob_db.job_id = job_id
    runJob_db.node_index = nodeIndex

    self.database.update(session, runJob_db)
    self.database.commit(session)

    return runJob_db

  def retrieveRun(self, runId, object = None):
    """ Returns the RunRecord for the given run id.
    """

    from occam.workflows.records.run import RunRecord

    import sql

    session = self.database.session()

    runs = sql.Table('runs')

    query = runs.select()
    query.where = (runs.id == int(runId))

    if object is not None:
      query.where = query.where & (runs.object_uid == object.id)

    self.database.execute(session, query)
    ret = self.database.fetch(session)

    if ret:
      ret = RunRecord(ret)

    return ret

  def runFor(self, jobId):
    """ Returns the RunRecord corresponding to the given job.
    """

    return self.datastore.retrieveRunFor(jobId)

  def nodesFor(self, runId):
    """ Yields nodes information about this run.
    """

  def jobsFor(self, runId, status = None, statusNot = None, nodeIndex = None):
    """ Yields the job ids for the given run.
    """

    import sql

    session = self.database.session()

    runs = sql.Table('runs')
    runJobs = sql.Table('run_jobs')
    jobs = sql.Table('jobs')

    query = runJobs.select(runJobs.job_id, runJobs.node_index)
    query.where = (runJobs.run_id == runId)
    if nodeIndex is not None:
      query.where = query.where & (runJobs.node_index == nodeIndex)

    join = query.join(jobs, type_ = "LEFT")
    join.condition = (join.right.id == query.job_id)
    if status is not None:
      if not isinstance(status, list):
        status = [status]
      join.condition = join.condition & (join.right.status.in_(status))
    if statusNot is not None:
      if not isinstance(statusNot, list):
        statusNot = [statusNot]
      for statusCheck in statusNot:
        join.condition = join.condition & (join.right.status != statusCheck)
    query = join.select()

    self.database.execute(session, query)
    rows = self.database.many(session, size=1000)

    ret = {}

    for x in rows:
      if x['node_index'] is None:
        continue
      if x['status'] is None:
        continue
      ret[x['node_index']] = ret.get(x['node_index'], {"jobs": []})
      ret[x['node_index']]['jobs'].append({"id": x['job_id'],
                                           "status": x['status'],
                                           "kind": x['kind'],
                                           "startTime": x['start_time'] and x['start_time'].isoformat(),
                                           "queueTime": x['queue_time'] and x['queue_time'].isoformat()})

    return {"nodes": ret}

  def isInputSourceSelf(self, workflow, node, wireIndex, index):
    pin = node.followInputWire(index)[index]
    return pin[1] < 0

  def resolveInputSourceWire(self, workflow, node, wireIndex, index):
    pin = node.followInputWire(wireIndex)[index]
    if pin[1] < 0:
      # It is the self pin
      return workflow.nodes[pin[0]].self

    return workflow.nodes[pin[0]].outputs[pin[1]]

  def resolveInputSource(self, workflow, node, wireIndex, index):
    pin = node.followInputWire(wireIndex)[index]
    return workflow.nodes[pin[0]]

  ##
  #
  #  This function takes an OCCAM object and returns it as a Workflow class
  #    or the first workflow contained in the object.
  #
  #  @param self The object pointer.
  #  @param object The OCCAM object
  #  @param person The person running the workflow
  #
  def resolve(self, object, person=None):
    try:
      workflow = self.create_workflow(object)
    except ValueError:
      try:
        workflow_object = self._getTypesWithin(object, "workflow", person)
        workflow_object = workflow_object[0]
        workflow = self.create_workflow(object)
      except ValueError as e:
        WorkflowManager.Log.error(
          "Object is of type %s and does not contain a workflow." %
            (objInfo.get('type', 'object'))
        )
        raise e
    return workflow

  def generateDefaultConfigurations(self, node, wireIndex, person = None):
    """ Creates, if needed, default configurations for the given node and wire.

    Args:
      node (Node): The workflow node to create the configurations for.
      wireIndex (int): The index of the input wire.
      person (Person): The person to authenticate as.

    Returns:
      list: A set of Object items for each configuration.
    """

    # Our goal is to have a list of configuration Objects
    ret = []

    # We have to resolve the object for the node
    object = self.objects.retrieve(id       = node.id,
                                   revision = node.revision,
                                   person   = person)

    objectInfo = self.objects.infoFor(object)
    inputMetadata = objectInfo.get('inputs', [])[wireIndex]

    # Get the base schema for this configuration
    schema = self.configurations.schemaFor(object, wireIndex, person = person)

    # Get the default configuration
    baseConfiguration = self.configurations.defaultsFor(schema)

    # This will permute the basic configuration
    for configuration in self.configurations.permute({}, baseConfiguration):
      WorkflowManager.Log.write("Generated default configuration for %s for %s" % (inputMetadata.get('name'), node.name))
      # Generate the configuration object and store that as a possible input
      # We will permute all the configuration inputs when we build tasks

      # This would generate a uuid that hashes the configuration
      #import uuid
      #print(uuid.uuid5(uuid.NAMESPACE_URL, json.dumps(configuration, sort_keys=True)))

      # Create the configuration object
      configObject = self.objects.write.create(name="configuration", object_type="configuration", info = {
        "file": inputMetadata.get('file', 'config.json'),
        "schema": {
          "id": object.id,
          "revision": object.revision,
          "file": inputMetadata.get('schema'),
          "type": "application/json",
          "name": objectInfo.get('name')
        }
      }, identity = person.identity)

      # Store the generated configuration
      self.objects.write.addFileTo(configObject, inputMetadata.get('file', 'config.json'), json.dumps(configuration))
      self.objects.write.commit(configObject, person.identity, message="Adds configuration")
      self.objects.write.store(configObject, person.identity)

      # TODO: Make sure to tie the subset of updated parameters to the output
      ret.append(configObject)

    return ret

  def generateConfigurations(self, node, wireIndex, inputObject, person = None):
    # Our goal is to have a list of configuration Objects
    ret = []

    # We have to resolve the object for the node
    object = self.objects.retrieve(id       = node.id,
                                   revision = node.revision,
                                   person   = person)

    objectInfo = self.objects.infoFor(object)
    inputMetadata = objectInfo.get('inputs', [])[wireIndex]

    schema = self.configurations.schemaFor(inputObject, person = person)
    data = self.configurations.dataFor(inputObject)
    baseConfiguration = self.configurations.defaultsFor(schema)
    subObjectInfo = self.objects.infoFor(inputObject)

    ranges = self.configurations.rangedValuesFor(data, schema)
    configurationSpace = self.configurations.expandRangedValue(ranges)
    for configuration in self.configurations.permute(configurationSpace, data):
      if configuration == data:
        # This permutation is the same as the base configuration
        # Which means, we aren't permuting!
        return [inputObject]

      WorkflowManager.Log.write("Generated configuration for %s for %s" % (inputMetadata.get('name'), node.name))

      # Generate the configuration object and store that as a possible input
      # We will permute all the configuration inputs when we build tasks

      # This would generate a uuid that hashes the configuration
      # We should ensure that common configurations are not constantly recreated
      #import uuid
      #print(uuid.uuid5(uuid.NAMESPACE_URL, json.dumps(configuration, sort_keys=True)))

      # Create the configuration object
      configObject = self.objects.write.create(name="configuration", object_type="configuration", info = {
        "file": inputMetadata.get('file', 'config.json'),
        "schema": {
          "id": objectInfo.get('id'),
          "revision": inputObject.revision,
          "file": inputMetadata.get('schema'),
          "type": "application/json",
          "name": objectInfo.get('name')
        },
        "generator": [{
          "id": inputObject.id,
          "revision": inputObject.revision,
          "file": subObjectInfo.get('file'),
          "name": subObjectInfo.get('name'),
          "type": subObjectInfo.get('type'),
        }]
      }, identity = person.identity)

      # Store the generated configuration
      self.objects.write.addFileTo(configObject, inputMetadata.get('file', 'config.json'), json.dumps(configuration))
      self.objects.write.commit(configObject, person.identity, message="Adds configuration")
      self.objects.write.store(configObject, person.identity)

      # TODO: Make sure to tie the subset of updated parameters to the output
      ret.append(configObject)

    return ret

  def nextNodes(self, finishedJob, person = None):
    """ This function returns a list of possible nodes to run after the given job completed.
    """

    # Get the Run this job is a part of, which represents the running workflow
    run = self.runFor(finishedJob.id)

    # Now, get the workflow object
    object = self.objects.retrieve(id       = run.workflow_uid,
                                   revision = run.workflow_revision,
                                   person   = person)

    if object is None:
      WorkflowManager.Log.error(
        "Could not find object %s@%s." %
          (run.workflow_uid, run.workflow_revision)
      )
      raise ValueError

    workflow = self.resolve(object, person=person)

    # First, get the node for this job
    runJob = self.runJobFor(finishedJob.id)
    # TODO: REMOVE THIS
    #runJob.node_index = 1
    finishedNode = workflow.nodes[runJob.node_index]

    # Next, we want the possible outputs of this node, which will be nodes we
    # may wish to execute next.
    visited_nodes   = []
    candidate_nodes = []

    for idx, output in enumerate([finishedNode.self, *finishedNode.outputs]):
      idx = idx - 1
      for output_node_to in finishedNode.followOutputWire(idx):
        node_idx = output_node_to[0]
        if node_idx not in visited_nodes:
          visited_nodes.append(node_idx)
          node = workflow.nodes[node_idx]
          candidate_nodes.append((node, output_node_to[1],))

    return candidate_nodes

  def generateNextTasks(self, nodes, finishedJob, run, person = None):
    """ This function generates new tasks based on the completion of a given job.
    """

    # Our goal is to create a list of tasks
    ret = []

    # Now, get the workflow object
    object = self.objects.retrieve(id       = run.workflow_uid,
                                   revision = run.workflow_revision,
                                   person   = person)

    if object is None:
      WorkflowManager.Log.error(
        "Could not find object %s@%s." %
          (run.workflow_uid, run.workflow_revision)
      )
      raise ValueError

    experiment = self.objectForRun(run.id, person=person)

    workflow = self.resolve(object, person=person)

    finishedNode = None

    if finishedJob:
      runJob = self.runJobFor(finishedJob.id)
      finishedNode = workflow.nodes[runJob.node_index]

    # For each of those possible running nodes, we want to look at any output
    # from previously executed jobs (not counting this finished node) and
    # determine how many times we will run this next node.

    # Our goal is to have a set of tasks (that we must permute and generate)
    taskSet = []

    # For each node to run...
    for node, wireIndex in nodes:
      # Our goal is to determine all inputs on each wire
      inputSet = [[]]

      # For each input wire...
      for inputWireIndex, inputWire in enumerate(node.inputs):
        # Get the input nodes attached to that wire
        input_nodes = node.followInputWire(inputWireIndex)

        # Our goal is to gather all possible outputs
        # We will permute based on the previous finished jobs
        wireSet = [[]]

        # Deal with the default configuration
        # (when nothing is attached to a configuration wire)
        if inputWire.get('type') == 'configuration' and len(input_nodes) == 0:
          configObjects = self.generateDefaultConfigurations(node      = node,
                                                             wireIndex = inputWireIndex,
                                                             person    = person)

          for configObject in configObjects:
            # Add it is a 'generated output' (although we generated it just now)
            wireSet = [[SimpleNamespace(object = configObject)]]
        else:
          # When there are nodes attached to this node at this wire,
          # For each such attached node...
          for input_node_to in input_nodes:
            nodeIndex       = input_node_to[0] # The attached node
            outputWireIndex = input_node_to[1] # The wire on the attached node

            inputNode = workflow.nodes[nodeIndex]
            inputNodeSet = []

            if outputWireIndex == -1:
              # The incoming node is passing itself as input

              # TODO: worry about 'script' run phases

              # Resolve the node
              inputNodeSet.append([SimpleNamespace(object_uid = inputNode.id, object_revision = inputNode.revision)])
            elif finishedNode:
              # Look at what the incoming node produced on that wire

              # If this is the node that just ran, only use the new output
              if nodeIndex == finishedNode.index:
                # We pass a single group of output
                inputNodeSet.append(self.jobs.outputsFor(finishedJob, wireIndex = outputWireIndex))
              else:
                # Gather finished jobs (that predate the finished one) pertaining to
                # the incoming nodes
                jobIds = self.jobsForNode(run.id, nodeIndex = nodeIndex)

                # Gather the outputs for all of these jobs
                # We will get an array where every item is a set of output for each previous job
                inputNodeSet.extend(self.jobs.outputsForAll(jobIds, wireIndex  = outputWireIndex,
                                                                    finishTime = finishedJob.finish_time))

            # At this point inputNodeSet will be an array of sets of output
            # Each item in the array should be used in a unique job (task)
            # So, we have to now maintain the unique possible jobs by permuting the wire set

            # First, pull out the object information to conform with other sections
            inputNodeSet = map(lambda outputSet:
                                 list(map(lambda jobOutput:
                                            SimpleNamespace(id       = jobOutput.object_uid,
                                                            revision = jobOutput.object_revision,
                                                            object   = None),
                                          outputSet)),
                               inputNodeSet)

            # Permute the wire set by generating a new wire set that contains this data
            newWireSet = []

            # For each new input, permute the existing known input sets
            newInputNodeSet = list(inputNodeSet)
            for newInput in newInputNodeSet:
              # This newInput is the set of input objects that needs to run within the task
              # We need to combine with the set of input objects already in the list
              for wireInputs in wireSet:
                wireInputSet = list(wireInputs)[:]
                wireInputSet.extend(list(newInput))
                newWireSet.append(wireInputSet)

            wireSet = newWireSet

        # pastOutputs has a set of possible input sets on that wire
        # That means, this might be an array of three items: [a], [b, c], [d]
        #   Which means it wants to spawn three jobs for this wire, one with [a]
        #   on it, one with [b, c] on it, and another with [d]
        # We have to now merge this with our complete understanding of this node.
        print("-------------- WIRE SET -----------------")
        print(wireSet)
        print("-------------- -------- -----------------")

        # Resolve each new input (as an iterator)
        newInputs = map(lambda outputSet:
                          list(map(lambda output:
                                     output.object or
                                     self.objects.retrieve(id       = output.id,
                                                           revision = output.revision,
                                                           person   = person),
                                   outputSet)),
                        wireSet)

        # If this is a configuration, permute the input objects
        if inputWire.get('type') == 'configuration':
          newInputs = list(newInputs)
          permutedInputSet = []

          # For each configuration...
          for newInputSet in newInputs:
            for newInput in newInputSet:
              # Permute the configuration and return a list of config objects
              configObjects = self.generateConfigurations(node        = node,
                                                          wireIndex   = inputWireIndex,
                                                          inputObject = newInput,
                                                          person      = person)

              # Add those to our updated list
              for config in configObjects:
                permutedInputSet.append([config])

          # Replace the known list of inputs with the permuted configuration list
          newInputs = permutedInputSet

        newNodeSet = []

        # For each new input, permute the existing known input sets
        newInputs = list(newInputs)
        for newInput in newInputs:
          for nodeInputs in inputSet:
            nodeInputSet = list(nodeInputs)[:]
            nodeInputSet.append(newInput)
            newNodeSet.append(nodeInputSet)

        inputSet = newNodeSet

        # For each possible output for this node, permute the input set
        # Permute them... for every previous option, add each of these new inputs
        #nodeSet.append(list(newInputs))

      taskSet.append((node, inputSet,))

    # From this point, taskSet will be, sadly to some, an array of arrays.

    # [ (node, <-- Each unique runnable candidate node (tuple with node and inputs)
    #   [ <-- Each possible task for that node (permutations of inputs)
    #     [ <-- Each input wire for that node
    #       [ <-- Each object that will be given as input to that wire/node/task
    # ] ] ]) ] Whew!

    # Then, we can generate those tasks
    for node, inputSet in taskSet:
      # Get the object
      uuid     = node.id
      revision = node.revision

      obj = self.objects.retrieve(id       = uuid,
                                  revision = revision,
                                  person   = person)

      if obj is None:
        return None

      for inputs in inputSet:
        ret.append((node, self.manifests.run(object    = obj,
                                             id        = uuid,
                                             revision  = revision,
                                             inputs    = inputs,
                                             generator = experiment,
                                             person    = person),))

    # Finally, we return the realized task set
    return ret

  def createJob(self, workflow, run_owner=None, run=None, existingPath = None, initialize=None, person=None):
    """ This function creates a job for the input object.

    Arguments:
      object(Object): The object that will be run by the job
      run_owner(RunRecord): The run that is executing the job
      run(RunRecord): The run that the job is executing (if workflow, not task)
      person(Object): The person running the workflow

    Returns:
      JobRecord: the created job
    """

    objectInfo = workflow.info

    # Top level workflows have no run_owner and aren't finalized
    finalize = None
    finalizeTag = None
    if run_owner is not None:
      finalize = "workflows"
      finalizeTag = str(run_owner.id)

    job = self.jobs.create(task         = objectInfo,
                           taskId       = workflow.id,
                           revision     = workflow.revision,
                           person       = person,
                           finalize     = finalize,
                           finalizeTag  = finalizeTag,
                           initialize   = initialize,
                           existingPath = existingPath)
    return job

  def _runExecutedByJob(self, job_id):
    """ Get the run that a job is executing.

    Arguments:
      job_id(int): The id of the job

    Returns:
      RunRecord: The run the job is executing, or None.
    """

    import sql

    # Get the run that is executed by job_id
    # Get connection job->run to identify which run is executed by job_id
    runJob = self.runJobFor(jobId=job_id)
    if runJob is None:
      WorkflowManager.Log.warning(
        "Job %s is not running a workflow." %
          (job_id)
      )
      return None

    # Get the run that job_id executes
    run = self.retrieveRun(runJob.run_id)
    if run is None:
      WorkflowManager.Log.error(
        "Could not find run %s." %
          (runJob.run_id)
      )
      return None

    return run
  #
  #  @param self The object pointer.
  #  @param workflow The workflow to inspect.
  #  @param person The person performing the operation
  #
  def getInitialNodes(self, workflow, person=None):
    nodes = []
    visited_nodes=[]

    # Get all head nodes (nodes without connected inputs)
    headNodes = workflow.getHeadNodes()
    for nodeIndex, node in enumerate(headNodes):
      # if one of those is a non executable input, follow the wire
      if not (self.isNodeRunnable(node, person=person) or
              self.isNodeWorkflow(node, person=person)):
        outputs = []
        outputs.append(node.self)
        outputs.extend(node.outputs)
        for wireIndex, wire in enumerate(outputs):
          for output in wire.get('connections', []):
            subNodeIndex, inputIndex, position = output.get('to')

            # Ignore visited nodes
            if subNodeIndex in visited_nodes:
              continue

            # Keep a list of visited nodes to avoid inserting the same node
            visited_nodes.append(subNodeIndex)
            visited_nodes=list(set(visited_nodes))
            subNode = workflow.nodes[subNodeIndex]
            nodes.append((subNode, -1,))
      else:
        if nodeIndex in visited_nodes:
          continue

        # Keep a list of visited nodes to avoid inserting the same node
        visited_nodes.append(nodeIndex)
        visited_nodes=list(set(visited_nodes))
        nodes.append((node, -1,))

    return nodes

  def _getRunnableNodes(self, workflow, run, person=None):
    # TODO: Extend to cyclic graphs
    nodes = []
    import sql
    from occam.workflows.records.run_job import RunJobRecord

    # Get already executed nodes
    runJobsTable = sql.Table("run_jobs")
    session = self.database.session()
    query = runJobsTable.select(where = (runJobsTable.run_id == run.id))
    self.database.execute(session, query)
    rows = self.database.many(session, size=1000)

    # Nothing was executed yet
    if len(rows) == 0:
      nodes = self._getInitialNodes(workflow,person)
    else:
      #TODO: Get next nodes
      for row in rows:
        node_idx = row["node_index"]
        executed_node = workflow.nodes[node_idx]
        self._getNodeOutputs(run, executed_node)
        for output_idx, output in enumerate(workflow.nodes[node_idx].outputs):
          # get indices (node, port)
          next_nodes_to = executed_node.followOutputWire(output_idx)
          next_nodes=[]
          # convert indexes to nodes ignoring dulpicates
          nodes_visited = []
          for next_node_to in next_nodes_to:
            if next_node_to[0] not in nodes_visited:
              nodes_visited.append(next_node_to[0])
              next_nodes.append(workflow.nodes[next_node_to[0]])


          for next_node in next_nodes:
            # TODO: Check all possible runs for this node (sets of inputs)
            #   This requires going to the database, and ask for all executable nodes, and inputs.
            #   Then, go through the job DB and check if that has already run...

            can_execute=True
            inputs=[]
            for input_idx, input in enumerate(next_node.inputs):
              # Assuming only one
              node_in_input_to = next_node.followInputWire(output_idx)[0]
              input_node = workflow.nodes[node_in_input_to[0]]
              # Check if input is connected to self's
              # Append those to list
              inputs.append(self._getNodeOutputs(run, input_node)[node_in_input_to[1]])
              # For each permutation of inputs, chck if there is already a job with such a task
            if can_execute:
              nodes.append(next_node)
      pass
    return nodes

  def createRun(self, owner, workflow, job, person=None):
    from occam.workflows.records.run import RunRecord
    session = self.database.session()

    run_db = RunRecord()

    # Unlock at first
    run_db.lock = 0

    # Establish the origin workflow container
    ownerInfo = self.objects.infoFor(owner)
    run_db.object_name     = ownerInfo.get('name')
    run_db.object_uid      = owner.id
    run_db.object_revision = owner.revision

    run_db.workflow_name     = workflow.name
    run_db.workflow_uid      = workflow.id
    run_db.workflow_revision = workflow.revision

    # Set up root job
    run_db.job_id = job.id

    # Establish when this is queued
    import datetime
    run_db.queue_time = datetime.datetime.now()

    # Establish who asked for this
    if person:
      run_db.person_uid = person.id

    print("--")
    print(ownerInfo)
    print(ownerInfo.get('name'))
    print(ownerInfo.get('id'))
    print(run_db._data)

    # Add to the database
    self.database.update(session, run_db)
    self.database.commit(session)

    return run_db

  def g(self, obj):
    return self.objects.retrieve(id = obj.id).id

  # TODO: Maybe add this to class object
  def _getTypesWithin(self, object, type, person=None):
    ret = []
    # Determine the actual workflow
    objInfo = self.objects.infoFor(object)

    # Look for a workflow within
    for contained in objInfo.get('contains', []):
      if contained.get('type') == type:
        ret.append(  self.objects.retrieve(id       = contained.get('id'),
                                           revision = contained.get('revision'),
                                           person   = person)
        )
    return ret

  def terminate(self, run):
    super_job = self.jobs.retrieve(job_id=run.job_id)[0]
    # Make sure the init process has finished!
    while (super_job.status != "finished") and (super_job.status != "failed"):
      import time
      time.sleep(2)
      WorkflowManager.Log.write("Waiting for init job (%d) to finish..."%run.job_id)
      super_job = self.jobs.retrieve(job_id=run.job_id)[0]

    jobs = self.jobsFor(run.id,
                        statusNot = ["failed", "finished"])
    for _, value in jobs["nodes"].items():
      for job_info in value["jobs"]:
        # Retrieve the job by its id, fail if it is not found
        jobs = self.jobs.retrieve(job_id=job_info["id"])
        if len(jobs) == 0:
          WorkflowManager.Log.error("Failed to find job %d wihtin run %d" % (job_info["id"], run.id))
          return -1;
        job = jobs[0]

        runForJob = self.runFor(job.id)
        if runForJob.id == run.id:
          if self.jobs.terminate(job) != 0:
            WorkflowManager.Log.error("Failed to terminate job %d wihtin run %d" % (job.id, run.id))
            return -1
        else:
          if self.terminate(runForJob) != 0:
            WorkflowManager.Log.error("Failed to terminate run %d wihtin run %d" % (runForJob.id, run.id))
            return -1
    self.fail(run)

    return 0;
