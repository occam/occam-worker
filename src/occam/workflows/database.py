# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sql

from occam.databases.manager import uses, datastore

from occam.jobs.database import JobDatabase

@uses(JobDatabase)
@datastore("workflows")
class WorkflowDatabase:
  """ Manages the database interactions for managing workflow runs.
  """

  def queryRun(self, id, key=None):
    """ Returns the SQL query to retrieve the given run by id.

    Args:
      id (str): The identifier for the run.

    Returns:
      sql.Query: The query to be executed.
    """

    runs = sql.Table("runs")
    if key:
      query = runs.select(jobs.__getattr__(key))
    else:
      query = runs.select()

    query.where = (runs.id == id)

    return query

  def retrieveRun(self, id):
    """ Returns a RunRecord for the given id.

    Args:
      id (str): The identifier for the run.

    Returns:
      RunRecord: The corresponding run record.
    """

    # Create a session
    from occam.workflows.records.run import RunRecord
    session = self.database.session()

    # Retrieve the query
    query = self.queryRun(id)

    # Execute the query
    row = self.database.fetch(session)

    # Return the result
    if row is not None:
      row = RunRecord(row)

    return row

  def queryRunJobFor(self, jobId, key=None):
    """ Returns the SQL query to retrieve a run's job information from the given job id.

    Args:
      jobId (str): The identifier for the job.

    Returns:
      sql.Query: The query to be executed.
    """

    from occam.workflows.records.run import RunRecord

    runJobs = sql.Table('run_jobs')
    if key:
      query = runJobs.select(runJobs.__getattr__(key))
    else:
      query = runJobs.select()

    query.where = (runJobs.job_id == jobId)

    return query

  def retrieveRunJobFor(self, jobId):
    """ Returns the RunJobRecord for the given job id.

    Args:
      jobId (str): The identifier for the job.

    Returns:
      RunJobRecord: The corresponding run job record.
    """

    # Create a session
    from occam.workflows.records.run_job import RunJobRecord
    session = self.database.session()

    # Retrieve the query
    query = self.queryRunJobFor(jobId)

    # Execute the query
    self.database.execute(session, query)
    row = self.database.fetch(session)

    # Return the result
    if row is not None:
      row = RunJobRecord(row)

    return row

  def queryRunFor(self, jobId):
    """ Returns the SQL query to retrieve a run from the given job id.

    Args:
      jobId (str): The identifier for the job.

    Returns:
      sql.Query: The query to be executed.
    """

    from occam.workflows.records.run import RunRecord

    runs = sql.Table('runs')

    subQuery = self.queryRunJobFor(jobId, key="run_id")

    query = runs.select()
    query.where = (runs.id.in_(subQuery))

    return query

  def retrieveRunFor(self, jobId):
    """ Returns the RunRecord for the given job id.

    Args:
      jobId (str): The identifier for the job.

    Returns:
      RunRecord: The corresponding run record.
    """

    # Create a session
    from occam.workflows.records.run import RunRecord
    session = self.database.session()

    # Retrieve the query
    query = self.queryRunFor(jobId)

    # Execute the query
    self.database.execute(session, query)
    row = self.database.fetch(session)

    # Return the result
    if row is not None:
      row = RunRecord(row)

    return row
