# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.log     import loggable
from occam.manager import uses

from occam.workflows.datatypes.node import Node
from occam.workflows.datatypes.wire import Wire

@loggable
class Workflow():
  def __init__(self, object, info, data):
    self.__object = object
    self.__info = info
    self.__data = data

    if ( self.__info.get('type', '') != 'workflow' ):
      raise ValueError("Object is not a workflow")

    self.__nodes = None
    self.__revision = self.__object.revision

  @property
  def data(self):
    return self.__data

  @property
  def info(self):
    return self.__info

  @property
  def name(self):
    return self.__info.get('name', 'unknown')

  @property
  def id(self):
    return self.__object.id

  @property
  def uid(self):
    return self.__object.uid

  @property
  def revision(self):
    return self.__revision

  @property
  def nodes(self):
    if self.__nodes is None:
      self.__nodes = []
      for idx, node in enumerate(self.data.get('connections', [])):
        self.__nodes.append(Node(node, idx, self))
    return self.__nodes

  def getTailNodes(self):
    """ Returns a list of nodes that have a dangling output.
    """
    ret = []
    for node in self.nodes:
      hasDanglingOutputs = False
      for output in node.outputs:
        if len(output.get("connections",[])) == 0:
          hasDanglingOutputs = True

      if(hasDanglingOutputs):
        ret.append(node)

    return ret

  def getHeadNodes(self):
    """ Returns a list of nodes where there are no executable inputs.

    These are the initial objects that will run when the workflow is computed.
    """
    # We will fill a list for the first objects encountered that do not have inputs
    ret = []
    # For every node, look at the inputs
    for node in self.nodes:
      connectedInputs = 0
      for input in node.inputs:
        connectedInputs += len(input.get("connections",[]))

      if connectedInputs == 0:
        ret.append(node)

    return ret
