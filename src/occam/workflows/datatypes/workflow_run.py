# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.objects.manager    import ObjectManager
from occam.jobs.manager       import JobManager
from occam.log                import loggable
from occam.manager            import uses
from workflow                 import Workflow
from occam.databases.manager  import DatabaseManager
from occam.workflows.datatypes.workflow import Workflow


@loggable
@uses(ObjectManager)
@uses(DatabaseManager)
@uses(JobManager)
class WorkflowRun():

  def __init__(self, run_id, person = None):
    self.__run_id = run_id
    self.__person = person

    # Set on demand #
    self.__record = None
    self.__workflow = None
    self.__object = None
    self.__job = None
    self.__super_run = None

  @property
  def record(self):
    """ Returns the RunRecord for this run.
    """
    if self.__record is None:

      from occam.workflows.records.run import RunRecord
      import sql

      session = self.database.session()
      runs = sql.Table('runs')

      query = runs.select()
      query.where = (runs.id == int(self.__run_id))

      self.database.execute(session, query)
      record = self.database.fetch(session)

      if record:
        self.__record = RunRecord(record)

    return self.__record

  @property
  def object(self):
    if self.__object is None:
        if self.__record is None:
          self.record

        ## Get the object that owns this job ##
        self.__object = self.objects.retrieve(
          uuid = self.__record.object_uid,
          revision = self.__record.object_revision,
          person   = self.__person)
    return self.__object

  @property
  def workflow(self):
    if self.__workflow is None:
        if self.__record is None:
          self.record

        ## Get the object that owns this job ##
        self.__workflow = Workflow(
          self.objects.retrieve(
            uuid = self.__record.workflow_uid,
            revision = self.__record.workflow_revision,
            person   = self.__person),
          person = self.__person)
    return self.__workflow

  @property
  def job(self):
    if self.__job is None:
      if self.__record is None:
        self.record
      self.__job = Job(self.jobs.jobRunFor(self.__record.id).job_id, self.__person)
    return self.__job

  @property
  def super_run(self):
    if self.__super_run is None:
      if self.__record is None:
        self.record
      job = self.job
      run = self.__runFor(job.id)
      if run is not None:
        self.__super_run = WorkflowRun(run.id, self.__person)
    return self.__super_run

  def sub_run(self, node_index):
    return self.__runInNode(node_index)


  def __runInNode(self, node_index):
    """ Returns the RunRecord corresponding to the given job.
    """

    from occam.workflows.records.run import RunRecord
    import sql

    session = self.database.session()
    runJobs = sql.Table('run_jobs')
    query = runJobs.select(runJobs.job_id,
                        where = (runJobs.run_id == self.__run_id) &
                                (runJobs.node_index == node_index)
                       )
    self.database.execute(session, query)
    ret = self.database.fetch(session)

    if ret:
      ret = WorkflowRun(self.jobs.jobRunFor(jobId=ret.get("job_id")).run_id, self.__person)
    return ret



  def __runFor(self, jobId):
    """ Returns the RunRecord corresponding to the given job.
    """

    from occam.workflows.records.run import RunRecord
    import sql

    session = self.database.session()
    runs = sql.Table('runs')
    runJobs = sql.Table('run_jobs')
    subQuery = runJobs.select(runJobs.run_id)
    subQuery.where = (runJobs.job_id == int(jobId))
    query = runs.select()
    query.where = (runs.id.in_(subQuery))
    self.database.execute(session, query)
    ret = self.database.fetch(session)
    if ret:
      ret = RunRecord(ret)
    return ret


@loggable
@uses(ObjectManager)
@uses(JobManager)
@uses(DatabaseManager)
class Job():
  def __init__(self, job_id, person = None):
    self.__job_id = job_id
    self.__person = person
    # Set on demand #
    self.__job = None

  @property
  def record(self):
    if self.__job is None:
      from occam.jobs.records.job import JobRecord
      import sql

      jobs = sql.Table('jobs')

      query = jobs.select()
      query.where = (jobs.id == self.__job_id)

      session = self.database.session()
      self.database.execute(session, query)
      ret = self.database.fetch(session)
      if ret is not None:
        self.__job = JobRecord(ret)
    return self.__job

  @property
  def id(self):
    if self.__job is None:
      self.record
    return self.__job.id

  @property
  def node_index(self):
    """
      Returns the index of the workflow node running this job
      Returns None if this is the top level workflow
    """
    pass
