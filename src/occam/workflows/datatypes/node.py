# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Node:
  def __init__(self, node, index, workflow):
    self.__node = node
    self.__index = index
    self.__workflow = workflow
    self.__object = None

  def isType(self, type):
    return (self.type == type)

  @property
  def object(self):
    return self.__object

  @property
  def workflow(self):
    return self.__workflow

  @property
  def raw(self):
    ret = self.__node
    ret["index"]=self.__index
    return ret

  @property
  def type(self):
    return self.__node.get("type", None)

  @property
  def index(self):
    return self.__index

  @property
  def name(self):
    return self.__node.get("name","unnamed")

  @property
  def inputs(self):
    return self.__node.get("inputs",[])

  @property
  def outputs(self):
    return self.__node.get("outputs",[])

  @property
  def self(self):
    return self.__node.get("self", {})

  @property
  def version(self):
    return self.__node.get("version",None)

  @property
  def revision(self):
    return self.__node["revision"]

  @property
  def id(self):
    return self.__node["id"]

  def connectedWire(self, wire):
    """ Returns the Wire from the perspective of the attached end of the given wire.
    """

  def __followWire(self, wire):
    """ Returns an array of tuples consisting of the node and the index when following the given wire.
    """
    connections = wire.get("connections", [])
    return [connection.get("to") for connection in connections]

  def followInputWire(self, inputIndex):
    """ Returns an array of tuples consisting of the node and the output index when following the given input.
    """
    return self.__followWire(self.inputs[inputIndex])

  def followOutputWire(self, outputIndex):
    """ Returns an array of tuples consisting of the node and the input index when following the given output.
    """
    if outputIndex == -1:
      return self.followSelf()
    return self.__followWire(self.outputs[outputIndex])

  def followSelf(self):
    return self.__followWire(self.self)

class Wire:
  def __init__(self):
    pass
