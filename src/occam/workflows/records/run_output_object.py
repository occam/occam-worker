# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.databases.manager import table

@table('run_output_objects')
class RunOutputObjectRecord:
  """
  """

  schema = {

    # Foreign Keys

    "run_id": {
      "foreign": {
        "key":   "run_id",
        "table": "run_outputs"
      }
    },

    "job_id": {
      "foreign": {
        "key":   "job_id",
        "table": "run_outputs"
      }
    },

    # Output wire length

    "output_index": {
      "type": "integer"
    },

    # Identifies the generated object

    "object_uid": {
      "type": "string",
      "length": 36
    },

    "object_revision": {
      "type": "string",
      "length": 128
    },
  }
