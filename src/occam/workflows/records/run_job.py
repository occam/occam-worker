# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.databases.manager import table

@table('run_jobs')
class RunJobRecord:
  """ This table is a simple join of RunRecord and JobRecord.

  This keeps track of which jobs have been created based on the given
  run, which in turn associates jobs and tasks with workflows.
  """

  schema = {

    # Foreign Keys

    "run_id": {
      "foreign": {
        "key":   "id",
        "table": "runs"
      }
    },

    "job_id": {
      "foreign": {
        "key":   "id",
        "table": "jobs"
      }
    },

    "node_index": {
      "type": "integer"
    }
  }
