# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.databases.manager import table

@table('run_outputs')
class RunOutputRecord:
  """ This table stores information about the output signal on completed Nodes.

  When a node is completed, it will create a set of outputs. For each output
  wire, denoted by "output_index", there will be a raised signal to the nodes
  attached to it. Upon finalization, new tasks will be generated when it sees
  such a signal or set of signals (if there are multiple inputs to the next
  node in the workflow)
  """

  schema = {

    # Foreign Keys

    "run_id": {
      "foreign": {
        "key":   "id",
        "table": "runs"
      }
    },

    "job_id": {
      "foreign": {
        "key":   "id",
        "table": "jobs"
      }
    },

    # Node index

    "node_index": {
      "type": "integer"
    },

    # Output wire index

    "output_index": {
      "type": "integer"
    },

    # Output wire length

    "output_count": {
      "type": "integer"
    }
  }
