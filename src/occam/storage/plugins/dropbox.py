# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os

from occam.object import Object
from occam.config import Config
from occam.git_repository import GitRepository

from occam.manager import uses

from occam.storage.manager import storage, StorageEstablishError
from occam.network.manager import NetworkManager

@storage('dropbox', accountInfo = {"accessKey": "string"})
@uses(NetworkManager)
class Dropbox:
  """ Links to the Dropbox API for storing/retrieving object content.
  """

  def detect(self):
    """ Returns True when Dropbox package is available.
    """
    
    ret = True
    try:
      import dropbox
    except:
      ret = False

    return ret

  def storePath(self):
    """ Returns the path to our Dropbox local storage.
    """

    return self.configuration.get('path', os.path.join(Config.root(), "dropbox"))

  # Interface methods follow

  def initialize(self, options):
    """ Establishes an Dropbox store.
    """

    # Create a new Dropbox configuration

    return {
    }

  def establish(self, accountInfo):
    """ Instantiates the storage based on the account information.

    Returns:
      dict: An updated accountInfo structure containing more information.
    """

    import dropbox
    try:
      dbx = dropbox.Dropbox(accountInfo['accessKey'])
    except:
      raise StorageEstablishError("Access Key not valid")

    dbx_account = dbx.users_get_current_account()
    accountInfo['name'] = dbx_account.name.display_name

    return accountInfo

  def unestablish(self, accountInfo):
    """ This does nothing.
    """

    return accountInfo

  def discoverNode(self, info):
    """ This Dropbox integration doesn't care about other nodes, so this does nothing.
    """

    return

  def discover(self, uuid, accountInfo):
    """ Returns whether or not the given object exists on the remote dropbox.
    """

    return False

  def isLocal(self, uuid, accountInfo):
    """ Returns whether or not the given object exists locally.
    """

    return False

  def clone(self, storageHash, accountInfo, path):
    """ Clones an object from the remote storage.

    Returns:
      GitRepository: The cloned repository.
    """

    return None

  def buildPathFor(self, uuid, storageHash, accountInfo, buildId, revision=None, create=False):
    """ Retrieves the path for the given storage hashes.
    """

    return None

  def resourcePathFor(self, uuid, create=False):
    """ Returns None since Dropbox does not distinguish resource data from repository data.
    """

    return None

  def repositoryPathFor(self, uuid, create=False):
    """ Returns None since Dropbox does not distinguish resource data from repository data.
    """

    return None

  def pathFor(self, storageHash, accountInfo, revision=None, data=False, create=False):
    """ Retrieves the path for the given storage hashes.
    """

    return None

  def retrieveDirectory(self, storageHash, accountInfo, revision, path):
    """ Retrieve the given directory structure from the given hash.
    """

    return None

  def retrieveFile(self, storageHash, accountInfo, revision, path):
    """ Retrieve the given file path from the given hash.
    """

    return None

  def instantiate(self, uuid, revision, path=None):
    """
    This will create a repository in the local store that reflects the given
    revision. Obviously, this is just a cache of the information in the git
    repository path (<uuid>/repository). It places this in a directory based
    on the revision (<uuid>/<revision>).

    Returns the path to the instantiated object.
    """

    return None

  def pull(self, uuid, storageHash, accountInfo):
    """ Pulls a copy of an existing object to the local store.
    """

  def pushResource(self, uuid, revision, path, accountInfo):
    """ Writes the data at the given path to the store as a revision of the resource object.
    """

    return None, None

  def pushBuild(self, uuid, revision, buildId, path, accountInfo):
    """ Writes a build of an object to the store.
    """

    return None, None

  def push(self, uuid, path, revision, accountInfo):
    """ Writes a new object to the store.
    """

    import dropbox
    dbx = dropbox.Dropbox(accountInfo['accessKey'])

    # Place the object in an appropriate place
    basePath = "/%s/%s" % (uuid, revision)

    # TODO: upload sessions to stream uploads

    # Go through each file in the given path and upload
    count = 0
    for root, dirs, files in os.walk(path):
      count = count + 1
    number = 0

    for root, dirs, files in os.walk(path):
      for file in files:
        curPath = os.path.relpath(root, path)
        curPath = os.path.realpath(basePath + "/" + curPath + "/" + file)
        with open(os.path.join(root, file), "rb") as f:
          pct = float(number) / float(count) * 100.0
          Dropbox.Log.updatePercentage(pct, file)
          dbx.files_upload(f.read(), curPath, dropbox.files.WriteMode.overwrite)
          number = number + 1

    return None, None, revision

  def purge(self, uuid, storageHash, accountInfo):
    """ Removes the object data from the store or marks it for deletion by a garbage collector.
    """

    # Delete the data at dropbox

  def garbageCollect(self):
    """ Notifies the storage backend that it should do some garbage collection.
    """

    # The Dropbox backend has no such gc, so this is a nop.
