# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# The local storage is responsibile for storing all occam data. It is assumed
# to always exist, and data is never deleted from here without an explicit
# command.

# Data is stored in the configurable path, usually in <occam-home>/objects
# Within this directory, the objects are referenced by their UUID. Pieces of
# UUID are keyed such that directory lookups do not crash on some filesystems.
# That is, the uuid 70c44134-8614-11e6-b3c6-f23c910a26c8 will be located within
# /.occam/objects/70/c4/70c44134-8614-11e6-b3c6-f23c910a26c8 and within that
# directory will be subdirectories.

# the /repository directory will have a git repository that will maintain any
# changes to the main object's contents.

# the /resource directory will have a set of hashed data files that pertain to
# the object. These are generally for objects that are wrapping externally
# sourced resources (found within the 'install' part of an object description.
# The hashes are defined by the resource handler associated with the resource.
# File resources, for instance, are simply the digest of the file. GitRepository
# resources do not have a hash to identify them. They merely exist within the
# resource directory. (And distinct from the /repository git package which only
# maintains the object description and metadata history *describing* the git
# resource)

import codecs
import json
import os
import shutil
from uuid import uuid4

from occam.log    import loggable
from occam.config import Config
from occam.git_repository import GitRepository

from occam.storage.manager import storage

@storage('local', priority=0)
class Local:
  """ Implements the logic to make use of IPFS distributed/federated storage.
  """

  def internalPathFor(self, uid, revision=None, identityURI=None, buildId=None, build=False, resource=False):
    """ An internal method to give us the path to a particular thing within the object.
    """

    path = self.storePathFor(uid, resource)

    if not revision is None:
      if identityURI:
        path = os.path.join(path, identityURI)
      elif build:
        path = os.path.join(path, "builds", revision)

        if buildId:
          path = os.path.join(path, buildId)
      else:
        path = os.path.join(path, "cache", revision)
    else:
      if identityURI:
        path = os.path.join(path, identityURI)
      elif buildId:
        path = os.path.join(path, buildId)
      elif not resource:
        path = os.path.join(path, "repository")

    return path

  def createPathFor(self, uid, identityURI=None, resource=False):
    """ Creates an internal commit pool and returns the path.
    """

    path = self.storeCreatePathFor(uid, resource)

    if not resource:
      cache_path = os.path.join(path, "cache")

      if not os.path.exists(cache_path):
        os.mkdir(cache_path)

      identityPath = None
      if identityURI:
        identityPath = os.path.join(path, identityURI)

      poolPath = os.path.join(path, "repository")

      if not os.path.exists(poolPath):
        os.mkdir(poolPath)

        # Initialize the commit pool
        GitRepository.create(poolPath)

      if identityPath and not os.path.exists(identityPath):
        os.mkdir(identityPath)

        # Initialize the identity index (point it to the pool)
        GitRepository.create(identityPath, alternates="../../../repository")

    if resource:
      return path, None

    return poolPath, identityPath

  # Interface methods follow

  def initialize(self, options):
    """
    """

    return None

  def establish(self, accountInfo):
    return accountInfo

  def unestablish(self, accountInfo):
    """ This does nothing.
    """

    return accountInfo

  def buildLogFor(self, uid, storageHash, accountInfo, buildId, revision=None):
    objectPath = self.internalPathFor(uid, revision=revision, buildId=buildId, build=True)

    if not os.path.exists(objectPath):
      return None

    path = objectPath + ".log"
    if os.path.exists(path):
      return open(path, "rb")

    return None

  def buildPathFor(self, uid, storageHash, accountInfo, buildId, revision=None, create=False):
    objectPath = self.internalPathFor(uid, revision=revision, buildId=buildId, build=True)

    if not os.path.exists(objectPath):
      if not create:
        return None

      if not os.path.exists(os.path.realpath(os.path.join(objectPath, "..", ".."))):
        os.mkdir(os.path.realpath(os.path.join(objectPath, "..", "..")))
      if not os.path.exists(os.path.realpath(os.path.join(objectPath, ".."))):
        os.mkdir(os.path.realpath(os.path.join(objectPath, "..")))
      os.mkdir(objectPath)

    return objectPath

  def resourcePathFor(self, uid, create=False):
    objectPath = self.internalPathFor(uid, resource=True)

    if not os.path.exists(objectPath):
      if create:
        return self.createPathFor(uid, resource=True)[0]

      return None

    return objectPath

  def repositoryPathFor(self, uid, identityURI=None, create=False):
    objectPath = self.internalPathFor(uid, identityURI=identityURI)

    if not os.path.exists(objectPath):
      if create:
        return self.createPathFor(uid, identityURI=identityURI)[0]

      return None

    return objectPath

  def pathFor(self, storageHash, accountInfo, revision=None, create=False):
    uid = storageHash['uid']
    objectPath = self.internalPathFor(uid, revision=revision)

    if not os.path.exists(objectPath) and create:
      if not os.path.exists(os.path.realpath(os.path.join(objectPath, "..", ".."))):
        os.mkdir(os.path.realpath(os.path.join(objectPath, "..", "..")))
      if not os.path.exists(os.path.realpath(os.path.join(objectPath, ".."))):
        os.mkdir(os.path.realpath(os.path.join(objectPath, "..")))
      os.mkdir(objectPath)

    if objectPath is None or not os.path.exists(objectPath):
      return None

    return objectPath

  def discoverNode(self, node):
    """
    """

    return

  def discover(self, uid):
    """ Returns whether or not the given object exists on the network.
    """

    # Local stores do not discover. They are private stores.
    return []

  def isLocal(self, uid):
    """ Returns whether or not the given object exists locally.
    """

    return False

  def clone(self, storageHash, accountInfo, path):
    """ Clones the given object to the given path.
    """

    uid = storageHash['uid']
    objectPath = self.internalPathFor(uid)

    git = GitRepository(objectPath)
    data = git.clone(path)

    return data

  def retrieveHistory(self, storageHash, accountInfo, revision, path):
    """ Retrieve the repository log from the given hash.
    """

    uid = storageHash['uid']
    objectPath = self.internalPathFor(uid)

    git = GitRepository(objectPath, revision=revision)
    data = git.history(path)

    return data

  def retrieveDirectory(self, storageHash, accountInfo, revision, path, buildId = None):
    """ Retrieve the given file path from the given hash.
    """

    uid = storageHash['uid']

    if buildId:
      buildPath = self.buildPathFor(uid, storageHash, accountInfo, buildId, revision=revision)
      buildPath = os.path.join(buildPath, path)

      data = {"items": []}
      for filename in os.listdir(buildPath):
        filepath = os.path.join(buildPath, filename)
        filetype = "blob"
        if os.path.isdir(filepath):
          filetype = "tree"

        result = os.lstat(filepath)
        data['items'].append({
          "name": filename,
          "size": result.st_size,
          "type": filetype,
          "mode": result.st_mode
        })
    else:
      objectPath = self.internalPathFor(uid)
      git = GitRepository(objectPath, revision=revision)
      data = git.retrieveDirectory(path)

    return data

  def retrieveFileStat(self, storageHash, accountInfo, revision, path, buildId = None):
    """ Retrieve the given file path from the given hash.
    """

    uid = storageHash['uid']

    if buildId:
      buildPath = self.buildPathFor(uid, storageHash, accountInfo, buildId, revision=revision)

      filepath = os.path.join(buildPath, path)

      filetype = "blob"
      if os.path.isdir(filepath):
        filetype = "tree"

      result = os.lstat(filepath)
      data = {
        "name": os.path.basename(path),
        "size": result.st_size,
        "type": filetype,
        "mode": result.st_mode
      }
    else:
      objectPath = self.internalPathFor(uid)
      git = GitRepository(objectPath, revision=revision)
      data = git.retrieveFileStat(path)

    return data

  def retrieveFile(self, storageHash, accountInfo, revision, path, buildId = None):
    """ Retrieve the given file path from the given hash.
    """

    uid = storageHash['uid']

    if buildId:
      buildPath = self.buildPathFor(uid, storageHash, accountInfo, buildId, revision=revision)

      filepath = os.path.join(buildPath, path)
      data = open(filepath, "rb")
    else:
      objectPath = self.internalPathFor(uid)
      git = GitRepository(objectPath, revision=revision)
      data = git.retrieveFile(path)

    return data

  def pull(self, uid, storageHash, accountInfo):
    """ Pulls a copy of an existing object to the local store.
    """

    # Local objects are already local!

    return True

  def storeCreatePathFor(self, uid, resource):
    """ Creates a path to store an object with the given uid.
    """

    if uid is None:
      return None

    code = uid
    codes = [code[0:2], code[2:4], code[4:6], code[6:8]]

    # Objects are in: .occam/objects/<id[0:2]>/<id[2:4]>/<id[4:6]>/<id[6:8]>/full-code
    if resource:
      path = self.configuration.get('resources', os.path.join(Config.root(), "resources"))
    else:
      path = self.configuration.get('objects', os.path.join(Config.root(), "objects"))

    if path is None:
      return None

    if not os.path.exists(path):
      os.mkdir(path)

    for codePart in codes:
      path = os.path.join(path, codePart)
      if not os.path.exists(path):
        os.mkdir(path)

    path = os.path.join(path, uid)
    if not os.path.exists(path):
      os.mkdir(path)

    return path

  def storePathFor(self, uid, resource):
    """ Returns the path the object of the given uid would be stored.
    """

    code = uid
    codes = [code[0:2], code[2:4], code[4:6], code[6:8]]

    # Objects are in: .occam/objects/<type>/<code[0:2]>/<code[2:4]>/full-code
    if resource:
      object_path = self.configuration.get('resources', os.path.join(Config.root(), "resources"))
    else:
      object_path = self.configuration.get('objects', os.path.join(Config.root(), "objects"))

    return os.path.realpath(os.path.join(object_path, *codes, uid))

  def pushResource(self, uid, revision, path, accountInfo):
    """ Writes the data at the given path to the store as a revision of the resource object.
    """

    storageHash = {
      'uid': uid,
      'revision': revision
    }

    if path.startswith(self.resourcePathFor(uid)):
      return storageHash, None

    return None, None

  def pushBuild(self, uid, revision, buildId, path, logPath, accountInfo):
    """
    Writes the data at the given path to the store as a build of the given
    object.
    """

    storageHash = {
      'uid': uid,
      'buildId': buildId,
      'revision': revision
    }

    # Determine the storage path
    storePath = self.buildPathFor(uid, storageHash, accountInfo, buildId, revision, create=True)
    if storePath != path:
      # Copy the directory tree
      shutil.rmtree(storePath)
      shutil.copytree(path, storePath)

      # Copy the log file
      if logPath:
        try:
          shutil.copyfile(logPath, storePath + ".log")
        except:
          pass

    return storageHash, None

  def push(self, uid, path, revision, accountInfo, identityURI):
    """ Writes a new object or revision to the store from the given path.
    """

    storageHash = {
      'uid': uid
    }

    # Place it in: <object path>/2-code/2-code/full-code/repository
    storagePath, identityPath = self.createPathFor(uid, identityURI)

    if path == storagePath:
      # Someone is attempting to store an object from the object store!
      # Thus, it is already stored... do nothing. report success.
      return storageHash, None, None

    # Copy the commits to the commit pool by fetching them (no merge)
    sourcePath = path
    from occam.git_repository import GitRepository

    if GitRepository.hasRevision(storagePath, revision):
      Local.Log.noisy("Version already exists in the object store")
    else:
      Local.Log.noisy("Pushing new version at %s to %s" % (sourcePath, storagePath))

      git = GitRepository(storagePath)

      from uuid import uuid4
      remote_name = str(uuid4())
      branch_name = revision

      git.addRemote(remote_name, sourcePath)
      git.fetch(remote_name)
      git.rmRemote(remote_name)

      # Create a reference branch for the given identity
      git = GitRepository(identityPath)

      # Look up if the parent revision is tracked, and if so,
      # replace that tag with this revision
      parentRevision = git.parent(revision)

      if parentRevision and git.hasBranch(parentRevision):
        git.deleteRef("refs/heads/%s" % (parentRevision))

      # Maintain a reference to this revision in the index
      git.updateRef("refs/heads/%s" % (revision), revision)

    return storageHash, None, None

  def purge(self, uid, storageHash, accountInfo):
    """ Removes, or queues removal, of an object from the store.
    
    It may not remove the object immediately and instead mark it for deletion by
    a garbage collector.
    """

    # Delete the content

    return True

  def garbageCollect(self):
    """
    Notifies the storage backend that it should do some garbage collection.
    """

    # Local stores do not garbage collect.

    return True
