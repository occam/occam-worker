# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os

from occam.config import Config
from occam.git_repository import GitRepository

from occam.manager import uses

from occam.storage.manager import storage
from occam.network.manager import NetworkManager

import subprocess

@storage('ipfs')
@uses(NetworkManager)
class IPFS:
  """ Implements the logic to make use of IPFS distributed/federated storage.
  """

  daemon = None

  def detect(self):
    """ Returns True when IPFS is available.
    """
    
    try:
      p = self.invokeIPFS([])
      code = p.wait()
    except:
      code = -1

    return code == 0

  @staticmethod
  def path():
    """ Returns the path to the IPFS binary.
    """

    return (os.path.realpath("%s/../../../../../go-ipfs" % (os.path.realpath(__file__))))

  def storePath(self):
    """ Returns the path to the ipfs instance.
    """

    return self.configuration.get('path', os.path.join(Config.root(), "ipfs"))

  @staticmethod
  def popen(command, stdout=subprocess.DEVNULL, stdin=None, stderr=subprocess.DEVNULL, cwd=None, env=None):
    return subprocess.Popen(command, stdout=stdout, stdin=stdin, stderr=stderr, cwd=cwd, env=env)

  def invokeIPFS(self, args, stdout=subprocess.PIPE, stdin=None, stderr=subprocess.DEVNULL, ipfsPath=None, env=None):
    """ Internal method to invoke IPFS with the given arguments
    """

    if env is None:
      env = {}
    if not 'IPFS_PATH' in env and not ipfsPath is None:
      env['IPFS_PATH'] = ipfsPath

    env['PATH'] = os.getenv('PATH')

    command = ['%s/ipfs' % (IPFS.path())] + args
    return IPFS.popen(command, stdout=stdout, stdin=stdin, stderr=stderr, env=env)

  @staticmethod
  def internalUUID(uuid):
    return "occam:%s" % (str(uuid))

  @staticmethod
  def uuidToIPFSHash(uuid, radix=58):
    """ Internal method to get an IPFS hash for a given occam uuid
    """

    # Get a representation of the uuid
    internalName = IPFS.internalUUID(uuid)

    # Retrieve the multihash for that string
    from occam.storage.plugins.ipfs_vendor.multihash import multihash
    hashedBytes = multihash.encode(internalName, multihash.SHA2_256)

    # Base58 encode
    if radix == 58:
      from occam.storage.plugins.ipfs_vendor.base58 import b58encode
      return b58encode(bytes(hashedBytes))
    else:
      import binascii
      return binascii.hexlify(bytes(hashedBytes)).decode('utf-8')

  def ipfsMountPath(self):
    return os.path.join(self.storePath(), "ipfs-mount")

  def ipnsMountPath(self):
    return os.path.join(self.storePath(), "ipns-mount")

  def IPFShost(self, ipfsPath):
    """ Internal method to return the host name for our own node.
    """

    p = self.invokeIPFS(["id"], ipfsPath=ipfsPath)
    reader = codecs.getreader('utf-8')
    import json
    try:
      config = json.load(reader(p.stdout))
    except:
      config = {}
    p.wait()

    if "ID" in config:
      return config["ID"]

    return ""

  def IPFSNodeHashToAddresses(self, nodeHash):
    """ Yields a set of IP addresses for the given node hash.
    """

    ipfsPath = self.storePath()

    p = self.invokeIPFS(["dht", "findpeer", nodeHash], ipfsPath=ipfsPath)
    addresses = p.stdout.read().decode('utf-8').strip().split("\n")
    p.wait()

    # Go through and pull out IP
    # They are in the form:
    # /ip4/<ip addr>/tcp/<port>
    ret = []
    for address in addresses:
      if address.startswith("/ip4/"):
        # Pull out IP address
        address = address[5:address.index('/', 5)]
        if address != "127.0.0.1" and address != "0.0.0.0":
          ret.append(address)

    return ret

  def IPFSIsDaemonRunning(self):
    if IPFS.daemon:
      return True

    ipfsMountPath = self.ipfsMountPath()
    ipnsMountPath = self.ipnsMountPath()

    if os.path.exists(os.path.join(ipnsMountPath, "local")):
      return True

    return False

  def IPFSStartDaemon(self):
    try:
      if IPFS.daemon:
        return IPFS.daemon
    except:
      pass

    ipfsMountPath = self.ipfsMountPath()
    ipnsMountPath = self.ipnsMountPath()

    if self.IPFSIsDaemonRunning():
      return

    if not os.path.exists(ipfsMountPath):
      try:
        os.mkdir(ipfsMountPath)
      except:
        pass

    if not os.path.exists(ipnsMountPath):
      try:
        os.mkdir(ipnsMountPath)
      except:
        pass

    IPFS.Log.noisy("Starting IPFS Daemon (%s, %s)" % (ipfsMountPath, ipnsMountPath))
    IPFS.daemon = self.invokeIPFS(["daemon", "--mount", "--mount-ipfs", ipfsMountPath, "--mount-ipns", ipnsMountPath], ipfsPath=self.storePath())

    # Wait until the files are mounted
    while not os.path.exists(os.path.join(ipnsMountPath, "local")):
      IPFS.daemon.poll()
      if not IPFS.daemon.returncode is None:
        break

    return IPFS.daemon

  def IPFSStopDaemon(self):
    if IPFS.daemon:
      import signal
      try:
        IPFS.daemon.send_signal(signal.SIGINT)
      except:
        pass
      IPFS.daemon = None

  def IPFSBlockExists(self, blockHash):
    import binascii
    from occam.storage.plugins.ipfs_vendor.base58 import b58decode

    ipfsPath = self.storePath()
    ipfsHash = binascii.hexlify(b58decode(blockHash)).decode('utf-8')
    ipfsBlockPath = os.path.join(ipfsPath, "blocks", ipfsHash[0:8], ipfsHash + ".data")
    return os.path.exists(ipfsBlockPath)

  # Interface methods follow

  def initialize(self, options):
    """ Establishes an IPFS store.
    """

    # Create a new IPFS configuration
    if not os.path.exists(os.path.join(self.storePath(), "config")):
      p = self.invokeIPFS(["init"], ipfsPath=self.storePath())
      p.wait()

      ipfsMountPath = self.ipfsMountPath()
      ipnsMountPath = self.ipnsMountPath()

      # Add mount paths
      if not os.path.exists(ipfsMountPath):
        try:
          os.mkdir(ipfsMountPath)
        except:
          pass

      if not os.path.exists(ipnsMountPath):
        try:
          os.mkdir(ipnsMountPath)
        except:
          pass

      # Modify that configuration to remove bootstraps

      # Set Bootstrap to []
      p = self.invokeIPFS(["config", "Bootstrap", "[]", "--json"], ipfsPath=self.storePath())
      p.wait()

    # Get Peer ID
    p = self.invokeIPFS(["config", "Identity.PeerID"], ipfsPath=self.storePath())
    nodeID = p.stdout.read().decode('utf-8').strip()
    p.wait()

    ipfsPort = 4001

    ipfsURLv4 = None
    if self.network.ip4():
      ipfsURLv4 = "/ip4/%s/tcp/%s/ipfs/%s" % (self.network.ip4(), ipfsPort, nodeID)
    if self.network.ip6():
      ipfsURLv6 = "/ip6/%s/tcp/%s/ipfs/%s" % (self.network.ip6(), ipfsPort, nodeID)

    return {
      "node": [ipfsURLv4]
    }

  def establish(self, accountInfo):
    """ This does nothing.
    """
    
    return accountInfo

  def unestablish(self, accountInfo):
    """ This does nothing.
    """

    return accountInfo

  def discoverNode(self, info):
    """
    """

    # Add the node's id as a bootstrap
    urls = info['node']

    # Only add 5 URLs, so nodes can't make us do ridiculous things
    maxURLs = 5
    if len(urls) < maxURLs:
      maxURLs = len(urls)

    for nodeURL in urls[0:maxURLs]:
      p = self.invokeIPFS(["bootstrap", "add", nodeURL], ipfsPath=self.storePath())
      p.wait()

  def discover(self, uuid):
    """ Returns whether or not the given object exists on the network.
    """

    IPFS.Log.write("attempting to discover %s" % (uuid))

    daemon = self.IPFSStartDaemon()

    p = self.invokeIPFS(["dht", "findprovs", IPFS.uuidToIPFSHash(uuid)], ipfsPath=self.storePath())
    hosts = p.stdout.read().decode('utf-8').strip().split("\n")
    p.wait()
    if hosts[0] == "":
      hosts = hosts[1:]

    ret = []
    for host in hosts:
      addresses = self.IPFSNodeHashToAddresses(host)
      if len(addresses) > 0:
        IPFS.Log.noisy("Found %s at %s" % (uuid, addresses))
      ret.extend(addresses)

    if len(hosts) == 0:
      IPFS.Log.noisy("%s not found" % (uuid))

    self.IPFSStopDaemon()

    return ret

  def isLocal(self, uuid):
    """ Returns whether or not the given object exists locally.
    """

    ipfsHash = IPFS.uuidToIPFSHash(uuid)
    return self.IPFSBlockExists(ipfsHash)

  def clone(self, storageHash, accountInfo, path):
    """
    """

    daemon = self.IPFSStartDaemon()

    objectPath = self.ipfsMountPath()
    objectPath = os.path.join(objectPath, storageHash["hash"])

    IPFS.Log.noisy("git at %s" % (objectPath))

    git = GitRepository(objectPath)
    ret = git.clone(path)

    self.IPFSStopDaemon()
    return ret

  def buildPathFor(self, uuid, storageHash, accountInfo, buildId, revision=None, create=False):
    """ Retrieves the path for the given storage hashes.
    """

    objectPath = self.ipfsMountPath()
    objectPath = os.path.join(objectPath, storageHash["hash"])

    # Only return it, however, if the daemon is running
    # Because, otherwise, how could it actually be there
    if self.IPFSIsDaemonRunning():
      return objectPath

    return None

  def resourcePathFor(self, uuid, create=False):
    """ Returns None since IPFS does not distinguish resource data from repository data.
    """
    return None

  def repositoryPathFor(self, uuid, create=False):
    """ Returns None since IPFS does not distinguish resource data from repository data.
    """
    return None

  def pathFor(self, storageHash, accountInfo, revision=None, data=False, create=False):
    """ Retrieves the path for the given storage hashes.
    """

    objectPath = self.ipfsMountPath()
    objectPath = os.path.join(objectPath, storageHash["hash"])

    # Only return it, however, if the daemon is running
    # Because, otherwise, how could it actually be there
    if self.IPFSIsDaemonRunning():
      return objectPath

    return None

  def retrieveDirectory(self, storageHash, accountInfo, revision, path):
    """ Retrieve the given directory structure from the given hash.
    """

    daemon = self.IPFSStartDaemon()

    objectPath = self.ipfsMountPath()
    objectPath = os.path.join(objectPath, storageHash["hash"])

    IPFS.Log.noisy("git at %s" % (objectPath))

    git = GitRepository(objectPath, revision=revision)
    data = git.retrieveDirectory(path)

    self.IPFSStopDaemon()
    return data

  def retrieveFile(self, storageHash, accountInfo, revision, path):
    """ Retrieve the given file path from the given hash.
    """

    daemon = self.IPFSStartDaemon()

    objectPath = self.ipfsMountPath()
    objectPath = os.path.join(objectPath, storageHash["hash"])

    IPFS.Log.noisy("git at %s" % (objectPath))

    git = GitRepository(objectPath, revision=revision)
    data = git.retrieveFile(path)

    self.IPFSStopDaemon()
    return data

  def instantiate(self, uuid, revision, path=None):
    """
    This will create a repository in the local store that reflects the given
    revision. Obviously, this is just a cache of the information in the git
    repository path (<uuid>/repository). It places this in a directory based
    on the revision (<uuid>/<revision>).

    Returns the path to the instantiated object.
    """

    repoPath = self.internalPathFor(uuid)
    if not os.path.exists(repoPath):
      return None

    if path is None:
      path = self.internalPathFor(uuid, revision)

      if not os.path.exists(path):
        if not os.path.exists(os.path.realpath(os.path.join(path, ".."))):
          os.mkdir(os.path.realpath(os.path.join(path, "..")))
        os.mkdir(path)

    git = GitRepository(repoPath)
    git.instantiate(path, revision=revision)

    return path

  def pull(self, uuid, storageHash, accountInfo):
    """ Pulls a copy of an existing object to the local store.
    """

    # Get the name of the advertisement token
    tagHash = IPFS.uuidToIPFSHash(uuid)

    # Pull down the advertisement token
    p = self.invokeIPFS(["pin", "add", tagHash], ipfsPath=self.storePath())
    p.wait()

    # Pull down the object
    p = self.invokeIPFS(["pin", "add", storageHash["hash"]], ipfsPath=self.storePath())
    p.wait()

  def pushResource(self, uuid, revision, path, accountInfo):
    """ Writes the data at the given path to the store as a revision of the resource object.
    """

    IPFS.Log.write("Pushing resource %s@%s" % (uuid, revision))

    # Write an advertisement token
    p = self.invokeIPFS(["block", "put"], stdin=subprocess.PIPE, ipfsPath=self.storePath())
    tokenHash = IPFS.internalUUID("%s-%s-%s" % ("resource", uuid, revision)).encode('utf-8')
    result = p.communicate(input=tokenHash)[0]

    p = self.invokeIPFS(["add", "-H", "-r", "-q", path], ipfsPath=self.storePath())

    ipfsHashes = p.stdout.read().decode('utf-8')
    hashes = ipfsHashes.strip().split("\n")

    if len(hashes) > 0:
      ipfsHash = hashes[-1]
      IPFS.Log.write("Hash: %s" % (ipfsHash))
    else:
      IPFS.Log.error("Did not write data.")
      return None

    storageHash = {
      "hash": ipfsHash
    }

    self.pull(uuid, storageHash)

    return storageHash, None

  def pushBuild(self, uuid, revision, buildId, path, accountInfo):
    """ Writes a build of an object to the store.
    """

    IPFS.Log.write("Pushing object build %s@%s (%s)" % (uuid, revision, buildId))

    # Write an advertisement token
    p = self.invokeIPFS(["block", "put"], stdin=subprocess.PIPE, ipfsPath=self.storePath())
    tokenHash = IPFS.internalUUID("%s-%s-%s" % (uuid, revision, buildId)).encode('utf-8')
    result = p.communicate(input=tokenHash)[0]

    p = self.invokeIPFS(["add", "-H", "-r", "-q", path], ipfsPath=self.storePath())

    ipfsHashes = p.stdout.read().decode('utf-8')
    hashes = ipfsHashes.strip().split("\n")

    if len(hashes) > 0:
      ipfsHash = hashes[-1]
      IPFS.Log.write("Hash: %s" % (ipfsHash))
    else:
      IPFS.Log.error("Did not write data.")

    storageHash = {
      "hash": ipfsHash
    }

    self.pull(uuid, storageHash)

    return storageHash, None

  def push(self, uuid, path, revision, accountInfo):
    """ Writes a new object to the store.
    """

    IPFS.Log.write("Pushing object %s." % (uuid))

    # Write an advertisement token
    p = self.invokeIPFS(["block", "put"], stdin=subprocess.PIPE, ipfsPath=self.storePath())
    tokenHash = IPFS.internalUUID(uuid).encode('utf-8')
    result = p.communicate(input=tokenHash)[0]

    # Pin the advertisement token
    tagHash = IPFS.uuidToIPFSHash(uuid)
    p = self.invokeIPFS(["pin", "add", tagHash], ipfsPath=self.storePath())
    p.wait()

    p = self.invokeIPFS(["add", "-H", "-r", "-q", path], ipfsPath=self.storePath())

    ipfsHashes = p.stdout.read().decode('utf-8')
    hashes = ipfsHashes.strip().split("\n")

    if len(hashes) > 0:
      ipfsHash = hashes[-1]
      IPFS.Log.write("Hash: %s" % (ipfsHash))
    else:
      IPFS.Log.error("Did not write data.")

    storageHash = {
      "hash": ipfsHash
    }

    self.pull(uuid, storageHash, None)

    return storageHash, None, revision

  def purge(self, uuid, storageHash, accountInfo):
    """ Removes the object data from the store or marks it for deletion by a garbage collector.
    """

    # Get the name of the advertisement token
    tagHash = IPFS.uuidToIPFSHash(uuid)

    # Unpin the advertisement token
    p = self.invokeIPFS(["pin", "rm", tagHash], ipfsPath=self.storePath())
    p.wait()

    # Unpin the object
    p = self.invokeIPFS(["pin", "rm", storageHash["hash"]], ipfsPath=self.storePath())
    p.wait()

  def garbageCollect(self):
    """ Notifies the storage backend that it should do some garbage collection.
    """

    p = self.invokeIPFS(["repo", "gc"], ipfsPath=self.storePath())
    p.wait()
