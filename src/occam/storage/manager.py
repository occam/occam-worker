# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.error          import OccamError
from occam.log            import loggable
from occam.git_repository import GitRepository

import shutil
import json
import os
import codecs
import subprocess

from occam.manager import uses, manager

from occam.notes.manager import NoteManager

@loggable
@uses(NoteManager)
@manager("storage")
class StorageManager:
  """ This OCCAM manager handles the storage backend.
  """

  handlers = {}
  priorities  = []
  handlerImpl = []
  instantiated = {}

  def __init__(self):
    import occam.storage.plugins.local
    import occam.storage.plugins.ipfs
    import occam.storage.plugins.dropbox
    import occam.storage.plugins.box

    # The mime-type generator
    self.mimes = None

  @staticmethod
  def register(storageName, handlerClass, priority=100, accountInfo=None):
    """ Adds a new storage backend type.

    Arguments:
      handlerClass (object): The handler that implements the storage interface.
      priority (int): A value that indicates when a system should be used as a fallback. Lower values are used first.
      accountInfo (dict): A description of the fields that a person has to provide.
    """

    import bisect

    index = bisect.bisect(StorageManager.priorities, priority)
    StorageManager.priorities.insert(index, priority)
    StorageManager.handlerImpl.insert(index, storageName)
    StorageManager.handlers[storageName] = {'class': handlerClass, 'accountInfo': accountInfo}

  def handlerFor(self, storageName):
    """ Returns an instance of a handler for the given name.
    """

    if not storageName in StorageManager.handlers:
      StorageManager.Log.error("storage backend %s not known" % (storageName))
      return None

    # Instantiate a storage backend if we haven't seen it before
    if not storageName in StorageManager.instantiated:
      # Pull the configuration (from the 'stores' subpath and keyed by the name)
      subConfig = self.configurationFor(storageName)

      # Create a driver instance
      instance = StorageManager.handlers[storageName]['class'](subConfig)

      # If there is a problem detecting the backend, cancel
      # This will set the value in the instantiations to None
      # TODO: cache this in the stores configuration? or detection file?
      if hasattr(instance, 'detect') and callable(instance.detect) and not instance.detect():
        instance = None

      # Set the instance
      StorageManager.instantiated[storageName] = instance

    if StorageManager.instantiated[storageName] is None:
      # The driver could not be initialized
      return None

    return StorageManager.instantiated[storageName]

  def configurationFor(self, storageName):
    """
    Returns the configuration for the given storage that is found within the
    occam configuration (config.yml) under the "stores" section under the
    given storage backend name.
    """

    config = self.configuration
    subConfig = config.get(storageName, {})

    return subConfig

  # Interface follows

  def initialize(self):
    """
    """

    ret = {}
    for k, v in StorageManager.handlers.items():
      subConfig = self.configurationFor(k)
      handler = self.handlerFor(k)
      if handler:
        info = handler.initialize(subConfig)
        if not info is None:
          ret[k] = info

    return ret

  def discoverNode(self, info):
    """
    """

    #for k, v in StorageManager.handlers.items():
    #  if k in info:
    #    self.handlerFor(k).discoverNode(info[k])

  def discover(self, uuid, revision=None):
    """ Returns a list of hosts where the given object is known to exist.
    """

    ret = []
    for k, v in StorageManager.handlers.items():
      handler = self.handlerFor(k)
      if handler:
        ret.extend(handler.discover(uuid))

    return ret

  def isLocal(self, uuid):
    """
    Returns whether or not the given object exists locally.
    """

    for k, v in StorageManager.handlers.items():
      handler = StorageManager.handlerFor(k)
      if handler:
        if handler.isLocal(uuid):
          return True

    return False

  def isKnown(self, uuid, revision):
    """
    Returns whether or not the given revision of the given object is known.
    This is generally used when a given revision is needed in a complete state.
    If it is not known directly, it will try the latest revision and will need
    to clone that object and pull out the revision in a writable path somewhere.

    If it is known, however, it can simply be read using the versioning of the
    file system itself.
    """

    hashes = {}
    for k in self.handlerImpl:
      storageInfo = self.notes.retrieveInvocation(uuid, category="stores/%s" % (k), key="store", revision=revision)
      if len(storageInfo) > 0:
        return True

    return None

  def buildLogFor(self, uuid, buildId, revision):
    """ Retrieves the build log for the given object and build.
    """

    hashes = {}
    for k in self.handlerImpl:
      storageInfo = self.notes.retrieve(uuid, category="stores/%s/builds" % (k), key="build-%s" % (buildId), revision=revision)
      if not storageInfo is None and len(storageInfo) > 0:
        handler = self.handlerFor(k)
        if handler:
          ret = handler.buildLogFor(uuid, storageInfo, None, buildId, revision=revision)
          if not ret is None:
            return ret

    return None

  def buildPathFor(self, uuid, buildId, revision, create=False):
    """ Retrieves a path for the built version of the object at the given revision.
    """

    hashes = {}
    for k in self.handlerImpl:
      storageInfo = self.notes.retrieve(uuid, category="stores/%s/builds" % (k), key="build-%s" % (buildId), revision=revision)
      if create or (not storageInfo is None and len(storageInfo) > 0):
        handler = self.handlerFor(k)
        if handler:
          ret = handler.buildPathFor(uuid, storageInfo, None, buildId, revision=revision, create=create)
          if not ret is None:
            return ret

    return None

  def repositoryPathFor(self, uuid, revision, identity=None, create=False):
    """ Retrieves a path for the object's data repository.
    """

    for k in self.handlerImpl:
      handler = self.handlerFor(k)
      if handler:
        ret = handler.repositoryPathFor(uuid, identityURI=identity, create=create)

        if not ret is None:
          return ret

  def resourcePathFor(self, uuid, create=False):
    """ Retrieves and/or creates the object's resource path.

    The resource path is some location to keep large (usually hashable) file
    data associated with the object.
    """

    for k in self.handlerImpl:
      handler = self.handlerFor(k)
      if handler:
        ret = handler.resourcePathFor(uuid, create=create)

        if not ret is None:
          return ret

  def pathFor(self, uuid, revision, buildId=None, create=False, backend = None, accountInfo = None):
    """ Retrieves a path that one can read the given object at the given revision.

    If create is True, the revision will be instantiated if no knowledge of it
    is known and no local cached copy of it is available. If it is created, it
    will be subsequently cached and stored.
    """

    backends = self.handlerImpl
    if backend:
      backends = [backend]
    elif accountInfo:
      # Do not allow an account info without a specific backend
      raise ValueError("cannot have an accountInfo specified without a backend")

    for k in backends:
      if buildId:
        storageInfo = self.notes.retrieve(uuid, category="stores/%s/builds" % (k), key="build-%s" % (buildId), revision=revision)

        if storageInfo:
          storageInfo = storageInfo[0]['value']
      else:
        storageInfo = self.storageHashFor(uuid, revision, k)

      if not storageInfo is None and len(storageInfo) > 0:
        handler = self.handlerFor(k)
        if handler:
          if buildId:
            ret = handler.buildPathFor(uuid, storageInfo, accountInfo, buildId=buildId, create=create, revision=revision)
          else:
            ret = handler.pathFor(storageInfo, accountInfo, revision=revision, create=create) 

        if not ret is None:
          return ret

    return None

  def pin(self, uuid):
    """
    Ensures that the object is preserved locally and not garbage collected when
    it is known to be replicated offsite.
    """

    for k, v in StorageManager.handlers.items():
      handler = self.handlerFor(k)
      if handler:
        if handler.pin(uuid):
          return True

    return False

  def purge(self, uuid):
    """
    Removes the given object from the local storage. Returns False if any
    storage mechanism could not remove the object and it persists.
    """

    for k in StorageManager.handlerImpl:
      handler = self.handlerFor(k)
      if handler:
        if not handler.purge(uuid, None):
          return False

    return True

  def pull(self, uuid):
    """
    """

    for k in StorageManager.handlerImpl:
      handler = self.handlerFor(k)
      if handler:
        if handler.pull(uuid, None, None):
          return True

    return False

  def push(self, uuid, revision, identity, path, backend = "local", accountInfo = None):
    """ Stores the given object located at the given path using the given backend.

    Args:
      uuid (str): The UUID for the object being pushed.
      revision (str): The intended revision string for the object.
      identity (str): The identity URI to use as the owner of the object.
      path (str): The origin path for the object.
      backend (str): The name of the storage backend to target. Default: "local"
      accountInfo (dict): The account specific information to provide the storage backend.

    Returns:
      tuple: A list containing the returned global storage info and the account specific storage info.
    """

    StorageManager.Log.noisy("Pushing to %s from %s" % (backend, path))

    storageHash = None
    accountHash = None

    handler = self.handlerFor(backend)
    if handler:
      storageHash, accountHash, storedRevision = handler.push(uuid, path, revision, accountInfo, identity)

      if backend != "local" and storageHash:
        self.notes.store(uuid, "stores/%s" % (backend), key="store", value=storageHash, revision=storedRevision)

    return storageHash, accountHash

  def pushResource(self, uuid, revision, path, backend = "local", accountInfo = None):
    """ Stores the given resource data located at the given path.

    Resource paths might be specific to their revision, or it may be the root
    path for an aggregate resource (such as a it repository)
    """

    storageHash = None
    accountHash = None

    handler = self.handlerFor(backend)
    if handler:
      storageHash, accountHash = handler.pushResource(uuid, revision, path, accountInfo)
      if not storageHash is None:
        self.notes.store(uuid, "stores/%s" % (backend), key="resource", value=storageHash, revision=revision)

    return storageHash, accountHash

  def pushBuild(self, uuid, revision, buildId, path, logPath=None, backend = "local", accountInfo = None):
    """ Stores the given object build located at the given path.
    """

    storageHash = None
    accountHash = None

    handler = self.handlerFor(backend)
    if handler:
      storageHash, accountHash = handler.pushBuild(uuid, revision, buildId, path, logPath, accountInfo)
      if not storageHash is None:
        self.notes.store(uuid, "stores/%s/builds" % (backend), key="build-%s" % (buildId), value=storageHash, revision=revision)

    return storageHash, accountHash

  def instantiate(self, uuid, revision, path):
    """ Places a copy of the given object at the given revision to the given path.

    The directory specified by path must not exist.
    """

    repositoryPath = None
    revisionPath   = None

    # Get the path for the given revision
    if repositoryPath is None:
      repositoryPath = self.handlerFor('local').pathFor({'uid': uuid}, None)
    if revisionPath is None:
      revisionPath = self.pathFor(uuid, revision=revision)

    ret = None

    if not revisionPath is None:
      # Copy this path
      StorageManager.Log.noisy("Cloning from %s" % (revisionPath))

      try:
        git = GitRepository(revisionPath, revision=revision)
        git.clone(path)

        newGitRepository = GitRepository(path, revision=revision)
        newGitRepository.reset(revision, hard=True)

        ret = path

        return ret
      except:
        pass

    if not repositoryPath is None:
      # Clone from this path
      StorageManager.Log.noisy("Cloning from %s" % (repositoryPath))

      git = GitRepository(repositoryPath)
      git.clone(path)

      newGitRepository = GitRepository(path, revision=revision)
      newGitRepository.reset(revision, hard=True)

      ret = path

      # TODO: Push revision back since we have never seen it before

    if not ret is None:
      return ret

    return None

  def clone(self, uuid, revision, path):
    """ Clones the object to the specifed local filesystem path.
    """

    hashes = {}
    for k in self.handlerImpl:
      storageInfo = self.storageHashFor(uuid, revision, k)
      if storageInfo is None:
        # We don't know how to retrieve from this storage.
        continue

      handler = self.handlerFor(k)
      if handler:
        ret = handler.clone(storageInfo, path)
        if not ret is None:
          return ret

    return None

  def storageHashFor(self, uuid, revision, storageType):
    """ Retrieves the storage information for the particular object/revision pair.
    """

    storageInfo = self.notes.retrieve(uuid, category="stores/%s" % (storageType), key="store", revision=revision)
    if storageType != "local" and (storageInfo is None or len(storageInfo) == 0):
      # We don't know how to retrieve from this storage.
      return None

    storageHash = {"uid": uuid, "revision": revision}
    if storageInfo:
      storageHash = storageInfo[0]["value"]

    return storageHash

  def retrieveHistory(self, uuid, revision, path = None):
    """ Retrieves an object history from the stored object at the given revision.
    """

    hashes = {}
    for k in self.handlerImpl:
      storageInfo = self.storageHashFor(uuid, revision, k)
      if storageInfo is None:
        # We don't know how to retrieve from this storage.
        continue
      try:
        handler = self.handlerFor(k)
        if handler:
          ret = handler.retrieveHistory(storageInfo, {}, revision, path)
      except IOError:
        ret = None

      if not ret is None:
        return ret

    raise IOError("File Not Found")

  def retrieveDirectory(self, uid, revision, path, buildId = None):
    """ Retrieves a directory listing for the given path by any means.
    """

    if path.startswith("/"):
      path = path[1:]

    hashes = {}
    for k in self.handlerImpl:
      if buildId:
        storageInfo = self.notes.retrieve(uid, category="stores/%s/builds" % (k), key="build-%s" % (buildId), revision=revision)
      else:
        storageInfo = self.notes.retrieve(uid, category="stores/%s" % (k), key="store", revision=revision)

      if storageInfo is None or len(storageInfo) == 0:
        # We don't know how to retrieve from this storage.
        continue

      try:
        handler = self.handlerFor(k)
        if handler:
          ret = handler.retrieveDirectory(storageInfo[0]["value"], {}, revision, path, buildId = buildId)
      except IOError:
        ret = None

      if not ret is None:
        return ret

    raise IOError("File Not Found")

  def retrieveFileStat(self, uuid, revision, path, buildId = None):
    """ Retrieves the file status by any means.
    """

    # TODO: return a stream

    if path.startswith("/"):
      path = path[1:]

    hashes = {}
    for k in self.handlerImpl:
      storageInfo = self.storageHashFor(uuid, revision, k)
      if storageInfo is None:
        # We don't know how to retrieve from this storage.
        continue

      StorageManager.Log.noisy("attempting to retrieve file from %s" % (k))
      handler = self.handlerFor(k)

      ret = None

      if handler:
        try:
          ret = handler.retrieveFileStat(storageInfo, None, revision, path, buildId = buildId)
        except IOError:
          ret = None

      if not ret is None:
        return ret

    raise IOError("File Not Found")

  def retrieveFile(self, uuid, revision, path, buildId = None):
    """ Retrieves the file by any means.
    """

    # TODO: return a stream

    if path.startswith("/"):
      path = path[1:]

    hashes = {}
    for k in self.handlerImpl:
      storageInfo = self.storageHashFor(uuid, revision, k)
      if storageInfo is None:
        # We don't know how to retrieve from this storage.
        continue

      StorageManager.Log.noisy("attempting to retrieve file from %s" % (k))
      handler = self.handlerFor(k)

      ret = None

      if handler:
        try:
          ret = handler.retrieveFile(storageInfo, None, revision, path, buildId = buildId)
        except IOError:
          ret = None

      if not ret is None:
        return ret

    raise IOError("File Not Found in %s: %s" % (uuid, path))

  def retrieveJSON(self, uuid, revision, path, buildId = None):
    """ Retrieves JSON data by any means.
    """

    from collections import OrderedDict

    data = self.retrieveFile(uuid, revision, path, buildId = buildId)

    return json.loads(data.decode('utf8'), object_pairs_hook=OrderedDict)

  def backendEntries(self, backend, person):
    """ Retrieves the information stored for the given person about the given backend.
    """

    handler = self.handlerFor(backend)

    ret = []
    if handler:
      ret = self.datastore.retrieveStorageBackendEntries(backend, person)

    return ret

  def backendEntry(self, backend, id, person):
    """ Retrieves the information stored for the given person about the given backend.
    """

    handler = self.handlerFor(backend)

    ret = None
    if handler:
      ret = self.datastore.retrieveStorageBackendEntry(backend, id, person)

    return ret

  def defaultBackendEntry(self, backend):
    """ Returns the default data for the given backend.
    """

    handler = self.handlerFor(backend)

    ret = None
    if handler:
      accountInfo = StorageManager.handlers[backend]['accountInfo']
      ret = {}

      for k,v in accountInfo.items():
        ret[k] = ""

    return ret

  def createBackendEntry(self, backend, person, info):
    """ Adds a backend entry with the initial info.

    Returns:
      (StorageBackendEntryRecord): The added record.
    """

    handler = self.handlerFor(backend)

    ret = None
    if handler:
      info = handler.establish(info)
      ret = self.datastore.createStorageBackendEntry(backend, person, info)

    return ret

  def destroyBackendEntry(self, record):
    """ Destroys a backend entry.
    """

    handler = self.handlerFor(record.backend)

    ret = None
    if handler:
      handler.unestablish(record.account_info)
      ret = self.datastore.destroy(record)

    return ret

  def updateBackendEntry(self, record):
    """ Updates a backend entry with the given info.

    Returns:
      (StorageBackendEntryRecord): The updated record.
    """

    handler = self.handlerFor(record.backend)

    ret = None
    if handler:
      record.account_info = handler.establish(record.account_info)
      ret = self.datastore.update(record)

    return ret

  def mimeTypeFor(self, path):
    """ Returns the naive mime information from the given path
    """

    # MIME Type
    ret = ["application/octet-stream"]

    # Add the requested name
    name = os.path.basename(path)

    if self.mimes is None:
      import mimetypes

      self.mimes = mimetypes.MimeTypes()
      self.mimes.read(os.path.join(os.path.dirname(__file__), "..", "..", "..", "data", "mime.types"))
      self.mimes.read(os.path.join(os.path.dirname(__file__), "..", "..", "..", "data", "extra.types"))

      self.fullnameMimes = {}
      fullnameTypes = os.path.join(os.path.dirname(__file__), "..", "..", "..", "data", "fullname.types")
      with open(fullnameTypes, "r") as f:
        for line in f:
          if line.strip().startswith("#") or line.strip() == "":
            continue

          try:
            v, k = line.strip().split(" ")
            self.fullnameMimes[k] = v
          except:
            pass

    ret = []
    if name in self.fullnameMimes:
      ret.append(self.fullnameMimes[name])

    ret.append(self.mimes.guess_type(name)[0])
    if ret[-1] is None:
      ret = ret[0:-1]

    # Also guess the name by looking at the first part
    # This is for files that are of the form "txt.output" on systems
    # that had filenames *start* with extensions (c64, etc)
    if "." in name:
      newName = ".".join(reversed(name.split(".", 1)))
      guess = self.mimes.guess_type(newName)[0]
      if guess:
        ret.append(guess)

    ret.append("application/octet-stream")

    return ret

def storage(name, priority=100, accountInfo=None):
  """ This decorator will register the given class as a storage backend.
  """

  def register_store(cls):
    StorageManager.register(name, cls, priority=priority, accountInfo=accountInfo)
    cls = loggable("StorageManager")(cls)

    def init(self, subConfig):
      self.configuration = subConfig

    cls.__init__ = init

    return cls

  return register_store

class StorageError(OccamError):
  """ Base class for all storage errors.
  """

  def __init__(self, message):
    self._message = message

  def __str__(self):
    return self._message

class StorageEstablishError(StorageError):
  """ When a storage backend cannot handle its initial information.
  """
