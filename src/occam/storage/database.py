# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sql

from occam.log import loggable
from occam.databases.manager import uses, datastore

from occam.objects.database import ObjectDatabase

@loggable
@datastore("storage")
@uses(ObjectDatabase)
class StorageDatabase:
  """ Manages the database interactions for storage backends.
  """

  def queryStorageBackendEntries(self, backend, person, id = None):
    storageBackendEntries = sql.Table("storage_backend_entries")

    subQuery = self.objects.queryObjectRecordFor(person, key = "id")

    query = storageBackendEntries.select()
    query.where = (storageBackendEntries.backend == backend) & (storageBackendEntries.person_id.in_(subQuery))

    if id is not None:
      query.where = query.where & (storageBackendEntries.id == id)

    return query

  def retrieveStorageBackendEntries(self, backend, person):
    from occam.storage.records.storage_backend_entry import StorageBackendEntryRecord

    session = self.database.session()

    query = self.queryStorageBackendEntries(backend, person)

    self.database.execute(session, query)

    return [StorageBackendEntryRecord(x) for x in self.database.many(session, size=100)]

  def retrieveStorageBackendEntry(self, backend, id, person):
    from occam.storage.records.storage_backend_entry import StorageBackendEntryRecord

    session = self.database.session()

    query = self.queryStorageBackendEntries(backend, person, id = id)

    self.database.execute(session, query)

    record = self.database.fetch(session)

    if record:
      record = StorageBackendEntryRecord(record)

    return record

  def createStorageBackendEntry(self, backend, person, info):
    from occam.storage.records.storage_backend_entry import StorageBackendEntryRecord

    session = self.database.session()

    subQuery = self.objects.queryObjectRecordFor(person, key = "id")

    self.database.execute(session, subQuery)
    person_db = self.database.fetch(session)

    record = None
    if person_db:
      record = StorageBackendEntryRecord()
      record.backend = backend
      record.person_id = person_db['id']
      record.account_info = info

      self.database.update(session, record)
      self.database.commit(session)

    return record

  def destroy(self, record):
    session = self.database.session()

    self.database.delete(session, record)
    self.database.commit(session)

    return record

  def update(self, record):
    session = self.database.session()

    self.database.update(session, record)
    self.database.commit(session)

    return record
