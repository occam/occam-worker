# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.commands.manager import command, option, argument

from occam.manager import uses

from occam.objects.manager import ObjectManager
from occam.storage.manager import StorageManager

from occam.key_parser import KeyParser

from occam.log import Log

import os

@command('storage', 'push',
  category      = 'Storage Management',
  documentation = "Copies an object to the given storage backend.")
@argument("object", type = "object")
@argument("backend", type = str)
@argument("id", type = str, nargs="?")
@option("-j", "--json", dest   = "to_json",
                        action = "store_true",
                        help   = "returns result as a json document")
@uses(ObjectManager)
@uses(StorageManager)
class StoragePushCommand:
  """ The push command will store the given object to the given store.
  """

  def do(self):
    backend = self.options.backend

    obj = self.objects.resolve(self.options.object, person = self.person)

    if obj is None:
      Log.error("Cannot find requested object.")
      return -1

    # Get the account info
    accountInfo = None
    if self.options.id:
      record = self.storage.backendEntry(backend, self.options.id, person = self.person)
      if record:
        accountInfo = record.account_info
      else:
        Log.error("Cannot find the storage info for %s at id %s" % (backend, self.options.id))
        return -1

    # Push the object from the local store
    # TODO: handle resource objects which are just data files
    localPath = self.objects.instantiate(obj)

    ret = self.storage.push(obj.uuid, obj.revision, localPath, backend     = backend,
                                                               accountInfo = accountInfo)

    storageHash, accountHash = ret

    ret = {}

    if storageHash and isinstance(storageHash, dict):
      ret["global"] = storageHash

    if accountHash and isinstance(accountHash, dict):
      ret["account"] = accountHash

    if self.options.to_json:
      import json
      Log.output(json.dumps(ret))
    else:
      for k,v in ret.items():
        Log.output("%s:" % (k))
        for subkey, value in v.items():
          Log.output("  %s: %s" % (subkey, value))

    return 0
