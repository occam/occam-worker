# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.commands.manager import command, option, argument

from occam.manager import uses

from occam.storage.manager import StorageManager

from occam.log import Log

import os

@command('storage', 'list',
  category      = 'Storage Management',
  documentation = "Lists the available storage backends.")
@option("-j", "--json", dest   = "to_json",
                        action = "store_true",
                        help   = "returns result as a json document")
@uses(StorageManager)
class List:
  """ The list command will yield the storage backends available.
  """

  def do(self):
    ret = {}

    for k,v in self.storage.handlers.items():
      handler = self.storage.handlerFor(k)
      if handler:
        ret[k] = {
          'backend': k,
          'accountInfo': (v.get('accountInfo', {}) or {})
        }

    if self.options.to_json:
      import json
      Log.output(json.dumps(ret))
    else:
      for k,v in ret.items():
        Log.output(k)

    return 0
