# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.commands.manager import command, option, argument

from occam.manager import uses

from occam.storage.manager import StorageManager

from occam.log import Log

import os

@command('storage', 'view',
  category      = 'Storage Management',
  documentation = "Views information about the given storage backend.")
@argument("backend", type = str)
@argument("id", type = str, nargs="?")
@option("-j", "--json", dest   = "to_json",
                        action = "store_true",
                        help   = "returns result as a json document")
@uses(StorageManager)
class StorageViewCommand:
  """ The view command will yield the storage backend info for the current account.
  """

  def do(self):
    if self.options.id:
      ret = [self.storage.backendEntry(self.options.backend, self.options.id, person = self.person)]
    else:
      ret = self.storage.backendEntries(self.options.backend, person = self.person)

    if self.options.to_json:
      import json
      ret = [{
               "id": record.id,
               "accountInfo": record.account_info,
               "backend": record.backend
             } for record in ret]
      Log.output(json.dumps(ret))
    else:
      for item in ret:
        for k,v in item._data.items():
          if k in ["id", "account_info", "backend"]:
            Log.output("%s: %s" % (k, v))

    return 0
