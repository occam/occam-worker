# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.commands.manager import command, option, argument

from occam.manager import uses

from occam.storage.manager import StorageManager

from occam.key_parser import KeyParser

from occam.log import Log

import os

@command('storage', 'set',
  category      = 'Storage Management',
  documentation = "Updates information about the given storage backend.")
@argument("backend", type = str)
@argument("id", type = str, nargs="?")
@option("-j", "--json", dest   = "to_json",
                        action = "store_true",
                        help   = "returns result as a json document")
@option("-t", "--input-type",  action  = "store",
                               dest    = "input_type",
                               default = "text",
                               help    = "determines what encoding the new value is. defaults to 'text'. 'json' for JSON encoded values.")
@option("-i", "--item", action  = "append",
                        dest    = "items",
                        nargs   = "+",
                        help    = "the key/value pair to set. When there is no value, the key is deleted.")
@uses(StorageManager)
class StorageSetCommand:
  """ The set command will update the given storage backend info for the current account.
  """

  def do(self):
    record = None
    if self.options.id is not "":
      record = self.storage.backendEntry(self.options.backend, self.options.id, person = self.person)
      if record is None:
        Log.error("Cannot find the given storage key record")
        return -1
      data = record.account_info
    else:
      Log.header("Adding storage backend keys")
      data = self.storage.defaultBackendEntry(self.options.backend)

    dirty = False
    keys  = []

    if self.options.items is None:
      self.options.items = []

    for item in self.options.items:
      if self.options.input_type == "json":
        if len(item) == 1:
          Log.noisy("Clearing key %s" % item[0])
        else:
          Log.noisy("Setting key %s to json value" % item[0])

          import json
          try:
            item[1] = json.loads(item[1])
          except:
            Log.error("Could not decode the given json value.")
            return -1

      else:
        if len(item) == 1:
          Log.noisy("Clearing key %s" % item[0])
        else:
          Log.noisy("Setting key %s" % item[0])

      # Parse the key ("foo.1.bar" -> foo[1]['bar'], essentially)
      parser = KeyParser()
      try:
        oldValue = parser.get(data, item[0])
        if len(item) == 1:
          if oldValue:
            parser.delete(data, item[0])
            dirty = True
        else:
          if oldValue != item[1]:
            parser.set(data, item[0], item[1])

            # Keep track of the keys we actually set
            keys.append(item[0])

            dirty = True
      except:
        Log.error("Could not set the value because the key was not found.")
        return -1

    # Update keys in the info and store
    if record is None:
      # Create
      record = self.storage.createBackendEntry(self.options.backend, self.person, data)
    else:
      if self.options.items == []:
        # Remove the storage entry
        Log.noisy("Removing backend key.")
        self.storage.destroyBackendEntry(record)
      elif dirty:
        # Commit the new object info
        Log.noisy("Updating backend key.")
        record.account_info = data
        record = self.storage.updateBackendEntry(record)

    if self.options.to_json:
      import json
      record = {
        "id": record.id,
        "accountInfo": data,
        "backend": record.backend
      }
      Log.output(json.dumps(record))
    else:
      for k,v in record._data.items():
        if k in ["id", "account_info", "backend"]:
          Log.output("%s: %s" % (k, v))

    return 0
